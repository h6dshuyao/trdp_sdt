/**********************************************************************************************************************/
/**
 * @file            tau_ldLadder_config.c
 *
 * @brief           Configuration for TRDP Ladder Topology Support
 *
 * @details
 *
 * @note            Project: TCNOpen TRDP prototype stack
 *
 * @author          Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This source code corresponds to TRDP_LADDER open source software.
 *          This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id: tau_ldLadder_config.c 2000 2019-08-15 13:08:09Z bloehr $
 */

#ifdef TRDP_OPTION_LADDER
/*******************************************************************************
 * INCLUDES
 */

#include "tau_xml.h"
#include "tau_ldLadder.h"
#include "tau_ldLadder_config.h"

/******************************************************************************
 *   Globals
 */

/* TRDP Config *****************************************************/
#ifdef XML_CONFIG_ENABLE

CHAR8 xmlConfigFileName[FILE_NAME_MAX_SIZE] = {0};	/*- @global 配置文件名称和路径 */
TRDP_XML_DOC_HANDLE_T   xmlConfigHandle;            /* 配置文件信息结构体 存放读取配置文件时产生的变量 */

/* 由tau_readXmlDeviceConfig读取的配置 */

TRDP_MEM_CONFIG_T       memoryConfigTAUL; /* 内存配置结构体 */
TRDP_SHAREMEM_CONFIG_T  sharememConfigTAUL; /* @global 共享内存配置结构体 */
TRDP_DBG_CONFIG_T       debugConfigTAUL; /* 日志配置结构体 */
UINT32 numComPar = 0;
TRDP_COM_PAR_T          *pComPar = NULL; /* 通信参数配置结构体 */
UINT32 numIfConfig = 0;					/* 网口配置数量 */
TRDP_IF_CONFIG_T        *pIfConfig = NULL; /* 网口基本配置结构体 */

/* 由tau_readXmlDatasetConfig读取的配置 */

UINT32 numComId = 0;							/* comid数量 */
TRDP_COMID_DSID_MAP_T   *pComIdDsIdMap = NULL;	/* comid-datasetId 列表配置 */
UINT32 numDataset = 0;							/** @global dataset数量 */
apTRDP_DATASET_T        apDataset = NULL;

/* 由tau_readXmlInterfaceConfig读取的配置  */

sSESSION_CONFIG_T       arraySessionConfigTAUL[MAX_SESSIONS];	/* 网口通信参数配置结构体 */
TRDP_EXCHG_PAR_T        *arrayExchgPar[LADDER_IF_NUMBER] = {0};	/* 通道配置结构体 */
UINT32					numExchgPar[LADDER_IF_NUMBER];						/* 网口通道配置数量 */



TRDP_APP_SESSION_T      appHandle1;              /*  @global Sub-network Id1 identifier to the library instance  */
TRDP_APP_SESSION_T      appHandle2;             /*  @global Sub-network Id2 identifier to the library instance  */

#endif /* ifdef XML_CONFIG_ENABLE */
#endif  /* TRDP_OPTION_LADDER */
