/**********************************************************************************************************************/
/**
 * @file            tau_ldLadder_config_def.h
 *
 * @brief           Configuration for TRDP Ladder Topology Support
 *
 * @details
 *
 * @note            Project: TCNOpen TRDP prototype stack
 *
 * @author          Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This source code corresponds to TRDP_LADDER open source software.
 *          This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id: tau_ldLadder_config_def.h 1999 2019-08-15 13:06:06Z bloehr $
 */

#ifdef TRDP_OPTION_LADDER

#ifndef TAU_LDLADDER_CONFIG_DEF_H_
#define TAU_LDLADDER_CONFIG_DEF_H_

/*******************************************************************************
 * INCLUDES
 */


#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************
 *   Globals
 */
/* TRDP Config *****************************************************/
#ifdef XML_CONFIG_ENABLE
/*  Session configurations  */
typedef struct
{
    TRDP_APP_SESSION_T      sessionHandle;
    TRDP_PD_CONFIG_T        pdConfig;
    TRDP_MD_CONFIG_T        mdConfig;
    TRDP_PROCESS_CONFIG_T   processConfig;
} sSESSION_CONFIG_T;
#endif

#ifdef __cplusplus
}
#endif

#endif /* TAU_LDLADDER_CONFIG_H_ */
#endif  /* TRDP_OPTION_LADDER */
