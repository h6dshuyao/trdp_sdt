/******************************************************************************/
/**
 * @file            tau_ldLadder.c
 *
 * @brief           Functions for Ladder Support TAUL API
 *
 * @details
 *
 * @note            Project: TCNOpen TRDP prototype stack
 *
 * @author          Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This source code corresponds to TRDP_LADDER open source software.
 *          This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id: tau_ldLadder.c 1998 2019-08-15 13:05:01Z bloehr $
 *
 */

#ifdef TRDP_OPTION_LADDER
/*******************************************************************************
 * INCLUDES
 */
#include <string.h>

#include <sys/ioctl.h>
#include <netinet/in.h>

#ifdef linux
#define __USE_GNU
//#include <linux/if.h>
#include <net/if.h>
#include <sched.h>
#elif defined(VXWORKS)
#include <net/if.h>
#include "cpusetCommon.h"
#else
#include <net/if.h>
#endif


#include <unistd.h>
#include "trdp_utils.h"
#include "trdp_if_light.h"
#include "vos_private.h"
#include "vos_thread.h"
#include "vos_sock.h"
#include "vos_shared_mem.h"
#include "tau_ladder.h"
#include "tau_ldLadder.h"
#include "tau_ldLadder_config.h"

#include "sdsink.h"
#include "sdsrc.h"

#ifdef TSN_STAMP_SERVICE
#include "GP_TSN.h"
#endif




/*******************************************************************************
 * DEFINES
 */
 

#define SHARE_MEMORY_STATE_OFFSET  0x3F600
#define MAX_SDT_HANDLE_NUMBER 100
/*******************************************************************************
 * TYPEDEFS
 */

/*for save of sdsrc generate output*/
typedef struct
{
    SDT_RC             sdsrc_rc; 
    UINT32				vdpLenth;
    UINT8              vdpData[TRDP_MAX_PD_DATA_SIZE];                    
}SDSRC_OUTPUT_T;



/******************************************************************************
 *   Globals
 */


PUBLISH_TELEGRAM_T      *pHeadPublishTelegram[MAX_SESSIONS]   = {NULL};	/** @global 发布通道链表头 */
SUBSCRIBE_TELEGRAM_T    *pHeadSubscribeTelegram[MAX_SESSIONS] = {NULL};	/** @global 订阅通道链表头 */

/* Mutex */
VOS_MUTEX_T pPublishTelegramMutex   = NULL;                      /* pointer to Mutex for Publish Telegram */
VOS_MUTEX_T pSubscribeTelegramMutex = NULL;                    /* pointer to Mutex for Subscribe Telegram */

/** @global 编组配置  */
TRDP_MARSHALL_CONFIG_T  marshallConfig = {&tau_marshall, &tau_unmarshall, NULL};

/** @global 冗余列表  */
REDUN_INFO_T  publishRedunList[MAX_REDID_NUM] = {0};


/** @global 子网1IP */
TRDP_IP_ADDR_T          subnetId1Address    = 0;
/** @global 子网2IP */
TRDP_IP_ADDR_T          subnetId2Address    = 0;

/** @static  子网1 网口索引 */
static UINT32          subnetId1Ifindex    = 0xffffffff;
/** @static 子网2 网口索引 */
static UINT32          subnetId2Ifindex    = 0xffffffff;


/* TAULpdMainThread */
VOS_THREAD_T        taulPdMainThreadHandle  = NULL;     /* Thread handle */
CHAR8               taulPdMainThreadName[]  = "TAULpdMainThread"; /* Thread name is TAUL PD Main Thread. */
TRDP_URI_HOST_T     nothingUriHost          = {""};     /* Nothing URI Host (IP Address) */
TRDP_URI_HOST_T     IP_ADDRESS_ZERO = {"0.0.0.0"};      /* IP Address 0.0.0.0 */
const TRDP_DEST_T   defaultDestination = {0};           /* Destination Parameter (id, SDT, URI) */

static INT32        ts_buffer[2048 / sizeof(INT32)];

SDT_CONFIG_T			sdsrcConfig[MAX_SDT_HANDLE_NUMBER]={{0}};
SDT_CONFIG_T			sdsinkConfig[MAX_SDT_HANDLE_NUMBER]={{0}};

static const TRDP_TIME_T max_tv = {0, 10000};/* @global 手动设置的周期上限，当我们需要循环中某些事件的处理周期小于最小PD周期时，在这里设置 */

/*save sdsrc generate output because we only generate vdp once for two apphandle*/
SDSRC_OUTPUT_T*	psdsrcOutputList=NULL;

TRDP_USER_CBFUNC_T		pTaulUserCbfuc=NULL;			/**< taul至服务层的回调函数	*/
TRDP_REDUN_CBFUNC_T		pTaulRedunCbfuc=NULL;			/**< taul至服务层的回调函数	*/
/*********************************************************************************************************************
* TAUL Local Function declare
*/

SDT_RC SdtServiceInit();

int getPort(INT32 socket);

#ifdef TSN_STAMP_SERVICE
/**
 * @brief	为所有发送通道的通信加入tsn服务
 * @note	需要在初始化完成后调用
 */
TRDP_ERR_T initTsnStamp(void);
#endif

/** callback function PD receive
 *
 *  @param[in]		pRefCon			user supplied context pointer
 *  @param[in]		argAppHandle	application handle returned by tlc_opneSession
 *  @param[in]		pPDInfo			pointer to PDInformation
 *  @param[in]		pData				pointer to receive PD Data
 *  @param[in]		dataSize        receive PD Data Size
 *
 */
void tau_ldRecvPdDs (
    void                    *pRefCon,
    TRDP_APP_SESSION_T      argAppHandle,
    const TRDP_PD_INFO_T    *pPDInfo,
    UINT8                   *pData,
    UINT32                  dataSize);

/**********************************************************************************************************************/
/** Return the PublishTelegram with same comId and IP addresses
 *
 *  @param[in]		pHeadPublishTelegram		pointer to head of queue
 *  @param[in]		comId						Publish comId
 *  @param[in]		srcIpAddr					source IP Address
 *  @param[in]		dstIpAddr               destination IP Address
 *
 *  @retval         != NULL						pointer to PublishTelegram
 *  @retval         NULL							No PublishTelegram found
 */
PUBLISH_TELEGRAM_T *searchPublishTelegramList (
    PUBLISH_TELEGRAM_T  *pHeadPublishTelegram,
    UINT32              comId,
    TRDP_IP_ADDR_T      srcIpAddr,
    TRDP_IP_ADDR_T      dstIpAddr);

/**********************************************************************************************************************/
/** Return the SubscribeTelegram with same comId and IP addresses
 *
 *  @param[in]          pHeadSubscribeTelegram	pointer to head of queue
 *  @param[in]		comId						Subscribe comId
 *  @param[in]		srcIpAddr					source IP Address
 *  @param[in]		dstIpAddr               destination IP Address
 *
 *  @retval         != NULL						pointer to Subscribe Telegram
 *  @retval         NULL							No Subscribe Telegram found
 */
SUBSCRIBE_TELEGRAM_T *searchSubscribeTelegramList (
    SUBSCRIBE_TELEGRAM_T    *pHeadSubscribeTelegram,
    UINT32                  comId,
    TRDP_IP_ADDR_T          srcIpAddr,
    TRDP_IP_ADDR_T          dstIpAddr);



/**********************************************************************************************************************/
/** Get the sub-Network Id for the current network Context.
 *
 *  @param[in,out]  pSubnetId			pointer to Sub-network Id
 *                                          Sub-network Id: SUBNET1 or SUBNET2
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter error
 *  @retval         TRDP_NOPUB_ERR		not published
 *  @retval         TRDP_NOINIT_ERR	handle invalid
 */
TRDP_ERR_T tau_ldGetNetworkContext (
    UINT32 *pSubnetId);

#ifdef __linux

/******************************************************************************/
/**
 *	@brief			为线程指定cpu
 *  @param[in]      iCpuID
 */
void SetThreadOnSpecifiedCPU(int iCpuID);

void SetThreadOnSpecifiedCPU(int iCpuID)
{
	cpu_set_t mask;
	CPU_ZERO(&mask);
	CPU_SET(iCpuID, &mask);
	if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) < 0)
	{
			printf("set cpu affinity failed %s\r\n",strerror(errno));
	}
}
#endif

/******************************************************************************/
/** PD/MD Telegrams configured for one interface.
 * PD: Publisher, Subscriber, Requester
 * MD: Caller, Replier
 *s
 *  @param[in]      ifIndex				interface Index
 *  @param[in]      numExchgPar			Number of Exchange Parameter
 *  @param[in]      pExchgPar			Pointer to Exchange Parameter
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_PARAM_ERR
 *	@retval			TRDP_MEM_ERR
 */
TRDP_ERR_T configureTelegrams (
    UINT32              ifIndex,
    UINT32              numExchgPar,
    TRDP_EXCHG_PAR_T    *pExchgPar);

/******************************************************************************/
/** Size of Dataset writing in Traffic Store
 *
 *  @param[out]     pDatasetSize    Pointer Host Byte order of dataset size
 *  @param[in]      pDataset        Pointer to one dataset
 *
 *  @retval         TRDP_NO_ERR     no error
 *  @retval         TRDP_PARAM_ERR  Parameter error
 *
 */
TRDP_ERR_T sizeWriteDatasetInTrafficStore (
    UINT32          *pDatasetSize,
    TRDP_DATASET_T  *pDataset);

/******************************************************************************/
/**
 *	@brief			根据传入的dataset配置计算用户数据编组后长度
 *	@details		创建临时源和目的buffer,用临时源和目的buffer进行编组，返回编组得到的目标数据长度。
 *  @note			由于创建的临时源buffer为全零，可变长element配置需要从源buffer的某些值获取数据长度，因此此函数将认为可变长element长度为0
 *  @param[out]  	pDatasetSize	计算得到的网络数据长度
 *  @param[in]      pDataset        dataset配置
 *
 *  @retval         TRDP_NO_ERR     no error
 *  @retval         TRDP_PARAM_ERR  Parameter error
 *
 */
TRDP_ERR_T sizeWriteDatasetInPackage (
    UINT32          *pDatasetSize,
    TRDP_DATASET_T  *pDataset);

/******************************************************************************/
/** Publisher Telegrams configured for one interface.
 *
 *  @param[in]      ifIndex				interface Index
 *  @param[in]      pExchgPar			Pointer to Exchange Parameter
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_PARAM_ERR
 *	@retval			TRDP_MEM_ERR
 */
TRDP_ERR_T publishTelegram (
    UINT32              ifIndex,
    TRDP_EXCHG_PAR_T    *pExchgPar);

/******************************************************************************/
/** Subscriber Telegrams configured for one interface.
 *
 *  @param[in]      ifIndex				interface Index
 *  @param[in]      numExchgPar			Number of Exchange Parameter
 *  @param[in]      pExchgPar			Pointer to Exchange Parameter
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_PARAM_ERR
 *	@retval			TRDP_MEM_ERR
 */
TRDP_ERR_T subscribeTelegram (
    UINT32              ifIndex,
    TRDP_EXCHG_PAR_T    *pExchgPar);

/**
 * 	@brief			根据传入的配置创建并初始化进程间通道，包括发布、订阅句柄（PUBLISH_TELEGRAM_T），供接口取用
 *	@note			不为进程间消息创建通道,Taul main Thread 不对通道做任何处理
 *  @param[in]      ifIndex             interface Index
 *  @param[in]      pExchgPar           Pointer to Exchange Parameter
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T ipcPubSubTelegram (
    UINT32              ifIndex,
    TRDP_EXCHG_PAR_T    *pExchgPar);

/**********************************************************************************************************************/
/** Append an Publish Telegram at end of List
 *
 *  @param[in]      ppHeadPublishTelegram          pointer to pointer to head of List
 *  @param[in]      pNewPublishTelegram            pointer to publish telegram to append
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter	error
 */
TRDP_ERR_T appendPublishTelegramList (
    PUBLISH_TELEGRAM_T  * *ppHeadPublishTelegram,
    PUBLISH_TELEGRAM_T  *pNewPublishTelegram);


/**********************************************************************************************************************/
/** Append an Subscribe Telegram at end of List
 *
 *  @param[in]      ppHeadSubscribeTelegram          pointer to pointer to head of List
 *  @param[in]      pNewSubscribeTelegram            pointer to Subscribe telegram to append
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter	error
 */
TRDP_ERR_T appendSubscribeTelegramList (
    SUBSCRIBE_TELEGRAM_T    * *ppHeadSubscribeTelegram,
    SUBSCRIBE_TELEGRAM_T    *pNewSubscribeTelegram);

/**********************************************************************************************************************/
/** Delete an Publish Telegram List
 *
 *  @param[in]      ppHeadPublishTelegram			pointer to pointer to head of queue
 *  @param[in]      pDeletePublishTelegram		pointer to element to delete
 *
 *  @retval         TRDP_NO_ERR					no error
 *  @retval         TRDP_PARAM_ERR				parameter   error
 *
 */
TRDP_ERR_T deletePublishTelegramList (
    PUBLISH_TELEGRAM_T  * *ppHeadPublishTelegram,
    PUBLISH_TELEGRAM_T  *pDeletePublishTelegram);

/**********************************************************************************************************************/
/** Delete an Subscribe Telegram List
 *
 *  @param[in]      ppHeadSubscribeTelegram			pointer to pointer to head of queue
 *  @param[in]      pDeleteSubscribeTelegram			pointer to element to delete
 *
 *  @retval         TRDP_NO_ERR					no error
 *  @retval         TRDP_PARAM_ERR				parameter   error
 *
 */
TRDP_ERR_T deleteSubscribeTelegramList (
    SUBSCRIBE_TELEGRAM_T    * *ppHeadSubscribeTelegram,
    SUBSCRIBE_TELEGRAM_T    *pDeleteSubscribeTelegram);

/**********************************************************************************************************************/
/** Force socket close.
 *
 *  @param[in]      appHandle          the handle returned by tlc_openSession
 *
 */
static void forceSocketClose (TRDP_APP_SESSION_T);
/******************************************************************************/
/** PD Main Process Init
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_THREAD_ERR
 */
TRDP_ERR_T tau_pd_main_proc_init (
    void);

/******************************************************************************/
/** TAUL PD Main Process Thread
 *
 */
VOS_THREAD_FUNC_T TAULpdMainThread (
    void);

#ifndef XML_CONFIG_ENABLE
/******************************************************************************/
/** Set TRDP Config Parameter From internal config
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_THREAD_ERR
 */
TRDP_ERR_T setConfigParameterFromInternalConfig (
    void);
#endif /* #ifdef XML_CONFIG_ENABLE */

/**
 * @brief		更新所有冗余状态
 * @details		从服务层读取冗余状态、维护本地冗余状态列表、设置变化了的双App冗余状态
 * @return		TRDP_NO_ERR 设置成功
 * @note		对于接口未获取到列表或接口提供的redId不存在与本模块的情况，不返回错误
 */
TRDP_ERR_T updateRedunStates(void);
/**
 * @brief		处理trdp的主备功能
 */
TRDP_ERR_T procRed(void);

/**********************************************************************************************************************/
/** TAUL Local Function define*/

/** Append an Publish Telegram at end of List
 *
 *  @param[in]      ppHeadPublishTelegram          pointer to pointer to head of List
 *  @param[in]      pNewPublishTelegram            pointer to publish telegram to append
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter   error
 */
TRDP_ERR_T appendPublishTelegramList (
    PUBLISH_TELEGRAM_T  * *ppHeadPublishTelegram,
    PUBLISH_TELEGRAM_T  *pNewPublishTelegram)
{
    PUBLISH_TELEGRAM_T  *iterPublishTelegram;
    extern VOS_MUTEX_T  pPublishTelegramMutex;
    VOS_ERR_T           vosErr = VOS_NO_ERR;

    /* Parameter Check */
    if (ppHeadPublishTelegram == NULL || pNewPublishTelegram == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /* First PublishTelegram ? */
    if (*ppHeadPublishTelegram == pNewPublishTelegram)
    {
        return TRDP_NO_ERR;
    }

    /* Ensure this List is last! */
    pNewPublishTelegram->pNextPublishTelegram = NULL;

    /* Check Mutex ? */
    if (pPublishTelegramMutex == NULL)
    {
        /* Create Publish Telegram Access Mutex */
        vosErr = vos_mutexCreate(&pPublishTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Create Publish Telegram  Mutex Err\n");
            return TRDP_MUTEX_ERR;
        }
        else
        {
            /* Lock Publish Telegram by Mutex */
            vosErr = vos_mutexLock(pPublishTelegramMutex);
            if (vosErr != VOS_NO_ERR)
            {
                vos_printLog(VOS_LOG_ERROR, "Publish Telegram Mutex Lock failed\n");
                return TRDP_MUTEX_ERR;
            }
        }
    }
    else
    {
        /* Lock Publish Telegram by Mutex */
        vosErr = vos_mutexLock(pPublishTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Publish Telegram Mutex Lock failed\n");
            return TRDP_MUTEX_ERR;
        }
    }

    /*- 如果链表头是NULL 即这是第一次为链表添加成员 */
    if (*ppHeadPublishTelegram == NULL)
    {
    	/*- 将链表头设置为当前成员 */
        *ppHeadPublishTelegram = pNewPublishTelegram;
        /* UnLock Publish Telegram by Mutex */
        vos_mutexUnlock(pPublishTelegramMutex);
        return TRDP_NO_ERR;
    }

    /*- 顺着链表找到最后一个成员 */
    for (iterPublishTelegram = *ppHeadPublishTelegram;
         iterPublishTelegram->pNextPublishTelegram != NULL;
         iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram)
    {
        ;
    }
    
    /*- 最后一个成员的pNext指向当前要添加的新成员 */
    iterPublishTelegram->pNextPublishTelegram = pNewPublishTelegram;
    /*- UnLock Publish Telegram by Mutex */
    vos_mutexUnlock(pPublishTelegramMutex);
    
    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/** Delete an Publish Telegram List
 *
 *  @param[in]      ppHeadPublishTelegram           pointer to pointer to head of queue
 *  @param[in]      pDeletePublishTelegram      pointer to element to delete
 *
 *  @retval         TRDP_NO_ERR                 no error
 *  @retval         TRDP_PARAM_ERR              parameter   error
 *
 */
TRDP_ERR_T deletePublishTelegramList (
    PUBLISH_TELEGRAM_T  * *ppHeadPublishTelegram,
    PUBLISH_TELEGRAM_T  *pDeletePublishTelegram)
{
    PUBLISH_TELEGRAM_T  *iterPublishTelegram;
    extern VOS_MUTEX_T  pPublishTelegramMutex;
    VOS_ERR_T           vosErr = VOS_NO_ERR;

    if (ppHeadPublishTelegram == NULL || *ppHeadPublishTelegram == NULL || pDeletePublishTelegram == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /* Check Mutex ? */
    if (pPublishTelegramMutex == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "Nothing Publish Telegram Mutex Err\n");
        return TRDP_MUTEX_ERR;
    }
    else
    {
        /* Lock Publish Telegram by Mutex */
        vosErr = vos_mutexLock(pPublishTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Publish Telegram Mutex Lock failed\n");
            return TRDP_MUTEX_ERR;
        }
    }

    /* handle removal of first element */
    if (pDeletePublishTelegram == *ppHeadPublishTelegram)
    {
        *ppHeadPublishTelegram = pDeletePublishTelegram->pNextPublishTelegram;
        vos_memFree(pDeletePublishTelegram);
        pDeletePublishTelegram = NULL;
        /* UnLock Publish Telegram by Mutex */
        vos_mutexUnlock(pPublishTelegramMutex);
        return TRDP_NO_ERR;
    }

    for (iterPublishTelegram = *ppHeadPublishTelegram;
         iterPublishTelegram != NULL;
         iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram)
    {
        if (iterPublishTelegram->pNextPublishTelegram == pDeletePublishTelegram)
        {
            iterPublishTelegram->pNextPublishTelegram = pDeletePublishTelegram->pNextPublishTelegram;
            vos_memFree(pDeletePublishTelegram);
            pDeletePublishTelegram = NULL;
            break;
        }
    }
    /* UnLock Publish Telegram by Mutex */
    vos_mutexUnlock(pPublishTelegramMutex);
    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/** Return the PublishTelegram with same comId and IP addresses
 *
 *  @param[in]      pHeadPublishTelegram        pointer to head of queue
 *  @param[in]      comId                       Publish comId
 *  @param[in]      srcIpAddr                   source IP Address
 *  @param[in]      dstIpAddr               destination IP Address
 *
 *  @retval         != NULL                     pointer to PublishTelegram
 *  @retval         NULL                            No PublishTelegram found
 */
PUBLISH_TELEGRAM_T *searchPublishTelegramList (
    PUBLISH_TELEGRAM_T  *pHeadPublishTelegram,
    UINT32              comId,
    TRDP_IP_ADDR_T      srcIpAddr,
    TRDP_IP_ADDR_T      dstIpAddr)
{
    PUBLISH_TELEGRAM_T  *iterPublishTelegram;
    extern VOS_MUTEX_T  pPublishTelegramMutex;
    VOS_ERR_T           vosErr = VOS_NO_ERR;

    /* Check Parameter */
    if (pHeadPublishTelegram == NULL
        || comId == 0
        || dstIpAddr == 0)
    {
        return NULL;
    }

    /* Check Mutex ? */
    if (pPublishTelegramMutex == NULL)
    {
        vos_printLog(VOS_LOG_DBG, "Nothing Publish Telegram Mutex Err\n");
        return NULL;
    }
    else
    {
        /* Lock Publish Telegram by Mutex */
        vosErr = vos_mutexLock(pPublishTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_DBG, "Publish Telegram Mutex Lock failed\n");
            return NULL;
        }
    }

    /* Check PublishTelegram List Loop */
    for (iterPublishTelegram = pHeadPublishTelegram;
         iterPublishTelegram != NULL;
         iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram)
    {
        /* Publish Telegram: We match if src/dst address is zero or matches, and comId */
        if ((iterPublishTelegram->comId == comId)
            && (iterPublishTelegram->srcIpAddr == 0 || iterPublishTelegram->srcIpAddr == srcIpAddr)
            && (iterPublishTelegram->dstIpAddr == 0 || iterPublishTelegram->dstIpAddr == dstIpAddr))
        {
            /* UnLock Publish Telegram by Mutex */
            vos_mutexUnlock(pPublishTelegramMutex);
            return iterPublishTelegram;
        }
        else
        {
            continue;
        }
    }
    /* UnLock Publish Telegram by Mutex */
    vos_mutexUnlock(pPublishTelegramMutex);
    return NULL;
}

/**********************************************************************************************************************/
/** Append an Subscribe Telegram at end of List
 *
 *  @param[in]      ppHeadSubscribeTelegram          pointer to pointer to head of List
 *  @param[in]      pNewSubscribeTelegram            pointer to Subscribe telegram to append
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter   error
 */
TRDP_ERR_T appendSubscribeTelegramList (
    SUBSCRIBE_TELEGRAM_T    * *ppHeadSubscribeTelegram,
    SUBSCRIBE_TELEGRAM_T    *pNewSubscribeTelegram)
{
    SUBSCRIBE_TELEGRAM_T    *iterSubscribeTelegram;
    extern VOS_MUTEX_T      pSubscribeTelegramMutex;
    VOS_ERR_T vosErr = VOS_NO_ERR;

    /* Parameter Check */
    if (ppHeadSubscribeTelegram == NULL || pNewSubscribeTelegram == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /* First Subscribe Telegram ? */
    if (*ppHeadSubscribeTelegram == pNewSubscribeTelegram)
    {
        return TRDP_NO_ERR;
    }

    /* Ensure this List is last! */
    pNewSubscribeTelegram->pNextSubscribeTelegram = NULL;

    /* Check Mutex ? */
    if (pSubscribeTelegramMutex == NULL)
    {
        /* Create Subscribe Telegram Access Mutex */
        vosErr = vos_mutexCreate(&pSubscribeTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Create Subscribe Telegram Mutex Err\n");
            return TRDP_MUTEX_ERR;
        }
        else
        {
            /* Lock Subscribe Telegram by Mutex */
            vosErr = vos_mutexLock(pSubscribeTelegramMutex);
            if (vosErr != VOS_NO_ERR)
            {
                vos_printLog(VOS_LOG_ERROR, "Subscribe Telegram Mutex Lock failed\n");
                return TRDP_MUTEX_ERR;
            }
        }
    }
    else
    {
        /* Lock Subscribe Telegram by Mutex */
        vosErr = vos_mutexLock(pSubscribeTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Subscribe Telegram Mutex Lock failed\n");
            return TRDP_MUTEX_ERR;
        }
    }

    if (*ppHeadSubscribeTelegram == NULL)
    {
        *ppHeadSubscribeTelegram = pNewSubscribeTelegram;
        /* UnLock Subscribe Telegram by Mutex */
        vos_mutexUnlock(pSubscribeTelegramMutex);
        return TRDP_NO_ERR;
    }

    for (iterSubscribeTelegram = *ppHeadSubscribeTelegram;
         iterSubscribeTelegram->pNextSubscribeTelegram != NULL;
         iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram)
    {
        ;
    }
    iterSubscribeTelegram->pNextSubscribeTelegram = pNewSubscribeTelegram;
    /* UnLock Subscribe Telegram by Mutex */
    vos_mutexUnlock(pSubscribeTelegramMutex);
    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/** Delete an Subscribe Telegram List
 *
 *  @param[in]      ppHeadSubscribeTelegram         pointer to pointer to head of queue
 *  @param[in]      pDeleteSubscribeTelegram            pointer to element to delete
 *
 *  @retval         TRDP_NO_ERR                 no error
 *  @retval         TRDP_PARAM_ERR              parameter   error
 *
 */
TRDP_ERR_T deleteSubscribeTelegramList (
    SUBSCRIBE_TELEGRAM_T    * *ppHeadSubscribeTelegram,
    SUBSCRIBE_TELEGRAM_T    *pDeleteSubscribeTelegram)
{
    SUBSCRIBE_TELEGRAM_T    *iterSubscribeTelegram;
    extern VOS_MUTEX_T      pSubscribeTelegramMutex;
    VOS_ERR_T vosErr = VOS_NO_ERR;

    if (ppHeadSubscribeTelegram == NULL || *ppHeadSubscribeTelegram == NULL || pDeleteSubscribeTelegram == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /* Check Mutex ? */
    if (pSubscribeTelegramMutex == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "Nothing Subscribe Telegram Mutex Err\n");
        return TRDP_MUTEX_ERR;
    }
    else
    {
        /* Lock Subscribe Telegram by Mutex */
        vosErr = vos_mutexLock(pSubscribeTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Subscribe Telegram Mutex Lock failed\n");
            return TRDP_MUTEX_ERR;
        }
    }

    /* handle removal of first element */
    if (pDeleteSubscribeTelegram == *ppHeadSubscribeTelegram)
    {
        *ppHeadSubscribeTelegram = pDeleteSubscribeTelegram->pNextSubscribeTelegram;
        vos_memFree(pDeleteSubscribeTelegram);
        pDeleteSubscribeTelegram = NULL;
        /* UnLock Subscribe Telegram by Mutex */
        vos_mutexUnlock(pSubscribeTelegramMutex);
        return TRDP_NO_ERR;
    }

    for (iterSubscribeTelegram = *ppHeadSubscribeTelegram;
         iterSubscribeTelegram != NULL;
         iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram)
    {
        if (iterSubscribeTelegram->pNextSubscribeTelegram == pDeleteSubscribeTelegram)
        {
            iterSubscribeTelegram->pNextSubscribeTelegram = pDeleteSubscribeTelegram->pNextSubscribeTelegram;
            vos_memFree(pDeleteSubscribeTelegram);
            pDeleteSubscribeTelegram = NULL;
            break;
        }
    }
    /* UnLock Subscribe Telegram by Mutex */
    vos_mutexUnlock(pSubscribeTelegramMutex);
    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/** Return the SubscribeTelegram with same comId and IP addresses
 *
 *  @param[in]          pHeadSubscribeTelegram  pointer to head of queue
 *  @param[in]      comId                       Subscribe comId
 *  @param[in]      srcIpAddr                   source IP Address
 *  @param[in]      dstIpAddr               destination IP Address
 *
 *  @retval         != NULL                     pointer to Subscribe Telegram
 *  @retval         NULL                            No Subscribe Telegram found
 */
SUBSCRIBE_TELEGRAM_T *searchSubscribeTelegramList (
    SUBSCRIBE_TELEGRAM_T    *pHeadSubscribeTelegram,
    UINT32                  comId,
    TRDP_IP_ADDR_T          srcIpAddr,
    TRDP_IP_ADDR_T          dstIpAddr)
{
    SUBSCRIBE_TELEGRAM_T    *iterSubscribeTelegram;
    extern VOS_MUTEX_T      pSubscribeTelegramMutex;
    VOS_ERR_T vosErr = VOS_NO_ERR;

    /* Check Parameter */
    if (pHeadSubscribeTelegram == NULL
        || comId == 0
        || dstIpAddr == 0)
    {
        return NULL;
    }
    /* Check Mutex ? */
    if (pSubscribeTelegramMutex == NULL)
    {
        vos_printLog(VOS_LOG_DBG, "Nothing Subscribe Telegram Mutex Err\n");
        return NULL;
    }
    else
    {
        /* Lock Subscribe Telegram by Mutex */
        vosErr = vos_mutexLock(pSubscribeTelegramMutex);
        if (vosErr != VOS_NO_ERR)
        {
            vos_printLog(VOS_LOG_DBG, "Subscribe Telegram Mutex Lock failed\n");
            return NULL;
        }
    }
    /* Check Subscribe Telegram List Loop */
    for (iterSubscribeTelegram = pHeadSubscribeTelegram;
         iterSubscribeTelegram != NULL;
         iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram)
    {
        /* Subscribe Telegram: We match if src/dst address is zero or matches, and comId */
        if ((iterSubscribeTelegram->comId == comId)
            && (iterSubscribeTelegram->srcIpAddr == 0 || iterSubscribeTelegram->srcIpAddr == srcIpAddr)
            && (iterSubscribeTelegram->dstIpAddr == 0 || iterSubscribeTelegram->dstIpAddr == dstIpAddr))
        {
            /* UnLock Subscribe Telegram by Mutex */
            vos_mutexUnlock(pSubscribeTelegramMutex);
            return iterSubscribeTelegram;
        }
        else
        {
            continue;
        }
    }
    /* UnLock Subscribe Telegram by Mutex */
    vos_mutexUnlock(pSubscribeTelegramMutex);
    return NULL;
}

/**
 *	@brief			为冗余管理列表添加成员
 *  @param[in]      pNewPublishTelegram            pointer to publish telegram to append
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter   error
 */
TRDP_ERR_T appendPublishRedunList (PUBLISH_TELEGRAM_T  *pNewPublishTelegram)
{
	TRDP_ERR_T          err = TRDP_NO_ERR;
    UINT32				i=0;

    /* Parameter Check */
    if (pNewPublishTelegram == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /* 通道配置了冗余 */
    if(0u!=pNewPublishTelegram->pPdParameter->redundant)
    {
        for(i=0;i<100;i++)
        {
        	if(publishRedunList[i].redId==pNewPublishTelegram->pPdParameter->redundant)
        	{
        		break;
        	}
        	else if(0u==publishRedunList[i].redId)
        	{
        		publishRedunList[i].redId=pNewPublishTelegram->pPdParameter->redundant;
        		break;
        	}
        }

        if(100==i)
        {
    		err =TRDP_MEM_ERR;
    		vos_printLog(VOS_LOG_ERROR, "no enough space in publish redun list\n");
        }
    }
    else
    {

    }

    return err;
}



/******************************************************************************/
/**
 *	@brief			遍历ExchgPar配置，判断通道类型，准备通道
 *	@note			暂时去除了MD通道，包括caller和replier的功能和PD的request功能
 *  @param[in]      ifIndex             interface Index
 *  @param[in]      numExchgPar         Number of Exchange Parameter
 *  @param[in]      pExchgPar           Pointer to Exchange Parameter
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T configureTelegrams (
    UINT32              ifIndex,
    UINT32              numExchgPar,
    TRDP_EXCHG_PAR_T    *pExchgPar)
{

    UINT32      telegramIndex = 0;
    TRDP_ERR_T  err = TRDP_NO_ERR;

    /* Get Telegram */
    for (telegramIndex = 0; telegramIndex < numExchgPar; telegramIndex++)
    {
        /*- 如果telegram配置了pd参数 */
        if (pExchgPar[telegramIndex].pPdPar != NULL)
        {
            /*- 当前版本认为未配置source且配置了一个destination的通道为发布通道 */
            if ((1 == pExchgPar[telegramIndex].destCnt)
                && (pExchgPar[telegramIndex].srcCnt == 0))
            {
            	/*- 为配置准备一个发布通道 */
                err = publishTelegram(ifIndex, &pExchgPar[telegramIndex]);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "configureTelegrams() failed. publishTelegram() error\n");
                    return err;
                }
                else
                {
                	/*- 当前版本不允许兼具发布订阅功能的通道，当前通道完成发布后即处理下一通道 */
                    continue;
                }
            }

            /*- 当前版本认为同时配置destination和source的通道为发布通道 */
            if ((pExchgPar[telegramIndex].destCnt > 0)
                && (pExchgPar[telegramIndex].srcCnt > 0)
                /*&& (strncmp(*pExchgPar[telegramIndex].pSrc->pUriHost1, IP_ADDRESS_ZERO, sizeof(IP_ADDRESS_ZERO)) != 0)*/)
            {
            	/*- 为配置准备一个订阅通道 */
                err = subscribeTelegram(ifIndex, &pExchgPar[telegramIndex]);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "configureTelegrams() failed. subscribeTelegram() error\n");
                    return err;
                }
                else
                {
                    continue;
                }
            }

            /*- 当前版本认为没有配置destination和source的通道为板内通信通道 */
            if ((pExchgPar[telegramIndex].destCnt == 0u)
                && (pExchgPar[telegramIndex].srcCnt == 0u))
            {
            	/*- 为配置准备一个板内通信通道 */
                err = ipcPubSubTelegram(ifIndex, &pExchgPar[telegramIndex]);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "configureTelegrams() failed. subscribeTelegram() error\n");
                    return err;
                }
                else
                {
                    continue;
                }
            }
        }
        else
        {
        	;//这里应该添加MD的处理
        }
    }
    return TRDP_NO_ERR;
}

/******************************************************************************/
/**
 *	@brief			根据传入的dataset配置计算解编组后的数据长度
 *	@details		创建临时源和目的buffer,先以0为起始地址计算解编数据的长度预测值；再用临时源和目的buffer进行试解编组，其中预测值的1.5倍用来限制解编组范围，返回试解编组得到的目标数据长度。
 *  @note			由于创建的临时源buffer为全零，可变长element配置需要从源buffer的某些值获取数据长度，因此此函数将认为可变长element长度为0
 *  @param[out]  	pDatasetSize	计算得到的主机数据长度
 *  @param[in]      pDataset        dataset配置
 *
 *  @retval         TRDP_NO_ERR     no error
 *  @retval         TRDP_PARAM_ERR  Parameter error
 *
 */
TRDP_ERR_T sizeWriteDatasetInTrafficStore (
    UINT32          *pDatasetSize,
    TRDP_DATASET_T  *pDataset)
{
    TRDP_ERR_T  err = TRDP_NO_ERR;
    UINT8       *pTempSrcDataset        = NULL;
    UINT8       *pTempDestDataset       = NULL;
    UINT32      datasetNetworkByteSize  = 0;

    /*- 为临时源buffer分配内存 */
    pTempSrcDataset = (UINT8 *)vos_memAlloc(TRDP_MAX_MD_DATA_SIZE);
    if (pTempSrcDataset == NULL)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. Temporary Source Dataset vos_memAlloc() Err\n");
        /* Free Temporary Source Dataset */
        vos_memFree(pTempSrcDataset);
        return TRDP_MEM_ERR;
    }

    /*- 为临时目标buffer分配内存 */
    pTempDestDataset = (UINT8 *)vos_memAlloc(TRDP_MAX_MD_DATA_SIZE);
    if (pTempSrcDataset == NULL)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. Temporary Destination Dataset vos_memAlloc() Err\n");
        /* Free Temporary Source Dataset */
        vos_memFree(pTempDestDataset);
        return TRDP_MEM_ERR;
    }

    /* 用传入的dataset配置和临时源Buffer计算解编组后的数据长度返回到datasetNetworkByteSize */
    err = tau_calcDatasetSize(
            marshallConfig.pRefCon,
            pDataset->id,
            pTempSrcDataset,
			TRDP_MAX_MD_DATA_SIZE,
            &datasetNetworkByteSize,
            &pDataset);
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. tau_calcDatasetSize datasetId: %d returns error = %d\n",
                     pDataset->id,
                     err);
        /* Free Temporary Source Dataset */
        vos_memFree(pTempSrcDataset);
        return TRDP_PARAM_ERR;
    }
    else
    {
        /*-使目标数据长度为datasetNetworkByteSize的1.5倍（向上取整） */
        *pDatasetSize = datasetNetworkByteSize + (datasetNetworkByteSize + 1) / 2;
    }

    /* 将临时源buffer中的数据解编组到临时目的buffer，同时计算解编组后的数据长度装入返回值 */
    err = tau_unmarshallDs(
            &marshallConfig.pRefCon,                /* pointer to user context */
            pDataset->id,                           /* datasetId */
            pTempSrcDataset,                        /* source pointer to received original message */
			TRDP_MAX_MD_DATA_SIZE,
            pTempDestDataset,                       /* destination pointer to a buffer for the treated message */
            pDatasetSize,                           /* destination Buffer Size */
            &pDataset);                         /* pointer to pointer of cached dataset */
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. tau_unmarshallDs DatasetId%d returns error %d\n",
                     pDataset->id,
                     err);
        /* Free Temporary Source Dataset */
        vos_memFree(pTempDestDataset);
        return err;
    }

    /*- 释放临时Buffer内存  */
    vos_memFree(pTempDestDataset);
    vos_memFree(pTempSrcDataset);

    return TRDP_NO_ERR;
}

/******************************************************************************/
/**
 *	@brief			根据传入的dataset配置计算用户数据编组后长度
 *	@details		创建临时源和目的buffer,用临时源和目的buffer进行编组，返回编组得到的目标数据长度。
 *  @note			由于创建的临时源buffer为全零，可变长element配置需要从源buffer的某些值获取数据长度，因此此函数将认为可变长element长度为0
 *  @param[out]  	pDatasetSize	计算得到的网络数据长度
 *  @param[in]      pDataset        dataset配置
 *
 *  @retval         TRDP_NO_ERR     no error
 *  @retval         TRDP_PARAM_ERR  Parameter error
 *
 */
TRDP_ERR_T sizeWriteDatasetInPackage (
    UINT32          *pDatasetSize,
    TRDP_DATASET_T  *pDataset)
{
    TRDP_ERR_T  err = TRDP_NO_ERR;
    UINT8       *pTempSrcDataset        = NULL;
    UINT8       *pTempDestDataset       = NULL;

    UINT32		tempDatasetSize=TRDP_MAX_MD_DATA_SIZE;

    /*- 为临时源buffer分配内存 */
    pTempSrcDataset = (UINT8 *)vos_memAlloc(TRDP_MAX_MD_DATA_SIZE);
    if (pTempSrcDataset == NULL)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. Temporary Source Dataset vos_memAlloc() Err\n");
        /* Free Temporary Source Dataset */
        vos_memFree(pTempSrcDataset);
        return TRDP_MEM_ERR;
    }

    /*- 为临时目标buffer分配内存 */
    pTempDestDataset = (UINT8 *)vos_memAlloc(TRDP_MAX_MD_DATA_SIZE);
    if (pTempSrcDataset == NULL)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. Temporary Destination Dataset vos_memAlloc() Err\n");
        /* Free Temporary Source Dataset */
        vos_memFree(pTempDestDataset);
        return TRDP_MEM_ERR;
    }

    /* 将临时源buffer中的数据解编组到临时目的buffer，同时计算解编组后的数据长度装入返回值 */
    err = tau_marshallDs(
            &marshallConfig.pRefCon,                /* pointer to user context */
            pDataset->id,                           /* datasetId */
            pTempSrcDataset,                        /* source pointer to received original message */
			TRDP_MAX_MD_DATA_SIZE,
            pTempDestDataset,                       /* destination pointer to a buffer for the treated message */
            &tempDatasetSize,                           /* destination Buffer Size */
            &pDataset);                         /* pointer to pointer of cached dataset */
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG,
                     "sizeWriteDatasetInTrafficStore() Failed. tau_unmarshallDs DatasetId%d returns error %d\n",
                     pDataset->id,
                     err);
        /* Free Temporary Source Dataset */
        vos_memFree(pTempDestDataset);
        return err;
    }
    else
    {
    	*pDatasetSize=tempDatasetSize;
    }

    /*- 释放临时Buffer内存  */
    vos_memFree(pTempDestDataset);
    vos_memFree(pTempSrcDataset);

    return TRDP_NO_ERR;
}


/*********************************************************************************************************************
* TAUL Interface Function determination
*/

/**
 * 	@brief			根据传入的配置创建并初始化一个发布通道，包括句柄（PUBLISH_TELEGRAM_T）和通道体(PD_ELE_T)，供周期运行阶段使用
 *	@note			函数的完整执行要求传入的telegram配置了一个dest(非组播)，对src无要求
 *  @param[in]      ifIndex             interface Index
 *  @param[in]      pExchgPar           Pointer to Exchange Parameter
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T publishTelegram (
    UINT32              ifIndex,
    TRDP_EXCHG_PAR_T    *pExchgPar)
{
    UINT32              i = 0;
    TRDP_IP_ADDR_T      networkByteIpAddr   = 0;            /* for convert URI to IP Address */
    PUBLISH_TELEGRAM_T  *pPublishTelegram   = NULL;
    UINT32              *pPublishDataset    = NULL;
    UINT32              *pNetworkByteDataset= NULL;
    TRDP_ERR_T          err = TRDP_NO_ERR;
    UINT32				serviceId=0;

#ifdef TSN_COMMU_TIME_TEST
    serviceId=0x07D8;
#endif

    /*- 为发布通道句柄（PUBLISH_TELEGRAM_T）分配内存 */
    pPublishTelegram = (PUBLISH_TELEGRAM_T *)vos_memAlloc(sizeof(PUBLISH_TELEGRAM_T));
    if (pPublishTelegram == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "publishTelegram() Failed. Publish Telegram vos_memAlloc() Err\n");
        err = TRDP_MEM_ERR;
        return err;
    }

    /*- 搜索dataset列表为当前通道句柄获取对应的datast配置指针 */
    for (i = 0; i < numDataset; i++)
    {
        /* DatasetId in dataSet config ? */
        if (pExchgPar->datasetId == apDataset[i]->id)
        {
            /* Set Dataset Descriptor */
            pPublishTelegram->pDatasetDescriptor = apDataset[i];
            break;
        }
    }

    /*- 未找到对应的dataset配置  */
    if (0==pPublishTelegram->pDatasetDescriptor)
    {
        vos_printLog(VOS_LOG_ERROR,
                     "publishTelegram() Failed. Dataset no find. datasetId: %d, comId: %d\n",
                     pExchgPar->datasetId,
                     pExchgPar->comId);
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);

        err = TRDP_PARAM_ERR;
        return err;
    }

    /*- 检查dest数量为1 */
    if (pExchgPar->destCnt != 1)
    {
        vos_printLog(VOS_LOG_ERROR, "publishTelegram() Failed. dstCnt Err. destCnt: %d\n", pExchgPar->destCnt);
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);
        err = TRDP_PARAM_ERR;
        return err;
    }



    /*- 通过配置数组下标确定当前通道句柄绑定的会话 */
    if (ifIndex == IF_INDEX_SUBNET1)
    {
        /* Set Application Handle: Subnet1 */
        pPublishTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
    }
    else if (ifIndex == IF_INDEX_SUBNET2)
    {
        /* Set Application Handle: Subnet2 */
        pPublishTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
    }
    else
    {
        /* ifIndex Error */
        vos_printLog(VOS_LOG_ERROR, "publishTelegram() Failed. ifIndex:%d error\n", ifIndex);
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);

        err = TRDP_PARAM_ERR;
        return err;
    }

    /* 根据dataset配置计算数据集在网络传输时用户数据的size存入通道句柄  */
    err = sizeWriteDatasetInPackage(&pPublishTelegram->datasetNetworkByteSize, pPublishTelegram->pDatasetDescriptor);
    //printf("[test print]result of dataset size is %d by sizeWriteDatasetInTrafficStore(),comId:%d\n",pPublishTelegram->dataset.size,pExchgPar->comId);

    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR,
                     "publishTelegram() Failed. sizeWriteDatasetInTrafficStore() returns error = %d\n",
                     err);
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);
        return TRDP_PARAM_ERR;
    }

    /*- 分配一块dataset网络传输时大小的内存 */
    pNetworkByteDataset = (UINT32 *)vos_memAlloc(pPublishTelegram->datasetNetworkByteSize);
    if (pNetworkByteDataset == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "publishTelegram() Failed. NetworkByte Dataset vos_memAlloc() Err\n");

        vos_memFree(pPublishTelegram);
        return TRDP_MEM_ERR;
    }


    /*- 当前通道配置了编组或当前会话配置了编组 */
    if (((pExchgPar->pPdPar->flags & TRDP_FLAGS_MARSHALL) == TRDP_FLAGS_MARSHALL)
        || ((arraySessionConfigTAUL[ifIndex].pdConfig.flags & TRDP_FLAGS_MARSHALL) == TRDP_FLAGS_MARSHALL))
    {

        /*- 根据dataset配置计算数据解编组后的size存入dataset.size */
        err = tau_calcDatasetSize(
                marshallConfig.pRefCon,
                pExchgPar->datasetId,
                (UINT8 *) pNetworkByteDataset,
				pPublishTelegram->datasetNetworkByteSize,
                &pPublishTelegram->dataset.size,
                &pPublishTelegram->pDatasetDescriptor);

        printf("[test print]result of dataset network size is %d by tau_calcDatasetSize(),comId:%d\n",pPublishTelegram->dataset.size,pExchgPar->comId);

        /*- 无论计算是否成功，释放NetworkByteSize的内存 */
        vos_memFree(pNetworkByteDataset);

        /*- 计算成功 */
        if (TRDP_NO_ERR == err)
        {
        	/*- 为数据集分配解编组后size的内存 */
        	pPublishDataset = (UINT32 *)vos_memAlloc(pPublishTelegram->dataset.size);
        }
        else
        {
            vos_printLog(VOS_LOG_ERROR,
                         "publishTelegram() Failed. tau_calcDatasetSize datasetId: %d returns error = %d\n",
                         pExchgPar->datasetId,
                         err);

            /* Free Publish Telegram */
            vos_memFree(pPublishTelegram);
            return TRDP_PARAM_ERR;
        }
    }
    /*- 当前通道禁用编组 */
    else
    {
    	/*- 直接获取NetworkByteSize作为主机长度 */
    	pPublishTelegram->dataset.size=pPublishTelegram->datasetNetworkByteSize;

        /*- 直接为数据集获取网络传输时大小的内存 */
        pPublishDataset = pNetworkByteDataset;

    }

    /*- 令通道句柄获取配置 */
    pPublishTelegram->pIfConfig = &pIfConfig[ifIndex];
    pPublishTelegram->pPdParameter = pExchgPar->pPdPar;
    pPublishTelegram->comId = pExchgPar->comId;
    /* @note 会话配置-pdConfig-sendParam 的配置中qos和ttl来自 interface层的pd-com-parameter；retries、tsn、vlan均为在setDefaultInterfaceValues函数中配置的0 */
    pPublishTelegram->pSendParam = &arraySessionConfigTAUL[ifIndex].pdConfig.sendParam;

    /** @note 拓扑值当前为0，未来可能可配置?? */
    pPublishTelegram->etbTopoCount = 0;
    pPublishTelegram->opTrnTopoCount = 0;

    /*- 令通道句柄获取dataset Buffer指针 */
    pPublishTelegram->dataset.pDatasetStartAddr = (UINT8 *)pPublishDataset;

    /*- 为通道句柄获取srcIp */
    /*- 配置了srcIP */
    if ((&pExchgPar->pSrc[0] != NULL) && (pExchgPar->pSrc[0].pUriHost1 != NULL))
    {
        networkByteIpAddr = vos_dottedIP(*(pExchgPar->pSrc[0].pUriHost1));
    	/*- 配置的srcIp无效（广播、组播 ）*/
        if ((networkByteIpAddr == BROADCAST_ADDRESS)
            || (vos_isMulticast(networkByteIpAddr)))
        {
            /*- 释放通道句柄和其dataset资源并返回错误 */
            vos_memFree(pPublishDataset);
            vos_memFree(pPublishTelegram);
            vos_printLog(VOS_LOG_ERROR,
                         "publishTelegram() Failed. Source IP Address1 Err. Source URI Host1: %s\n",
                         (char *)pExchgPar->pSrc[0].pUriHost1);
            return TRDP_PARAM_ERR;
        }
        /*- 配置的srcIp有效 */
        else
        {
            /*- 配置的srcIp装入通道 */
            pPublishTelegram->srcIpAddr = networkByteIpAddr;
        }
    }
    /*- 未配置srcIP */
    else
    {
    	/*- 实际子网Ip装入通道 */
        if (ifIndex == 0)
        {
            /* Set Source IP Address : Subnet1 I/F Address */
            pPublishTelegram->srcIpAddr = subnetId1Address;
        }
        else
        {
            /* Set Source IP Address : Subnet2 I/F Address */
            pPublishTelegram->srcIpAddr = subnetId2Address;
        }
    }

    /*- 为通道句柄获取destIp */
    /*- 配置了destIP */
    if ((pExchgPar->pDest != NULL) && (pExchgPar->pDest[0].pUriHost != NULL))
    {
        networkByteIpAddr = vos_dottedIP(*(pExchgPar->pDest[0].pUriHost));
        /*- 配置的destIp无效（0）*/
        if (networkByteIpAddr == 0)
        {
        	/*- 释放通道句柄和其dataset资源并返回错误 */
            vos_memFree(pPublishDataset);
            vos_memFree(pPublishTelegram);
            vos_printLog(VOS_LOG_ERROR,
                         "publishTelegram() Failed. Destination IP Address Err. Destination URI Host: %s\n",
                         (char *)pExchgPar->pDest[ifIndex].pUriHost);
            return TRDP_PARAM_ERR;
        }
        /*- 配置的destIp有效 */
        else
        {
        	/*- 配置的destIp装入通道 */
            pPublishTelegram->dstIpAddr = networkByteIpAddr;
        }
    }
    else
    {
    	/*- 释放通道句柄和其dataset资源并返回错误 */
        vos_memFree(pPublishDataset);
        vos_memFree(pPublishTelegram);
        vos_printLog(VOS_LOG_ERROR,
                     "publishTelegram() Failed. Destination IP Address Err. Destination URI Host: %s\n",
                     (char *)pExchgPar->pDest[ifIndex].pUriHost);
        return TRDP_PARAM_ERR;
    }

    /*- 创建发布通道并装入用户数据 */
    err = tlp_publish(
            pPublishTelegram->appHandle,                                    /* our application identifier */
            &pPublishTelegram->pubHandle,                                   /* our publish identifier */
			(void *)pPublishTelegram, 								/* 令发布通道的userRef获取其句柄结构体指针 @note 为tsn网络延迟测试新增，原为null */
			NULL, serviceId,										/* callBackFunc,serviceId */
			pPublishTelegram->comId,
            pPublishTelegram->etbTopoCount,
            pPublishTelegram->opTrnTopoCount,
            pPublishTelegram->srcIpAddr,                                    /* 配置或来自设备(未配置)的srcIp */
            pPublishTelegram->dstIpAddr,                                    /* 配置的destIp */
            pPublishTelegram->pPdParameter->cycle,                          /* 通道配置的周期 */
            pPublishTelegram->pPdParameter->redundant,                      /* 0 - Non-redundant, > 0 valid redundancy group */
            pPublishTelegram->pPdParameter->flags,
            pPublishTelegram->pSendParam,                                   /* 继承自会话pd配置 */
            pPublishTelegram->dataset.pDatasetStartAddr,                    /*- 用户数据buffer指针 */
            pPublishTelegram->dataset.size);                      /* 使用通道主机数据长度 */

    if (err != TRDP_NO_ERR)
    {
    	/*- 释放通道句柄和其dataset资源并返回错误 */
        vos_memFree(pPublishDataset);
        vos_memFree(pPublishTelegram);
        vos_printLog(VOS_LOG_ERROR, "publishTelegram() Failed. Publish Telegram tlp_publish() Err:%d\n", err);
        return err;
    }
    else
    {
        /*- 令通道获取配置的共享内存偏移地址 */
        pPublishTelegram->pubHandle->offset = pPublishTelegram->pPdParameter->offset;

    	/*- 将通道加入到全局的冗余组状态列表 */
    	err = appendPublishRedunList(pPublishTelegram);
        if (err != TRDP_NO_ERR)
        {
            /* Free Publish Dataset */
            vos_memFree(pPublishDataset);
            /* Free Publish Telegram */
            vos_memFree(pPublishTelegram);
            vos_printLog(VOS_LOG_ERROR,
                         "publishTelegram() Failed. Publish Telegram appendPublishRedunList() Err:%d\n",
                         err);
        }
        else
        {
            /*- 将当前通道加入到全局的发布通道链表 */
            err = appendPublishTelegramList(&pHeadPublishTelegram[ifIndex], pPublishTelegram);
            if (err != TRDP_NO_ERR)
            {
                /* Free Publish Dataset */
                vos_memFree(pPublishDataset);
                /* Free Publish Telegram */
                vos_memFree(pPublishTelegram);
                vos_printLog(VOS_LOG_ERROR,
                             "publishTelegram() Failed. Publish Telegram appendPublishTelegramList() Err:%d\n",
                             err);
            }
        }
    }

    return TRDP_NO_ERR;
}

/******************************************************************************/
/** Subscriber Telegrams configured for one interface.
 *
 *  @param[in]      ifIndex             interface Index
 *  @param[in]      numExchgPar         Number of Exchange Parameter
 *  @param[in]      pExchgPar           Pointer to Exchange Parameter
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T subscribeTelegram (
    UINT32              ifIndex,
    TRDP_EXCHG_PAR_T    *pExchgPar)
{
    UINT32                  i = 0;
    UINT32                  datasetIndex        = 0;
    TRDP_IP_ADDR_T          networkByteIpAddr   = 0;            /* for convert URI to IP Address */
    SUBSCRIBE_TELEGRAM_T    *pSubscribeTelegram = NULL;
    UINT32                  *pSubscribeDataset  = NULL;
    TRDP_ERR_T              err = TRDP_NO_ERR;

    /*- 检查src数量 */
    if (pExchgPar->srcCnt == 0)
    {
        vos_printLog(VOS_LOG_ERROR, "subscribeTelegram() Failed. srcCnt Err. srcCnt: %d\n", pExchgPar->srcCnt);
        return TRDP_PARAM_ERR;
    }
    /*- 检查dest数量 */
    else if (pExchgPar->destCnt != 1)
    {
        vos_printLog(VOS_LOG_ERROR,
                     "subscribeTelegram() Failed. destCnt Err. destCnt: %d\n",
                     pExchgPar->destCnt);
        return TRDP_PARAM_ERR;
    }
    else
    {
        /*- 遍历所有src*/
        for (i = 0; i < pExchgPar->srcCnt; i++)
        {
            /*- 为订阅通道句柄结构体申请内存 */
            pSubscribeTelegram = (SUBSCRIBE_TELEGRAM_T *)vos_memAlloc(sizeof(SUBSCRIBE_TELEGRAM_T));
            if (pSubscribeTelegram == NULL)
            {
                vos_printLog(VOS_LOG_ERROR, "SubscribeTelegram() Failed. Subscribe Telegram vos_memAlloc() Err\n");
                return TRDP_MEM_ERR;
            }
            else
            {

            }

            /*- 当前配置为第一个src */
            if (i == 0)
            {
                /*- 为通道获取对应的dataset配置 */
                for (datasetIndex = 0; datasetIndex < numDataset; datasetIndex++)
                {
                    if (pExchgPar->datasetId == apDataset[datasetIndex]->id)
                    {
                        pSubscribeTelegram->pDatasetDescriptor = apDataset[datasetIndex];
                        break;
                    }
                }

                /*- 无对应dataset */
                if (pSubscribeTelegram->pDatasetDescriptor == 0)
                {
                    vos_printLog(VOS_LOG_ERROR,
                                 "subscribeTelegram() Failed. Dataset Err. datasetId: %d, comId: %d\n",
                                 pExchgPar->datasetId,
                                 pExchgPar->comId);
                    /*- 释放订阅通道句柄结构体 */
                    vos_memFree(pSubscribeTelegram);
                    return TRDP_PARAM_ERR;
                }

                /*- 配置了destIp */
                if ((&pExchgPar->pDest[0] != NULL) && (pExchgPar->pDest[0].pUriHost != NULL))
                {
                    networkByteIpAddr = vos_dottedIP(*(pExchgPar->pDest[0].pUriHost));

					/*- 通道句柄获取destIp */
					pSubscribeTelegram->dstIpAddr = networkByteIpAddr;

                    /* @note destIp可为任何地址 */

                }
                else
                {
                	/*- 释放通道句柄结构体内存并汇报错误 */
                    vos_printLog(VOS_LOG_ERROR,"subscribeTelegram() Failed. Destination IP Address is NULL.\n");
                    vos_memFree(pSubscribeTelegram);
                    return TRDP_PARAM_ERR;
                }

            }//处理通道第一个src的配置结束

            /*- 为通道获取appHandle */

            if (ifIndex == IF_INDEX_SUBNET1)
            {
                /* Set Application Handle: Subnet1 */
                pSubscribeTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
            }
            else if (ifIndex == IF_INDEX_SUBNET2)
            {
                /* Set Application Handle: Subnet2 */
                pSubscribeTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
            }
            else
            {
                /* ifIndex Error */
                vos_printLog(VOS_LOG_ERROR, "subscribeTelegram() Failed. ifIndex:%d error\n", ifIndex);
                /* Free Subscribe Telegram */
                vos_memFree(pSubscribeTelegram);
                return TRDP_PARAM_ERR;
            }




            /*- 为通道计算网络数据长度 */
            err = sizeWriteDatasetInPackage(&pSubscribeTelegram->datasetNetworkByteSize,
                                                 pSubscribeTelegram->pDatasetDescriptor);
            if (err != TRDP_NO_ERR)
            {
            	/*- 释放通道句柄结构体内存并汇报错误 */
                vos_printLog(VOS_LOG_ERROR,
                             "subscribeTelegram() Failed. sizeWriteDatasetInTrafficStore() returns error = %d\n",
                             err);
                vos_memFree(pSubscribeTelegram);
                return TRDP_PARAM_ERR;
            }

            /*- 为dataset分配内存 */
            pSubscribeDataset = (UINT32 *)vos_memAlloc(pSubscribeTelegram->datasetNetworkByteSize);
            if (pSubscribeDataset == NULL)
            {
            	/*- 释放通道句柄结构体内存并汇报错误 */
                vos_printLog(VOS_LOG_ERROR, "subscribeTelegram() Failed. Subscribe Dataset vos_memAlloc() Err\n");
                vos_memFree(pSubscribeTelegram);
                return TRDP_MEM_ERR;
            }
            else
            {

            }

            /*- 通道或app配置了启用编组 */
            if (((pExchgPar->pPdPar->flags & TRDP_FLAGS_MARSHALL) == TRDP_FLAGS_MARSHALL)
                || ((arraySessionConfigTAUL[ifIndex].pdConfig.flags & TRDP_FLAGS_MARSHALL) == TRDP_FLAGS_MARSHALL))
            {
                /*- 计算编组后的数据长度 */
                err = tau_calcDatasetSize(
                        marshallConfig.pRefCon,
                        pExchgPar->datasetId,
                        (UINT8 *) pSubscribeDataset,
						pSubscribeTelegram->datasetNetworkByteSize,
                        &pSubscribeTelegram->dataset.size,
                        &pSubscribeTelegram->pDatasetDescriptor);

                /* Free Subscribe Dataset */
                vos_memFree(pSubscribeDataset);

                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR,
                                 "subscribeTelegram() Failed. tau_calcDatasetSize datasetId: %d returns error = %d\n",
                                 pExchgPar->datasetId,
                                 err);

                    /* Free Subscribe Telegram */
                    vos_memFree(pSubscribeTelegram);
                    return TRDP_PARAM_ERR;
                }
                else
                {
                	pSubscribeDataset=(UINT32 *)vos_memAlloc(pSubscribeTelegram->dataset.size);
                }
            }
            else
            {
            	pSubscribeTelegram->dataset.size=pSubscribeTelegram->datasetNetworkByteSize;
            }

            /*- 配置句柄结构体 */

            /* Set Dataset Buffer */
            pSubscribeTelegram->dataset.pDatasetStartAddr = (UINT8 *)pSubscribeDataset;
            /* Set If Config */
            pSubscribeTelegram->pIfConfig = &pIfConfig[ifIndex];
            /* Set PD Parameter */
            pSubscribeTelegram->pPdParameter = pExchgPar->pPdPar;
            /* Set comId */
            pSubscribeTelegram->comId = pExchgPar->comId;

            /* @note 当前版本订阅通道拓扑也直接置为零 */
            pSubscribeTelegram->etbTopoCount = 0;
            pSubscribeTelegram->opTrnTopoCount = 0;

            /* @note 	在通道句柄中使用配置的srcIp，此处srcIp的含义是接收端面向的发送端的源Ip，而不是接收端绑定的Ip,可以是
             * 			-0：不限制源
             * 			-单播：指定源或源范围
             * 			不可以是：
             * 			-组播或广播
             */

            /* srcIp配置存在 */
            if ((&pExchgPar->pSrc[i] != NULL) || (pExchgPar->pSrc[i].pUriHost1 != NULL))
            {
                networkByteIpAddr = vos_dottedIP(*(pExchgPar->pSrc[i].pUriHost1));

                /*- srcIp配置了广播或组播 */
                if ((BROADCAST_ADDRESS==networkByteIpAddr) || vos_isMulticast(networkByteIpAddr))
                {
                	/*- @note 对于无效的srcIp配置，视为不指定源Ip并在log中汇报错误 */
                	pSubscribeTelegram->srcIpAddr = IP_ADDRESS_NOTHING;
                	vos_printLog(VOS_LOG_ERROR,"invalid srcIp config for comId %d.\n",pSubscribeTelegram->comId);
                }
                else
                {
                    pSubscribeTelegram->srcIpAddr = networkByteIpAddr;
                }

                if(pExchgPar->pSrc[i].pUriHost2 != NULL)
                {
                	networkByteIpAddr = vos_dottedIP(*(pExchgPar->pSrc[i].pUriHost2));
                    if (BROADCAST_ADDRESS==networkByteIpAddr)
                    {
                    	pSubscribeTelegram->srcIpAddr2 = IP_ADDRESS_NOTHING;
                    }
                    else
                    {
                        pSubscribeTelegram->srcIpAddr2 = networkByteIpAddr;
                    }
                }
            }
            else
            {
            	/*- 释放通道句柄结构体内存和dataset内存并汇报错误 */
                vos_memFree(pSubscribeDataset);
                vos_memFree(pSubscribeTelegram);
                vos_printLog(VOS_LOG_ERROR,
                             "subscribeTelegram() Failed. Source IP is NULL.\n");
                return TRDP_PARAM_ERR;
            }

            /*- 令订阅通道句柄结构体的pUserRef成员获取它本身 */
            pSubscribeTelegram->pUserRef = (void *)pSubscribeTelegram;

            /*- 创建并配置订阅通道 */
            err = tlp_subscribe(
                    pSubscribeTelegram->appHandle,
                    &pSubscribeTelegram->subHandle,
                    pSubscribeTelegram->pUserRef,                                   /* 订阅中userRef用来传递订阅句柄结构体指针 */
                    NULL, 															/* 回调函数传入NULL以使用app默认的回调函数 */
					0,                                                        		/* serviceId传入默认值 0 */
                    pSubscribeTelegram->comId,                                      /* ComID */
                    pSubscribeTelegram->etbTopoCount,
                    pSubscribeTelegram->opTrnTopoCount,
					pSubscribeTelegram->srcIpAddr, pSubscribeTelegram->srcIpAddr2, /* @note 原版本不将订阅通道句柄结构体配置的srcIp传递给订阅通道，即强制令订阅通道不限制源，现版本传递配置的srcIp，对配置文件的要求为：如果限制源发送端，填写期望的源，如果不限制源Ip,填写0.0.0.0或者不写 */

					/* @note	srcIp的使用：
					 * 			app的realIp来自配置或实际设备网口Ip(无配置时)
					 * 			对于接收：	通道句柄结构体中的srcIp1、2来自telegram配置
					 * 						通道中srcIp1、2来自调用tlp_subscribe时填入，原来直接写0，现在传递通道句柄结构体中的srcIp1、2
					 * 						socket绑定的ip由调用request_socket时填入，目前填的是app里配置的realIp，若接收组播就在request_socket内部由绑定策略函数修改为0，
					 *
					 * 						创建通道时，检查通道srcIp（完全匹配）等来判断通道是否重复；接收数据时，检查通道srcIp等（通道srcIp为0也算匹配）来判断是否有对应订阅
					 *
					 *			对于发送：	通道句柄结构体中的srcIp(只有一个)来自telegram配置或设备真实Ip(若无配置，当前版本不允许为发布通道配置src字段(configureTelegram判断条件)，所以就是后者)
					 *						通道中srcIp1来自调用tlp_publish时填入，目前填的是通道句柄结构体中的srcIp(但是通道中仍然留了srcIp2,应检查对发布通道第二个srcIp的多余处理（检查了，没有多余处理）)
					 *						socket绑定的ip由调用request_socket时填入,目前填的是通道句柄结构体中的srcIp
					 *
					 *						创建通道时，检查通道srcIp等（通道srcIp为0也算匹配），来判断通道是否重复
					 */
                    pSubscribeTelegram->dstIpAddr,
                    pSubscribeTelegram->pPdParameter->flags,
                    NULL,                                                           /* default interface */
                    pSubscribeTelegram->pPdParameter->timeout,                      /* Time out in us   */
                    pSubscribeTelegram->pPdParameter->toBehav);                     /* delete invalid data on timeout */
            if (err != TRDP_NO_ERR)
            {
            	/*- 释放通道句柄结构体内存和dataset内存并汇报错误 */
                vos_memFree(pSubscribeDataset);
                vos_memFree(pSubscribeTelegram);
                vos_printLog(VOS_LOG_ERROR,
                             "subscribeTelegram() Failed. Subscribe Telegram tlp_subscribe() Err:%d\n",
                             err);
                return err;
            }
            else
            {
                /*- 将创建好的通道加入到全局订阅通道句柄链表中 */
                err = appendSubscribeTelegramList(&pHeadSubscribeTelegram[ifIndex], pSubscribeTelegram);
                if (err != TRDP_NO_ERR)
                {
                	/*- 释放通道句柄结构体内存和dataset内存并汇报错误 */
                    vos_memFree(pSubscribeDataset);
                    vos_memFree(pSubscribeTelegram);
                    vos_printLog(
                        VOS_LOG_ERROR,
                        "subscribeTelegram() Failed. Subscribe Telegram appendSubscribeTelegramList() Err:%d\n",
                        err);
                    return err;
                }
            }
        }
    }
    return TRDP_NO_ERR;
}

/**
 * 	@brief			根据传入的配置创建并初始化进程间通道，包括发布、订阅句柄（PUBLISH_TELEGRAM_T），供接口取用
 *	@note			不为进程间消息创建通道,Taul main Thread 不对通道做任何处理
 *  @param[in]      ifIndex             interface Index
 *  @param[in]      pExchgPar           Pointer to Exchange Parameter
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T ipcPubSubTelegram (
    UINT32              ifIndex,
    TRDP_EXCHG_PAR_T    *pExchgPar)
{
	TRDP_ERR_T          err = TRDP_NO_ERR;

    PUBLISH_TELEGRAM_T  *pPublishTelegram   = NULL;
    SUBSCRIBE_TELEGRAM_T    *pSubscribeTelegram = NULL;

    UINT32                  datasetIndex        = 0;
    pTRDP_DATASET_T		pDataset=NULL;

    /*- 为发布通道句柄（PUBLISH_TELEGRAM_T）分配内存 */
    pPublishTelegram = (PUBLISH_TELEGRAM_T *)vos_memAlloc(sizeof(PUBLISH_TELEGRAM_T));
    if (pPublishTelegram == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "publishTelegram() Failed. Publish Telegram vos_memAlloc() Err\n");
        err = TRDP_MEM_ERR;
        return err;
    }

	/*- 为订阅通道句柄结构体申请内存 */
	pSubscribeTelegram = (SUBSCRIBE_TELEGRAM_T *)vos_memAlloc(sizeof(SUBSCRIBE_TELEGRAM_T));
	if (pSubscribeTelegram == NULL)
	{
		vos_printLog(VOS_LOG_ERROR, "SubscribeTelegram() Failed. Subscribe Telegram vos_memAlloc() Err\n");
		return TRDP_MEM_ERR;
	}
	else
	{

	}

    /*- 搜索dataset列表为当前通道句柄获取对应的datast配置指针 */
    for (datasetIndex = 0; datasetIndex < numDataset; datasetIndex++)
    {
        /* DatasetId in dataSet config ? */
        if (pExchgPar->datasetId == apDataset[datasetIndex]->id)
        {
            /* Set Dataset Descriptor */
        	pDataset = apDataset[datasetIndex];
            break;
        }
    }

    /*- 未找到对应的dataset配置  */
    if (NULL == pDataset)
    {
        vos_printLog(VOS_LOG_ERROR,
                     "ipcPubSubTelegram() Failed. Dataset no find. datasetId: %d, comId: %d\n",
                     pExchgPar->datasetId,
                     pExchgPar->comId);

        /*- 释放发布和订阅通道句柄结构体 */
        vos_memFree(pPublishTelegram);
		vos_memFree(pSubscribeTelegram);

        err = TRDP_PARAM_ERR;
        return err;
    }

    pPublishTelegram->pDatasetDescriptor = pDataset;
	pSubscribeTelegram->pDatasetDescriptor = pDataset;

    /*- 通过配置数组下标确定当前通道句柄绑定的会话 */
    if (ifIndex == IF_INDEX_SUBNET1)
    {
        /* Set Application Handle: Subnet1 */
        pPublishTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
        pSubscribeTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
    }
    else if (ifIndex == IF_INDEX_SUBNET2)
    {
        /* Set Application Handle: Subnet2 */
        pPublishTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
        pSubscribeTelegram->appHandle = arraySessionConfigTAUL[ifIndex].sessionHandle;
    }
    else
    {
        /* ifIndex Error */
        vos_printLog(VOS_LOG_ERROR, "ipcPubSubTelegram() Failed. ifIndex:%d error\n", ifIndex);
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);
		/* Free Subscribe Telegram */
		vos_memFree(pSubscribeTelegram);

        err = TRDP_PARAM_ERR;
        return err;
    }

    /* 根据dataset配置计算数据集在网络传输时用户数据的size存入通道句柄  */
    err = sizeWriteDatasetInPackage(&pPublishTelegram->datasetNetworkByteSize, pPublishTelegram->pDatasetDescriptor);
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR,
                     "ipcPubSubTelegram() Failed. sizeWriteDatasetInTrafficStore() returns error = %d\n",
                     err);
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);
        return TRDP_PARAM_ERR;
    }

	/*- 直接获取NetworkByteSize作为主机长度 */
	pPublishTelegram->dataset.size=pPublishTelegram->datasetNetworkByteSize;

	/*- 为通道计算网络数据长度 */
	err = sizeWriteDatasetInPackage(&pSubscribeTelegram->datasetNetworkByteSize,pSubscribeTelegram->pDatasetDescriptor);
	if (err != TRDP_NO_ERR)
	{
		/*- 释放通道句柄结构体内存并汇报错误 */
		vos_printLog(VOS_LOG_ERROR,
					 "ipcPubSubTelegram() Failed. sizeWriteDatasetInTrafficStore() returns error = %d\n",
					 err);
		vos_memFree(pSubscribeTelegram);
		return TRDP_PARAM_ERR;
	}

	/*- 直接获取NetworkByteSize作为主机长度 */
	pSubscribeTelegram->dataset.size=pSubscribeTelegram->datasetNetworkByteSize;

    /*- 通道句柄的缓冲区入口置空 */
    pPublishTelegram->dataset.pDatasetStartAddr = NULL;
	pSubscribeTelegram->dataset.pDatasetStartAddr = NULL;

	/*- 令通道句柄获取配置 */

    pPublishTelegram->pIfConfig = &pIfConfig[ifIndex];
    pPublishTelegram->pPdParameter = pExchgPar->pPdPar;
    pPublishTelegram->comId = pExchgPar->comId;

	pSubscribeTelegram->pIfConfig = &pIfConfig[ifIndex];
	pSubscribeTelegram->pPdParameter = pExchgPar->pPdPar;
	pSubscribeTelegram->comId = pExchgPar->comId;

    /** 用不到的配置置空 */
    pPublishTelegram->etbTopoCount = 0;
    pPublishTelegram->opTrnTopoCount = 0;

	pSubscribeTelegram->etbTopoCount = 0;
	pSubscribeTelegram->opTrnTopoCount = 0;

	pSubscribeTelegram->srcIpAddr = IP_ADDRESS_NOTHING;
	pPublishTelegram->srcIpAddr = IP_ADDRESS_NOTHING;
	pSubscribeTelegram->dstIpAddr = IP_ADDRESS_NOTHING;
	pPublishTelegram->dstIpAddr = IP_ADDRESS_NOTHING;

    pPublishTelegram->pSendParam = NULL;

	/*- 令订阅通道句柄结构体的pUserRef成员获取它本身 */
	pSubscribeTelegram->pUserRef = (void *)pSubscribeTelegram;

    /*- 将当前通道加入到全局的发布通道链表 */
    err = appendPublishTelegramList(&pHeadPublishTelegram[ifIndex], pPublishTelegram);
    if (err != TRDP_NO_ERR)
    {
        /* Free Publish Telegram */
        vos_memFree(pPublishTelegram);
        vos_printLog(VOS_LOG_ERROR,
                     "ipcPubSubTelegram() Failed. Publish Telegram appendPublishTelegramList() Err:%d\n",
                     err);
    }

	/*- 将通道加入到全局订阅通道句柄链表中 */
	err = appendSubscribeTelegramList(&pHeadSubscribeTelegram[ifIndex], pSubscribeTelegram);
	if (err != TRDP_NO_ERR)
	{
		/*- 释放通道句柄结构体内存并汇报错误 */
		vos_memFree(pSubscribeTelegram);
		vos_printLog(
			VOS_LOG_ERROR,
			"ipcPubSubTelegram() Failed. Subscribe Telegram appendSubscribeTelegramList() Err:%d\n",
			err);
		return err;
	}

    return TRDP_NO_ERR;
}

/******************************************************************************/
/** PD Main Process Init
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_THREAD_ERR
 */
TRDP_ERR_T tau_pd_main_proc_init (
    void)
{
    VOS_ERR_T           vosErr = VOS_NO_ERR;
    extern VOS_THREAD_T taulPdMainThreadHandle;         /* Thread handle */

    /* TAULpdMainThread */
    extern CHAR8        taulPdMainThreadName[];             /* Thread name is TAUL PD Main Thread. */


    /* Create TAULpdMainThread */
    vosErr = vos_threadCreate(&taulPdMainThreadHandle,
                              taulPdMainThreadName,
                              VOS_THREAD_POLICY_OTHER,
                              TAUL_PROCESS_PRIORITY,
                              80,
                              TAUL_PROCESS_THREAD_STACK_SIZE,
                              (void *)TAULpdMainThread,
                              NULL);
    if (vosErr != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "TRDP TAULpdMainThread Create failed. VOS Error: %d\n", vosErr);
        return TRDP_THREAD_ERR;
    }
    return TRDP_NO_ERR;
}



/******************************************************************************/
/** TAUL PD Main Process Thread
 *
 */


VOS_THREAD_FUNC_T TAULpdMainThread (
    void)
{
	vos_threadDelay(3000000);


	/*- 设置trdp运行于0核 */
#if defined(linux)
	SetThreadOnSpecifiedCPU(0);

#elif defined (VXWORKS)
    cpuset_t affinity;
    CPUSET_ZERO(affinity);
    CPUSET_SET(affinity,0);  // CPU 0
    if(OK==taskCpuAffinitySet(taskIdSelf(),affinity))
    {
        printf("TRDP taskCpuAffinitySet seccessfully \r\n");
    }
    else
    {
        printf("TRDP taskCpuAffinitySet failed \r\n");
    }
#endif

    PD_ELE_T    *iterPD1 = NULL;
    PD_ELE_T    *iterPD2 = NULL;
    TRDP_TIME_T nowTime = {0};
    TRDP_ERR_T  err = TRDP_NO_ERR;
	INT32               rv = 0;
    const UINT16 msgTypePrNetworkByteOder = vos_htons(TRDP_MSG_PR); /* message type of PD request */

    fd_set      rfds;
    INT32       noOfDesc    = 0;
    TRDP_TIME_T tv          = max_tv;

    INT32       noOfDesc2   = 0;
    TRDP_TIME_T tv2         = max_tv;
    BOOL8       linkUpDown  = TRUE;
    UINT32      writeSubnetId;

#if 0
    /* testcheck*/
    VOS_TIMEVAL_T startTimeVal;
    VOS_TIMEVAL_T endTimeVal;
    INT32         startReason = 0;
#endif
    /*- 等待全局apphandle设置完成 */
    while (1)
    {
        /* Ladder Topology ? */
        if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
        {
            /* Ladder Topology : Application Handle Ready ? */
            if (appHandle1 != NULL && appHandle2 != NULL)
            {
                break;
            }
        }
        else
        {
            /* Not Ladder Topology : Application Handle Ready ? */
            if (appHandle1 != NULL)
            {
                break;
            }
        }
        /* Wait 1ms */
        vos_threadDelay(1000);
    }

    /*- 主循环 */

    printf("trdp main thread cyc run begin!\n");

    while (1)
    {
        noOfDesc    = 0;
        tv          = max_tv;
        tv2         = max_tv;
        noOfDesc2   = 0;
        linkUpDown  = TRUE;

        /*- 清空用于select调用的文件描述符集 */
        FD_ZERO(&rfds);

        /*- 为select计算超时时长并返回关联的文件描述符 */
        tlc_getInterval(appHandle1,
                        (TRDP_TIME_T *) &tv,
                        (TRDP_FDS_T *) &rfds,
                        &noOfDesc);

        /* @note 如果我们需要处理更快于最小周期的事项，手动设置max_tv */

        if (vos_cmpTime((TRDP_TIME_T *) &tv, (TRDP_TIME_T *) &max_tv) > 0)
        {
            tv = max_tv;
        }

        if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
        {
            tlc_getInterval(appHandle2,
                            (TRDP_TIME_T *) &tv2,
                            (TRDP_FDS_T *) &rfds,
                            &noOfDesc2);

            if (vos_cmpTime((TRDP_TIME_T *) &tv2, (TRDP_TIME_T *) &max_tv) > 0)
            {
                tv2 = max_tv;
            }
        }

        /*- 获取两个apphandle中最的socekt句柄值 */
        if (noOfDesc > noOfDesc2)
        {
            ;
        }
        else
        {
            noOfDesc = noOfDesc2;
        }

        /*- 获取两个apphandle中更小的下一任务间隔 */
        if (vos_cmpTime(&tv, &tv2) > 0)
        {
            tv = tv2;
        }
#if 0
        vos_getTime(&endTimeVal);
        vos_printLog(VOS_LOG_ERROR,"[start time|end time(us)|recv?]	%llu	%llu	%d\n",(startTimeVal.tv_sec*1000000llu+startTimeVal.tv_usec),(endTimeVal.tv_sec*1000000llu+endTimeVal.tv_usec),startReason);
#endif
        /*- 等待描述符可读/可写或超时 */
        rv = vos_select((int)noOfDesc + 1, &rfds, NULL, NULL, (VOS_TIMEVAL_T *)&tv);
#if 0
        vos_getTime(&startTimeVal);
        startReason = rv;
#endif
        /*- 请求app1和app2（若启用）的总锁 */
        vos_mutexLock(appHandle1->mutex);
        
        if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
        {
            vos_mutexLock(appHandle2->mutex);
            iterPD2 = appHandle2->pSndQueue;
        }
        
        /* @note 当前处理需使两个interface中telegram配置的内容和顺序都一模一样，任何在另一interface中无对应配置的telegram都不被处理 */
        
        /*- 同时遍历app1和app2的发送通道 */
        for (iterPD1 = appHandle1->pSndQueue; iterPD1 != NULL; iterPD1 = iterPD1->pNext)
        {
        	/*- app1和app2通道配置不同步 */
        	if (NULL != iterPD2 && iterPD1->addr.comId != iterPD2->addr.comId)
			{
        		/*- 跳过后续对app1和app2当前通道所有处理 */
        		printf("two interface config differently!app1comid:%d,app2comid:%d\n",iterPD1->addr.comId,iterPD2->addr.comId);
        		continue;
			}

        	/*- 通道配置为PD reply专用 不在周期循环中处理 */
        	/* 此处等效于检查两个app的comid */
        	if (iterPD1->addr.comId == TRDP_GLOBAL_STATS_REPLY_COMID)
			{
        		/*- 跳过后续对app1和app2当前通道所有处理 */
				continue;
			}

            /*- 获取当前时间 */
            vos_getTime(&nowTime);

            /*- 通道类型不为PD request */
            if ((iterPD1->pFrame->frameHead.msgType != msgTypePrNetworkByteOder) && (NULL == iterPD2 || iterPD2->pFrame->frameHead.msgType != msgTypePrNetworkByteOder))
            {
            	
                /*-当前通道已到达put任务时间  */
                if (vos_cmpTime((TRDP_TIME_T *)&iterPD1->timeToPut, (TRDP_TIME_T *)&nowTime) < 0)
                {
                	
                	SDT_HANDLE sdsrcHandle=0;
                	UINT32 	srctelegramConfigSdtOrNot=0;
                	SDT_RC 	sdsrc_rc=SDT_OK;
                	
                    /* Check comId which Publish our statistics packet */
                    if (iterPD1->addr.comId != TRDP_GLOBAL_STATISTICS_COMID)
                    {
                        /*- 从共享内存获取用户数据 */
                        tau_ldLockTrafficStore();
                        memcpy((void *)ts_buffer,
                               (UINT8 *)(pTrafficStoreAddr + iterPD1->offset),
							   iterPD1->dataSize);
                        tau_ldUnlockTrafficStore();
                        
                    	/*- 通过comid查找对应的sdt通道句柄 */
                    	for(int i=0;i<MAX_SDT_HANDLE_NUMBER;i++)
                    	{
                    		if (sdsrcConfig[i].comId==iterPD1->addr.comId)
                    		{
                    			sdsrcHandle=sdsrcConfig[i].sdtHandle;
                    			srctelegramConfigSdtOrNot=1;
                    			break;
                    		}
                    	}

                    	/*- 未查询到对应的sdt通道 */
                    	if (0==srctelegramConfigSdtOrNot)
                    	{
                    		/*- 将用户数据拷贝到通道 */
                            vos_printLog(VOS_LOG_INFO,"appHandle1 no sdsrc config for comid %d\n",iterPD1->addr.comId);

                            err = tlp_put(
                                    appHandle1,
                                    iterPD1,
                                    (UINT8 *)ts_buffer,
                                    iterPD1->dataSize);
                            if (err != TRDP_NO_ERR)
                            {
                                vos_printLog(VOS_LOG_ERROR, "TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
                            }
                    	}
                    	/*- 查询到对应的sdt通道 */
                    	else
                    	{
                            /*- 初始化当前sdt通道 */
                    		psdsrcOutputList[sdsrcHandle].vdpLenth=TRDP_MAX_PD_DATA_SIZE;

                    		/*- 使用当前sdt通道封装VDP */
                    		sdsrc_rc=sdsrc_generate_VDP(sdsrcHandle,(UINT8*)ts_buffer,iterPD1->dataSize-SDT_TAIL_SIZE,psdsrcOutputList[sdsrcHandle].vdpData,&psdsrcOutputList[sdsrcHandle].vdpLenth);
                    		/* @note 这里要求sdsrc_generate_VDP的第四个参数必须等于第二个参数+SDT_TAIL_SIZE,否则下一步put会失败 */
                    		psdsrcOutputList[sdsrcHandle].sdsrc_rc=sdsrc_rc;
                    		
                    		/*- 封装VDP成功 */
                            if(SDT_OK==sdsrc_rc)
                            {
                            	/*- 将封装后的用户数据拷贝到通道 */
                                err = tlp_put(
                                        appHandle1,
                                        iterPD1,
										psdsrcOutputList[sdsrcHandle].vdpData,
										psdsrcOutputList[sdsrcHandle].vdpLenth);
                                if (err != TRDP_NO_ERR)
                                {
                                	printf("TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
                                    vos_printLog(VOS_LOG_ERROR, "TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
                                }
                            }
                            /*- 封装VDP失败 */
                            else
                            {
                            	/*- 将用户数据拷贝到通道 */
                            	vos_printLog(VOS_LOG_ERROR, "apphandle1 sdsrc generate vdp failed,error code:%d\n",sdsrc_rc);

                               	/* @note 该通道配置了sdt但是组包失败，dataSize中预留了16位sdt包尾，此时拷贝入通道的最后16byte是共享内存中无法确定的无效数据，置0防护一下 */
                            	memset((void *)(ts_buffer+iterPD1->dataSize-SDT_TAIL_SIZE),0,SDT_TAIL_SIZE);

                                err = tlp_put(
                                        appHandle1,
                                        iterPD1,
                                        (UINT8 *)ts_buffer,
                                        iterPD1->dataSize);
                                if (err != TRDP_NO_ERR)
                                {
                                	printf("TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
                                    vos_printLog(VOS_LOG_ERROR, "TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
                                }
                            }
                    	}
                    }//完成PD发布通道的put处理
                    else
                    {
                    	//??当前未添加处理统计通道的功能

                    }//完成统计发布通道的put处理
                    
                    /*- 更新app1的下一put任务时间 */
                    vos_addTime(&iterPD1->timeToPut, &iterPD1->interval);

                    /*- app1的超时已超过一周期 */
                    if (vos_cmpTime(&iterPD1->timeToPut, &nowTime) <= 0)
                    {
                    	/*- 计算超时周期数（向下取整）*/
                    	long differTimeUs=(nowTime.tv_sec-iterPD1->timeToPut.tv_sec)*1000000+(nowTime.tv_usec-iterPD1->timeToPut.tv_usec);
                    	long differT=1+(differTimeUs/(iterPD1->interval.tv_sec*1000000+iterPD1->interval.tv_usec));

                    	/*- 当前通道配置了sdt */
                        if(srctelegramConfigSdtOrNot)
                        {
                        	/*- 增加sdt序列号以保持序列号与时钟的同步性 */
                        	sdsrc_SSCvalue_add_value(sdsrcHandle,(UINT32)differT);
                        }
                        //printf("apphandle1:now it is %d cycle later than time to go now:%d:%d timeToPut:%d:%d\n",differT,nowTime.tv_sec,nowTime.tv_usec,iterPD1->timeToPut.tv_sec,iterPD1->timeToPut.tv_usec);

                        /*- app1的下一put任务时间跳到最近的整数倍周期时间后 */
                        TRDP_TIME_T jumpTime;
                        jumpTime.tv_sec=(differT*(iterPD1->interval.tv_sec*1000000+iterPD1->interval.tv_usec))/1000000;
                        jumpTime.tv_usec=(differT*(iterPD1->interval.tv_sec*1000000+iterPD1->interval.tv_usec))%1000000;
                        vos_addTime(&iterPD1->timeToPut, &jumpTime);
                    }
                    
                    
                    /*- app2有效 */
                    if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
                    {

						/* Check comId which Publish our statistics packet */
						if (iterPD2->addr.comId != TRDP_GLOBAL_STATISTICS_COMID)
						{
							if (0==srctelegramConfigSdtOrNot)
							{
								vos_printLog(VOS_LOG_INFO,"appHandle2 no sdsrc config for comid %d \n",iterPD2->addr.comId);
								//printf("appHandle2 no sdsrc config for comid %d\n",iterPD->addr.comId);*/
								err = tlp_put(
										appHandle2,
										iterPD2,
										(UINT8 *)ts_buffer,
										iterPD2->dataSize);
								if (err != TRDP_NO_ERR)
								{
									printf("check lenth:%d==%d?\n",iterPD2->dataSize,psdsrcOutputList[sdsrcHandle].vdpLenth);
									vos_printLog(VOS_LOG_ERROR, "TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
								}
							}	
							else
							{
								/* use vdp generated in appHandle1 */
								if(SDT_OK==psdsrcOutputList[sdsrcHandle].sdsrc_rc)
								{
									//printf("apphandle2 sdsrc generate vdp success\n");
									err = tlp_put(
											appHandle2,
											iterPD2,
											psdsrcOutputList[sdsrcHandle].vdpData,
											psdsrcOutputList[sdsrcHandle].vdpLenth);
									if (err != TRDP_NO_ERR)
									{
										vos_printLog(VOS_LOG_ERROR, "TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
									}
								}
								else
								{	/* in case of generate error we: */
									printf("apphandle2 sdsrc generate vdp failed,error code %d\n",err);
									err = tlp_put(
											appHandle2,
											iterPD2,
											(UINT8 *)ts_buffer,
											iterPD2->dataSize);
									if (err != TRDP_NO_ERR)
									{
										vos_printLog(VOS_LOG_ERROR, "TAULpdMainThread() Failed. tlp_put() Err: %d\n", err);
									}
								}
							}
						}
						   
                    }//完成PD常规发布通道的put处理
					else
					{
						//??当前未添加处理统计通道的功能

					}//完成统计发布通道的put处理

                }//完成到时通道的处理
                /*- 通道未到put时间 */
                else
                {
                	;//未到时通道不做任何处理
                }
            }//完成pd publish通道的处理
            /*- 通道类型为pd request */
            else
            {
            	//??当前未添加处理pd request通道的功能
            }//完成pd request通道的处理
            
            if(NULL!=iterPD2)
            {
            	iterPD2 = iterPD2->pNext;
            }

        }//完成一组（app1+app2）通道的处理
        
        vos_mutexUnlock(appHandle1->mutex);
        if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
        {
        	vos_mutexUnlock(appHandle2->mutex);
        }


        /* 启用冗余app且select超时无接收 */
        /* 	@note rv<=0当app最近下一任务时间到达且无udp接收时就会产生
			如果select超时时间来自发布通道，则正常接收会触发该条件，用该条件来触发主网检查是低效率的方法
			考虑使用TIMEOUT标志作为触发条件?? */
        if ((rv <= 0) && (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE))
        {
            /*- 获取当前使用的子网 */
            err = tau_getNetworkContext(&writeSubnetId);
            if (err != TRDP_NO_ERR)
            {
                vos_printLog(VOS_LOG_DBG, "prep Sub-network tau_getNetworkContext error\n");
            }

            /*- 检查当前使用的子网通断 */
            tau_checkLinkUpDown(writeSubnetId, &linkUpDown);

            /*- 连接失效 */
            if (linkUpDown == FALSE)
            {
                /*- 获取另一子网 */
                if ( writeSubnetId == SUBNET1)
                {
                    vos_printLog(VOS_LOG_DBG, "Subnet1 Link Down. Change Receive Subnet\n");
                    writeSubnetId = SUBNET2;
                }
                else
                {
                    vos_printLog(VOS_LOG_DBG, "Subnet2 Link Down. Change Receive Subnet\n");
                    writeSubnetId = SUBNET1;
                }
                /*- 切换至另一子网 */
                err = tau_setNetworkContext(writeSubnetId);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_DBG, "prep Sub-network tau_setNetworkContext error\n");
                }
                else
                {
                    vos_printLog(VOS_LOG_DBG, "tau_setNetworkContext() set subnet:0x%x\n", writeSubnetId);
                }
            }
        }

        /*- 处理主备功能 */
        err=procRed();
        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_DBG, "TAUL main thread update redun states failed\n");
        }

        tlc_process(appHandle1, (TRDP_FDS_T *) &rfds, &rv);

        if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
        {
            tlc_process(appHandle2, (TRDP_FDS_T *) &rfds, &rv);
        }

    }// 主循环一个周期处理结束
}

/**
 * @brief		处理trdp的主备功能
 */
TRDP_ERR_T procRed(void)
{
	TRDP_ERR_T err=TRDP_NO_ERR;

	/*- 服务层没有提供获取主备的回调函数或没有配置任何冗余通道 */
	if(NULL==pTaulRedunCbfuc || 0u==publishRedunList[0].redId)
	{
		err=TRDP_NO_ERR;
	}
	else
	{
		err=updateRedunStates();
	}

	return err;
}

/**
 * @brief		更新所有冗余状态
 * @details		从服务层读取冗余状态、维护本地冗余状态列表、设置变化了的双App冗余状态
 * @return		TRDP_NO_ERR 设置成功
 * @note		对于接口未获取到列表或接口提供的redId不存在与本模块的情况，不返回错误
 */
TRDP_ERR_T updateRedunStates(void)
{
	TRDP_ERR_T errApp1=TRDP_NO_ERR;
	TRDP_ERR_T errApp2=TRDP_NO_ERR;
	TRDP_ERR_T err=TRDP_NO_ERR;

	UINT32 redunIdNum=MAX_REDID_NUM;
	REDUN_INFO_T  readedRedunList[MAX_REDID_NUM]={0};
	UINT32 i=0;
	UINT32 j=0;

	pTaulRedunCbfuc(readedRedunList,&redunIdNum);

	/*- 依次处理获取的所有redId */
	for(i=0;i<redunIdNum;i++)
	{
		/*- 获取到一条统一设置指令 */
		if(0u==readedRedunList[i].redId)
		{
			/*- 为所有组设置冗余状态 */
			errApp1=tlp_setRedundant(appHandle1,0u,readedRedunList[i].leader);
			errApp2=tlp_setRedundant(appHandle2,0u,readedRedunList[i].leader);

			if(TRDP_NO_ERR==errApp1 && TRDP_NO_ERR==errApp2)
			{
				/* 更新本地状态列表 */
				for(j=0;j<MAX_REDID_NUM;j++)
				{
					/*- 冗余Id为0 */
					if(0u==publishRedunList[j].redId)
					{
						/*- 列表已结束 */
						break;
					}
					else
					{
						publishRedunList[j].leader=readedRedunList[i].leader;
					}
				}
			}
			else
			{
				err=(TRDP_NO_ERR==errApp1 ? errApp2 : errApp1);
			}

			/*- 不再处理获取的其他消息 */
			break;
		}
		/*- 获取到一条普通设置指令 */
		else
		{
			/*- 查询本地状态列表 */
			for(j=0;j<MAX_REDID_NUM;j++)
			{
				/*- 列表单元为空 */
				if(0==publishRedunList[j].redId)
				{
					//输入的冗余id不存在时不返回错误
					vos_printLog(VOS_LOG_DBG, "set redundant state failed, redId %d doesn't exist in local redId list\n", readedRedunList[i].redId);
					break;
				}
				/*- 冗余Id存在 */
				else if(readedRedunList[i].redId==publishRedunList[j].redId)
				{
					/*- Id状态变化 */
					if(readedRedunList[i].leader!=publishRedunList[j].leader)
					{
						/*- 设置Id为新状态 */
						errApp1=tlp_setRedundant(appHandle1,readedRedunList[i].redId,readedRedunList[i].leader);
						errApp2=tlp_setRedundant(appHandle2,readedRedunList[i].redId,readedRedunList[i].leader);

						/*- 设置成功*/
						if(TRDP_NO_ERR==errApp1 && TRDP_NO_ERR==errApp2)
						{
							/*- 更新本地状态 */
							publishRedunList[j].leader=readedRedunList[i].leader;
						}
						/*- 设置失败*/
						else
						{
							/*- 记录错误 */
							err=(TRDP_NO_ERR==errApp1 ? errApp2:errApp1);
						}

					}

					break;
				}
			}
		}
	}

	return err;
}




/**********************************************************************************************************************/
/** TAUL API
 */
/******************************************************************************/
/** Initialize TAUL and create shared memory if required.
 *  Create Traffic Store mutex, Traffic Store
 *
 *  @param[in]      pPrintDebugString   Pointer to debug print function
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T tau_ldInit (TRDP_PRINT_DBG_T pPrintDebugString,TRDP_USER_CBFUNC_T pUserCbfunc,TRDP_REDUN_CBFUNC_T pRedunCbfunc)
{
	/*- 初始化返回值 */
	
    TRDP_ERR_T      err     = TRDP_NO_ERR;
    int sessionIndex=0;
    
#ifdef TSN_PTP_SERVICE
    INT32 ptpClockId[LADDER_IF_NUMBER]={-1,-1};
#endif

    /*-清空由初始化流程设置的全局变量 */
    appHandle1   = NULL;
    appHandle2  = NULL;

    for(sessionIndex=0;sessionIndex<MAX_SESSIONS;sessionIndex++)
    {
        pHeadPublishTelegram[sessionIndex]    = NULL;
        pHeadSubscribeTelegram[sessionIndex]  = NULL;
    }

    pPublishTelegramMutex   = NULL;
    pSubscribeTelegramMutex = NULL;
	
	printf("##################### trdp module:%s %s ##################### \r\n",TRDP_MODULE_VER,TRDP_MODULE_VER_NOTE);

	/*- 令taul模块获取服务层传递的回调函数入口 */
	pTaulUserCbfuc=pUserCbfunc;
	pTaulRedunCbfuc=pRedunCbfunc;

#ifdef XML_CONFIG_ENABLE
	
    /*- 准备读取配置文件 */
    err = tau_prepareXmlDoc(xmlConfigFileName, &xmlConfigHandle);
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tau_prepareXmlDoc() error\n");
        return err;
    }
    
    /*- 读取基本配置 */
    err = tau_readXmlDeviceConfig(&xmlConfigHandle,
                                  &memoryConfigTAUL,
								  &sharememConfigTAUL,
                                  &debugConfigTAUL,
                                  &numComPar,
                                  &pComPar,
                                  &numIfConfig,
                                  &pIfConfig);
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tau_readXmlDeviceConfig() error\n");
        return err;
    }

    printf("check sharemem config size:%d offset:%d\n",sharememConfigTAUL.size,sharememConfigTAUL.indexOffset);
    vos_printLog(VOS_LOG_ERROR,"#####################trdp module:%s %s##################### \r\n",TRDP_MODULE_VER,TRDP_MODULE_VER_NOTE);
#endif

    /*- 检查网口配置数量，有效数量为1或2 */
    if ((numIfConfig > 2) || (numIfConfig <= 0))
    {
    	err=TRDP_PARAM_ERR;
    	vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. I/F config number=%d invalid\n",numIfConfig);
    	return err;
    }

    /*- 初始化trdp内存块并初始化vos模块  */
    err = tlc_init(pPrintDebugString,            /* debug print function */
    		       NULL,
    	           &memoryConfigTAUL);           /* Use application supplied memory */
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tlc_init() error = %d\n", err);
        return err;
    }
    
    UINT32          getNoOfIfaces = NUM_ED_INTERFACES;
    VOS_IF_REC_T    ifAddressTable[NUM_ED_INTERFACES];

    /*- 读取设备网口 */
    if (vos_getInterfaces(&getNoOfIfaces, ifAddressTable) != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. vos_getInterfaces() error.\n");
        return TRDP_SOCK_ERR;
    }
    
    /*- 用配置网口名识别设备网口获取子网的实际ip地址 */
    UINT32          ifIndex = 0;
    UINT32          devIfIndex   = 0;

    /*- 遍历trdp会话配置 */
    for(ifIndex=0;ifIndex<numIfConfig;ifIndex++)
    {
    	/*- 查询设备网口信息列表 */
        for (devIfIndex = 0; devIfIndex < getNoOfIfaces; devIfIndex++)
        {
        	/*- 设备网口名称与trdp会话配置的匹配 */
            if (strncmp(ifAddressTable[devIfIndex].name, pIfConfig[ifIndex].ifName, sizeof(pIfConfig[ifIndex].ifName)) == 0)
            {
            	/*- 设备网口名称与trdp会话配置的匹配 */
            	if(0u==ifIndex)
            	{
            		subnetId1Address = (TRDP_IP_ADDR_T)(ifAddressTable[devIfIndex].ipAddr);
            	}
            	else if(1u==ifIndex)
            	{
            		subnetId2Address = (TRDP_IP_ADDR_T)(ifAddressTable[devIfIndex].ipAddr);
            	}

#ifdef TSN_PTP_SERVICE
                ptpClockId[ifIndex]=GetPTPClockID(pIfConfig[ifIndex].ifName);
                if(-1==ptpClockId[ifIndex])
                {
                	vos_printLog(VOS_LOG_ERROR, "get tsn ptp clock id for device %s failed\n",pIfConfig[ifIndex].ifName);
                }
#endif

#ifdef INTERFACE_SEPARATE
            	if(0u==ifIndex)
            	{
            		subnetId1Ifindex=if_nametoindex(pIfConfig[ifIndex].ifName);
            		vos_printLog(VOS_LOG_ERROR, "trdp interfase %u %s config get ip:%s,index:%u from device\n",ifIndex,pIfConfig[ifIndex].ifName,vos_ipDotted(ifAddressTable[devIfIndex].ipAddr),subnetId1Ifindex);
            	}
            	else if(1u==ifIndex)
            	{
            		subnetId2Ifindex=if_nametoindex(pIfConfig[ifIndex].ifName);
            		vos_printLog(VOS_LOG_ERROR, "trdp interfase %u %s config get ip:%s,index:%u from device\n",ifIndex,pIfConfig[ifIndex].ifName,vos_ipDotted(ifAddressTable[devIfIndex].ipAddr),subnetId2Ifindex);
            	}
#endif


                break;
            }
        }
    }

#ifdef XML_CONFIG_ENABLE
    /*- 读取数据集配置 */
    err = tau_readXmlDatasetConfig(&xmlConfigHandle,
                                   &numComId,
                                   &pComIdDsIdMap,
                                   &numDataset,
                                   &apDataset);
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tau_readXmlDatasetConfig() error = %d\n", err);
        return err;
    }
    else
	{
    	printf("read dataset config success:\n");
    	for(int k=0;k<numDataset;k++)
    	{
    		printf("dataset id=%d\t",apDataset[k]->id);

    	}
		printf("\n");
	}
#endif /* ifdef XML_CONFIG_ENABLE */


    BOOL8           marshallInitFirstTime       = TRUE;
    TRDP_MARSHALL_CONFIG_T *pMarshallConfigPtr  = NULL;

    for (ifIndex = 0; ifIndex < numIfConfig; ifIndex++)
    {
    	/*-  */
#ifdef XML_CONFIG_ENABLE
        /*- 获取 I/F 配置参数 */
        err = tau_readXmlInterfaceConfig(&xmlConfigHandle,
                                         pIfConfig[ifIndex].ifName,
                                         &arraySessionConfigTAUL[ifIndex].processConfig,
                                         &arraySessionConfigTAUL[ifIndex].pdConfig,
                                         &arraySessionConfigTAUL[ifIndex].mdConfig,
                                         &numExchgPar[ifIndex],
                                         &arrayExchgPar[ifIndex]);
        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tau_readXmlInterfaceConfig() error = %d\n", err);
            return err;
        }
#endif /* ifdef XML_CONFIG_ENABLE */

        /* @note	如果未初始化marshall模块，则不能使用marshall中涉及查询dataset配置的功能，包括：
         * 			1.调用marshall/unmarshall等接口时未提供ds配置，需要函数自己查询ds配置
         * 			2.处理的ds配置中有复合类型的元素，需要函数查询复合类型的ds配置
         *
         * 			因此最好为任何配置了启用marshall的telegram的interface配置启用marshall
         */

        /*- 当前interface配置了启用marshall? */
        if ((arraySessionConfigTAUL[ifIndex].pdConfig.flags & TRDP_FLAGS_MARSHALL) == TRDP_FLAGS_MARSHALL)
        {
            /* Set MarshallConfig */
            pMarshallConfigPtr = &marshallConfig;

            /*- 首次初始化mashall模块? */
            if (marshallInitFirstTime == TRUE)
            {
                /*- 初始化marshall模块 */
                err = tau_initMarshall(&marshallConfig.pRefCon, numComId, pComIdDsIdMap, numDataset, apDataset);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tau_initMarshall() returns error = %d\n", err);
                    return err;
                }
                else
                {
                    /*- 标记marshall模块已初始化 */
                    marshallInitFirstTime = FALSE;
                }
            }
        }

        /*- 优先使用配置的ip创建会话，未配置则使用设备子网ip */

        TRDP_IP_ADDR_T  ownIpAddress = 0;

        if (pIfConfig[ifIndex].hostIp == IP_ADDRESS_NOTHING)
        {
            /* Set Real I/F Address */
            /* Is I/F subnet1 ? */
            if (ifIndex == IF_INDEX_SUBNET1)
            {
                ownIpAddress = subnetId1Address;
            }
            /* Is I/F subnet2 ? */
            else if (ifIndex == IF_INDEX_SUBNET2)
            {
                ownIpAddress = subnetId2Address;
            }
            else
            {
                vos_printLog(VOS_LOG_ERROR, "tau_ldInit() Failed. I/F Own IP Address Err.\n");
                return TRDP_PARAM_ERR;
            }
        }
        else
        {
            /* Set I/F Config Address */
            ownIpAddress = pIfConfig[ifIndex].hostIp;
        }

        /* Set PD Call Back Funcktion in MD Config */
        arraySessionConfigTAUL[ifIndex].pdConfig.pfCbFunction = &tau_ldRecvPdDs;


#ifndef TSN_PTP_SERVICE
        err = tlc_openSession(&arraySessionConfigTAUL[ifIndex].sessionHandle,   /* appHandle */
                              ownIpAddress,                                         /* own IP   */
                              pIfConfig[ifIndex].leaderIp,
                              pMarshallConfigPtr,                               /* Marshalling or no Marshalling*/
                              &arraySessionConfigTAUL[ifIndex].pdConfig,            /* PD Config */
                              &arraySessionConfigTAUL[ifIndex].mdConfig,            /* MD Config */
                              &arraySessionConfigTAUL[ifIndex].processConfig);      /* Process Config */
#else
        err = tlc_openSession(&arraySessionConfigTAUL[ifIndex].sessionHandle,   /* appHandle */
                              ownIpAddress,                                         /* own IP   */
                              pIfConfig[ifIndex].leaderIp,
                              pMarshallConfigPtr,                               /* Marshalling or no Marshalling*/
                              &arraySessionConfigTAUL[ifIndex].pdConfig,            /* PD Config */
                              &arraySessionConfigTAUL[ifIndex].mdConfig,            /* MD Config */
							  ptpClockId[ifIndex],									/* ptp时钟id */
                              &arraySessionConfigTAUL[ifIndex].processConfig);      /* Process Config */
#endif

        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tlc_openSession() error: %d interface: %s\n",
                         err, pIfConfig[ifIndex].ifName);
            return err;
        }

    }
    /*- 创建共享内存和锁 */
    err = tau_ladder_init();
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. TRDP Ladder Support Initialize failed\n");
        return err;
    }

    for (ifIndex = 0; ifIndex < numIfConfig; ifIndex++)
    {
        err = configureTelegrams(ifIndex, numExchgPar[ifIndex], arrayExchgPar[ifIndex]);
        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. configureTelegrams() error.\n");
            return err;
        }
    }

#ifdef TSN_STAMP_SERVICE

    err=initTsnStamp();
    if (err != TRDP_NO_ERR)
    {
    	printf("tau_ldInit() init tsn stamp failed,error code:%d\n",err);
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() init tsn stamp failed,error code:%d\n",err);
    }
    else
    {
    	printf("join tsn stamp service for all telegrams success!\n");
    }

#endif


    /*sdt init*/
    SDT_RC sdtrc=SdtServiceInit();
    if(SDT_OK!=sdtrc)
    {
    	printf("sdt init failed,error code:%d\n",sdtrc);
    }
    else
    {
    	printf("sdt config success\n");
    }

    /* 为sdt修改通道句柄中的数据长度 */
    SUBSCRIBE_TELEGRAM_T					*SubIter = NULL;
    PUBLISH_TELEGRAM_T						*PubIter = NULL;
    UINT32									i=0;

    printf("adjust dataset size for sdt:\n");
    for(i=0;i<MAX_SDT_HANDLE_NUMBER && sdsinkConfig[i].comId!=0;i++)
    {
    	for(ifIndex=0;ifIndex<numIfConfig;ifIndex++)
    	{
        	for (SubIter = pHeadSubscribeTelegram[ifIndex];SubIter != NULL;SubIter = SubIter->pNextSubscribeTelegram)
        	{
        		if(SubIter->comId==sdsinkConfig[i].comId)
        		{
        			SubIter->dataset.size-=16u;
        			printf("sdsink handle:%u	comid:%u	adj interface:%u	new data size:%u\n",i,sdsinkConfig[i].comId,ifIndex,SubIter->dataset.size);
        		}
        	}
    	}
    }

    for(i=0;i<MAX_SDT_HANDLE_NUMBER && sdsrcConfig[i].comId!=0;i++)
    {
    	for(ifIndex=0;ifIndex<numIfConfig;ifIndex++)
    	{
        	for (PubIter = pHeadPublishTelegram[ifIndex];PubIter != NULL;PubIter = PubIter->pNextPublishTelegram)
        	{
        		if(PubIter->comId==sdsrcConfig[i].comId)
        		{
        			PubIter->dataset.size-=16u;
        			printf("sdsrc handle:%u	comid:%u	adj interface:%u	new data size:%u\n",i,sdsrcConfig[i].comId,ifIndex,PubIter->dataset.size);
        		}
        	}
    	}
    }

    printf("adjust dataset size for sdt finished\n");

    /* Set Application Handle : Subnet1 */
    appHandle1 = arraySessionConfigTAUL[IF_INDEX_SUBNET1].sessionHandle;
    arraySessionConfigTAUL[IF_INDEX_SUBNET1].sessionHandle->ifIndex=subnetId1Ifindex;

    /* Set Application Handle : Subnet2 */
    if (numIfConfig >= LADDER_IF_NUMBER)
    {
        appHandle2 = arraySessionConfigTAUL[IF_INDEX_SUBNET2].sessionHandle;
        arraySessionConfigTAUL[IF_INDEX_SUBNET2].sessionHandle->ifIndex=subnetId2Ifindex;
    }
    else
    {
        appHandle2 = (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE;
    }

    vos_threadDelay(3000);

    /* Create TAUL PD Main Thread */
    err = tau_pd_main_proc_init();
    if (err != TRDP_NO_ERR)
    {
    	printf("tau_ldInit() failed. tau_pd_main_proc_init() error.\n");
        vos_printLog(VOS_LOG_ERROR, "tau_ldInit() failed. tau_pd_main_proc_init() error.\n");
        return err;
    }
   
    return TRDP_NO_ERR;
}



/******************************************************************************/
/** Finalize TAUL
 *  Terminate PDComLadder and Clean up.
 *  Delete Traffic Store mutex, Traffic Store.
 *
 *  Note:
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_MEM_ERR
 */
TRDP_ERR_T tau_ldTerminate (
    void)
{
    TRDP_ERR_T  err = TRDP_NO_ERR;
    VOS_ERR_T   vosErr          = VOS_NO_ERR;
    extern VOS_THREAD_T     taulPdMainThreadHandle;             /* Thread handle */
    PUBLISH_TELEGRAM_T      *iterPublishTelegram    = NULL;
    SUBSCRIBE_TELEGRAM_T    *iterSubscribeTelegram  = NULL;
    UINT32 i;
    int sessionIndex=0;

    /* TAUL MAIN Thread Terminate */
    vosErr = vos_threadTerminate(taulPdMainThreadHandle);
    if (vosErr != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "TRDP TAULpdMainThread Terminate failed. VOS Error: %d\n", vosErr);
        err = TRDP_THREAD_ERR;
    }

    /* Waiting ran out for TAUL */
    for (;; )
    {
        vosErr = vos_threadIsActive(taulPdMainThreadHandle);
        if (vosErr != VOS_NO_ERR)
        {
            break;
        }

        vos_threadDelay(1000);
    }


    /*  Free allocated memory - parsed telegram configuration */
    for (i = 0; i < LADDER_IF_NUMBER; i++)
    {
        tau_freeTelegrams(numExchgPar[i], arrayExchgPar[i]);
    }
    /* Free allocated memory */
    /* Free Communication Parameter */
    if (pComPar)
    {
        pComPar     = NULL;
        numComPar   = 0;
    }
    /* Free I/F Config */
    if (pIfConfig)
    {
        vos_memFree(pIfConfig);
        pIfConfig   = NULL;
        numIfConfig = 0;
    }
    /* Free ComId-DatasetId Map */
    if (pComIdDsIdMap)
    {
        pComIdDsIdMap   = NULL;
        numComId        = 0;
    }
    /* Free Dataset */
    if (apDataset)
    {
        /* Free dataset structures */
        pTRDP_DATASET_T pDataset;
        UINT32          i;
        for (i = numDataset - 1; i > 0; i--)
        {
            pDataset = apDataset[i];
            vos_memFree(pDataset);
        }
        /*  Free array of pointers to dataset structures    */
        vos_memFree(apDataset);
        apDataset   = NULL;
        numDataset  = 0;
    }
    /*  Free parsed xml document    */
    tau_freeXmlDoc(&xmlConfigHandle);

    /* UnPublish Loop */
    for(sessionIndex=0;sessionIndex<MAX_SESSIONS;sessionIndex++)
    {
        do
        {
            /* First Publish Telegram ? */
            if (iterPublishTelegram == NULL)
            {
                /* Is there Publish Telegram ? */
                if (pHeadPublishTelegram[sessionIndex] == NULL)
                {
                    break;
                }
                else
                {
                    /* Set First Publish Telegram  */
                    iterPublishTelegram = pHeadPublishTelegram[sessionIndex];
                }
            }
            else
            {
                /* Set Next Publish Telegram */
                iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram;
            }
            /* Check Publish comId Valid */
            if (iterPublishTelegram->comId > 0)
            {
                /* unPublish */
                err = tlp_unpublish(iterPublishTelegram->appHandle, iterPublishTelegram->pubHandle);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "tau_ldterminate() failed. tlp_unpublish() error = %d\n", err);
                }
                else
                {
                    /* Display TimeStamp when unPublish time */
                    vos_printLog(VOS_LOG_DBG, "%s ComId:%d Destination IP Address:%d unPublish.\n",
                                 vos_getTimeStamp(), iterPublishTelegram->pubHandle->addr.comId,
                                 iterPublishTelegram->pubHandle->addr.destIpAddr);
                }
            }
            /* Free Publish Dataset */
            vos_memFree(iterPublishTelegram->dataset.pDatasetStartAddr);
            iterPublishTelegram->dataset.pDatasetStartAddr = NULL;
            iterPublishTelegram->dataset.size = 0;
            /* Free Publish Telegram */
            vos_memFree(iterPublishTelegram);
        }
        while (iterPublishTelegram->pNextPublishTelegram != NULL);
        /* Display TimeStamp when close Session time */
        vos_printLog(VOS_LOG_INFO, "%s All unPublish.\n", vos_getTimeStamp());

        /* UnSubscribe Loop */
        do
        {
            /* First Subscribe Telegram ? */
            if (iterSubscribeTelegram == NULL)
            {
                /* Is there Subscribe Telegram ? */
                if (pHeadSubscribeTelegram == NULL)
                {
                    break;
                }
                else
                {
                    /* Set First Subscribe Telegram  */
                    iterSubscribeTelegram = pHeadSubscribeTelegram;
                }
            }
            else
            {
                /* Set Next Subscribe Telegram */
                iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram;
            }
            /* Check Susbscribe comId Valid */
            if (iterSubscribeTelegram->comId > 0)
            {
                /* unSubscribe */
                err = tlp_unsubscribe(iterSubscribeTelegram->appHandle, iterSubscribeTelegram->subHandle);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "tau_ldterminate() failed. tlp_unsubscribe() error = %d\n", err);
                }
                else
                {
                    /* Display TimeStamp when unSubscribe time */
                    vos_printLog(VOS_LOG_DBG, "%s ComId:%d Destination IP Address:%d unSubscribe.\n",
                                 vos_getTimeStamp(), iterSubscribeTelegram->subHandle->addr.comId,
                                 iterSubscribeTelegram->subHandle->addr.destIpAddr);
                }
            }
            /* Free Subscribe Dataset */
            vos_memFree(iterSubscribeTelegram->dataset.pDatasetStartAddr);
            iterSubscribeTelegram->dataset.pDatasetStartAddr = NULL;
            iterSubscribeTelegram->dataset.size = 0;
            /* Free Subscribe Telegram */
            vos_memFree(iterSubscribeTelegram);
        }
        while (iterSubscribeTelegram->pNextSubscribeTelegram != NULL);
        /* Display TimeStamp when close Session time */
        vos_printLog(VOS_LOG_INFO, "%s All unSubscribe.\n", vos_getTimeStamp());
    }

    /* Ladder Terminate */
    err = tau_ladder_terminate();
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldTerminate failed. tau_ladder_terminate() error = %d\n", err);
    }
    else
    {
        /* Display TimeStamp when tau_ladder_terminate time */
        vos_printLog(VOS_LOG_INFO, "%s TRDP Ladder Terminate.\n", vos_getTimeStamp());
    }


    /* Close linkUpDown check socket */
    tau_closeCheckLinkUpDown();

    /* Force close sockets to appHandle */
    forceSocketClose(appHandle1);
    forceSocketClose(appHandle2);

    /*  Close appHandle session(Subnet1) */
    if (appHandle1 != NULL)
    {
        err = tlc_closeSession(appHandle1);
        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Subnet1 tlc_closeSession() error = %d\n", err);

        }
        else
        {
            /* Display TimeStamp when close Session time */
            vos_printLog(VOS_LOG_INFO, "%s Subnet1 Close Session.\n", vos_getTimeStamp());
        }
    }

    /*  Close appHandle2 session(Subnet2) */
    if (appHandle2 != (TRDP_APP_SESSION_T) LADDER_TOPOLOGY_DISABLE)
    {
        err = tlc_closeSession(appHandle2);
        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "Subnet2 tlc_closeSession() error = %d\n", err);
        }
        else
        {
            /* Display TimeStamp when close Session time */
            vos_printLog(VOS_LOG_INFO, "%s Subnet2 Close Session.\n", vos_getTimeStamp());
        }
        appHandle2 = NULL;
    }

    /* Delete mutexes */
    if (pPublishTelegramMutex)
    {
        vos_mutexDelete(pPublishTelegramMutex);
    }
    if (pSubscribeTelegramMutex)
    {
        vos_mutexDelete(pSubscribeTelegramMutex);
    }

    /* TRDP Terminate */
    if (appHandle1 != NULL)
    {
        err = tlc_terminate();
        if (err != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "tlc_terminate() error = %d\n", err);
        }
        else
        {
            /* Display TimeStamp when tlc_terminate time */
            vos_printLog(VOS_LOG_INFO, "%s TRDP Terminate.\n", vos_getTimeStamp());
        }
        appHandle1 = NULL;
    }
    
    /*free memory for sdsrc output list*/
    vos_memFree(psdsrcOutputList);

    return err;
}

/**********************************************************************************************************************/
/** TAUL API for PD
 */
/**********************************************************************************************************************/
/** Set the network Context.
 *
 *  @param[in]      SubnetId            Sub-network Id: SUBNET1 or SUBNET2 or SUBNET_AUTO
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_UNKNOWN_ERR    tau_setNetworkContext() error
 *  @retval         TRDP_IO_ERR         link down error
 */
TRDP_ERR_T  tau_ldSetNetworkContext (
    UINT32 subnetId)
{
    TRDP_ERR_T  err         = TRDP_NO_ERR;
    BOOL8       linkUpDown  = TRUE;                 /* Link Up Down information TRUE:Up FALSE:Down */

    /* Check SubnetId Type */
    switch (subnetId)
    {
        case SUBNET_AUTO:
            /* Subnet1 Link Up ? */
            tau_checkLinkUpDown(SUBNET1, &linkUpDown);
            /* Subnet1 is Link Up */
            if (linkUpDown == TRUE)
            {
                /* Set network context: Subnet1 */
                err = tau_setNetworkContext(SUBNET1);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "tau_ldSetNetworkContext() failed. tau_setNetworkContext() error\n");
                    err = TRDP_UNKNOWN_ERR;
                }
                else
                {
                    vos_printLog(VOS_LOG_DBG, "tau_ldSetNetworkContext() set subnet%d\n", SUBNET_NO_1 + 1);
                }
            }
            else
            {
                /* Subnet2 Link Up ? */
                tau_checkLinkUpDown(SUBNET2, &linkUpDown);
                /* Subnet2 is Link Up */
                if (linkUpDown == TRUE)
                {
                    /* Set network context: Subnet2 */
                    err = tau_setNetworkContext(SUBNET2);
                    if (err != TRDP_NO_ERR)
                    {
                        vos_printLog(VOS_LOG_ERROR, "tau_ldSetNetworkContext() failed. tau_setNetworkContext() error\n");
                        err = TRDP_UNKNOWN_ERR;
                    }
                    else
                    {
                        vos_printLog(VOS_LOG_DBG, "tau_ldSetNetworkContext() set subnet%d\n", SUBNET_NO_2 + 1);
                    }
                }
                else
                {
                    err = tau_setNetworkContext(SUBNET1);
                    vos_printLog(VOS_LOG_DBG, "tau_ldSetNetworkContext() set subnet%d\n", SUBNET_NO_1 + 1);
                }
            }
            break;
        case SUBNET1:
            /* Set network context: Subnet1 */
            err = tau_setNetworkContext(SUBNET1);
            if (err != TRDP_NO_ERR)
            {
                vos_printLog(VOS_LOG_ERROR, "tau_ldSetNetworkContext() failed. tau_setNetworkContext() error\n");
                err = TRDP_UNKNOWN_ERR;
            }
            else
            {
                vos_printLog(VOS_LOG_DBG, "tau_ldSetNetworkContext() set subnet%d\n", SUBNET_NO_1 + 1 );
            }
            break;
        case SUBNET2:
            /* Set network context: Subnet2 */
            err = tau_setNetworkContext(SUBNET2);
            if (err != TRDP_NO_ERR)
            {
                vos_printLog(VOS_LOG_ERROR, "tau_ldSetNetworkContext() failed. tau_setNetworkContext() error\n");
                err = TRDP_UNKNOWN_ERR;
            }
            else
            {
                vos_printLog(VOS_LOG_DBG, "tau_ldSetNetworkContext() set subnet%d\n", SUBNET_NO_2 + 1);
            }
            break;
        default:
            vos_printLog(VOS_LOG_ERROR, "tau_ldSetNetworkContext() failed. SubnetId error\n");
            err = TRDP_PARAM_ERR;
            break;
    }
    return err;
}

/**********************************************************************************************************************/
/** Get the sub-Network Id for the current network Context.
 *
 *  @param[in,out]  pSubnetId           pointer to Sub-network Id
 *                                          Sub-network Id: SUBNET1 or SUBNET2
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_UNKNOWN_ERR    tau_getNetworkContext() error
 */
TRDP_ERR_T  tau_ldGetNetworkContext (
    UINT32 *pSubnetId)
{
    TRDP_ERR_T err = TRDP_NO_ERR;

    /* Check Parameter */
    if (pSubnetId == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldGetNetworkContext() failed. pSubnetId error\n");
        err = TRDP_PARAM_ERR;
    }
    else
    {
        /* Get Network Context */
        if (tau_getNetworkContext(pSubnetId) != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "tau_ldGetNetworkContext() failed. tau_getNetworkContext() error\n");
            err = TRDP_UNKNOWN_ERR;
        }
    }
    return err;
}

/**********************************************************************************************************************/
/** Get Traffic Store accessibility.
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_NOPUB_ERR      not published
 *  @retval         TRDP_NOINIT_ERR handle invalid
 */
TRDP_ERR_T  tau_ldLockTrafficStore (
    void)
{
    TRDP_ERR_T err = TRDP_NO_ERR;

    err = tau_lockTrafficStore();
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldLockTrafficeStore() failed\n");
        return err;
    }
    return err;
}

/**********************************************************************************************************************/
/** Release Traffic Store accessibility.
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_NOPUB_ERR      not published
 *  @retval         TRDP_NOINIT_ERR handle invalid
 */
TRDP_ERR_T  tau_ldUnlockTrafficStore (
    void)
{
    TRDP_ERR_T err = TRDP_NO_ERR;

    err = tau_unlockTrafficStore();
    if (err != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_ldUnlockTrafficeStore() failed\n");
        return err;
    }
    return err;
}

/**********************************************************************************************************************/
/**
 *	@brief			为订阅通道处理写共享内存操作
 *	@details		包括为正常接收处理写共享内存和为超时通道执行超时策略（若为zero则清空共享内存）
 *  @param[in]      pRefCon         user supplied context pointer
 *  @param[in]      argAppHandle           application handle returned by tlc_opneSession
 *  @param[in]      pPDInfo         pointer to PDInformation
 *  @param[in]      pData               pointer to receive PD Data
 *  @param[in]      dataSize        receive PD Data Size
 *
 */
void tau_ldRecvPdDs (
    void                    *pRefCon,
    TRDP_APP_SESSION_T      argAppHandle,
    const TRDP_PD_INFO_T    *pPDInfo,
    UINT8                   *pData,
    UINT32                  dataSize)
{
    UINT32          subnetId;                   /* Using Sub-network Id */
    UINT32          displaySubnetId;       /* Using Sub-network Id for Display log */
    UINT32          offset;                               /* Traffic Store Offset Address */
    extern UINT8    *pTrafficStoreAddr;         /* pointer to pointer to Traffic Store Address */

    SUBSCRIBE_TELEGRAM_T *pSubscribeTelegram;

    /* 0.必要参数检查 */

    /*- 非超时处理的空调用 */
    if (((NULL == pData) || (0 == dataSize))
        && ((pPDInfo->resultCode != TRDP_TIMEOUT_ERR_SIG) && (pPDInfo->resultCode != TRDP_SINGLE_LINKDOWN_SIG)))
    {
    	/*- 退出 */
        vos_printLog(VOS_LOG_DBG, "There is no data which save at Traffic Store\n");
        return;
    }

    /*- 获取通道句柄结构体指针失败 */
    if ((pSubscribeTelegram = (SUBSCRIBE_TELEGRAM_T *)pPDInfo->pUserRef) == NULL)
    {
    	return;
    }

    /* 1.通知服务层的判断 */

    /*- 本次调用是通道超时后的首次重连或通道超时 */
    if (TRDP_RECONNECT_SIG==pPDInfo->resultCode || TRDP_TIMEOUT_ERR_SIG==pPDInfo->resultCode || (TRDP_SINGLE_LINKDOWN_SIG==pPDInfo->resultCode))
    {
    	/* 通知服务层执行相应策略 */
		if(NULL!=pTaulUserCbfuc)
    	{
    		pTaulUserCbfuc(pRefCon,argAppHandle,pPDInfo,pData,dataSize);
    	}
    	
#ifdef LADDER_COMPARE
        if(TRDP_SINGLE_LINKDOWN_SIG==pPDInfo->resultCode)
        {
        	/*- 双网取新时，单网超时只需通知服务层，不再往后处理共享内存的用户数据部分了 */
        	return;
        }
#endif
    }

    /* 2.更新共享内存用户数据的判断 */

#ifndef LADDER_COMPARE

    /*- 获取当前使用子网 */
    tau_getNetworkContext(&subnetId);

    /*- 当前使用子网1且本次调用来自appHandle1 */
    if ((subnetId == SUBNET1) && (argAppHandle == appHandle1) /*&& ((pPDInfo->srcIpAddr & SUBNET2_NETMASK) == subnetId)*/)
    {
        /*- 继续后续处理 */
        ;
    }
    /*- 当前使用子网2且本次调用来自appHandle2 */
    else if ((subnetId == SUBNET2) && (argAppHandle == appHandle2)/* && ((pPDInfo->srcIpAddr & SUBNET2_NETMASK) == subnetId)*/)
    {
    	/*- 继续后续处理 */
        ;
    }
    /* 本次调用来自不与当前使用子网对应的apphandle */
    else
    {
    	/*- 不处理用户数据 */
        return;
    }
#else
    /*- 获取当前使用子网 */
	if(argAppHandle==appHandle1)
	{
		subnetId=SUBNET1;
	}
	else if(argAppHandle==appHandle2)
	{
		subnetId=SUBNET2;
	}

    //如果在处理udp包阶段就进行了双网口数据比较取更新的操作，这里就不能再限定只允许当前主subnetId对应的apphandle写共享内存，否则会造成丢包

#endif

    /* 3.处理用户数据 */

    /* 超时处理 */
    if (pPDInfo->resultCode == TRDP_TIMEOUT_ERR_SIG)
    {
    	/*- 获取相应的sdt通道句柄 */
    	SDT_HANDLE sdsinkHandle=0;
    	UINT32 sinktelegramConfigSdtOrNot=0;
    	for(int i=0;i<MAX_SDT_HANDLE_NUMBER;i++)
    	{
    		if (sdsinkConfig[i].comId==pPDInfo->comId)
    		{
    			sdsinkHandle=sdsinkConfig[i].sdtHandle;
    			sinktelegramConfigSdtOrNot=1;
    			//printf("sdtHandle%d for comid %d \n",sdsinkHandle,pPDInfo->comId);
    			break;
    		}
    	}

    	/*- 通道未配置sdt */
    	if (0==sinktelegramConfigSdtOrNot)
    	{
    		/*- 无需操作 */
    	}
    	/*- 通道配置了sdt */
    	else
    	{
        	/*- 解析vdp */
            UINT8   vpdBuffer[SDT_VPDMAX] = {0};
            UINT32	vpdLength = SDT_VPDMAX;

            SDT_RC sdsink_rc=SDT_OK;
            sdsink_rc=sdsink_parse_VDP(sdsinkHandle, pData,dataSize,vpdBuffer,&vpdLength);

            /*- sdt校验失败 */
            if(SDT_OK!=sdsink_rc && SDT_INVALID_VPD!=sdsink_rc)
            {
            	vos_printLog(VOS_LOG_ERROR,"sdsink parse error: %d \n",sdsink_rc);
            }
    	}

        /*- 当前通道配置了超时归零策略 */
        if (pSubscribeTelegram->pPdParameter->toBehav == TRDP_TO_SET_TO_ZERO)
        {
            /*- 清空相应共享内存 */
            offset = pSubscribeTelegram->pPdParameter->offset;
            tau_ldLockTrafficStore();
            memset((void *)(pTrafficStoreAddr + offset), 0, pSubscribeTelegram->dataset.size);
            tau_ldUnlockTrafficStore();

        	/*- 打印调试信息 */
        	displaySubnetId = ((subnetId == SUBNET1) ? SUBNET1_ID : SUBNET2_ID);

#ifdef LADDER_COMPARE
            vos_printLog(VOS_LOG_DBG,
                         "comId:%d Timeout,lastly used SubnetId:%d. Traffic Store Clear.\n",
                         displaySubnetId,
                         pPDInfo->comId);
#else
            vos_printLog(VOS_LOG_DBG,
                         "SubnetId:%d comId:%d Timeout. Traffic Store Clear.\n",
                         displaySubnetId,
                         pPDInfo->comId);
#endif
        }
        else
        {
        	/*- 当前通道未配置超时归零策略则不在日志记录超时 */
        }
    }
    /* 非超时处理 */
    else
    {
        /* Get offset Address */
        offset = pSubscribeTelegram->pPdParameter->offset;

        /*- 通道禁用编组 */
        if ((pSubscribeTelegram->pPdParameter->flags & TRDP_FLAGS_MARSHALL) != TRDP_FLAGS_MARSHALL)
        {	  
        	/*- 获取相应的sdt通道句柄 */
        	SDT_HANDLE sdsinkHandle=0;
        	UINT32 sinktelegramConfigSdtOrNot=0;

        	for(int i=0;i<MAX_SDT_HANDLE_NUMBER;i++)
        	{
        		if (sdsinkConfig[i].comId==pPDInfo->comId)
        		{
        			sdsinkHandle=sdsinkConfig[i].sdtHandle;
        			sinktelegramConfigSdtOrNot=1;
        			//printf("sdtHandle%d for comid %d \n",sdsinkHandle,pPDInfo->comId);
        			break;
        		}
        	}

        	/*- 通道未配置sdt */
        	if (0==sinktelegramConfigSdtOrNot)
        	{
        		/*- 用户数据直接拷贝到共享内存 */
                vos_printLog(VOS_LOG_INFO,"no sdt config for comid %d \n",pPDInfo->comId);

                tau_ldLockTrafficStore();
                memcpy((void *)(pTrafficStoreAddr + offset), pData, dataSize);
                tau_ldUnlockTrafficStore();
        	}	
        	/*- 通道配置了sdt */
        	else
        	{
            	/*- 解析vdp */
                UINT8   vpdBuffer[SDT_VPDMAX] = {0}; 
                UINT32	vpdLength = SDT_VPDMAX;
            	
                SDT_RC sdsink_rc=SDT_OK;
                sdsink_rc=sdsink_parse_VDP(sdsinkHandle, pData,dataSize,vpdBuffer,&vpdLength);
                
                /*- sdt校验通过 */
                if(SDT_OK==sdsink_rc)
                {
                	/*- 用户数据拷贝到共享内存 */
                	
                    tau_ldLockTrafficStore();
                    memcpy((void *)(pTrafficStoreAddr + offset), vpdBuffer, vpdLength);
                    tau_ldUnlockTrafficStore();
                }
                /*- sdt校验不通过 */
                else if(SDT_INVALID_VPD==sdsink_rc)
                {   
                	/*- 相应共享内存区域置全零 */
                	//printf("sdsink invalied vdp rc:%d\n",sdsink_rc);
                    tau_ldLockTrafficStore();
                    //memcpy((void *)(pTrafficStoreAddr + offset), pData, dataSize);
                    memset((void *)(pTrafficStoreAddr + offset), 0, dataSize-SDT_TAIL_SIZE);
                    tau_ldUnlockTrafficStore();
                }
                /*- sdt校验失败 */
                else
                {
                	/*- 相应共享内存区域置全零 */
                	vos_printLog(VOS_LOG_ERROR,"sdsink parse error: %d \n",sdsink_rc);
                    tau_ldLockTrafficStore();
                    //memcpy((void *)(pTrafficStoreAddr + offset), pData, dataSize);
                    memset((void *)(pTrafficStoreAddr + offset), 0, dataSize-SDT_TAIL_SIZE);
                    tau_ldUnlockTrafficStore();
                }
        	}
        }
        /*- 通道允许编组 */
        else
        {
        	//??添加这个功能
        }
    }
}

/**********************************************************************************************************************/
/** All UnPublish
 *
 *  @retval         TRDP_NO_ERR
 *
 */
TRDP_ERR_T tau_ldAllUnPublish (
    void)
{
    TRDP_ERR_T  err = TRDP_NO_ERR;
    extern VOS_THREAD_T taulPdMainThreadHandle;                 /* Thread handle */
    PUBLISH_TELEGRAM_T  *iterPublishTelegram = NULL;
    int sessionIndex=0;

    /* UnPublish Loop */
	for(sessionIndex=0;sessionIndex<MAX_SESSIONS;sessionIndex++)
	{
	    do
	    {
	        /* First Publish Telegram ? */
	        if (iterPublishTelegram == NULL)
	        {
	            /* Is there Publish Telegram ? */
	            if (pHeadPublishTelegram[sessionIndex] == NULL)
	            {
	                break;
	            }
	            else
	            {
	                /* Set First Publish Telegram  */
	                iterPublishTelegram = pHeadPublishTelegram[sessionIndex];
	            }
	        }
	        else
	        {
	            /* Set Next Publish Telegram */
	            iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram;
	        }
	        /* Check Publish comId Valid */
	        if (iterPublishTelegram->comId > 0)
	        {
	            /* unPublish */
	            err = tlp_unpublish(iterPublishTelegram->appHandle, iterPublishTelegram->pubHandle);
	            if (err != TRDP_NO_ERR)
	            {
	                vos_printLog(VOS_LOG_ERROR, "tau_ldterminate() failed. tlp_unpublish() error = %d\n", err);
	            }
	            else
	            {
	                /* Display TimeStamp when unPublish time */
	                vos_printLog(VOS_LOG_DBG, "%s ComId:%d Destination IP Address:%d unPublish.\n",
	                             vos_getTimeStamp(), iterPublishTelegram->pubHandle->addr.comId,
	                             iterPublishTelegram->pubHandle->addr.destIpAddr);
	            }
	        }
	        /* Free Publish Dataset */
	        vos_memFree(iterPublishTelegram->dataset.pDatasetStartAddr);
	        iterPublishTelegram->dataset.pDatasetStartAddr = NULL;
	        iterPublishTelegram->dataset.size = 0;
	        /* Free Publish Telegram */
	        vos_memFree(iterPublishTelegram);
	    }
	    while (iterPublishTelegram->pNextPublishTelegram != NULL);

	}

    /* Display TimeStamp when close Session time */
    vos_printLog(VOS_LOG_INFO, "%s All unPublish.\n", vos_getTimeStamp());

    return err;
}



/**********************************************************************************************************************/
/** Force socket close.
 *
 *  @param[in]      appHandle          the handle returned by tlc_openSession
 *
 */
static void forceSocketClose (TRDP_APP_SESSION_T appHandle)
{
    TRDP_ERR_T  err;
    INT32       i;

    for (i = 0; i < VOS_MAX_SOCKET_CNT; i++)
    {
        if (appHandle->ifacePD[i].sock > VOS_INVALID_SOCKET)
        {
            err = (TRDP_ERR_T)vos_sockClose(appHandle->ifacePD[i].sock);
            if (err != TRDP_NO_ERR)
            {
                vos_printLog(VOS_LOG_DBG, "Failure closed socket %d\n", appHandle->ifacePD[i].sock);
            }
            else
            {
                vos_printLog(VOS_LOG_DBG, "Closed socket %d\n", appHandle->ifacePD[i].sock);
            }
            appHandle->ifacePD[i].sock = VOS_INVALID_SOCKET;
        }
    }
}


UINT32 tau_GetDataLenByComid(UINT32 comId)
{
	PUBLISH_TELEGRAM_T*		pPubTelegram=NULL;
	SUBSCRIBE_TELEGRAM_T*	pSubTelegram=NULL;
	UINT32					ifIndex=0;

	UINT32					pubDataLen=0;
	UINT32					subDataLen=0;

	UINT32					dataLen=0;
	for(ifIndex=0;ifIndex<numIfConfig;ifIndex++)
	{
		for(pPubTelegram=pHeadPublishTelegram[ifIndex];NULL!=pPubTelegram;pPubTelegram=pPubTelegram->pNextPublishTelegram)
		{
			if(comId == pPubTelegram->comId)
			{
				pubDataLen=pPubTelegram->dataset.size;
			}
			else
			{

			}
		}

		for(pSubTelegram=pHeadSubscribeTelegram[ifIndex];NULL!=pSubTelegram;pSubTelegram=pSubTelegram->pNextSubscribeTelegram)
		{
			if(comId == pSubTelegram->comId)
			{
				subDataLen=pSubTelegram->dataset.size;
			}
			else
			{

			}
		}

		if(0u==dataLen)
		{
			dataLen=pubDataLen+subDataLen;

			if(pubDataLen!=dataLen && subDataLen!=dataLen)
			{
				dataLen=0u;
			}
		}
		else
		{
			if(pubDataLen+subDataLen!=dataLen)
			{
				dataLen=0u;
			}
		}
	}

	return dataLen;
}

/**********************************************************************************************************************/
/** Read sdt config.
 *
 *  @param[in]      
 *
 */
SDT_RC SdtServiceInit()
{
	/****************************read sdt config*****************************************************/
	SDT_RC  sdtresult = SDT_OK;
	TRDP_XML_DOC_HANDLE_T	docHnd;
	TRDP_ERR_T rc_readxml=TRDP_NO_ERR;
	UINT32	sdsrc_count=0;
	UINT32	sdsink_count=0;
	
	printf("##################### SDT Version:%s(%s)##################### \r\n",SDT_MODULE_VER,SDT_MODULE_NOTE);
	
	rc_readxml = tau_prepareXmlDoc(xmlConfigFileName, &docHnd);
	if (rc_readxml != TRDP_NO_ERR)
	{
		printf("Failed to prepare XML document: %d\n", rc_readxml);
		return SDT_ERROR;
	}
	
	apTRDP_DATASET_T        apDataset = NULL;
	UINT32					NumDataset;
	readXmlDatasets(docHnd.pXmlDocument, &NumDataset, &apDataset);
	
	
	trdp_XMLRewind(docHnd.pXmlDocument);
	trdp_XMLEnter(docHnd.pXmlDocument);
	if (trdp_XMLSeekStartTag(docHnd.pXmlDocument, "device") == 0) /* Optional */
	{
		trdp_XMLEnter(docHnd.pXmlDocument);
		if (trdp_XMLSeekStartTag(docHnd.pXmlDocument, "bus-interface-list") == 0)
		{	
			trdp_XMLEnter(docHnd.pXmlDocument);
			if (trdp_XMLSeekStartTag(docHnd.pXmlDocument, "bus-interface") == 0)
			{

				trdp_XMLEnter(docHnd.pXmlDocument);
				
				TRDP_EXCHG_PAR_T    *pExchgPar = NULL;
				
				UINT32 tlg_count = 0;
				tlg_count = (UINT32) trdp_XMLCountStartTag(docHnd.pXmlDocument, "telegram");
                if (tlg_count > 0u)
                {
                    pExchgPar = (TRDP_EXCHG_PAR_T *)vos_memAlloc(tlg_count * sizeof(TRDP_EXCHG_PAR_T));
                    if (pExchgPar == NULL)
                    {
                        vos_printLog(VOS_LOG_ERROR,
                                     "%lu Bytes failed to allocate while reading XML telegram definitions!\n",
                                     (unsigned long) (tlg_count * sizeof(TRDP_EXCHG_PAR_T)));
                        return SDT_ERROR;
                    }
                    else
                    {
                    	memset(pExchgPar,0,tlg_count * sizeof(TRDP_EXCHG_PAR_T));
                    }
                }
                int tlg_id=0;
                while ((tlg_id < tlg_count) && (trdp_XMLSeekStartTag(docHnd.pXmlDocument, "telegram") == 0))
                {
                    trdp_XMLEnter(docHnd.pXmlDocument);
                    
                    rc_readxml = readTelegramDef(docHnd.pXmlDocument, &pExchgPar[tlg_id]);

                    trdp_XMLLeave(docHnd.pXmlDocument);
                    
                    if (rc_readxml != TRDP_NO_ERR)
                    {
                        return SDT_ERROR;
                    }
                    tlg_id++;
                }             
				trdp_XMLLeave(docHnd.pXmlDocument);

				UINT32 sdt_src_id=0;
				UINT32 sdt_sink_id=0;
				for (tlg_id=0;tlg_id<tlg_count;tlg_id++)
				{
					if (pExchgPar[tlg_id].destCnt)
					{
			            for (int i = 0; i < pExchgPar[tlg_id].destCnt; i++)
			            {
			                TRDP_DEST_T* pDest = &(pExchgPar[tlg_id].pDest[i]);

			                if (pDest->pSdtPar)
			                {
			                	sdsrcConfig[sdt_src_id].comId=pExchgPar[tlg_id].comId;
			                	sdsrcConfig[sdt_src_id].datasetId=pExchgPar[tlg_id].datasetId;
			                	sdsrcConfig[sdt_src_id].comParId=pExchgPar[tlg_id].comParId;
			                	sdsrcConfig[sdt_src_id].ipId=pDest->id;
			                	sdsrcConfig[sdt_src_id].offset=pExchgPar[tlg_id].pPdPar->offset;
			                	sdsrcConfig[sdt_src_id].smi1=pDest->pSdtPar->smi1;
			                	sdsrcConfig[sdt_src_id].smi2=pDest->pSdtPar->smi2;
			                	sdsrcConfig[sdt_src_id].udv=pDest->pSdtPar->udv;
			                	sdsrcConfig[sdt_src_id].rxPeriod=pDest->pSdtPar->rxPeriod;
			                	sdsrcConfig[sdt_src_id].txPeriod=pDest->pSdtPar->txPeriod;
			                	sdsrcConfig[sdt_src_id].nRxSafe=pDest->pSdtPar->nrxSafe;
			                	sdsrcConfig[sdt_src_id].nGuard=pDest->pSdtPar->nGuard;
			                	sdsrcConfig[sdt_src_id].K4=pDest->pSdtPar->cmThr;
			                	sdsrcConfig[sdt_src_id].channelIdentity=STA_SDSRC_CHANNEL_A;
			                	sdt_src_id++;
			                }  
			            }
					}
					if (pExchgPar[tlg_id].srcCnt)
					{
			            for (int i = 0; i < pExchgPar[tlg_id].srcCnt; i++)
			            {
			                TRDP_SRC_T* pSrc = &(pExchgPar[tlg_id].pSrc[i]);

			                if (pSrc->pSdtPar)
			                {
			                	sdsinkConfig[sdt_sink_id].comId=pExchgPar[tlg_id].comId;
			                	sdsinkConfig[sdt_sink_id].datasetId=pExchgPar[tlg_id].datasetId;
			                	sdsinkConfig[sdt_sink_id].comParId=pExchgPar[tlg_id].comParId;
			                	sdsinkConfig[sdt_sink_id].ipId=pSrc->id;
			                	sdsinkConfig[sdt_sink_id].offset=pExchgPar[tlg_id].pPdPar->offset;
			                	sdsinkConfig[sdt_sink_id].smi1=pSrc->pSdtPar->smi1;
			                	sdsinkConfig[sdt_sink_id].smi2=pSrc->pSdtPar->smi2;
			                	sdsinkConfig[sdt_sink_id].udv=pSrc->pSdtPar->udv;
			                	sdsinkConfig[sdt_sink_id].rxPeriod=pSrc->pSdtPar->rxPeriod;
			                	sdsinkConfig[sdt_sink_id].txPeriod=pSrc->pSdtPar->txPeriod;
			                	sdsinkConfig[sdt_sink_id].nRxSafe=pSrc->pSdtPar->nrxSafe;
			                	sdsinkConfig[sdt_sink_id].nGuard=pSrc->pSdtPar->nGuard;
			                	sdsinkConfig[sdt_sink_id].K4=pSrc->pSdtPar->cmThr;
			                	sdsinkConfig[sdt_sink_id].channelIdentity=STA_SDSINK_CHANNEL;
			                	sdt_sink_id++;
			                }  
			            }
					}
				}
				sdsrc_count=sdt_src_id;
				sdsink_count=sdt_sink_id;
			}
			trdp_XMLLeave(docHnd.pXmlDocument);	
		}
		trdp_XMLLeave(docHnd.pXmlDocument);
	}
	trdp_XMLLeave(docHnd.pXmlDocument);

	
	UINT32	handleIndex=0;
	for (handleIndex=0;handleIndex<sdsrc_count;handleIndex++)
	{
		UINT8 findnull=1;
		for(int i=0;i<NumDataset;i++)
		{
			if(apDataset[i]->id==sdsrcConfig[handleIndex].datasetId)
			{
				UINT32 sumSize=0;
				for(int j=0;j<apDataset[i]->numElement;j++)
				{
					sumSize=sumSize+apDataset[i]->pElement->size;
				}
				sdsrcConfig[handleIndex].datasetSize=sumSize;
				findnull=0;
				break;
			}
		}
		if(findnull)
			printf("no match datasite for id%d\n",sdsrcConfig[handleIndex].datasetId);	
	}
	
	for (handleIndex=0;handleIndex<sdsink_count;handleIndex++)
	{
		UINT8 findnull=1;
		for(int i=0;i<NumDataset;i++)
		{
			if(apDataset[i]->id==sdsinkConfig[handleIndex].datasetId)
			{
				UINT32 sumSize=0;
				for(int j=0;j<apDataset[i]->numElement;j++)
				{
					sumSize=sumSize+apDataset[i]->pElement->size;
				}
				sdsinkConfig[handleIndex].datasetSize=sumSize;
				findnull=0;
				break;
			}
		}
		if(findnull)
			printf("no match datasite for id%d\n",sdsinkConfig[handleIndex].datasetId);	
	}
	
	/************************************creat sdt channel**************************************************/
    for (handleIndex = 0; handleIndex < sdsink_count; ++handleIndex)
    {
    	sdtresult = sdsink_create_channel(&sdsinkConfig[handleIndex].sdtHandle, &sdsinkConfig[handleIndex]);
        if ( SDT_OK!= sdtresult)                          
        {
            printf("sdsink create channel failed!error code:%d\n",sdtresult);
            memset(&sdsinkConfig[handleIndex],0,sizeof(SDT_CONFIG_T));
        }
    }
    for(handleIndex = 0; handleIndex < sdsrc_count; ++handleIndex)
    {
    	sdtresult = sdsrc_create_channel(&sdsrcConfig[handleIndex].sdtHandle, &sdsrcConfig[handleIndex]);
        if (SDT_OK != sdtresult)
        {
        	printf("sdsrc create channel failed!error code:%d\n",sdtresult);
        	memset(&sdsrcConfig[handleIndex],0,sizeof(SDT_CONFIG_T));
        }
    }
    
	printf("sdsrc channel create:\n");
	for(int i=0;i<sdsrc_count;i++)
	{
		printf("comid:%d	sdsrcHandle:%d	smi1:%d	channelid:%d	udv:%d	rxPeriod:%d	txPeriod:%d	nRxSafe:%d	nGuard:%d	cmr(K4):%d\n",sdsrcConfig[i].comId,sdsrcConfig[i].sdtHandle,sdsrcConfig[i].smi1,sdsrcConfig[i].channelIdentity,sdsrcConfig[i].udv,sdsrcConfig[i].rxPeriod,sdsrcConfig[i].txPeriod,sdsrcConfig[i].nRxSafe,sdsrcConfig[i].nGuard,sdsrcConfig[i].K4);
	}
	printf("sdsink channel create:\n");
	for(int i=0;i<sdsink_count;i++)
	{
		printf("comid:%d	sdsinkHandle:%d	smi1:%d	channelid:%d	udv:%d	rxPeriod:%d	txPeriod:%d	nRxSafe:%d	nGuard:%d	cmr(K4):%d\n",
				sdsinkConfig[i].comId,sdsinkConfig[i].sdtHandle,sdsinkConfig[i].smi1,sdsinkConfig[i].channelIdentity,sdsinkConfig[i].udv,sdsinkConfig[i].rxPeriod,sdsinkConfig[i].txPeriod,sdsinkConfig[i].nRxSafe,sdsinkConfig[i].nGuard,sdsinkConfig[i].K4);
	}
	
	/*allocate memory for sdsrc output list if there is at least one sdt config*/
	if(0!=sdsrc_count)
	{
		psdsrcOutputList=(SDSRC_OUTPUT_T*)vos_memAlloc(sizeof(SDSRC_OUTPUT_T) * sdsrc_count);
	    if (psdsrcOutputList == NULL)
	    {
	        vos_printLog(VOS_LOG_ERROR,
	                     "SdtServicefInit() Failed. psdsrcOutputList vos_memAlloc() Err\n");
	        sdtresult=SDT_ERROR;
	    }
	}
	
	return sdtresult;
	
}

#ifdef TSN_COMMU_TIME_TEST
TRDP_ERR_T tau_getTsnCommuTimeTestStatics(const CHAR8* ifname,TRDP_TSN_STATISTICS_T *pTsnPdStatics)
{
	TRDP_ERR_T rt=TRDP_UNKNOWN_ERR;
	UINT8 ifIndex=0;

	if(NULL!=pTsnPdStatics && NULL!=ifname)
	{
		for(ifIndex=0;ifIndex<numIfConfig;ifIndex++)
		{
			if(0==vos_strnicmp(ifname,pIfConfig[ifIndex].ifName,sizeof(pIfConfig[ifIndex].ifName)))
			{
				rt=trdp_getTsnCommuTimeTestStatics(arraySessionConfigTAUL[ifIndex].sessionHandle,pTsnPdStatics);
				break;
			}
		}

		if(ifIndex >= numIfConfig)
		{
			rt=TRDP_PARAM_ERR;
	        vos_printLog(VOS_LOG_ERROR,"%s failed,ifname %s does not exist in interface config\n",__FUNCTION__,ifname);
		}
	}
	else
	{
		rt=TRDP_PARAM_ERR;
	}

	return rt;
}

TRDP_ERR_T tau_clearTsnCommuTimeTestStatics(const CHAR8* ifname)
{
	TRDP_ERR_T rt=TRDP_UNKNOWN_ERR;
	UINT8 ifIndex=0;

	if(NULL!=ifname)
	{
		for(ifIndex=0;ifIndex<numIfConfig;ifIndex++)
		{
			if(0==vos_strnicmp(ifname,pIfConfig->ifName,sizeof(pIfConfig->ifName)))
			{
				rt=trdp_clearTsnCommuTimeTestStatics(arraySessionConfigTAUL[ifIndex].sessionHandle);
				break;
			}
		}

		if(ifIndex >= numIfConfig)
		{
			rt=TRDP_PARAM_ERR;
	        vos_printLog(VOS_LOG_ERROR,"%s failed,ifname %s does not exist in interface config\n",__FUNCTION__,ifname);
		}
	}
	else
	{
		rt=TRDP_PARAM_ERR;
	}

	return rt;
}

#endif

#ifdef TSN_STAMP_SERVICE
/**
 * @brief	为所有发送通道的通信加入tsn服务
 * @note	需要在初始化完成后调用
 */
TRDP_ERR_T initTsnStamp(void)
{
	UINT8 ifIndex=0;
    PD_ELE_T* iter;
    INT32 socket=-1;
    TSNINFO tsnInfo={0};
    INT32 port=-1;
    INT32 priority=0;
    INT32 vlanId=0;
    PUBLISH_TELEGRAM_T* pPublishTelegram=NULL;
    TSN_STATUS_T err=RET_UNKNOWN_ERR;
    TRDP_ERR_T rt=TRDP_UNKNOWN_ERR;

    for (ifIndex = 0; ifIndex < numIfConfig; ifIndex++)
    {
        for(iter=arraySessionConfigTAUL[ifIndex].sessionHandle->pSndQueue;iter!=NULL;iter=iter->pNext)
        {
        	socket=arraySessionConfigTAUL[ifIndex].sessionHandle->ifacePD[iter->socketIdx].sock;
        	if(-1==socket)
        	{
        		rt=TRDP_INIT_ERR;
        		vos_printLog(VOS_LOG_ERROR,"get socket of session%d comid%d failed, socket index:%d\n",ifIndex,iter->addr.comId,iter->socketIdx);
        		break;
        	}

        	port=vos_getPort(socket);
        	if(-1==port)
        	{
        		vos_printLog(VOS_LOG_ERROR,"get port of socket %d failed,socket is in session %d comId %d\n",socket,ifIndex,iter->addr.comId);
        		rt=TRDP_SOCK_ERR;
        		break;
        	}

        	pPublishTelegram=(PUBLISH_TELEGRAM_T*)iter->pUserRef;
        	priority=(INT32)pPublishTelegram->pPdParameter->tsnPri;
        	vlanId=(INT32)pPublishTelegram->pPdParameter->vlanId;
        	if(0==vlanId)
        	{
        		printf("init tsn stamp for session %d comId %d no need!tsn-priority cfg:%d vlanId:%d\n",ifIndex,iter->addr.comId,pPublishTelegram->pPdParameter->tsnPri,vlanId);
        		rt=TRDP_NO_ERR;
        		continue;
        	}
        	else if((0>vlanId) || (4095<=vlanId))
        	{
        		printf("init tsn stamp for session %d comId %d param err!tsn-priority cfg:%d vlanId:%d\n",ifIndex,iter->addr.comId,pPublishTelegram->pPdParameter->tsnPri,vlanId);
        		rt=TRDP_PARAM_ERR;
        		break;
        	}

        	if(priority>=4)
        	{
        		priority=4;
        	}
        	else if(priority>=2)
        	{
        		priority=2;
        	}
        	else if(priority>=1)
        	{
        		priority=1;
        	}
        	else
        	{
        		priority=0;
        	}

        	tsnInfo.srcPort=port;
        	vos_strncpy(tsnInfo.ifname,pIfConfig[ifIndex].ifName,sizeof(tsnInfo.ifname));
        	tsnInfo.dstPort=arraySessionConfigTAUL[ifIndex].sessionHandle->pdDefault.port;
        	tsnInfo.dstIP=htonl(iter->addr.destIpAddr);
        	tsnInfo.vlanID=vlanId;
        	tsnInfo.pcp=priority;

        	err=TSN_Init(&tsnInfo);
        	if(RET_NO_ERR!=err)
        	{
        		printf("init tsn stamp for session %d comId %d failed,err code:%d,src port:%d,dest port:%d,vlan id:%d,pcp:%d\n" ,ifIndex,iter->addr.comId,(UINT32)err
        				,port,tsnInfo.dstPort,tsnInfo.vlanID,tsnInfo.pcp);
        		vos_printLog(VOS_LOG_ERROR,"init tsn stamp for session %d comId %d failed,err code:%d,src port:%d,dest port:%d,vlan id:%d,pcp:%d\n" ,ifIndex,iter->addr.comId,(UINT32)err
        				,port,tsnInfo.dstPort,tsnInfo.vlanID,tsnInfo.pcp);
        		rt=TRDP_SOCK_ERR;
        	}
        	else
        	{
        		printf("init tsn stamp for session %d comId %d sucess!src port:%d,dest port:%d,dest ip:%u,vlan id:%d,pcp:%d\n" ,ifIndex,iter->addr.comId,port,tsnInfo.dstPort,tsnInfo.dstIP,tsnInfo.vlanID,tsnInfo.pcp);
        		rt=TRDP_NO_ERR;
        	}

        }
    }

    return rt;
}
#endif

#endif /* TRDP_OPTION_LADDER */
