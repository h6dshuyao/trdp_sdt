/**********************************************************************************************************************/
/**
 * @file            tau_ldLadder_config.h
 *
 * @brief           Configuration for TRDP Ladder Topology Support
 *
 * @details
 *
 * @note            Project: TCNOpen TRDP prototype stack
 *
 * @author          Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This source code corresponds to TRDP_LADDER open source software.
 *          This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id: tau_ldLadder_config.h 1999 2019-08-15 13:06:06Z bloehr $
 *
 */

#ifdef TRDP_OPTION_LADDER

#ifndef TAU_LDLADDER_CONFIG_H_
#define TAU_LDLADDER_CONFIG_H_

/*******************************************************************************
 * INCLUDES
 */

#ifdef __cplusplus
extern "C" {
#endif

#include "tau_ldLadder_config_def.h"

//在pdcom中引用了本头文件之后，就必须增加以下三个引用才能编译通过，也就是extern的时候本来可以直接把tau_ldLadder_config.c里的变量类型也拿过来，但是现在却必须重新引用了
#include "tau_xml.h"
#include "tau_ldLadder.h"
#include "tau_ldLadder_config.h"

/******************************************************************************
 *   Globals
 */

/* TRDP Config *****************************************************/
#ifdef XML_CONFIG_ENABLE
/* XML Config File : Enable */
/* XML Config File Name */
extern CHAR8 xmlConfigFileName[FILE_NAME_MAX_SIZE];      /* XML Config File Name */
extern TRDP_XML_DOC_HANDLE_T    xmlConfigHandle;            /* XML Config Handle */
extern TAU_LD_CONFIG_T          *pTaulConfig;               /* TAUL Config */

/*  General parameters from xml configuration file */
extern TRDP_MEM_CONFIG_T        memoryConfigTAUL;
extern TRDP_SHAREMEM_CONFIG_T	sharememConfigTAUL; /* @global 共享内存配置结构体 */
extern TRDP_DBG_CONFIG_T        debugConfigTAUL;
extern UINT32 numComPar;
extern TRDP_COM_PAR_T           *pComPar;
extern UINT32 numIfConfig;
extern TRDP_IF_CONFIG_T         *pIfConfig;

/*  Dataset configuration from xml configuration file */
extern UINT32 numComId;
extern TRDP_COMID_DSID_MAP_T    *pComIdDsIdMap;
extern UINT32 numDataset;
extern apTRDP_DATASET_T         apDataset;


/*  Array of session configurations - one for each interface, only numIfConfig elements actually used  */
extern sSESSION_CONFIG_T        arraySessionConfigTAUL[MAX_SESSIONS];
/* Array of Exchange Parameter */
extern TRDP_EXCHG_PAR_T         *arrayExchgPar[LADDER_IF_NUMBER];
/*  Exchange Parameter from xml configuration file */
extern UINT32 numExchgPar[LADDER_IF_NUMBER];                                      /* Number of Exchange Parameter */
/* TRDP_EXCHG_PAR_T             *pExchgPar = NULL;			/ * Pointer to Exchange Parameter * / */

/* Application Handle */
extern TRDP_APP_SESSION_T   appHandle1;                      /*	Sub-network Id1 identifier to the library instance	*/
extern TRDP_APP_SESSION_T   appHandle2;                 /*	Sub-network Id2 identifier to the library instance	*/

#endif

#ifdef __cplusplus
}
#endif
#endif /* TAU_LDLADDER_CONFIG_H_ */
#endif  /* TRDP_OPTION_LADDER */
