/******************************************************************************/
/**
 * @file            tau_ladder.c
 *
 * @brief           使用共享内存实现双会话-子网（TRDP_SESSION）冗余的功能
 *
 * @details			提供共享内存创建接口、跨进程锁使用接口、和子网切换接口
 *
 * @note
 *
 * @author          Kazumasa Aiba, Toshiba Corporation
 *
 */

#ifndef TAU_LADDER_H_
#define TAU_LADDER_H_

#ifdef TRDP_OPTION_LADDER
/*******************************************************************************
 * INCLUDES
 */
#include "trdp_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 * DEFINES
 */


/* @note 使用tal_ladder.c提供的子网切换操作接口必须使用如下的子网标识符 */
#define SUBNET1             0x00000000
#define SUBNET2             0x00002000

#define NUM_ED_INTERFACES   10              /* number of End Device Interfaces */

/* ID仅用于报错时显示，ID与子网标识符一一对应 */
#define SUBNET1_ID      1                   /* SUBNETID Type1 */
#define SUBNET2_ID      2                   /* SUBNETID Type2 */

/***********************************************************************************************************************
 * GLOBAL VARIABLES
 */

/* Traffic Store */
extern UINT8        *pTrafficStoreAddr;     /* pointer to pointer to Traffic Store Address */

/***********************************************************************************************************************
 * API
 */
/******************************************************************************/
/** Initialize TRDP Ladder Support
 *  Create Traffic Store mutex, Traffic Store, PDComLadderThread.
 *
 *	Note:
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_MUTEX_ERR
 */
TRDP_ERR_T tau_ladder_init (
    void);

/******************************************************************************/
/** Finalize TRDP Ladder Support
 *  Delete Traffic Store mutex, Traffic Store.
 *
 *	Note:
 *
 *	@retval			TRDP_NO_ERR
 *	@retval			TRDP_MEM_ERR
 */
TRDP_ERR_T tau_ladder_terminate (
    void);


/**********************************************************************************************************************/
/** Set SubNetwork Context.
 *
 *  @param[in]      SubnetId			Sub-network Id: SUBNET1 or SUBNET2
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter error
 *  @retval         TRDP_NOPUB_ERR		not published
 *  @retval         TRDP_NOINIT_ERR	handle invalid
 */

TRDP_ERR_T tau_setNetworkContext (
    UINT32 subnetId);

/**********************************************************************************************************************/
/** Get SubNetwork Context.
 *
 *  @param[in,out]  pSubnetId			pointer to Sub-network Id
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter error
 *  @retval         TRDP_NOPUB_ERR		not published
 *  @retval         TRDP_NOINIT_ERR	handle invalid
 */

TRDP_ERR_T tau_getNetworkContext (
    UINT32 *pSubnetId);

/**********************************************************************************************************************/
/** Get Traffic Store accessibility.
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter error
 *  @retval         TRDP_NOPUB_ERR		not published
 *  @retval         TRDP_NOINIT_ERR	handle invalid
 */

TRDP_ERR_T tau_lockTrafficStore (
    void);

/**********************************************************************************************************************/
/** Release Traffic Store accessibility.
 *
 *  @retval         TRDP_NO_ERR			no error
 *  @retval         TRDP_PARAM_ERR		parameter error
 *  @retval         TRDP_NOPUB_ERR		not published
 *  @retval         TRDP_NOINIT_ERR	handle invalid
 */

TRDP_ERR_T tau_unlockTrafficStore (
    void);

/**********************************************************************************************************************/
/** Check Link up/down
 *
 *  @param[in]		checkSubnetId			check Sub-network Id
 *  @param[out]		pLinkUpDown          pointer to check Sub-network Id Link Up Down TRUE:Up, FALSE:Down
 *
 *  @retval         TRDP_NO_ERR				no error
 *  @retval         TRDP_PARAM_ERR			parameter err
 *  @retval         TRDP_SOCK_ERR			socket err
 *
 *
 */
TRDP_ERR_T tau_checkLinkUpDown (
    UINT32  checkSubnetId,
    BOOL8   *pLinkUpDown);

/**********************************************************************************************************************/
/** Close check Link up/down
 *
 *  @retval         TRDP_NO_ERR				no error
 *
 */

TRDP_ERR_T tau_closeCheckLinkUpDown (void);


#ifdef __cplusplus
}
#endif
#endif  /* TRDP_OPTION_LADDER */
#endif /* TAU_LADDER_H_ */
