/******************************************************************************/
/**
 * @file            tau_ladder.c
 *
 * @brief           使用共享内存实现双会话-子网（TRDP_SESSION）冗余的功能
 *
 * @details			提供共享内存创建接口、跨进程锁使用接口、和子网切换接口
 *
 * @note
 *
 * @author          Kazumasa Aiba, Toshiba Corporation
 *
 */

#ifdef TRDP_OPTION_LADDER
/*******************************************************************************
 * INCLUDES
 */
#include <string.h>

#include <sys/ioctl.h>
#include <netinet/in.h>
#ifdef __linux
#   include <linux/if.h>
#else
#include <net/if.h>
#endif
#include <unistd.h>

#include "vos_private.h"
#include "vos_shared_mem.h"
#include "tau_ladder.h"

#include "tau_ldLadder_config.h"

/*******************************************************************************
 * DEFINES
 */
#define TRAFFICSTORE_SYNC_NAME "/sem_trdpapp"
#define MAX_TRAFFIC_STORE_SIZE 0x40000u

/*******************************************************************************
 * TYPEDEFS
 */

/******************************************************************************
 *   Locals
 */

/******************************************************************************
 *   Globals
 */

/* Traffic Store Mutex */
VOS_PUBLIC_MUTEX_T pTrafficStoreMutex = NULL;                    /* Pointer to Mutex for Traffic Store */

/* Traffic Store */
CHAR8       TRAFFIC_STORE[] = "/ladder_ts";              /* Traffic Store shared memory name */
UINT8       *pTrafficStoreAddr;                          /* pointer to pointer to Traffic Store Address */
VOS_SHRD_T  pTrafficStoreHandle;                        /* Pointer to Traffic Store Handle */

/* Sub-net */
UINT32  usingSubnetId;                                   /* Using SubnetId */

/******************************************************************************/

/**
 *  @brief	创建共享内存区域和共享内存锁
 *
 *    Note:
 *
 *    @retval            TRDP_NO_ERR
 *    @retval            TRDP_MUTEX_ERR
 */
TRDP_ERR_T tau_ladder_init (void)
{
    UINT32              trafficStoreSize = 0;
    TRDP_ERR_T          ret     = TRDP_MUTEX_ERR;
    VOS_ERR_T           vosErr  = VOS_NO_ERR;

#ifndef SHM_MUTEX_IN_SHM
    vosErr = vos_publicMutexCreate(&pTrafficStoreMutex,TRAFFICSTORE_SYNC_NAME);
    if (vosErr != VOS_NO_ERR)
    {

        vos_printLog(VOS_LOG_ERROR, "TRDP Traffic Store Mutex Create failed. VOS Error: %d\n", vosErr);
        return ret;
    }
#else

#endif

    /* Create the Traffic Store */

    if(0!=sharememConfigTAUL.size && sharememConfigTAUL.size<=MAX_TRAFFIC_STORE_SIZE)
    {
    	trafficStoreSize=sharememConfigTAUL.size;
    	vosErr = vos_sharedOpen(TRAFFIC_STORE, &pTrafficStoreHandle, &pTrafficStoreAddr, &trafficStoreSize);
    }
    else
    {
    	vos_printLog(VOS_LOG_ERROR, "TRDP Traffic Store Create failed. invalid traffic store size: %d\n", sharememConfigTAUL.size);
    	vosErr = VOS_PARAM_ERR;
    }


    if (vosErr != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "TRDP Traffic Store Create failed. VOS Error: %d\n", vosErr);
        ret = TRDP_MEM_ERR;
    }
    else
    {
        pTrafficStoreHandle->sharedMemoryName = TRAFFIC_STORE;
        ret = TRDP_NO_ERR;
    }

    return ret;    /* TRDP_NO_ERR */
}

/******************************************************************************/
/** Finalize TRDP Ladder Support
 *  Delete Traffic Store mutex, Traffic Store.
 *
 *    Note:
 *
 *    @retval            TRDP_NO_ERR
 *    @retval            TRDP_MEM_ERR
 */
TRDP_ERR_T tau_ladder_terminate (void)
{
    extern UINT8        *pTrafficStoreAddr;             /* pointer to pointer to Traffic Store Address */
    extern VOS_PUBLIC_MUTEX_T  pTrafficStoreMutex;               /* Pointer to Mutex for Traffic Store */
    TRDP_ERR_T          err = TRDP_NO_ERR;

    /* Delete Traffic Store */
    tau_lockTrafficStore();
    if (vos_sharedClose(pTrafficStoreHandle, pTrafficStoreAddr) != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "Release Traffic Store shared memory failed\n");
        err = TRDP_MEM_ERR;
    }
    tau_unlockTrafficStore();

    /* Delete Traffic Store Mutex */
    vos_publicMutexDelete(pTrafficStoreMutex);

    return err;
}


/**********************************************************************************************************************/
/** Set SubNetwork Context.
 *
 *  @param[in]      SubnetId           Sub-network Id: SUBNET1 or SUBNET2
 *
 *  @retval         TRDP_NO_ERR            no error
 *  @retval         TRDP_PARAM_ERR        parameter error
 *  @retval         TRDP_NOPUB_ERR        not published
 *  @retval         TRDP_NOINIT_ERR    handle invalid
 */
TRDP_ERR_T  tau_setNetworkContext (
    UINT32 subnetId)
{
    /* Check Sub-network Id */
    if ((subnetId == SUBNET1) || (subnetId == SUBNET2))
    {
        /* Set usingSubnetId */
        usingSubnetId = subnetId;
        return TRDP_NO_ERR;
    }
    else
    {
        return TRDP_PARAM_ERR;
    }
}

/**********************************************************************************************************************/
/** Get SubNetwork Context.
 *
 *  @param[in,out]  pSubnetId            pointer to Sub-network Id
 *
 *  @retval         TRDP_NO_ERR            no error
 *  @retval         TRDP_PARAM_ERR        parameter error
 *  @retval         TRDP_NOPUB_ERR        not published
 *  @retval         TRDP_NOINIT_ERR    handle invalid
 */
TRDP_ERR_T  tau_getNetworkContext (
    UINT32 *pSubnetId)
{
    if (pSubnetId == NULL)
    {
        return TRDP_PARAM_ERR;
    }
    else
    {
        /* Get usingSubnetId */
        *pSubnetId = usingSubnetId;
        return TRDP_NO_ERR;
    }
}

/**********************************************************************************************************************/
/** Get Traffic Store accessibility.
 *
 *  @retval         TRDP_NO_ERR            no error
 *  @retval         TRDP_MUTEX_ERR        mutex error
 */
TRDP_ERR_T  tau_lockTrafficStore (
    void)
{
    extern VOS_PUBLIC_MUTEX_T  pTrafficStoreMutex;                   /* pointer to Mutex for Traffic Store */
    VOS_ERR_T           err = VOS_NO_ERR;

    /* Lock Traffic Store by Mutex */
    err = vos_publicMutexLock(pTrafficStoreMutex);
    if (err != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG, "TRDP Traffic Store Mutex Lock failed\n");
        return TRDP_MUTEX_ERR;
    }
    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/** Release Traffic Store accessibility.
 *
 *  @retval         TRDP_NO_ERR            no error
 *  @retval         TRDP_MUTEX_ERR        mutex error
 *
  */
TRDP_ERR_T  tau_unlockTrafficStore (
    void)
{
    extern VOS_PUBLIC_MUTEX_T pTrafficStoreMutex;                            /* pointer to Mutex for Traffic Store */

    /* Lock Traffic Store by Mutex */
    if (vos_publicMutexUnlock(pTrafficStoreMutex) != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "TRDP Traffic Store Mutex Unlock failed\n");
        return TRDP_MUTEX_ERR;
    }

    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/**
 *	@brief			查询指定的网口是否处于工作状态
 *  @param[in]        checkSubnetId            check Sub-network Id
 *  @param[out]        pLinkUpDown          pointer to check Sub-network Id Link Up Down TRUE:Up, FALSE:Down
 *
 *  @retval         TRDP_NO_ERR                no error
 *  @retval         TRDP_PARAM_ERR            parameter err
 *  @retval         TRDP_SOCK_ERR            socket err
 *
 *
 */
static int ifGetSocket = 0;

TRDP_ERR_T  tau_checkLinkUpDown (UINT32  checkSubnetId,BOOL8   *pLinkUpDown)
{
    struct ifreq    ifRead;
#if defined(linux)
    CHAR8           SUBNETWORK_ID1_IF_NAME[]    = "eth1";
    CHAR8           SUBNETWORK_ID2_IF_NAME[]    = "eth2";
#elif defined(VXWORKS)
    CHAR8           SUBNETWORK_ID1_IF_NAME[]    = "temac0";
    CHAR8           SUBNETWORK_ID2_IF_NAME[]    = "temac1";
#endif

    /*- 检查入参存在 */
    if (pLinkUpDown == NULL)
    {
        vos_printLog(VOS_LOG_ERROR, "tau_checkLinkUpDown pLinkUpDown parameter err\n");
        return TRDP_PARAM_ERR;
    }

    /*- 初始化IO状态结构体 */
    memset(&ifRead, 0, sizeof(ifRead));

    /*- 获取检查的网络名 */
    if (checkSubnetId == SUBNET1)
    {
        strncpy(ifRead.ifr_name, SUBNETWORK_ID1_IF_NAME, IFNAMSIZ - 1);
    }
    else if (checkSubnetId == SUBNET2)
    {
        strncpy(ifRead.ifr_name, SUBNETWORK_ID2_IF_NAME, IFNAMSIZ - 1);
    }
    else
    {
        vos_printLog(VOS_LOG_ERROR, "tau_checkLinkUpDown Check SubnetId failed\n");
        return TRDP_PARAM_ERR;
    }


    /*- 还未创建用于获取网口的socket */
    if (ifGetSocket <= 0)
    {
        /*- 创建用于获取网口的socket */
        ifGetSocket = socket(AF_INET, SOCK_DGRAM, 0);
        if (ifGetSocket == -1)
        {
            vos_printLog(VOS_LOG_ERROR, "tau_checkLinkUpDown socket descriptor err.\n");
            return TRDP_SOCK_ERR;
        }
    }

    /*- 获取网口状态 */
    if (ioctl(ifGetSocket, SIOCGIFFLAGS, &ifRead) != 0)
    {
        vos_printLog(VOS_LOG_ERROR, "Get I/F Information failed\n");
        return TRDP_SOCK_ERR;
    }

    /*- 检查标志位 */
    if (((ifRead.ifr_ifru.ifru_flags & IFF_UP) == IFF_UP)
        && ((ifRead.ifr_ifru.ifru_flags & IFF_RUNNING) == IFF_RUNNING))
    {
        /* Link Up */
        *pLinkUpDown = TRUE;
    }
    else
    {
        /* Link Down */
        *pLinkUpDown = FALSE;
    }

    return TRDP_NO_ERR;
}

/**********************************************************************************************************************/
/**
 *	@brief			关闭为检查网络状态建立的socket
 *  @retval         TRDP_NO_ERR                no error
 *
 */

TRDP_ERR_T  tau_closeCheckLinkUpDown (void)
{
    if (ifGetSocket)
    {
        close(ifGetSocket);
        ifGetSocket = 0;
    }
    return TRDP_NO_ERR;
}

#endif /* TRDP_OPTION_LADDER */
