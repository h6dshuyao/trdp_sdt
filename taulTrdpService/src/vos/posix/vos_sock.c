/**********************************************************************************************************************/
/**
 * @file            posix/vos_sock.c
 *
 * @brief           Socket functions
 *
 * @details         OS abstraction of IP socket functions for UDP and TCP
 *
 * @note            Project: TCNOpen TRDP prototype stack
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 * @remarks This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Bombardier Transportation Inc. or its subsidiaries and others, 2013. All rights reserved.
 */
/*
* $Id: vos_sock.c 2137 2019-12-06 17:40:07Z bloehr $
*
*      BL 2019-08-27: Changed send failure from ERROR to WARNING
*      SB 2019-07-11: Added includes linux/if_vlan.h and linux/sockios.h
*      BL 2019-06-17: Ticket #191 Add provisions for TSN / Hard Real Time (open source)
*      V 2.0.0 --------- ^^^ -----------
*      V 1.4.2 --------- vvv -----------
*      SB 2019-02-18: Ticket #227: vos_sockGetMAC() not name dependant anymore
*      BL 2019-01-29: Ticket #233: DSCP Values not standard conform
*      BL 2018-11-26: Ticket #208: Mapping corrected after complaint (Bit 2 was set for prio 2 & 4)
*      BL 2018-07-13: Ticket #208: VOS socket options: QoS/ToS field priority handling needs update
*      BL 2018-06-20: Ticket #184: Building with VS 2015: WIN64 and Windows threads (SOCKET instead of INT32)
*      BL 2018-05-03: Ticket #194: Platform independent format specifiers in vos_printLog
*      BL 2018-03-06: 64Bit endian swap added
*      BL 2017-05-22: Ticket #122: Addendum for 64Bit compatibility (VOS_TIME_T -> VOS_TIMEVAL_T)
*      BL 2017-05-08: Compiler warnings
*      BL 2016-07-06: Ticket #122 64Bit compatibility (+ compiler warnings)
*      BL 2015-11-20: Compiler warnings fixed (lines 392 + 393)
*      BL 2014-08-25: Ticket #51: change underlying function for vos_dottedIP
*
*/



/***********************************************************************************************************************
 * INCLUDES
 */



#include <stdlib.h>

#if defined(POSIX)
#include <stdio.h>
#include <stddef.h>
#include <stdint.h>

#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/if.h>
#include <byteswap.h>
#include <linux/if_vlan.h>
#include <linux/sockios.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <ifaddrs.h>

#elif defined(VXWORKS)
#include "vxWorks.h"
#include "vos_mem.h"
#include "ctype.h"
#include "muxLib.h"
#include "ioLib.h"
#include "ioctl.h"
#include "vos_types.h"

#endif


#include "vos_utils.h"
#include "vos_thread.h"
#include "vos_private.h"
#include "vos_sock.h"

/***********************************************************************************************************************
 * DEFINITIONS
 */


#if defined(linux)
const CHAR8 *cDefaultIface = "eth1";
#elif defined(VXWORKS)
const CHAR8 *cDefaultIface = "temac0";
#endif

/***********************************************************************************************************************
 *  LOCALS
 */

BOOL8           vosSockInitialised = FALSE;

struct ifreq    gIfr;

/***********************************************************************************************************************
 * LOCAL FUNCTIONS
 */

#ifdef POSIX

BOOL8       vos_getMacAddress (UINT8        *pMacAddr,
                               const char   *pIfName);
#endif

/**********************************************************************************************************************/
/** Get the MAC address for a named interface.
 *
 *  @param[out]         pMacAddr   pointer to array of MAC address to return
 *  @param[in]          pIfName    pointer to interface name
 *
 *  @retval             TRUE if successfull
 */
BOOL8 vos_getMacAddress (
    UINT8       *pMacAddr,
    const char  *pIfName)
{
#if defined(linux)
    struct ifreq    ifinfo;
    int             sd;
    int             result = -1;

    if (pIfName == NULL)
    {
        strcpy(ifinfo.ifr_name, cDefaultIface);
    }
    else
    {
        strcpy(ifinfo.ifr_name, pIfName);
    }
    sd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sd != -1)
    {
        result = ioctl(sd, SIOCGIFHWADDR, &ifinfo);
        close(sd);
    }
    if ((result == 0) && (ifinfo.ifr_hwaddr.sa_family == 1))
    {
        memcpy(pMacAddr, ifinfo.ifr_hwaddr.sa_data, IFHWADDRLEN);
        return TRUE;
    }
    else
    {
        return FALSE;
    }
#elif defined(VXWORKS)
    BOOL8   found = FALSE;
    END_OBJ *pEndObj;
    CHAR8   interface_name[VOS_MAX_IF_NAME_SIZE];
    INT16   interface_unit = 0;
    INT16   ix = 0;
    /*
      Separate the interface name e.g "fec" from the
      interface number "fec0".
    */
    vos_strncpy(interface_name, pIfName, END_NAME_MAX);
    while ((isdigit((int)interface_name[ix]) == 0) && (ix < END_NAME_MAX))
    {
        ix++;
    }
    interface_unit      = (INT16)strtol(interface_name + ix, NULL, 10);
    interface_name[ix]  = '\0';

    /* Get the END object so that we can extract the PHY address */
    pEndObj = endFindByName(interface_name, interface_unit );
    if (pEndObj != NULL)
    {
        /* There are two kinds of PHY data structures */
        if (pEndObj->flags & END_MIB_2233)
        {
            if (pEndObj->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.addrLength == VOS_MAC_SIZE)
            {
                memcpy( pMacAddr, pEndObj->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.phyAddress,
                        (UINT32)pEndObj->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.addrLength);
                found = TRUE;
            }
            else
            {
                vos_printLog(VOS_LOG_ERROR, "pEndObj->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.addrLength = %ld\n",
                             pEndObj->pMib2Tbl->m2Data.mibIfTbl.ifPhysAddress.addrLength );
            }
        }
        else
        {
            if (pEndObj->mib2Tbl.ifPhysAddress.addrLength == VOS_MAC_SIZE)
            {
                memcpy(pMacAddr, pEndObj->mib2Tbl.ifPhysAddress.phyAddress,
                       (UINT32)pEndObj->mib2Tbl.ifPhysAddress.addrLength);
                found = TRUE;
            }
            else
            {
                vos_printLog(VOS_LOG_ERROR, "pEndObj->mib2Tbl.ifPhysAddress.addrLength = %ld\n",
                             pEndObj->mib2Tbl.ifPhysAddress.addrLength);
            }
        }
    }
    if (found != TRUE)
    {
        for (ix = 0; ix < VOS_MAC_SIZE; ix++)
        {
            pMacAddr[ix] = 0;
        }
    }
    return found;

#endif
}

/**********************************************************************************************************************/
/** Enlarge send and receive buffers to TRDP_SOCKBUF_SIZE if necessary.
 *
 *  @param[in]      sock            socket descriptor
 *
 *  @retval         VOS_NO_ERR       no error
 *  @retval         VOS_SOCK_ERR     buffer size can't be set
 */
 
#if defined(POSIX)
EXT_DECL VOS_ERR_T vos_sockSetBuffer (SOCKET sock)
{
    int         optval      = 0;
    socklen_t   option_len  = sizeof(optval);

    (void)getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (int *)&optval, &option_len);
    if (optval < TRDP_SOCKBUF_SIZE)
    {
        optval = TRDP_SOCKBUF_SIZE;
        if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (int *)&optval, option_len) == -1)
        {
            (void)getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (int *)&optval, &option_len);
            vos_printLog(VOS_LOG_WARNING, "Send buffer size out of limit (max: %d)\n", optval);
            return VOS_SOCK_ERR;
        }
    }
    vos_printLog(VOS_LOG_INFO, "Send buffer limit = %d\n", optval);

    (void)getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (int *)&optval, &option_len);
    if (optval < TRDP_SOCKBUF_SIZE)
    {
        optval = TRDP_SOCKBUF_SIZE;
        if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (int *)&optval, option_len) == -1)
        {
            (void)getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (int *)&optval, &option_len);
            vos_printLog(VOS_LOG_WARNING, "Recv buffer size out of limit (max: %d)\n", optval);
            return VOS_SOCK_ERR;
        }
    }
    vos_printLog(VOS_LOG_INFO, "Recv buffer limit = %d\n", optval);

    return VOS_NO_ERR;
}
#elif defined(VXWORKS)
VOS_ERR_T vos_sockSetBuffer (SOCKET sock)
{
    int optval = 0;
    socklen_t option_len = sizeof(optval);

    (void)getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char *)&optval, (socklen_t *)&option_len);
    if (optval < TRDP_SOCKBUF_SIZE)
    {
        optval = TRDP_SOCKBUF_SIZE;
        if (setsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char *)&optval, option_len) == -1)
        {
            (void)getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (char *)&optval, (socklen_t *)&option_len);
            vos_printLog(VOS_LOG_WARNING, "Send buffer size out of limit (max: %u)\n", optval);
            return VOS_SOCK_ERR;
        }
    }
    vos_printLog(VOS_LOG_INFO, "Send buffer limit = %u\n", optval);

    (void)getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *)&optval, (socklen_t *)&option_len);
    if (optval < TRDP_SOCKBUF_SIZE)
    {
        optval = TRDP_SOCKBUF_SIZE;
        if (setsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *)&optval, option_len) == -1)
        {
            (void)getsockopt(sock, SOL_SOCKET, SO_RCVBUF, (char *)&optval, (socklen_t *)&option_len);
            vos_printLog(VOS_LOG_WARNING, "Recv buffer size out of limit (max: %u)\n", optval);
            return VOS_SOCK_ERR;
        }
    }
    vos_printLog(VOS_LOG_INFO, "Recv buffer limit = %u\n", optval);

    return VOS_NO_ERR;
}

#endif


#ifdef VXWORKS
/**********************************************************************************************************************/
/** Swap 64 bit value if necessary.
 *
 *  @param[in]      value            Input
 *
 *  @retval         swapped input if on little endian
 */

#define bswap64(x)	    (((UINT64)((x) & 0x00000000000000ffULL) << 56)  | \
			             ((UINT64)((x) & 0x000000000000ff00ULL) << 40)  | \
			             ((UINT64)((x) & 0x0000000000ff0000ULL) << 24)  | \
			             ((UINT64)((x) & 0x00000000ff000000ULL) << 8)   | \
			             ((UINT64)((x) & 0x000000ff00000000ULL) >> 8)   | \
			             ((UINT64)((x) & 0x0000ff0000000000ULL) >> 24)  | \
			             ((UINT64)((x) & 0x00ff000000000000ULL) >> 40)  | \
			             ((UINT64)((x) & 0xff00000000000000ULL) >> 56))

#if _BYTE_ORDER == _LITTLE_ENDIAN

#define ntohll(x)	bswap64(x)
#define htonll(x)	bswap64(x)

#else

#define ntohll(x)	(x)
#define htonll(x)	(x)

#endif
#endif
/***********************************************************************************************************************
 * GLOBAL FUNCTIONS
 */

/**********************************************************************************************************************/
/** Byte swapping.
 *
 *  @param[in]          val             Initial value.
 *
 *  @retval             swapped value
 */

EXT_DECL UINT16 vos_htons (
    UINT16 val)
{
    return htons(val);
}

EXT_DECL UINT16 vos_ntohs (
    UINT16 val)
{
    return ntohs(val);
}

EXT_DECL UINT32 vos_htonl (
    UINT32 val)
{
    return htonl(val);
}

EXT_DECL UINT32 vos_ntohl (
    UINT32 val)
{
    return ntohl(val);
}

EXT_DECL UINT64 vos_htonll (
    UINT64 val)
{
#ifdef __linux
#   ifdef L_ENDIAN
    return bswap_64(val);
#   else
    return val;
#   endif
#else
    return htonll(val);
#endif
}

EXT_DECL UINT64 vos_ntohll (
    UINT64 val)
{
#ifdef __linux
#   ifdef L_ENDIAN
    return bswap_64(val);
#   else
    return val;
#   endif
#else
    return ntohll(val);
#endif
}

/**********************************************************************************************************************/
/** Convert IP address from dotted dec. to !host! endianess
 *
 *  @param[in]          pDottedIP     IP address as dotted decimal.
 *
 *  @retval             address in UINT32 in host endianess
 *                      0 (Zero) if error
 */
EXT_DECL UINT32 vos_dottedIP (
    const CHAR8 *pDottedIP)
{
    struct in_addr addr;
    if (inet_aton(pDottedIP, &addr) <= 0)
    {
        return VOS_INADDR_ANY;          /* Prevent returning broadcast address on error */
    }
    else
    {
        return vos_ntohl(addr.s_addr);
    }
}

/**********************************************************************************************************************/
/** Convert IP address to dotted dec. from !host! endianess.
 *
 *  @param[in]          ipAddress   address in UINT32 in host endianess
 *
 *  @retval             IP address as dotted decimal.
 */

EXT_DECL const CHAR8 *vos_ipDotted (
    UINT32 ipAddress)
{
    static CHAR8 dotted[16];

    (void)snprintf(dotted, sizeof(dotted), "%u.%u.%u.%u",
                   (unsigned int)(ipAddress >> 24),
                   (unsigned int)((ipAddress >> 16) & 0xFF),
                   (unsigned int)((ipAddress >> 8) & 0xFF),
                   (unsigned int)(ipAddress & 0xFF));

    return dotted;
}

/**********************************************************************************************************************/
/** Check if the supplied address is a multicast group address.
 *
 *  @param[in]          ipAddress   IP address to check.
 *
 *  @retval             TRUE        address is multicast
 *  @retval             FALSE       address is not a multicast address
 */

EXT_DECL BOOL8 vos_isMulticast (
    UINT32 ipAddress)
{
    return IN_MULTICAST(ipAddress);
}


/**********************************************************************************************************************/
/** select function.
 *  Set the ready sockets in the supplied sets.
 *    Note: Some target systems might define this function as NOP.
 *
 *  @param[in]      highDesc          max. socket descriptor + 1
 *  @param[in,out]  pReadableFD       pointer to readable socket set
 *  @param[in,out]  pWriteableFD      pointer to writeable socket set
 *  @param[in,out]  pErrorFD          pointer to error socket set
 *  @param[in]      pTimeOut          pointer to time out value
 *
 *  @retval         number of ready file descriptors
 */

EXT_DECL INT32 vos_select (
    SOCKET          highDesc,
    VOS_FDS_T       *pReadableFD,
    VOS_FDS_T       *pWriteableFD,
    VOS_FDS_T       *pErrorFD,
    VOS_TIMEVAL_T   *pTimeOut)
{
    return select(highDesc, (fd_set *) pReadableFD, (fd_set *) pWriteableFD,
                  (fd_set *) pErrorFD, (struct timeval *) pTimeOut);
}

/**********************************************************************************************************************/
/** Get a list of interface addresses
 *  The caller has to provide an array of interface records to be filled.
 *
 *  @param[in,out]  pAddrCnt          in:   pointer to array size of interface record
 *                                    out:  pointer to number of interface records read
 *  @param[in,out]  ifAddrs           array of interface records
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   pMAC == NULL
 */
EXT_DECL VOS_ERR_T vos_getInterfaces (
    UINT32          *pAddrCnt,
    VOS_IF_REC_T    ifAddrs[])
{
    int success;
    struct ifaddrs  *addrs;
    struct ifaddrs  *cursor;
    unsigned int    count = 0;

    if (pAddrCnt == NULL ||
        *pAddrCnt == 0 ||
        ifAddrs == NULL)
    {
        return VOS_PARAM_ERR;
    }

    success = getifaddrs(&addrs) == 0;
    if (success)
    {
        cursor = addrs;
        while (cursor != 0 && count < *pAddrCnt)
        {
            if (cursor->ifa_addr != NULL && cursor->ifa_addr->sa_family == AF_INET)
            {
                memcpy(&ifAddrs[count].ipAddr, &cursor->ifa_addr->sa_data[2], 4);
                ifAddrs[count].ipAddr = vos_ntohl(ifAddrs[count].ipAddr);
                memcpy(&ifAddrs[count].netMask, &cursor->ifa_netmask->sa_data[2], 4);
                ifAddrs[count].netMask = vos_ntohl(ifAddrs[count].netMask);
                if (cursor->ifa_name != NULL)
                {
                    strncpy((char *) ifAddrs[count].name, cursor->ifa_name, VOS_MAX_IF_NAME_SIZE);
                    ifAddrs[count].name[VOS_MAX_IF_NAME_SIZE - 1] = 0;
                }
                vos_printLog(VOS_LOG_INFO, "IP-Addr for '%s': %u.%u.%u.%u\n",
                             ifAddrs[count].name,
                             (unsigned int)(ifAddrs[count].ipAddr >> 24) & 0xFF,
                             (unsigned int)(ifAddrs[count].ipAddr >> 16) & 0xFF,
                             (unsigned int)(ifAddrs[count].ipAddr >> 8)  & 0xFF,
                             (unsigned int)(ifAddrs[count].ipAddr        & 0xFF));
                if (vos_getMacAddress(ifAddrs[count].mac, ifAddrs[count].name) == TRUE)
                {
                    vos_printLog(VOS_LOG_INFO, "Mac-Addr for '%s': %02x:%02x:%02x:%02x:%02x:%02x\n",
                                 ifAddrs[count].name,
                                 (unsigned int)ifAddrs[count].mac[0],
                                 (unsigned int)ifAddrs[count].mac[1],
                                 (unsigned int)ifAddrs[count].mac[2],
                                 (unsigned int)ifAddrs[count].mac[3],
                                 (unsigned int)ifAddrs[count].mac[4],
                                 (unsigned int)ifAddrs[count].mac[5]);
                }
                if (cursor->ifa_flags & IFF_RUNNING)
                {
                    ifAddrs[count].linkState = TRUE;
                }
                else
                {
                    ifAddrs[count].linkState = FALSE;
                }
                count++;
            }
            cursor = cursor->ifa_next;
        }

        freeifaddrs(addrs);
    }
    else
    {
        return VOS_PARAM_ERR;
    }

    *pAddrCnt = count;
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Get the state of an interface
 *
 *
 *  @param[in]      ifAddress       address of interface to check
 *
 *  @retval         TRUE            interface is up and ready
 *                  FALSE           interface is down / not ready
 */
EXT_DECL BOOL8 vos_netIfUp (
    VOS_IP4_ADDR_T ifAddress)
{
    struct ifaddrs  *addrs;
    struct ifaddrs  *cursor;
    VOS_IF_REC_T    ifAddrs;

    ifAddrs.linkState = FALSE;

    if (getifaddrs(&addrs) == 0)
    {
        cursor = addrs;
        while (cursor != 0)
        {
            if (cursor->ifa_addr != NULL && cursor->ifa_addr->sa_family == AF_INET)
            {
                memcpy(&ifAddrs.ipAddr, &cursor->ifa_addr->sa_data[2], 4);
                ifAddrs.ipAddr = vos_ntohl(ifAddrs.ipAddr);
                /* Exit if first (default) interface matches */
                if (ifAddress == VOS_INADDR_ANY || ifAddress == ifAddrs.ipAddr)
                {
                    if (cursor->ifa_flags & IFF_UP)
                    {
                        ifAddrs.linkState = TRUE;
                    }
                    /* vos_printLog(VOS_LOG_INFO, "cursor->ifa_flags = 0x%x\n", cursor->ifa_flags); */
                    break;
                }
            }
            cursor = cursor->ifa_next;
        }

        freeifaddrs(addrs);

    }
    return ifAddrs.linkState;
}


/*    Sockets    */

/**********************************************************************************************************************/
/** Initialize the socket library.
 *  Must be called once before any other call
 *
 *  @retval         VOS_NO_ERR          no error
 *  @retval         VOS_SOCK_ERR        sockets not supported
 */

EXT_DECL VOS_ERR_T vos_sockInit (void)
{
    memset(&gIfr, 0, sizeof(gIfr));
    vosSockInitialised = TRUE;

    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** De-Initialize the socket library.
 *  Must be called after last socket call
 *
 */

EXT_DECL void vos_sockTerm (void)
{
    vosSockInitialised = FALSE;
}

/**********************************************************************************************************************/
/** Return the MAC address of the default adapter.
 *
 *  @param[out]     pMAC            return MAC address.
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   pMAC == NULL
 *  @retval         VOS_SOCK_ERR    socket not available or option not supported
 */

EXT_DECL VOS_ERR_T vos_sockGetMAC (
    UINT8 pMAC[VOS_MAC_SIZE])
{
    UINT32          i;
    UINT32          AddrCount = VOS_MAX_NUM_IF;
    VOS_IF_REC_T    ifAddrs[VOS_MAX_NUM_IF];
    VOS_ERR_T       err;

    if (pMAC == NULL)
    {
        vos_printLogStr(VOS_LOG_ERROR, "Parameter error\n");
        return VOS_PARAM_ERR;
    }

    err = vos_getInterfaces(&AddrCount, ifAddrs);

    if (err == VOS_NO_ERR)
    {
        for (i = 0u; i < AddrCount; ++i)
        {
            if (ifAddrs[i].mac[0] ||
                ifAddrs[i].mac[1] ||
                ifAddrs[i].mac[2] ||
                ifAddrs[i].mac[3] ||
                ifAddrs[i].mac[4] ||
                ifAddrs[i].mac[5])
            {
                if (vos_getMacAddress(pMAC, ifAddrs[i].name) == TRUE)
                {
                    return VOS_NO_ERR;
                }
            }
        }
    }

    return VOS_SOCK_ERR;
}

/**********************************************************************************************************************/
/** Create an UDP socket.
 *  Return a socket descriptor for further calls. The socket options are optional and can be
 *  applied later.
 *  Note: Some targeted systems might not support every option.
 *
 *  @param[out]     pSock           pointer to socket descriptor returned
 *  @param[in]      pOptions        pointer to socket options (optional)
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   pSock == NULL
 *  @retval         VOS_SOCK_ERR    socket not available or option not supported
 */

EXT_DECL VOS_ERR_T vos_sockOpenUDP (
    SOCKET                  *pSock,
    const VOS_SOCK_OPT_T    *pOptions)
{
    int sock;

    if (!vosSockInitialised)
    {
        return VOS_INIT_ERR;
    }

    if (pSock == NULL)
    {
        vos_printLogStr(VOS_LOG_ERROR, "Parameter error\n");
        return VOS_PARAM_ERR;
    }

    if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_ERROR, "socket() failed (Err: %s)\n", buff);
        return VOS_SOCK_ERR;
    }

    if ((vos_sockSetOptions(sock, pOptions) != VOS_NO_ERR)
        || (vos_sockSetBuffer(sock) != VOS_NO_ERR))
    {
        close(sock);
        vos_printLogStr(VOS_LOG_ERROR, "socket() failed, setsockoptions or buffer failed!\n");
        return VOS_SOCK_ERR;
    }

    *pSock = (SOCKET) sock;

    vos_printLog(VOS_LOG_DBG, "vos_sockOpenUDP: socket()=%d success\n", (int)sock);
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Create a TCP socket.
 *  Return a socket descriptor for further calls. The socket options are optional and can be
 *  applied later.
 *
 *  @param[out]     pSock           pointer to socket descriptor returned
 *  @param[in]      pOptions        pointer to socket options (optional)
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   pSock == NULL
 *  @retval         VOS_SOCK_ERR    socket not available or option not supported
 */

EXT_DECL VOS_ERR_T vos_sockOpenTCP (
    SOCKET                  *pSock,
    const VOS_SOCK_OPT_T    *pOptions)
{
    int sock;

    if (!vosSockInitialised)
    {
        return VOS_INIT_ERR;
    }

    if (pSock == NULL)
    {
        vos_printLogStr(VOS_LOG_ERROR, "Parameter error\n");
        return VOS_PARAM_ERR;
    }

    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_ERROR, "socket() failed (Err: %s)\n", buff);
        return VOS_SOCK_ERR;
    }

    if ((vos_sockSetOptions(sock, pOptions) != VOS_NO_ERR)
        || (vos_sockSetBuffer(sock) != VOS_NO_ERR))
    {
        close(sock);
        return VOS_SOCK_ERR;
    }

    *pSock = (SOCKET) sock;

    vos_printLog(VOS_LOG_INFO, "vos_sockOpenTCP: socket()=%d success\n", (int)sock);
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Close a socket.
 *  Release any resources aquired by this socket
 *
 *  @param[in]      sock            socket descriptor
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown
 */

EXT_DECL VOS_ERR_T vos_sockClose (
    SOCKET sock)
{
    if (close(sock) == -1)
    {
        vos_printLog(VOS_LOG_ERROR,
                     "vos_sockClose(%d) called with unknown descriptor\n", (int)sock);
        return VOS_PARAM_ERR;
    }
    else
    {
        vos_printLog(VOS_LOG_DBG,
                     "vos_sockClose(%d) okay\n", (int)sock);
    }

    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Set socket options.
 *  Note: Some targeted systems might not support every option.
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      pOptions        pointer to socket options (optional)
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown
 */

EXT_DECL VOS_ERR_T vos_sockSetOptions (
    SOCKET                  sock,
    const VOS_SOCK_OPT_T    *pOptions)
{
    int sockOptValue = 0;

    if (pOptions)
    {
        if (1 == pOptions->reuseAddrPort)
        {
            sockOptValue = 1;
#ifdef SO_REUSEPORT
            if (setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() SO_REUSEPORT failed (Err: %s)\n", buff);
            }
#else
            if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() SO_REUSEADDR failed (Err: %s)\n", buff);
            }
#endif
        }
		
#if defined(VXWORKS)
		sockOptValue = (pOptions->nonBlocking == TRUE) ? TRUE : FALSE;
        if (ioctl(sock, FIONBIO, &sockOptValue) == -1)
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_ERROR, "setsockopt() O_NONBLOCK failed (Err: %s)\n", buff);
            return VOS_SOCK_ERR;
        }
#elif defined(POSIX)
		 if (1 == pOptions->nonBlocking)
        {
            if (fcntl(sock, F_SETFL, O_NONBLOCK) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() O_NONBLOCK failed (Err: %s)\n", buff);
                return VOS_SOCK_ERR;
            }
        }
#endif

        if (pOptions->qos > 0 && pOptions->qos < 8)
        {
#ifdef SO_NET_SERVICE_TYPE
            /* Darwin/BSD: Depending on socket typ, sets the layer 2 priority as well.
             NET_SERVICE_TYPE_BE    0  Best effort
             NET_SERVICE_TYPE_BK    1  Background system initiated
             NET_SERVICE_TYPE_SIG   2  Signaling
             NET_SERVICE_TYPE_VI    3  Interactive Video
             NET_SERVICE_TYPE_VO    4  Interactive Voice
             NET_SERVICE_TYPE_RV    5  Responsive Multimedia Audio/Video
             NET_SERVICE_TYPE_AV    6  Multimedia Audio/Video Streaming
             NET_SERVICE_TYPE_OAM   7  Operations, Administration, and Management
             NET_SERVICE_TYPE_RD    8  Responsive Data
             */
            sockOptValue = (int) pOptions->qos + 1;
            if (sockOptValue == 1) /* Best effort and background share same priority */
            {
                sockOptValue = 0;
            }
            if (setsockopt(sock, SOL_SOCKET, SO_NET_SERVICE_TYPE, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() SO_NET_SERVICE_TYPE failed (Err: %s)\n", buff);
            }
#endif

            /* The QoS value (0-7) was mapped to MSB bits 7-5, bit 2 was set for local use.
             TOS field is deprecated and has been succeeded by RFC 2474 and RFC 3168. Usage depends on IP-routers.
             This field is now called the "DS" (Differentiated Services) field and the upper 6 bits contain
             a value called the "DSCP" (Differentiated Services Code Point).
             QoS as priority value is now mapped to DSCP values and the Explicit Congestion Notification (ECN)
             is set to 0

             IEC61375-3-4 Chap 4.6.3 defines the binary representation of DSCP field as:
                    LLL000
             where
                    LLL: priority level (0-7) defined in Chap 4.6.2

             */

            sockOptValue = (int) pOptions->qos << 5;    /* The lower 2 bits are the ECN field! */
            if (setsockopt(sock, IPPROTO_IP, IP_TOS, (char *)&sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_TOS failed (Err: %s)\n", buff);
            }


#ifdef SO_PRIORITY
            /* if available (and the used socket is tagged) set the VLAN PCP field as well. */
            sockOptValue = (int) pOptions->qos;
            if (setsockopt(sock, SOL_SOCKET, SO_PRIORITY, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() SO_PRIORITY failed (Err: %s)\n", buff);
            }
#ifdef POSIX
            {
                struct vlan_ioctl_args vlan_args;
                vlan_args.cmd = SET_VLAN_EGRESS_PRIORITY_CMD;
                vlan_args.u.skb_priority    = sockOptValue;
                vlan_args.vlan_qos          = pOptions->qos;
                vlan_args.u.name_type       = VLAN_NAME_TYPE_RAW_PLUS_VID;
                strcpy(vlan_args.device1, pOptions->ifName);

                (void) ioctl(sock, SIOCSIFVLAN, &vlan_args);
            }
#endif
#endif
        }
        if (pOptions->ttl > 0)
        {
            sockOptValue = pOptions->ttl;
            if (setsockopt(sock, IPPROTO_IP, IP_TTL, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_TTL failed (Err: %s)\n", buff);
            }
        }
        if (pOptions->ttl_multicast > 0)
        {
            sockOptValue = pOptions->ttl_multicast;
            if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_MULTICAST_TTL failed (Err: %s)\n", buff);
            }
        }
        if (pOptions->no_mc_loop > 0)
        {
            /* Default behavior is ON * / */
            sockOptValue = 0;
            if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_MULTICAST_LOOP failed (Err: %s)\n", buff);
            }
        }
#ifdef SO_NO_CHECK
        /* Default behavior is ON * / */
        if (pOptions->no_udp_crc > 0)
        {
            sockOptValue = 1;
            if (setsockopt(sock, SOL_SOCKET, SO_NO_CHECK, &sockOptValue,
                           sizeof(sockOptValue)) == -1)
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_WARNING, "setsockopt() SO_NO_CHECK failed (Err: %s)\n", buff);
            }
        }
#endif
    }
    /*  Include struct in_pktinfo in the message "ancilliary" control data.
        This way we can get the destination IP address for received UDP packets */
    sockOptValue = 1;
#if defined(IP_RECVDSTADDR)
    if (setsockopt(sock, IPPROTO_IP, IP_RECVDSTADDR, &sockOptValue, sizeof(sockOptValue)) == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_RECVDSTADDR failed (Err: %s)\n", buff);
    }
#elif defined(IP_PKTINFO)
    if (setsockopt(sock, IPPROTO_IP, IP_PKTINFO, &sockOptValue, sizeof(sockOptValue)) == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_PKTINFO failed (Err: %s)\n", buff);
    }
#else
    vos_printLogStr(VOS_LOG_WARNING, "setsockopt() Source address filtering is not available on platform!\n");
#endif

    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Join a multicast group.
 *  Note: Some targeted systems might not support this option.
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      mcAddress       multicast group to join
 *  @param[in]      ipAddress       depicts interface on which to join, default 0 for any
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_SOCK_ERR    option not supported
 */

EXT_DECL VOS_ERR_T vos_sockJoinMC (
    SOCKET  sock,
    UINT32  mcAddress,
    UINT32  ipAddress)
{
    struct ip_mreq  mreq;
    VOS_ERR_T       result = VOS_NO_ERR;
    char buff[VOS_MAX_ERR_STR_SIZE];

    if (sock == -1)
    {
        result = VOS_PARAM_ERR;
    }
    /* Is this a multicast address? */
    else if (IN_MULTICAST(mcAddress))
    {
        mreq.imr_multiaddr.s_addr   = vos_htonl(mcAddress);
        mreq.imr_interface.s_addr   = vos_htonl(ipAddress);

        {
            char    mcStr[16];
            char    ifStr[16];

            strncpy(mcStr, inet_ntoa(mreq.imr_multiaddr), sizeof(mcStr));
            mcStr[sizeof(mcStr) - 1] = 0;
            strncpy(ifStr, inet_ntoa(mreq.imr_interface), sizeof(ifStr));
            ifStr[sizeof(ifStr) - 1] = 0;

            vos_printLog(VOS_LOG_INFO, "joining MC: %s on iface %s\n", mcStr, ifStr);
        }

        /*errno = 0;*/
        if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) == -1 &&
            errno != EADDRINUSE)
        {
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_ERROR, "setsockopt() IP_ADD_MEMBERSHIP failed (Err: %s)\n", buff);
            result = VOS_SOCK_ERR;
        }
        else
        {
            result = VOS_NO_ERR;
        }

        /* Disable multicast loop back */
        /* only useful for streaming
        {
            UINT32 enMcLb = 0;


            if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, &enMcLb, sizeof(enMcLb)) == -1
                 && errno != 0)
            {
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_ERROR, "setsockopt() IP_MULTICAST_LOOP failed (%s)\n", buff);
                result = VOS_SOCK_ERR;
            }
            else
            {
                result = (result == VOS_SOCK_ERR) ? VOS_SOCK_ERR : VOS_NO_ERR;
            }
        }
        */
    }
    else
    {
        result = VOS_PARAM_ERR;
    }

    return result;
}

/**********************************************************************************************************************/
/** Leave a multicast group.
 *  Note: Some targeted systems might not support this option.
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      mcAddress       multicast group to join
 *  @param[in]      ipAddress       depicts interface on which to leave, default 0 for any
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_SOCK_ERR    option not supported
 */

EXT_DECL VOS_ERR_T vos_sockLeaveMC (
    SOCKET  sock,
    UINT32  mcAddress,
    UINT32  ipAddress)
{
    struct ip_mreq  mreq;
    VOS_ERR_T       result = VOS_NO_ERR;

    if (sock == -1)
    {
        result = VOS_PARAM_ERR;
    }
    /* Is this a multicast address? */
    else if (IN_MULTICAST(mcAddress))
    {
        mreq.imr_multiaddr.s_addr   = vos_htonl(mcAddress);
        mreq.imr_interface.s_addr   = vos_htonl(ipAddress);

        {
            char    mcStr[16];
            char    ifStr[16];

            strncpy(mcStr, inet_ntoa(mreq.imr_multiaddr), sizeof(mcStr));
            mcStr[sizeof(mcStr) - 1] = 0;
            strncpy(ifStr, inet_ntoa(mreq.imr_interface), sizeof(ifStr));
            ifStr[sizeof(ifStr) - 1] = 0;

            vos_printLog(VOS_LOG_INFO, "leaving MC: %s on iface %s\n", mcStr, ifStr);
        }

        /*errno = 0;*/
        if (setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, &mreq, sizeof(mreq)) == -1)
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_ERROR, "setsockopt() IP_DROP_MEMBERSHIP failed (Err: %s)\n", buff);

            result = VOS_SOCK_ERR;
        }
        else
        {
            result = VOS_NO_ERR;
        }
    }
    else
    {
        result = VOS_PARAM_ERR;
    }

    return result;
}

/**********************************************************************************************************************/
/** Send UDP data.
 *  Send data to the supplied address and port.
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      pBuffer         pointer to data to send
 *  @param[in,out]  pSize           In: size of the data to send, Out: no of bytes sent
 *  @param[in]      ipAddress       destination IP
 *  @param[in]      port            destination port
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      data could not be sent
 *  @retval         VOS_BLOCK_ERR   Call would have blocked in blocking mode
 */

EXT_DECL VOS_ERR_T vos_sockSendUDP (
    SOCKET      sock,
    const UINT8 *pBuffer,
    UINT32      *pSize,
    UINT32      ipAddress,
    UINT16      port)
{
    struct sockaddr_in destAddr;
    ssize_t sendSize    = 0;
    size_t  size        = 0;

    if (sock == -1 || pBuffer == NULL || pSize == NULL)
    {
        vos_printLog(VOS_LOG_WARNING, "param err.sock:%d,pbuffer is %s,pSize is %s\n",
        		sock,pBuffer == NULL?"null":"not null" , pSize == NULL?"null":"not null");
        return VOS_PARAM_ERR;
    }

    size    = *pSize;
    *pSize  = 0;

    /*      We send UDP packets to the address  */
    memset(&destAddr, 0, sizeof(destAddr));
    destAddr.sin_family         = AF_INET;
    destAddr.sin_addr.s_addr    = vos_htonl(ipAddress);
    destAddr.sin_port           = vos_htons(port);

    do
    {
        /*errno       = 0;*/
        sendSize = sendto(sock,
                          (const char *)pBuffer,
                          size,
                          0,
                          (struct sockaddr *) &destAddr,
                          sizeof(destAddr));

        if (sendSize >= 0)
        {
            *pSize += (UINT32) sendSize;
        }

        if (sendSize == -1 && errno == EWOULDBLOCK)
        {
            vos_printLog(VOS_LOG_WARNING, "sendto() to %s:%u EWOULDBLOCK,socekt:%d errno:%d\n",
                    inet_ntoa(destAddr.sin_addr), (unsigned int)port,sock,errno);
            return VOS_BLOCK_ERR;
        }
    }
    while (sendSize == -1 && errno == EINTR);

    if (sendSize == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_WARNING, "sendto() to %s:%u failed (Err: %s),socekt:%d errno:%d\n",
                     inet_ntoa(destAddr.sin_addr), (unsigned int)port, buff,sock,errno);
        return VOS_IO_ERR;
    }
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/**
 *	@brief			从指定的socket接收udp包到指定buffer,同时返回包udp信息
 *	@details		-调用者需提供足够大小的buffer,否则该次调用将只拷贝buffer大小的数据并将拷贝的数据长度返回到pSize，为完整地接收数据，调用者需要重复调用此函数直到pSize返回0
 *					-若socket创建时配置了阻塞，该函数将阻塞直到有包接收、socket被关闭或出现错误中断
 *					-若定义了无阻塞，当无包接收时该函数将立即返回VOS_NODATA_ERR
 *					-若提供了指针，将返回源和目的Ip及源端口号
 *  @param[in]      sock            socket descriptor
 *  @param[out]     pBuffer         pointer to applications data buffer
 *  @param[in,out]  pSize           pointer to the received data size
 *  @param[out]     pSrcIPAddr      pointer to source IP
 *  @param[out]     pSrcIPPort      pointer to source port
 *  @param[out]     pDstIPAddr      pointer to dest IP
 *  @param[in]      peek            if true, leave data in queue
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      data could not be read
 *  @retval         VOS_NODATA_ERR  no data
 *  @retval         VOS_BLOCK_ERR   Call would have blocked in blocking mode
 */

EXT_DECL VOS_ERR_T vos_sockReceiveUDP (
    SOCKET  sock,
    UINT8   *pBuffer,
    UINT32  *pSize,
    UINT32  *pSrcIPAddr,
    UINT16  *pSrcIPPort,
    UINT32  *pDstIPAddr,
    BOOL8   peek,
	UINT8	*precvIfindex)
{

    /*- 必要参数不存在 */
    if (sock == -1 || pBuffer == NULL || pSize == NULL)
    {
        return VOS_PARAM_ERR;
    }

	ssize_t				rcvSize = 0;

    union
    {
        struct cmsghdr  cm;
        char            raw[32];
    } control_un;

    struct msghdr       msg;//@var udp通信信息结构体,用于获取本次接收的各种信息

    struct sockaddr_in  srcAddr;//@var 用于获取发送方地址
    socklen_t           sockLen = sizeof(srcAddr);

    struct iovec        iov;//@var 数据向量,用于提供buffer地址和长度

    iov.iov_base    = pBuffer;
    iov.iov_len     = *pSize;

    struct cmsghdr      *cmsg;

    /*- 清空udp通信信息结构体和控制结构体 */
    memset(&msg, 0, sizeof(msg));
    memset(&control_un, 0, sizeof(control_un));

    /*- 填充udp通信信息结构体 */
    msg.msg_iov         = &iov;//数据向量
    msg.msg_iovlen      = 1;
    msg.msg_name        = &srcAddr;//保存发送方地址
    msg.msg_namelen     = sockLen;
    msg.msg_control     = &control_un.cm;//传入可配控制符
    msg.msg_controllen  = sizeof(control_un);

    /*- 初始化返回值 */
    *pSize = 0;

    do
    {
        rcvSize = recvmsg(sock, &msg, (peek != 0) ? MSG_PEEK : 0);

        /*- 接收成功 */
        if (rcvSize != -1)
        {
        	/*- 调用提供了用于返回destIp的指针 */
            if (pDstIPAddr != NULL)
            {
            	/*- 遍历返回的cmsghdr结构体 */
                for (cmsg = CMSG_FIRSTHDR(&msg); cmsg != NULL; cmsg = CMSG_NXTHDR(&msg, cmsg))
                {
#if defined(IP_RECVDSTADDR)
                	/*- 返回的cmsghdr为RECVDSTADDR控制符的结果 */
                    if (cmsg->cmsg_level == IPPROTO_IP && cmsg->cmsg_type == IP_RECVDSTADDR)
                    {
                    	/*- 获取cmsghdr的数据部分 */
                        struct in_addr *pia = (struct in_addr *)CMSG_DATA(cmsg);

                        /*- 提取相应destIp成员装入返回参数 */
                        *pDstIPAddr = (UINT32)vos_ntohl(pia->s_addr);
                    }
#elif defined(IP_PKTINFO)
                    /*- 返回的cmsghdr为IP_PKTINFO控制符的结果 */
                    if (cmsg->cmsg_level == SOL_IP && cmsg->cmsg_type == IP_PKTINFO)
                    {
                    	/*- 获取cmsghdr的数据部分 */
                        struct in_pktinfo *pia = (struct in_pktinfo *)CMSG_DATA(cmsg);

                        /*- 提取相应destIp成员和ifindex成员装入返回参数 */
                        *pDstIPAddr = (UINT32)vos_ntohl(pia->ipi_addr.s_addr);
                        *precvIfindex=pia->ipi_ifindex;
                    }
#endif
                }
            }

            /*- 调用提供了用于返回srcIp的指针 */
            if (pSrcIPAddr != NULL)
            {
                *pSrcIPAddr = (uint32_t) vos_ntohl(srcAddr.sin_addr.s_addr);
            }

            /*- 调用提供了用于返回源端口的指针 */
            if (pSrcIPPort != NULL)
            {
                *pSrcIPPort = (UINT16) vos_ntohs(srcAddr.sin_port);
            }
        }

        /*- 若为阻塞错误则直接返回 ??阻塞了还能从recvmsg函数里出来吗 */
        if (rcvSize == -1 && errno == EWOULDBLOCK)
        {
            return VOS_BLOCK_ERR;
        }
    }
    /*- 若为系统中断导致的接收失败则再次尝试 */
    while (rcvSize == -1 && errno == EINTR);

    /*- 接收失败 */
    if (rcvSize == -1)
    {
        if (errno == ECONNRESET)
        {
            /* 将ICMP端口不可达错误 (上一发送返回的错误码，不由本次发送产生),视为无错误 */
            return VOS_NO_ERR;
        }
        else
        {
            /*- errno对应的错误描述传入buff */
        	char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);

            /*- 打印错误信息 */
            vos_printLog(VOS_LOG_ERROR, "recvmsg() failed (Err: %s)\n", buff);
            return VOS_IO_ERR;
        }
    }
    /*- 无包接收 */
    else if (rcvSize == 0)
    {
        return VOS_NODATA_ERR;
    }
    /*- 成功接收 */
    else
    {
    	/*- 返回接收的包的长度 */
        *pSize = (UINT32) rcvSize;  /* We will not expect larger packets (max. size is 65536 anyway!) */
        return VOS_NO_ERR;
    }
}

/**********************************************************************************************************************/
/** Bind a socket to an address and port.
 *
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      ipAddress       source IP to receive on, 0 for any
 *  @param[in]      port            port to receive on, 17224 for PD
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      Input/Output error
 *  @retval         VOS_MEM_ERR     resource error
 */

EXT_DECL VOS_ERR_T vos_sockBind (
    SOCKET  sock,
    UINT32  ipAddress,
    UINT16  port)
{
    struct sockaddr_in srcAddress;

    if (sock == -1)
    {
        return VOS_PARAM_ERR;
    }

    /* Allow the socket to be bound to an address and port
        that is already in use */

    memset((char *)&srcAddress, 0, sizeof(srcAddress));
    srcAddress.sin_family       = AF_INET;
    srcAddress.sin_addr.s_addr  = vos_htonl(ipAddress);
    srcAddress.sin_port         = vos_htons(port);

    vos_printLog(VOS_LOG_INFO, "trying to bind to: %s:%hu\n",
                 inet_ntoa(srcAddress.sin_addr), port);

    /*  Try to bind the socket to the PD port. */
#if defined(linux)
    if (bind(sock, (struct sockaddr *)&srcAddress, sizeof(srcAddress)) == -1)
    {
        switch (errno)
        {
            case EADDRINUSE:
            case EINVAL:
                /* Already bound, we keep silent */
                vos_printLogStr(VOS_LOG_WARNING, "already bound!\n");
                break;
            default:
            {
                char buff[VOS_MAX_ERR_STR_SIZE];
                STRING_ERR(buff);
                vos_printLog(VOS_LOG_ERROR, "binding to %s:%hu failed (Err: %s)\n",
                             inet_ntoa(srcAddress.sin_addr), port, buff);
                return VOS_SOCK_ERR;
            }
        }
    }

#elif defined(VXWORKS)


	/*  Try to bind the socket to the PD port. */
	if (bind(sock, (struct sockaddr *)&srcAddress, sizeof(srcAddress)) == -1)
	{
		char buff[VOS_MAX_ERR_STR_SIZE];
		STRING_ERR(buff);
		vos_printLog(VOS_LOG_ERROR, "bind() failed (Err: %s)\n", buff);
		return VOS_SOCK_ERR;
	}
#endif
    return VOS_NO_ERR;

}

/**********************************************************************************************************************/
/** Listen for incoming connections.
 *
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      backlog            maximum connection attempts if system is busy
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      Input/Output error
 *  @retval         VOS_MEM_ERR     resource error
 */

EXT_DECL VOS_ERR_T vos_sockListen (
    SOCKET  sock,
    UINT32  backlog)
{
    if (sock == -1)
    {
        return VOS_PARAM_ERR;
    }
    if (listen(sock, (int) backlog) == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_ERROR, "listen() failed (Err: %s)\n", buff);
        return VOS_IO_ERR;
    }
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Accept an incoming TCP connection.
 *  Accept incoming connections on the provided socket. May block and will return a new socket descriptor when
 *  accepting a connection. The original socket *pSock, remains open.
 *
 *
 *  @param[in]      sock            Socket descriptor
 *  @param[out]     pSock           Pointer to socket descriptor, on exit new socket
 *  @param[out]     pIPAddress      source IP to receive on, 0 for any
 *  @param[out]     pPort           port to receive on, 17224 for PD
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   NULL parameter, parameter error
 *  @retval         VOS_UNKNOWN_ERR sock descriptor unknown error
 */

EXT_DECL VOS_ERR_T vos_sockAccept (
    SOCKET  sock,
    SOCKET  *pSock,
    UINT32  *pIPAddress,
    UINT16  *pPort)
{
    struct sockaddr_in srcAddress;
    int connFd = -1;

    if (pSock == NULL || pIPAddress == NULL || pPort == NULL)
    {
        return VOS_PARAM_ERR;
    }

    memset((char *)&srcAddress, 0, sizeof(srcAddress));

    srcAddress.sin_family       = AF_INET;
    srcAddress.sin_addr.s_addr  = vos_htonl(*pIPAddress);
    srcAddress.sin_port         = vos_htons(*pPort);

    for (;; )
    {
        socklen_t sockLen = sizeof(srcAddress);

        connFd = accept(sock, (struct sockaddr *) &srcAddress, &sockLen);
        if (connFd < 0)
        {
            switch (errno)
            {
                /*Accept return -1 and errno = EWOULDBLOCK,
                when there is no more connection requests.*/
                case EWOULDBLOCK:
                {
                    *pSock = connFd;
                    return VOS_NO_ERR;
                }
                case EINTR:         break;
                case ECONNABORTED:  break;
#if defined (EPROTO)
                case EPROTO:        break;
#endif
                default:
                {
                    char buff[VOS_MAX_ERR_STR_SIZE];
                    STRING_ERR(buff);
                    vos_printLog(VOS_LOG_ERROR,
                                 "accept() listenFd(%d) failed (Err: %s)\n",
                                 (int)*pSock,
                                 buff);
                    return VOS_UNKNOWN_ERR;
                }
            }
        }
        else
        {
            *pIPAddress = vos_htonl(srcAddress.sin_addr.s_addr);
            *pPort      = vos_htons(srcAddress.sin_port);
            *pSock      = connFd;
            break;         /* success */
        }
    }
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Open a TCP connection.
 *
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      ipAddress       destination IP
 *  @param[in]      port            destination port
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      Input/Output error
 */

EXT_DECL VOS_ERR_T vos_sockConnect (
    SOCKET  sock,
    UINT32  ipAddress,
    UINT16  port)
{
    struct sockaddr_in dstAddress;

    if (sock == -1)
    {
        return VOS_PARAM_ERR;
    }

    memset((char *)&dstAddress, 0, sizeof(dstAddress));
    dstAddress.sin_family       = AF_INET;
    dstAddress.sin_addr.s_addr  = vos_htonl(ipAddress);
    dstAddress.sin_port         = vos_htons(port);

    if (connect(sock, (const struct sockaddr *) &dstAddress,
                sizeof(dstAddress)) == -1)
    {
        if ((errno == EINPROGRESS)
            || (errno == EWOULDBLOCK)
            || (errno == EALREADY))
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_WARNING, "connect() problem: %s\n", buff);
            return VOS_BLOCK_ERR;
        }
        else if (errno != EISCONN)
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_WARNING, "connect() failed (Err: %s)\n", buff);
            return VOS_IO_ERR;
        }
        else
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_DBG, "connect() %d: %s\n", (int) sock, buff);
        }
    }
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Send TCP data.
 *  Send data to the supplied address and port.
 *
 *  @param[in]      sock            socket descriptor
 *  @param[in]      pBuffer         pointer to data to send
 *  @param[in,out]  pSize           In: size of the data to send, Out: no of bytes sent
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      data could not be sent
 *  @retval         VOS_NOCONN_ERR  no TCP connection
 *  @retval         VOS_BLOCK_ERR   Call would have blocked in blocking mode
 */

EXT_DECL VOS_ERR_T vos_sockSendTCP (
    SOCKET      sock,
    const UINT8 *pBuffer,
    UINT32      *pSize)
{
    ssize_t sendSize    = 0;
    size_t  bufferSize  = 0;

    if (sock == -1 || pBuffer == NULL || pSize == NULL)
    {
        return VOS_PARAM_ERR;
    }

    bufferSize  = (size_t) *pSize;
    *pSize      = 0;

    /* Keep on sending until we got rid of all data or we received an unrecoverable error */
    do
    {
        sendSize = write(sock, pBuffer, bufferSize);
        if (sendSize >= 0)
        {
            bufferSize  -= (size_t) sendSize;
            pBuffer     += sendSize;
            *pSize      += (UINT32) sendSize;
        }
        if (sendSize == -1 && errno == EWOULDBLOCK)
        {
            return VOS_BLOCK_ERR;
        }
    }
    while (bufferSize && !(sendSize == -1 && errno != EINTR));

    if (sendSize == -1)
    {
        char buff[VOS_MAX_ERR_STR_SIZE];
        STRING_ERR(buff);
        vos_printLog(VOS_LOG_WARNING, "send() failed (Err: %s)\n", buff);

        if ((errno == ENOTCONN)
            || (errno == ECONNREFUSED)
            || (errno == EHOSTUNREACH))
        {
            return VOS_NOCONN_ERR;
        }
        else
        {
            return VOS_IO_ERR;
        }
    }
    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/** Receive TCP data.
 *  The caller must provide a sufficient sized buffer. If the supplied buffer is smaller than the bytes received, *pSize
 *  will reflect the number of copied bytes and the call should be repeated until *pSize is 0 (zero).
 *  If the socket was created in blocking-mode (default), then this call will block and will only return if data has
 *  been received or the socket was closed or an error occured.
 *  If called in non-blocking mode, and no data is available, VOS_NODATA_ERR will be returned.
 *
 *  @param[in]      sock            socket descriptor
 *  @param[out]     pBuffer         pointer to applications data buffer
 *  @param[in,out]  pSize           pointer to the received data size
 *
 *  @retval         VOS_NO_ERR      no error
 *  @retval         VOS_PARAM_ERR   sock descriptor unknown, parameter error
 *  @retval         VOS_IO_ERR      data could not be read
 *  @retval         VOS_NODATA_ERR  no data
 *  @retval         VOS_BLOCK_ERR   Call would have blocked in blocking mode
 */

EXT_DECL VOS_ERR_T vos_sockReceiveTCP (
    SOCKET  sock,
    UINT8   *pBuffer,
    UINT32  *pSize)
{
    ssize_t rcvSize     = 0;
    size_t  bufferSize  = (size_t) *pSize;

    *pSize = 0;

    if (sock == -1 || pBuffer == NULL || pSize == NULL)
    {
        return VOS_PARAM_ERR;
    }

    do
    {
        rcvSize = read(sock, pBuffer, bufferSize);
        if (rcvSize > 0)
        {
            bufferSize  -= (size_t) rcvSize;
            pBuffer     += rcvSize;
            *pSize      += (UINT32) rcvSize;
            vos_printLog(VOS_LOG_DBG, "received %lu bytes (Socket: %d)\n", (unsigned long)rcvSize, (int) sock);
        }

        if (rcvSize == -1 && errno == EWOULDBLOCK)
        {
            if (*pSize == 0)
            {
                return VOS_BLOCK_ERR;
            }
            else
            {
                return VOS_NO_ERR;
            }
        }
    }
    while ((bufferSize > 0 && rcvSize > 0) || (rcvSize == -1 && errno == EINTR));

    if ((rcvSize == -1) && !(errno == EMSGSIZE))
    {
        if (errno == ECONNRESET)
        {
            return VOS_NODATA_ERR;
        }
        else
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_WARNING, "receive() failed (Err: %s)\n", buff);
            return VOS_IO_ERR;
        }
    }
    else if (*pSize == 0)
    {
        if (errno == EMSGSIZE)
        {
            return VOS_MEM_ERR;
        }
        else
        {
            return VOS_NODATA_ERR;
        }
    }
    else
    {
        return VOS_NO_ERR;
    }
}

/**********************************************************************************************************************/
/** Set Using Multicast I/F
 *
 *  @param[in]      sock                        socket descriptor
 *  @param[in]      mcIfAddress                 using Multicast I/F Address
 *
 *  @retval         VOS_NO_ERR                  no error
 *  @retval         VOS_PARAM_ERR               sock descriptor unknown, parameter error
 *  @retval         VOS_SOCK_ERR                option not supported
 */
EXT_DECL VOS_ERR_T vos_sockSetMulticastIf (
    SOCKET  sock,
    UINT32  mcIfAddress)
{
    struct sockaddr_in  multicastIFAddress;
    VOS_ERR_T           result = VOS_NO_ERR;

    if (sock == -1)
    {
        result = VOS_PARAM_ERR;
    }
    else
    {
        /* Multicast I/F setting */
        memset((char *)&multicastIFAddress, 0, sizeof(multicastIFAddress));
        multicastIFAddress.sin_addr.s_addr = vos_htonl(mcIfAddress);

        if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, &multicastIFAddress.sin_addr, sizeof(struct in_addr)) == -1)
        {
            char buff[VOS_MAX_ERR_STR_SIZE];
            STRING_ERR(buff);
            vos_printLog(VOS_LOG_WARNING, "setsockopt() IP_MULTICAST_IF failed (Err: %s)\n", buff);
            result = VOS_SOCK_ERR;
        }
        else
        {
            /*    DEBUG
                struct sockaddr_in myAddress = {0};
                socklen_t optionSize ;
                getsockopt(sock, IPPROTO_IP, IP_MULTICAST_IF, &myAddress.sin_addr, &optionSize);
            */
            result = VOS_NO_ERR;
        }
    }
    return result;
}


/**********************************************************************************************************************/
/**
 * 	@brief			根据绑定Ip策略处理srcIp,返回应绑定的Ip
 * 	@details		当前策略为，令接收组播的通道绑定0，其他情况仍绑定原srcIp
 *  @param[in]      srcIP           IP to bind to (0 = any address)
 *  @param[in]      mcGroup         MC group to join (0 = do not join)
 *  @param[in]      rcvMostly       primarily used for receiving (tbd: bind on sender, too?)
 *
 *  @retval         Address to bind to
 */
EXT_DECL VOS_IP4_ADDR_T vos_determineBindAddr ( VOS_IP4_ADDR_T  srcIP,
                                                VOS_IP4_ADDR_T  mcGroup,
                                                VOS_IP4_ADDR_T  rcvMostly)
{
    /* On Linux, binding to an interface address will prevent receiving of MCs! */
    if (vos_isMulticast(mcGroup) && rcvMostly)
    {
        return 0;
    }
    else
    {
        return srcIP;
    }
}

/**
 *	@brief			get binding port of socket
 *  @param[in]      sock                       socket descriptor
 *  @retval         -1                 			error
 */
EXT_DECL INT32 vos_getPort(INT32 socket)
{
	struct sockaddr_in sockAddr;

	INT32 port=-1;

	socklen_t nlen = (socklen_t)sizeof(sockAddr);

	if(-1!=getsockname(socket, (struct sockaddr*)&sockAddr, &nlen))
	{
		port= (INT32)ntohs(sockAddr.sin_port);
	}

	return port;
}
