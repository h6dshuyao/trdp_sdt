/**********************************************************************************************************************/
/**
 * @file            vos_mem.c
 *
 * @brief           为应用提供内存管理功能
 *
 * @details         模块有两部分：	1.基于配置的内存预分配和管理，封装了malloc()和free()
 * 									2.封装排序、拷贝、比较等库函数
 *					模块内存预分配和管理的策略概述：
 *					-初始化阶段，首先用malloc()一获取应用全部所需内存空间
 *					-定义从小到大的多种块规格，应用请求内存时，以块为单位从初始化时取得的内存空间分出部分，将块的入口提供给应用
 *					-已经提供给应用的块，模块不再拥有其入口，应用释放内存时，将块入口归还给模块
 *					-内存一旦从总的内存空间分配到块，再也不会返回，但模块可以直接提供拥有的块的入口给应用使用
 *					-由于上一条，在最坏的情况下，模块仅能为应用提供 总空间/最大块规格 个内存块
 *
 * @note            本模块源码提供了队列处理功能，当前版本trdp应用中没有用到，先删掉了
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 */

/***********************************************************************************************************************
 * INCLUDES
 */

#include <stdio.h>
#include <stddef.h>
#ifndef VXWORKS
#include <stdint.h>
#endif
#include <stdlib.h>

#include <errno.h>
#include <string.h>

#ifdef POSIX
#include <strings.h>
#include <unistd.h>
#include <pthread.h>
#endif

#ifdef ESP32
#include <pthread.h>
#endif

#ifndef PTHREAD_MUTEX_INITIALIZER
#define PTHREAD_MUTEX_INITIALIZER  0 /* Dummy */
#endif

#include "vos_types.h"
#include "vos_utils.h"
#include "vos_mem.h"
#include "vos_thread.h"
#include "vos_private.h"

/***********************************************************************************************************************
 * DEFINITIONS
 */

typedef struct memBlock
{
    UINT32          size;           /* 内存块数据部分大小 */
    struct memBlock *pNext;         /* 下一内存块指针 */
                                    /* 数据部分存放在头字段之后 */
} MEM_BLOCK_T;

typedef struct
{
    UINT32  freeSize;             /* 预分配区域剩余空间大小  @note该量表征可被分配的所有空间大小，包括剩余预分配区域和被释放的块  */
    UINT32  minFreeSize;          /* 预分配区域剩余空间历史最小值 */
    UINT32  allocCnt;             /* 已分配给应用的块数量 */
    UINT32  allocErrCnt;          /* 分配失败计数 */
    UINT32  freeErrCnt;           /* 释放失败计数 */
    UINT32  blockCnt[VOS_MEM_NBLOCKSIZES];  /* 各规格已分配块个数列表 */
    UINT32  preAlloc[VOS_MEM_NBLOCKSIZES];  /* 各规格预分配块个数列表 */

} MEM_STATISTIC_T;

typedef struct
{
    struct VOS_MUTEX    mutex;          /* 锁，用来保护多线程时对gMem的操作 */
    UINT8               *pArea;         /* 预分配内存区域起始指针 */
    UINT8               *pFreeArea;     /* 剩余预分配区域起始指针 */
    UINT32              memSize;        /* 预分配内存区域总空间 */
    UINT32              allocSize;      /* 预分配内存区域已分配空间大小 @note该量表征累计分配的空间大小，即使块被释放也不会增加，因此用来计算剩余预分配区域大小 */
    UINT32              noOfBlocks;     /* 预分配块规格总个数 */
    BOOL8               wasMalloced;    /* 预分配启用标志 */

    /* @note 预分配内存块规格列表,每个单元记录了一种规格内存块的大小和位置，一共有15种规格的内存块 */
    struct
    {
        UINT32      size;               /* 块规格 */
        MEM_BLOCK_T *pFirst;            /* 内存块链表头指针 */
    } freeBlock[VOS_MEM_NBLOCKSIZES];

    MEM_STATISTIC_T memCnt;             /* 动态统计量结构体 */
} MEM_CONTROL_T;

/***********************************************************************************************************************
 *  LOCALS
 */

static MEM_CONTROL_T gMem =
{
    {0, PTHREAD_MUTEX_INITIALIZER}, /*锁 */
	NULL, NULL, 0L, 0L, 0L,/* 预分配内存相关指针和空间大小 */
	FALSE,/* 预分配启用标志 */
    {
        {0L, NULL}, /* 块规格,内存块链表头指针*/
		{0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL},{0L, NULL},
		{0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL}, {0L, NULL}
    },/* 预分配内存块列表*/

    {
    		0, 0, 0, 0, 0, /* 各种计数值*/
    		{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, /* 各规格已分配块个数列表 */
			VOS_MEM_PREALLOCATE/* 各规格预分配块个数列表 */
    }/* 动态统计信息结构体*/
};/* @global vos_mem模块内存管理功能全部信息 */

/***********************************************************************************************************************
 * GLOBAL FUNCTIONS
 */

/**********************************************************************************************************************/
/**
 *	@brief			根据配置初始化vos_mem模块
 *	@details		-传入非0size则执行模块初始化，即预分配内存区域和内存块，供后续的vos_memAlloc()和vos_memFree()在case2使用
 *					-传入0size则无需初始化模块，函数不进行任何操作，则gMem保留初始0值，则后续的vos_memAlloc()和vos_memFree()将采取case1
 *					-若发生任何错误则模块处在未初始化状态，后续的vos_memAlloc()和vos_memFree()将采取case1
 *					-如果配置要求的内存块大小超过总大小的一半，则不预分配内存块
 *  @param[in]      pMemoryArea        Pointer to memory area to use
 *  @param[in]      size               Size of provided memory area
 *  @param[in]      fragMem            Pointer to list of preallocated block sizes, used to fragment memory for large blocks
 *  @retval         VOS_NO_ERR         no error
 *  @retval         VOS_PARAM_ERR      parameter out of range/invalid
 *  @retval         VOS_MEM_ERR        no memory available
 *  @retval         VOS_MUTEX_ERR      no mutex available
 */

EXT_DECL VOS_ERR_T vos_memInit (
    UINT8           *pMemoryArea,
    UINT32          size,
    const UINT32    fragMem[VOS_MEM_NBLOCKSIZES])
{
	/*- 创建并初始化局部变量 */
    UINT32  i, j, max;
    UINT32  preAllocSize = 0;
    UINT32  blockSize[VOS_MEM_NBLOCKSIZES] = VOS_MEM_BLOCKSIZES;        /* @note 15种规格的块在宏定义中配置 */
    UINT8   *p[VOS_MEM_MAX_PREALLOCATE];

    for (i = 0; i < VOS_MEM_MAX_PREALLOCATE; i++)
    {
        p[i] = NULL;
    }

    /* 首先根据参数判断模块case */

    /*- case1:调用不要求预分配内存 */
    if (pMemoryArea == NULL && size == 0)
    {
        return VOS_NO_ERR;
    }
    /*- case2:调用指定了所需内存size */
    else if (size != 0)
    {
    	/*- 调用未提供内存区域 */
        if (pMemoryArea == NULL)
        {
        	/*- 在模块内请求内存并标记未来的释放工作 */
            gMem.pArea = (UINT8 *) malloc(size);
            if (gMem.pArea == NULL)
            {
                return VOS_MEM_ERR;
            }
            gMem.wasMalloced = TRUE;
        }
        /*- 调用已提供了内存区域 */
        else
        {
        	/*- 令全局内存配置获取该内存区域的位置，无需在模块内请求内存，未来的释放工作也不由模块执行 */
            gMem.pArea = pMemoryArea;
        }
    }
    /*- 调用提供了内存区域却不要求分配内存 */
    else
    {
        return VOS_PARAM_ERR;
    }

    /*- 创建锁 */
    if (vos_mutexLocalCreate(&gMem.mutex) != VOS_NO_ERR)
    {
    	/*- 将模块恢复到没有执行初始化的状态并返回错误 */
        vos_printLogStr(VOS_LOG_ERROR, "vos_memInit Mutex creation failed\n");

        if(gMem.wasMalloced)
        {
        	free(gMem.pArea);
        }

        gMem.pArea = NULL;

        return VOS_MUTEX_ERR;
    }

    /* 至此，gMem的值指向case2且不再改变，继续模块初始化流程 */

    /*- 配置传递 */
    gMem.memSize = size;
    gMem.memCnt.freeSize    = size;
    gMem.memCnt.minFreeSize = size;
    gMem.pFreeArea  = gMem.pArea;

    gMem.noOfBlocks = (UINT32) VOS_MEM_NBLOCKSIZES;

    /*- 若传入了预分配块配置 */
    if (fragMem != NULL)
    {
    	/*- 传入的配置覆盖全局预分配块个数列表 */
        for (i = 0; i < (UINT32) VOS_MEM_NBLOCKSIZES; i++)
        {
            gMem.memCnt.preAlloc[i] = fragMem[i];
        }
    }
    else
    {
    	/*- 全局预分配块个数列表保留默认值 */
    }

    /*- 计算预分配内存大小总值 */
    for (i = 0; i < (UINT32) VOS_MEM_NBLOCKSIZES; i++)
    {
    	preAllocSize += gMem.memCnt.preAlloc[i] * blockSize[i];
    }

    /*- 如果预分配所需的内存超过了要求总内存的一半 */
    if (preAllocSize > size / 2)
    {
    	/*- 清空全局预分配列表 */
        for (i = 0; i < (UINT32) VOS_MEM_NBLOCKSIZES; i++)
        {
            gMem.memCnt.preAlloc[i] = 0;
        }
        vos_printLogStr(VOS_LOG_INFO, "vos_memInit() Pre-Allocation disabled\n");
    }

    preAllocSize = 0;

    /*- 初始化预分配内存块 */
    for (i = 0; i < (UINT32) VOS_MEM_NBLOCKSIZES; i++)
    {
        gMem.freeBlock[i].size  = blockSize[i];
        max = gMem.memCnt.preAlloc[i];
        preAllocSize += blockSize[i];

        if (max > VOS_MEM_MAX_PREALLOCATE)
        {
            max = VOS_MEM_MAX_PREALLOCATE;
        }

        for (j = 0; j < max; j++)
        {
            p[j] = vos_memAlloc(blockSize[i]);
            if (p[j] == NULL)
            {
                vos_printLog(VOS_LOG_ERROR,
                             "vos_memInit() Pre-Allocation size exceeds overall memory size!!! (%u > %u)\n",
							 preAllocSize,
                             size);
                break;
            }
        }

        for (j = 0; j < max; j++)
        {
            if (p[j])
            {
                vos_memFree(p[j]);
            }
        }
    }

    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/**
 *	@brief			释放预分配内存
 *	@note			-要求传入的指针必须是vos模块预分配内存的指针
 *					-本函数需在进程中止的最后一步调用
 *					-即使传入错误参数，本函数依然以相同的方式执行全部功能，可以考虑不需要参数??）
 *  @param[in]      pMemoryArea        Pointer to memory area used
 */

EXT_DECL void vos_memDelete (
    UINT8 *pMemoryArea)
{
	/*- 指定的释放区域错误 */
    if (pMemoryArea != NULL && pMemoryArea != gMem.pArea)
    {
        vos_printLogStr(VOS_LOG_ERROR, "vos_memDelete() ERROR wrong pointer/parameter\n");
    }

    /* 初始化了全局内存配置锁 */
    if (gMem.mutex.magicNo != 0)
    {
    	/* 删除这个锁 */
        vos_mutexLocalDelete(&gMem.mutex);
    }

    /* 初始化时由模块预分配了内存 */
    if (gMem.wasMalloced && gMem.pArea != NULL)
    {
    	/* 释放预分配的内存 */
        free(gMem.pArea);
    }

    /* 清空全局内存配置 */
    memset(&gMem, 0, sizeof(gMem));
}

/**********************************************************************************************************************/
/**
 *	@brief			分配一块大于等于所请求size的内存区域，返回区域指针
 *	@details		-根据vos_mem模块初始化时的配置，case2:使用初始化时预分配的内存或case1:直接封装malloc（）
 *					-为返回的内存块清零
 *  @param[in]      size            Size of requested block
 *
 *  @retval         非NULL	成功
 *  @retval         NULL：	-size=0的无效调用
 *  						-vos_mem模块初始化时预分配了内存情况下，size超过预分配块最大size的调用
 *  						-请求vos_mem模块锁失败
 *  						-没有可用的足够大的预分配内存块同时预分配内存区域也不足				-
 */

EXT_DECL UINT8 *vos_memAlloc (
    UINT32 size)
{
    UINT32      i, blockSize;
    MEM_BLOCK_T *pBlock;

    /*- 无效调用 */
    if (size == 0)
    {
        gMem.memCnt.allocErrCnt++;
        vos_printLog(VOS_LOG_ERROR, "vos_memAlloc Requested size = %u\n", size);
        return NULL;
    }

    /*- case1:vos_mem模块初始化时没有预分配内存 */
    if (gMem.memSize == 0 && gMem.pArea == NULL)
    {
    	/*- 使用标准堆内存  */
        UINT8 *p = (UINT8 *) malloc(size);

        if (p != NULL)
        {
        	/*- 清空将返回的内存区域  */
            memset(p, 0, size);
        }

        vos_printLog(VOS_LOG_DBG, "vos_memAlloc() %p, size\t%u\n", (void *) p, size);

        return p;
    }

    /*- case2:vos_mem模块初始化时预分配了内存 */

    /*- 对size向上取四的整数倍 */
    size = ((size + sizeof(UINT32) - 1) / sizeof(UINT32)) * sizeof(UINT32);

    /*- 遍历内存块规格列表查找可以容纳本次调用请求内存的最小内存块规格 */
    for (i = 0; i < gMem.noOfBlocks; i++)
    {
        if (size <= gMem.freeBlock[i].size)
        {
            break;
        }
    }

    /*- 请求的内存超限 */
    if (i >= gMem.noOfBlocks)
    {
        gMem.memCnt.allocErrCnt++;

        vos_printLog(VOS_LOG_DBG, "vos_memAlloc No block size big enough. Requested size=%d\n", size);

        return NULL;
    }

    /*- 请求全局内存配置锁失败 */
    if (vos_mutexLock(&gMem.mutex) != VOS_NO_ERR)
    {
        gMem.memCnt.allocErrCnt++;

        vos_printLogStr(VOS_LOG_DBG, "vos_memAlloc can't get semaphore\n");

        return NULL;
    }
    /*- 请求全局内存配置锁成功 */
    else
    {
    	/*- 提取当前内存块规格链表第一个block */
        blockSize   = gMem.freeBlock[i].size;
        pBlock      = gMem.freeBlock[i].pFirst;

        /*- 第一个block存在 */
        if (pBlock != NULL)
        {
            /* 当前规格block链表头指针指向链表中下一个block，也就是从链表中删除当前block */
            gMem.freeBlock[i].pFirst = pBlock->pNext;
        }
        /*- 第一个block不存在 */
        else
        {
            /*- 预分配内存区域尚足够 */
            if ((gMem.allocSize + blockSize + sizeof(MEM_BLOCK_T)) < gMem.memSize)
            {
            	/*- 从剩余预分配内存区获取一块内存块 */

                pBlock = (MEM_BLOCK_T *) gMem.pFreeArea;

                gMem.pFreeArea  = (UINT8 *) gMem.pFreeArea + (sizeof(MEM_BLOCK_T) + blockSize);

                /*- 更新已分配内存 */
                gMem.allocSize  += blockSize + sizeof(MEM_BLOCK_T);

                /*- 更新统计量 */

                gMem.memCnt.blockCnt[i]++;
            }
            /*- 预分配内存空间不足 */
            else
            {
            	/*- 继续遍历内存块规格配置列表直到查找到已有block的内存块配置 */
                while ((++i < gMem.noOfBlocks) && (pBlock == NULL))
                {
                	/*- 提取当前内存块规格链表第一个block */
                    pBlock = gMem.freeBlock[i].pFirst;

                    /*- 第一个block存在 */
                    if (pBlock != NULL)
                    {
                        vos_printLog(
                            VOS_LOG_DBG,
                            "vos_memAlloc() Used a bigger buffer size=%d asked size=%d\n",
                            gMem.freeBlock[i].size,
                            size);

                        /* 当前规格block链表头指针指向链表中下一个block，也就是从链表中删除当前block */
                        gMem.freeBlock[i].pFirst = pBlock->pNext;

                        blockSize = gMem.freeBlock[i].size;
                    }
                }
            }
        }

        /*- 释放全局内存锁 */
        if (vos_mutexUnlock(&gMem.mutex) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_DBG, "vos_mutexUnlock() failed\n");
        }

        /*- 获取内存块成功 */
        if (pBlock != NULL)
        {
            /*- 配置块头size字段 */
            pBlock->size = blockSize;

            /*- 更新统计量 */
            gMem.memCnt.freeSize -= blockSize + sizeof(MEM_BLOCK_T);

            if (gMem.memCnt.freeSize < gMem.memCnt.minFreeSize)
            {
                gMem.memCnt.minFreeSize = gMem.memCnt.freeSize;
            }

            gMem.memCnt.allocCnt++;

            /*- 清空将返回的内存 */
            memset((UINT8 *) pBlock + sizeof(MEM_BLOCK_T), 0, blockSize);

            vos_printLog(VOS_LOG_DBG,
                         "vos_memAlloc() %p, size\t%u\n",
                         (void *) ((UINT8 *) pBlock + sizeof(MEM_BLOCK_T)),
                         size);

            /*- 返回块的数据部分指针 */
            return (UINT8 *) pBlock + sizeof(MEM_BLOCK_T);
        }
        /*- 获取内存块失败 */
        else
        {
            /*- 报内存不足错误 */
            vos_printLog(VOS_LOG_ERROR, "vos_memAlloc() Not enough memory, size %u\n", size);
            gMem.memCnt.allocErrCnt++;
            return NULL;
        }
    }
}


/**********************************************************************************************************************/
/**
 *	@brief			释放指定的内存块
 *	@details		根据vos_mem模块初始化时的配置，case2:为初始化时预分配的内存处理释放或case1:直接封装free（）
 *	@note			函数仅通过log报错，这意味着应用释放内存失败不会有任何错误处理
 *  @param[in]      pMemBlock         Pointer to memory block to be freed
 */

EXT_DECL void vos_memFree (void *pMemBlock)
{
    UINT32      i;
    UINT32      blockSize;
    MEM_BLOCK_T *pBlock;

    /*- 无效调用 */
    if (pMemBlock == NULL)
    {
        gMem.memCnt.freeErrCnt++;
        vos_printLogStr(VOS_LOG_ERROR, "vos_memFree() ERROR NULL pointer\n");
        return;
    }

    /*- case1:vos_mem模块初始化时没有预分配内存 */
    if (gMem.memSize == 0 && gMem.pArea == NULL)
    {
        vos_printLog(VOS_LOG_DBG, "vos_memFree() %p\n", pMemBlock);
        free(pMemBlock);
        return;
    }

    /*- case2:vos_mem模块初始化时预分配了内存 */

    /*- 调用指定的内存块不在vos_mem模块预分配内存范围内 */
    if (((UINT8 *)pMemBlock < gMem.pArea) ||
        ((UINT8 *)pMemBlock >= (gMem.pArea + gMem.memSize)))
    {
        gMem.memCnt.freeErrCnt++;
        vos_printLogStr(VOS_LOG_ERROR, "vos_memFree ERROR returned memory not within allocated memory\n");
        return;
    }

    /*- 获取全局内存配置锁失败 */
    if (vos_mutexLock(&gMem.mutex) != VOS_NO_ERR)
    {
        gMem.memCnt.freeErrCnt++;

        vos_printLogStr(VOS_LOG_ERROR, "vos_memFree can't get semaphore\n");
    }
    /*- 获取全局内存配置锁成功 */
    else
    {
        /*- 获取块头信息字段指针和块size */
        pBlock      = (MEM_BLOCK_T *) ((UINT8 *) pMemBlock - sizeof(MEM_BLOCK_T));
        blockSize   = pBlock->size;

        /*- 遍历列表，查询本次调用想要释放的内存块对应的预分配内存块规格 */
        for (i = 0; i < gMem.noOfBlocks; i++)
        {
            if (blockSize == gMem.freeBlock[i].size)
            {
                break;
            }
        }

        /*- 未找到对应的预分配内存块规格 */
        if (i >= gMem.noOfBlocks)
        {
            gMem.memCnt.freeErrCnt++;

            vos_printLogStr(VOS_LOG_DBG, "vos_memFree illegal sized memory\n");
        }
        /*- 找了到对应的预分配内存块规格 */
        else
        {
        	/*- 更新统计量 */
            gMem.memCnt.freeSize += blockSize + sizeof(MEM_BLOCK_T);
            gMem.memCnt.allocCnt--;

            /*- 将释放的内存块重新加入到可用内存块链表 */
            pBlock->pNext = gMem.freeBlock[i].pFirst;
            gMem.freeBlock[i].pFirst = pBlock;

            vos_printLog(VOS_LOG_DBG, "vos_memFree() %p, size %u\n", pMemBlock, pBlock->size);

            /*- 清空block头size字段，防护用户用相同的指针反复调用vos_memFree()的情况 */
            pBlock->size = 0;
        }

        /*- 释放全局内存配置锁 */
        if (vos_mutexUnlock(&gMem.mutex) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_DBG, "vos_mutexUnlock() failed\n");
        }
    }
}



/**********************************************************************************************************************/
/**
 *	@brief			返回vos_mem模块当前的使用情况，包括已分配内存大小、剩余可用内存大小、剩余可用块数量、错误数等到各返回值参数。
 *  @param[out]     pAllocatedMemory    Pointer to allocated memory size
 *  @param[out]     pFreeMemory         Pointer to free memory size
 *  @param[out]     pMinFree            Pointer to minimal free memory size in statistics interval
 *  @param[out]     pNumAllocBlocks     Pointer to number of allocated memory blocks
 *  @param[out]     pNumAllocErr        Pointer to number of allocation errors
 *  @param[out]     pNumFreeErr         Pointer to number of free errors
 *  @param[out]     blockSize           Pointer to list of memory block sizes
 *  @param[out]     usedBlockSize       Pointer to list of used memoryblocks
 *  @retval         VOS_NO_ERR          no error
 *  @retval         VOS_INIT_ERR        module not initialised
 */

EXT_DECL VOS_ERR_T vos_memCount (
    UINT32  *pAllocatedMemory,
    UINT32  *pFreeMemory,
    UINT32  *pMinFree,
    UINT32  *pNumAllocBlocks,
    UINT32  *pNumAllocErr,
    UINT32  *pNumFreeErr,
    UINT32  blockSize[VOS_MEM_NBLOCKSIZES],
    UINT32  usedBlockSize[VOS_MEM_NBLOCKSIZES])
{
    UINT32 i;

    if (gMem.memSize == 0 && gMem.pArea == NULL)
    {
        /* normal heap memory is used */
        *pAllocatedMemory   = 0;
        *pFreeMemory        = 0;
        *pMinFree           = 0;
        *pNumAllocBlocks    = 0;
        *pNumAllocErr       = 0;
        *pNumFreeErr        = 0;

        memset(blockSize, 0, sizeof(blockSize[VOS_MEM_NBLOCKSIZES]));
        memset(usedBlockSize, 0, sizeof(usedBlockSize[VOS_MEM_NBLOCKSIZES]));
    }

    *pAllocatedMemory   = gMem.memSize;
    *pFreeMemory        = gMem.memCnt.freeSize;
    *pMinFree           = gMem.memCnt.minFreeSize;
    *pNumAllocBlocks    = gMem.memCnt.allocCnt;
    *pNumAllocErr       = gMem.memCnt.allocErrCnt;
    *pNumFreeErr        = gMem.memCnt.freeErrCnt;

    for (i = 0; i < (UINT32) VOS_MEM_NBLOCKSIZES; i++)
    {
        usedBlockSize[i]    = gMem.memCnt.blockCnt[i];
        blockSize[i]        = gMem.freeBlock[i].size;
    }

    return VOS_NO_ERR;
}

/**********************************************************************************************************************/
/*  Sorting/Searching                                                                                                 */
/**********************************************************************************************************************/

/**********************************************************************************************************************/
/** Sort an array.
 *  This is just a wrapper for the standard qsort function.
 *  @param[in,out]  pBuf            Pointer to the array to sort
 *  @param[in]      num             number of elements
 *  @param[in]      size            size of one element
 *  @param[in]      compare         Pointer to compare function
 *                                      return -n if arg1 < arg2,
 *                                      return 0  if arg1 == arg2,
 *                                      return +n if arg1 > arg2
 *                                  where n is an integer != 0
 *  @retval         none
 */

EXT_DECL void vos_qsort (
    void        *pBuf,
    UINT32      num,
    UINT32      size,
    int         (*compare)(
        const   void *,
        const   void *))
{
    qsort(pBuf, num, size, compare);
}


/**********************************************************************************************************************/
/** Binary search in a sorted array.
 *  This is just a wrapper for the standard bsearch function.
 *
 *  @param[in]      pKey            Key to search for
 *  @param[in]      pBuf            Pointer to the array to search
 *  @param[in]      num             number of elements
 *  @param[in]      size            size of one element
 *  @param[in]      compare         Pointer to compare function
 *                                      return -n if arg1 < arg2,
 *                                      return 0  if arg1 == arg2,
 *                                      return +n if arg1 > arg2
 *                                  where n is an integer != 0
 *  @retval         Pointer to found element or NULL
 */

EXT_DECL void *vos_bsearch (
    const void  *pKey,
    const void  *pBuf,
    UINT32      num,
    UINT32      size,
    int         (*compare)(
        const   void *,
        const   void *))
{
    return bsearch(pKey, pBuf, num, size, compare);/*lint !e586 why? */
}


/**********************************************************************************************************************/
/** Case insensitive string compare.
 *
 *  @param[in]      pStr1         Null terminated string to compare
 *  @param[in]      pStr2         Null terminated string to compare
 *  @param[in]      count         Maximum number of characters to compare
 *
 *  @retval         0  - equal
 *  @retval         <0 - string1 less than string 2
 *  @retval         >0 - string 1 greater than string 2
 */

EXT_DECL INT32 vos_strnicmp (
    const CHAR8 *pStr1,
    const CHAR8 *pStr2,
    UINT32      count )
{
#if (defined (WIN32) || defined (WIN64))
    return (INT32) _strnicmp((const char *)pStr1, (const char *)pStr2, (size_t) count);
#else
    return (INT32) strncasecmp((const char *)pStr1, (const char *)pStr2, (size_t) count);
#endif
}


/**********************************************************************************************************************/
/** String copy with length limitation.
 *
 *  @param[in]      pStrDst       Destination string
 *  @param[in]      pStrSrc       Null terminated string to copy
 *  @param[in]      count         Maximum number of characters to copy
 *
 *  @retval         none
 */

EXT_DECL void vos_strncpy (
    CHAR8       *pStrDst,
    const CHAR8 *pStrSrc,
    UINT32      count )
{
#if (defined (WIN32) || defined (WIN64))
    CHAR8 character = pStrDst[count];
    (void) strncpy_s((char *)pStrDst, (size_t)(count + 1), (const char *)pStrSrc, (size_t) count);
    pStrDst[count] = character;
#else
    (void) strncpy((char *)pStrDst, (const char *)pStrSrc, (size_t) count); /*lint !e920: return value not used */
#endif
}

/**********************************************************************************************************************/
/** String concatenation with length limitation.
 *
 *  @param[in]      pStrDst       Destination string
 *  @param[in]      count         Size of destination buffer
 *  @param[in]      pStrSrc       Null terminated string to append
 *
 *  @retval         none
 */

EXT_DECL void vos_strncat (
    CHAR8       *pStrDst,
    UINT32      count,
    const CHAR8 *pStrSrc)
{
#if (defined (WIN32) || defined (WIN64))
    (void) strcat_s((char *)pStrDst, (size_t) count, (const char *)pStrSrc);
#else
    (void) strncat((char *)pStrDst, (const char *)pStrSrc, (size_t) count); /*lint !e920: return value not used */
#endif
}


