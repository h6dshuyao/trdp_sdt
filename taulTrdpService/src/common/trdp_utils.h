/**********************************************************************************************************************/
/**
 * @file            trdp_utils.h
 *
 * @brief           TRDP通信支持功能
 *
 * @details			trdp协议工具函数
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 */


#ifndef TRDP_UTILS_H
#define TRDP_UTILS_H

/*******************************************************************************
 * INCLUDES
 */

#include <stdio.h>

#include "trdp_private.h"
#include "vos_utils.h"

/*******************************************************************************
 * DEFINES
 */

#define TRDP_INVALID_SOCKET_INDEX  -1

/***********************************************
 * ********************************
 * TYPEDEFS
 */

/*******************************************************************************
 * GLOBAL FUNCTIONS
 */

extern TRDP_LOG_T gDebugLevel;

void    printSocketUsage(
    TRDP_SOCKETS_T iface[]);

BOOL8   trdp_SockIsJoined (
    const TRDP_IP_ADDR_T    mcList[VOS_MAX_MULTICAST_CNT],
    TRDP_IP_ADDR_T          mcGroup);

BOOL8   trdp_SockAddJoin(
    TRDP_IP_ADDR_T mcList[VOS_MAX_MULTICAST_CNT],
    TRDP_IP_ADDR_T mcGroup);

BOOL8   trdp_SockDelJoin(
    TRDP_IP_ADDR_T mcList[VOS_MAX_MULTICAST_CNT],
    TRDP_IP_ADDR_T mcGroup);



PD_ELE_T        *trdp_queueFindComId (
    PD_ELE_T    *pHead,
    UINT32      comId);

PD_ELE_T        *trdp_findSubAddr (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *pAddr,
    UINT32              comId);


PD_ELE_T        *trdp_queueFindSubAddr (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *pAddr);

PD_ELE_T        *trdp_queueFindExistingSub (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *pAddr);

PD_ELE_T        *trdp_queueFindPubAddr (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr);

void            trdp_queueDelElement (
    PD_ELE_T    * *pHead,
    PD_ELE_T    *pDelete);

void            trdp_queueAppLast (
    PD_ELE_T    * *pHead,
    PD_ELE_T    *pNew);

void            trdp_queueInsFirst (
    PD_ELE_T    * *pHead,
    PD_ELE_T    *pNew);

#if MD_SUPPORT
MD_ELE_T    *trdp_MDqueueFindAddr (
    MD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr);

void        trdp_MDqueueDelElement (
    MD_ELE_T    * *ppHead,
    MD_ELE_T    *pDelete);

void        trdp_MDqueueAppLast (
    MD_ELE_T    * *pHead,
    MD_ELE_T    *pNew);

void        trdp_MDqueueInsFirst (
    MD_ELE_T    * *ppHead,
    MD_ELE_T    *pNew);
#endif

INT32   trdp_getCurrentMaxSocketCnt (
    TRDP_SOCK_TYPE_T    type);

void trdp_setCurrentMaxSocketCnt (
    TRDP_SOCK_TYPE_T    type,
    INT32               currentMaxSocketCnt);

void trdp_initSockets (
    TRDP_SOCKETS_T  iface[],
    UINT8           noOfEntries);

void    trdp_initUncompletedTCP (
    TRDP_APP_SESSION_T appHandle);

void    trdp_resetSequenceCounter (
    PD_ELE_T        *pElement,
    TRDP_IP_ADDR_T  srcIP,
    TRDP_MSG_T      msgType);

TRDP_IP_ADDR_T  trdp_findMCjoins (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_IP_ADDR_T      mcGroup);

TRDP_ERR_T      trdp_requestSocket(
    TRDP_SOCKETS_T iface[],
    UINT16 port,
    const TRDP_SEND_PARAM_T * params,
    TRDP_IP_ADDR_T srcIP,
    TRDP_IP_ADDR_T mcGroup,
    TRDP_SOCK_TYPE_T type,
    TRDP_OPTION_T options,
    BOOL8 rcvMostly,
    SOCKET useSocket,
    INT32                   * pIndex,
    TRDP_IP_ADDR_T cornerIp);

void trdp_releaseSocket(
    TRDP_SOCKETS_T iface[],
    INT32 lIndex,
    UINT32 connectTimeout,
    BOOL8 checkAll,
    TRDP_IP_ADDR_T mcGroupUsed);


UINT32  trdp_packetSizePD (
    UINT32 dataSize);

UINT32  trdp_packetSizePD2 (
    UINT32 dataSize);

UINT32  trdp_packetSizeMD (
    UINT32 dataSize);

int trdp_checkSequenceCounter (
    PD_ELE_T        *pElement,
    UINT32          sequenceCounter,
    TRDP_IP_ADDR_T  srcIP,
    TRDP_MSG_T      msgType);

BOOL8 trdp_isAddressed (
    const TRDP_URI_USER_T   listUri,
    const TRDP_URI_USER_T   destUri);


BOOL8 trdp_validTopoCounters (
    UINT32  etbTopoCnt,
    UINT32  opTrnTopoCnt,
    UINT32  etbTopoCntFilter,
    UINT32  opTrnTopoCntFilter);

BOOL8 trdp_isInIPrange (
    TRDP_IP_ADDR_T  receivedSrcIP,
    TRDP_IP_ADDR_T  listenedSourceIPlow,
    TRDP_IP_ADDR_T  listenedSourceIPhigh);

#endif
