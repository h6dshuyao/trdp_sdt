/**********************************************************************************************************************/
/**
 * @file            tlc_if.c
 *
 * @brief           ECN通信功能
 *
 * @details			trdp所有功能的封装
 *
 * @note            Project: TCNOpen TRDP prototype stack
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 */

/***********************************************************************************************************************
 * INCLUDES
 */

#include <string.h>

#include "trdp_if_light.h"
#include "trdp_utils.h"
#include "trdp_pdcom.h"
#include "trdp_stats.h"

#include "vos_sock.h"
#include "vos_mem.h"
#include "vos_utils.h"

#ifdef HIGH_PERF_INDEXED
#include "trdp_pdindex.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 * TYPEDEFS
 */

/***********************************************************************************************************************
 * LOCALS
 */

static TRDP_APP_SESSION_T   sSession        = NULL;/** @global trdp中的会话结构体本体，看起来是一个，实际是一个链表 */
static VOS_MUTEX_T          sSessionMutex   = NULL;/** @global tlc模块全局锁，保护多线程操作sSession */
static BOOL8 sInited = FALSE; /** @global trdp内存块初始化标志位，控制仅执行一次trdp内存块初始化 */

/******************************************************************************
 * LOCAL FUNCTIONS
 */

BOOL8               trdp_isValidSession (TRDP_APP_SESSION_T pSessionHandle);
TRDP_ERR_T          trdp_getAccess (TRDP_APP_SESSION_T  pSessionHandle, int force);
void                trdp_releaseAccess (TRDP_APP_SESSION_T pSessionHandle);

/***********************************************************************************************************************
 * GLOBAL FUNCTIONS
 */

/**********************************************************************************************************************/
/**
 *	@brief			检查appHandle有效性
 *	@details		在全局appHandle链表中查找传入的appHandle
 *  @param[in]    	pSessionHandle        待检查的appHandle
 *  @retval       TRUE                  is valid
 *  @retval       FALSE                 is invalid
 */
BOOL8    trdp_isValidSession (
    TRDP_APP_SESSION_T pSessionHandle)
{
    TRDP_SESSION_PT pSession = NULL;
    BOOL8 found = FALSE;

    /*- 检查传入的appHandle存在 */
    if (pSessionHandle == NULL)
    {
        return FALSE;
    }

    /*- 请求tlc模块全局锁（tlc模块初始化时创建） */
    if (vos_mutexLock(sSessionMutex) != VOS_NO_ERR)
    {
        return FALSE;
    }

    /*- 遍历全局appHandle链表 */
    pSession = sSession;

    while (pSession)
    {
    	/*- 传入的appHandle已加入全局appHandle链表 */
        if (pSession == (TRDP_SESSION_PT) pSessionHandle)
        {
        	/*- 标记查找到 */
            found = TRUE;
            break;
        }
        pSession = pSession->pNext;
    }

    if (vos_mutexUnlock(sSessionMutex) != VOS_NO_ERR)
    {
        vos_printLogStr(VOS_LOG_ERROR, "vos_mutexUnlock() failed\n");
    }

    return found;
}



/**********************************************************************************************************************/
/** Get mutual access to the session
 *  Take all mutexes of that session
 *
 *  @param[in]      appHandle           A handle for further calls to the trdp stack
 *  @param[in]      force               If TRUE, access the session even if we cannot get the mutex.
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_INIT_ERR
 *  @retval         TRDP_MUTEX_ERR
 */
TRDP_ERR_T  trdp_getAccess (TRDP_APP_SESSION_T appHandle, int force)
{
    TRDP_ERR_T ret = TRDP_INIT_ERR;
    VOS_ERR_T (*mutexLock)(VOS_MUTEX_T) = (force == TRUE)? vos_mutexTryLock : vos_mutexLock;

    if (appHandle != NULL)
    {
        ret = (TRDP_ERR_T) mutexLock(appHandle->mutex);
        if (ret == TRDP_NO_ERR)
        {
            /*  Wait for any ongoing communications by getting the other mutexes as well */
            ret = (TRDP_ERR_T) mutexLock(appHandle->mutexTxPD);
            if (ret == TRDP_NO_ERR)
            {
                ret = (TRDP_ERR_T) mutexLock(appHandle->mutexRxPD);
                if (ret != TRDP_NO_ERR)
                {
                    /* In case of error release the locks already taken. */
                    (void) vos_mutexUnlock(appHandle->mutexTxPD);
                    (void) vos_mutexUnlock(appHandle->mutex);
                    vos_printLog(VOS_LOG_DBG, "taking mutexRxPD failed (%d)\n", ret);
                }
            }
            else
            {
                (void) vos_mutexUnlock(appHandle->mutex);
                vos_printLog(VOS_LOG_DBG, "taking mutexTxPD failed (%d)\n", ret);
            }
        }
        else
        {
            vos_printLog(VOS_LOG_DBG, "taking mutex failed (%d)\n", ret);
        }
    }
    return ret;
}

/**********************************************************************************************************************/
/** Release access to the session
 *
 *  @param[in]      appHandle          A handle for further calls to the trdp stack
 *  @retval         realIP
 */
void  trdp_releaseAccess (TRDP_APP_SESSION_T appHandle)
{
    /* In case of an error we cannot do anything, except logging... */
    VOS_ERR_T err = vos_mutexUnlock(appHandle->mutexRxPD);
    if (err != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG, "releasing mutexRxPD failed (%d)\n", err);
    }
    err = vos_mutexUnlock(appHandle->mutexTxPD);
    if (err != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG, "releasing mutexTxPD failed (%d)\n", err);
    }
    err = vos_mutexUnlock(appHandle->mutex);
    if (err != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_DBG, "releasing mutex failed (%d)\n", err);
    }
}


/**********************************************************************************************************************/
/**
*
*  @brief			启动vos_units\vos_sock\vos_mem三个模块初始化并创建tlc模块锁
*
*  @param[in]      pPrintDebugString   调试信息打印函数的指针
*  @param[in]      pRefCon             使用说明函数指针，此处不使用
*  @param[in]      pMemConfig          内存配置结构体
*
*  @retval         TRDP_NO_ERR         no error
*  @retval         TRDP_MEM_ERR        memory allocation failed
*  @retval         TRDP_PARAM_ERR      initialization error
*/
EXT_DECL TRDP_ERR_T tlc_init (
    const TRDP_PRINT_DBG_T  pPrintDebugString,
    void                    *pRefCon,
    const TRDP_MEM_CONFIG_T *pMemConfig)
{
    TRDP_ERR_T ret = TRDP_NO_ERR;

    /*    Init memory subsystem and the session mutex */
    if (sInited == FALSE)
    {
        /*- 初始化vos_utils模块 */
        ret = (TRDP_ERR_T) vos_init(pRefCon, pPrintDebugString);

        if (ret != TRDP_NO_ERR)
        {
            vos_printLog(VOS_LOG_ERROR, "vos_init() failed (Err: %d)\n", ret);
        }
        else
        {
        	/*- 初始化vos_sock模块 */
        	ret = vos_sockInit();

            if (ret != TRDP_NO_ERR)
            {
                vos_printLog(VOS_LOG_ERROR, "vos_sockInit() failed (Err: %d)\n", ret);
            }
            else
            {
                if (pMemConfig == NULL)
                {
                    ret = (TRDP_ERR_T) vos_memInit(NULL, 0, NULL);
                }
                else
                {
                    ret = (TRDP_ERR_T) vos_memInit(pMemConfig->p, pMemConfig->size, pMemConfig->prealloc);
                }

                if (ret != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "vos_memInit() failed (Err: %d)\n", ret);
                }
                else
                {
                    ret = (TRDP_ERR_T) vos_mutexCreate(&sSessionMutex);

                    if (ret != TRDP_NO_ERR)
                    {
                        vos_printLog(VOS_LOG_ERROR, "vos_mutexCreate() failed (Err: %d)\n", ret);
                    }
                }
            }
        }

        if (ret == TRDP_NO_ERR)
        {
            sInited = TRUE;
            vos_printLog(VOS_LOG_INFO, "TRDP Stack successfully initiated\n");
        }
    }
    else
    {
        vos_printLogStr(VOS_LOG_ERROR, "TRDP already initalised\n");

        ret = TRDP_INIT_ERR;
    }

    return ret;
}

/**********************************************************************************************************************/
/** Open a session with the TRDP stack.
 *
 *  @brief		tlc_openSession 向参数pAppHandle返回一个用于将来 使用会话模块的结构体
 *	@details	根据传入的会话参数创建并配置会话结构体，并使用通用请求comid进行一次发布订阅
 *  @param[out]     pAppHandle          A handle for further calls to the trdp stack
 *  @param[in]      ownIpAddr           Own IP address, can be different for each process in multihoming systems,
 *                                      if zero, the default interface / IP will be used.
 *  @param[in]      leaderIpAddr        IP address of redundancy leader
 *  @param[in]      pMarshall           Pointer to marshalling configuration
 *  @param[in]      pPdDefault          Pointer to default PD configuration
 *  @param[in]      pMdDefault          Pointer to default MD configuration
 *  @param[in]      pProcessConfig      Pointer to process configuration
 *                                      only option parameter is used here to define session behavior
 *                                      all other parameters are only used to feed statistics
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_INIT_ERR       not yet inited
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_SOCK_ERR       socket error
 */

EXT_DECL TRDP_ERR_T tlc_openSession (
    TRDP_APP_SESSION_T              *pAppHandle,
    TRDP_IP_ADDR_T                  ownIpAddr,
    TRDP_IP_ADDR_T                  leaderIpAddr,
    const TRDP_MARSHALL_CONFIG_T    *pMarshall,
    const TRDP_PD_CONFIG_T          *pPdDefault,
    const TRDP_MD_CONFIG_T          *pMdDefault,
#ifdef TSN_PTP_SERVICE
	INT32							ptpClockId,
#endif
    const TRDP_PROCESS_CONFIG_T     *pProcessConfig)
{
    TRDP_ERR_T      ret         = TRDP_NO_ERR;
    TRDP_SESSION_PT pSession    = NULL;
    TRDP_PUB_T      dummyPubHndl    = NULL;
    TRDP_SUB_T      dummySubHandle  = NULL;

    /*- 检查入参有效性 */
    if (pAppHandle == NULL)
    {
        vos_printLogStr(VOS_LOG_ERROR, "tlc_openSession() failed\n");
        ret = TRDP_PARAM_ERR;
        return ret;
    }

    /*- 检查trdp内存块初始化情况 */
    if (sInited == FALSE)
    {
        vos_printLogStr(VOS_LOG_ERROR, "tlc_openSession() called uninitialized\n");
        ret = TRDP_INIT_ERR;
        return ret;
    }

    /*- 为临时TRDP SESSION分配内存 */
    pSession = (TRDP_SESSION_PT) vos_memAlloc(sizeof(TRDP_SESSION_T));
    if (pSession == NULL)
    {
        vos_printLogStr(VOS_LOG_ERROR, "vos_memAlloc() failed\n");
        ret = TRDP_MEM_ERR;
        return ret;
    }

#ifdef HIGH_PERF_INDEXED
    ret = trdp_indexInit(pSession);
    if (ret != TRDP_NO_ERR)
    {
        vos_printLogStr(VOS_LOG_ERROR, "trdp_indexInit() failed\n");
        return ret;
    }
#endif

    /*- 传入的实际IP和主IP填入临时TRDP_SESSION结构体 */
    pSession->realIP    = ownIpAddr;
    pSession->virtualIP = leaderIpAddr;


#ifdef TSN_PTP_SERVICE
    /*- 设置ptp时钟id */
    pSession->ptpClockId = -1;
    pSession->ptpClockId=ptpClockId;
#endif

    /*- 填充临时TRDP_SESSION结构体中的pd配置(这里的default是对于telegram而言的)，对于apphandle仅有这一个pd配置，对应的是配置文件中的pd配置 */
    pSession->pdDefault.pfCbFunction    = NULL;
    pSession->pdDefault.pRefCon         = NULL;
    pSession->pdDefault.flags           = TRDP_FLAGS_NONE;
    pSession->pdDefault.timeout         = TRDP_PD_DEFAULT_TIMEOUT;
    pSession->pdDefault.toBehavior      = TRDP_TO_SET_TO_ZERO;
    pSession->pdDefault.port            = TRDP_PD_UDP_PORT;
    pSession->pdDefault.sendParam.qos   = TRDP_PD_DEFAULT_QOS;
    pSession->pdDefault.sendParam.ttl   = TRDP_PD_DEFAULT_TTL;


    /*- 用传入的配置参数配置临时TRDP_SESSION结构体 */
    ret = tlc_configSession(pSession, pMarshall, pPdDefault, pMdDefault, pProcessConfig);
    if (ret != TRDP_NO_ERR)
    {
        vos_memFree(pSession);
        return ret;
    }

    /*- 为临时TRDP_SESSION分别创建总、发送、接收进程内锁 */
    ret = (TRDP_ERR_T) vos_mutexCreate(&pSession->mutex);
    ret += (TRDP_ERR_T) vos_mutexCreate(&pSession->mutexTxPD); /*lint !e656 Only checking for error code TRDP_NO_ERR, which is 0 */
    ret += (TRDP_ERR_T) vos_mutexCreate(&pSession->mutexRxPD); /*lint !e656 Only checking for error code TRDP_NO_ERR, which is 0 */


    if (ret != TRDP_NO_ERR)
    {
        vos_memFree(pSession);
        vos_printLogStr(VOS_LOG_ERROR, "Serious error: Creating one of the mutexes failed\n");
        return TRDP_INIT_ERR;
    }

    /*- 清空临时TRDP_SESSION中的周期控制变量 */

    vos_clearTime(&pSession->nextJob);

    /*- 获取当前时间作为临时TRDP_SESSION的启动时间 */
    vos_getTime(&pSession->initTime);

    /*- 初始化临时TRDP_SESSION的关联socket列表 */
    trdp_initSockets(pSession->ifacePD, TRDP_MAX_PD_SOCKET_CNT);

    /*- 补充设置临时TRDP_SESSION的统计量成员中的ip */
    pSession->stats.ownIpAddr       = ownIpAddr;
    pSession->stats.leaderIpAddr    = leaderIpAddr;

    /*- 补充设置临时TRDP_SESSION的统计量成员 */
    trdp_initStats(pSession);

    /*  Get a buffer to receive PD   */
    pSession->pNewFrame = (PD_PACKET_T *) vos_memAlloc(TRDP_MAX_PD_PACKET_SIZE);
    if (pSession->pNewFrame == NULL)
    {
        vos_memFree(pSession);
        vos_printLogStr(VOS_LOG_ERROR, "Out of meory!\n");
        ret = TRDP_MEM_ERR;
        return ret;
    }

    /*- 在全局锁下 */
    ret = (TRDP_ERR_T) vos_mutexLock(sSessionMutex);

    if (ret != TRDP_NO_ERR)
    {
        vos_memFree(pSession->pNewFrame);
        vos_memFree(pSession);
        vos_printLog(VOS_LOG_ERROR, "vos_mutexLock() failed (Err: %d)\n", ret);
    }
    else
    {
        /*- 临时TRDP_SESSION传给传入的apphandle */
        pSession->pNext = sSession;
        sSession        = pSession;
        *pAppHandle     = pSession;

    	/* @note 下面的操作是为了发布和订阅统计专用通道，当前版本不需要 */
#ifdef TCT_NEW_TRDP_VERSION
        unsigned int		retries;
        /* Define standard send parameters to prvent pdpublish to use tsn in case... */
        TRDP_SEND_PARAM_T   defaultParams = TRDP_PD_DEFAULT_SEND_PARAM;



        for (retries = 0; retries < TRDP_IF_WAIT_FOR_READY; retries++)
        {
            /*  Publish our statistics packet   */
            ret = tlp_publish(pSession,                 /*    our application identifier    */
                              &dummyPubHndl,            /*    our pulication identifier     */
                              NULL, NULL,
                              0u,
                              TRDP_GLOBAL_STATS_REPLY_COMID, /*    ComID to send                 */
                              0u,                       /*    local consist only            */
                              0u,                       /*    no orient/direction info      */
                              0u,                       /*    default source IP             */
                              0u,                       /*    where to send to              */
                              0u,                       /*    Cycle time in ms              */
                              0u,                       /*    not redundant                 */
                              TRDP_FLAGS_NONE,          /*    No callbacks                  */
                              &defaultParams,           /*    default qos and ttl           */
                              NULL,                     /*    这里为要发布的buffer传入null,函数执行一定失败，未来启用时应传入统计数据TRDP_STATISTICS_T  */
                              sizeof(TRDP_STATISTICS_T));
            /*- 发生TRDP_SOCK_ERR且传入的IP地址为0 */
            if ((ret == TRDP_SOCK_ERR) &&
                (ownIpAddr == VOS_INADDR_ANY))          /*  do not wait if own IP was set (but invalid)    */
            {
            	/*- 等待一段时间后再次尝试发布 */
                (void) vos_threadDelay(1000000u);
            }
            else
            {
            	/* 成功、非网络原因的错误或者传入的IP地址非0则不再尝试 */
                break;
            }
        }

        /*- 发布成功 */
        if (ret == TRDP_NO_ERR)
        {
        	/*- 传入的ProcessConfig定义了禁用统计选项 */
            if ((pProcessConfig != NULL) && ((pProcessConfig->options & TRDP_OPTION_NO_PD_STATS) != 0))
            {
            	/*- 取消统计回应消息发布 */
                ret = tlp_unpublish(pSession, dummyPubHndl);
            }
            else
            {
            	/*- 订阅对方的回复 */
                ret = tlp_subscribe(pSession,               /*    our application identifier    */
                                    &dummySubHandle,        /*    our subscription identifier   */
                                    NULL,
                                    NULL,
                                    0u,
                                    TRDP_STATISTICS_PULL_COMID, /*    ComID                         */
                                    0u,                     /*    etbtopocount: local consist only  */
                                    0u,                     /*    optrntopocount                    */
                                    0u, 0u,                 /*    Source IP filters                  */
                                    0u,                     /*    Default destination (or MC Group) */
                                    TRDP_FLAGS_NONE,        /*    packet flags                      */
                                    NULL,                   /*    default interface                    */
                                    TRDP_INFINITE_TIMEOUT,  /*    Time out in us                    */
                                    TRDP_TO_DEFAULT);       /*    delete invalid data on timeout    */
            }
        }

        if (ret == TRDP_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_INFO, "TRDP session opened successfully\n");
        }
#endif

        if (vos_mutexUnlock(sSessionMutex) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_ERROR, "vos_mutexUnlock() failed\n");
        }
    }

    return ret;
}




/**********************************************************************************************************************/
/** (Re-)configure a session.
 *	@brief 		为传入的apphandle配置marshall,pd,md,process传入的参数
 *	@details	tlc_configSession is called by openSession, but may also be called later on to change the defaults.
 *  			Only the supplied settings (pointer != NULL) will be evaluated.

 *  @param[in]      appHandle           A handle for further calls to the trdp stack
 *  @param[in]      pMarshall           Pointer to marshalling configuration
 *  @param[in]      pPdDefault          Pointer to default PD configuration
 *  @param[in]      pMdDefault          Pointer to default MD configuration
 *  @param[in]      pProcessConfig      Pointer to process configuration
 *                                      only option parameter is used here to define session behavior
 *                                      all other parameters are only used to feed statistics
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_INIT_ERR       not yet inited
 *  @retval         TRDP_PARAM_ERR      parameter error
 */
EXT_DECL TRDP_ERR_T tlc_configSession (
    TRDP_APP_SESSION_T              appHandle,
    const TRDP_MARSHALL_CONFIG_T    *pMarshall,
    const TRDP_PD_CONFIG_T          *pPdDefault,
    const TRDP_MD_CONFIG_T          *pMdDefault,
    const TRDP_PROCESS_CONFIG_T     *pProcessConfig)
{
	TRDP_ERR_T ret=TRDP_UNKNOWN_ERR;
    TRDP_SESSION_PT pSession = appHandle;

    /*- 检查传入参数的存在性 */
    if (pSession == NULL)
    {
    	ret=TRDP_PARAM_ERR;
        return ret;
    }

    /*- ProcessConfig参数存在  */
    if (pProcessConfig != NULL)
    {
    	/*- 用processConfig配置apphandle */
        pSession->option = pProcessConfig->options;
        pSession->stats.processCycle    = pProcessConfig->cycleTime;
        pSession->stats.processPrio     = pProcessConfig->priority;
        vos_strncpy(pSession->stats.hostName, pProcessConfig->hostName, TRDP_MAX_LABEL_LEN - 1);
        vos_strncpy(pSession->stats.leaderName, pProcessConfig->leaderName, TRDP_MAX_LABEL_LEN - 1);
    }
    /*- Marshall参数存在  */
    if (pMarshall != NULL)
    {
    	/*- apphandle保存Marshall配置指针 */
        pSession->marshall = *pMarshall;
    }
    /*- PDconfig参数存在  */
    if (pPdDefault != NULL)
    {
        /*-对于apphandle PD配置 优先使用传入的TRDP_SESSION中已经配置的非默认值，其次优先使用PDconfig中的有效参数 */

        if ((pSession->pdDefault.pfCbFunction == NULL) &&
            (pPdDefault->pfCbFunction != NULL))
        {
            pSession->pdDefault.pfCbFunction = pPdDefault->pfCbFunction;
        }

        if ((pSession->pdDefault.pRefCon == NULL) &&
            (pPdDefault->pRefCon != NULL))
        {
            pSession->pdDefault.pRefCon = pPdDefault->pRefCon;
        }


        if ((pSession->pdDefault.port == TRDP_PD_UDP_PORT) &&
            (pPdDefault->port != 0u))
        {
            pSession->pdDefault.port = pPdDefault->port;
        }

        if ((pSession->pdDefault.timeout == TRDP_PD_DEFAULT_TIMEOUT) &&
            (pPdDefault->timeout != 0u))
        {
            pSession->pdDefault.timeout = pPdDefault->timeout;
        }

        if ((pSession->pdDefault.toBehavior == TRDP_TO_DEFAULT) &&
            (pPdDefault->toBehavior != TRDP_TO_DEFAULT))
        {
            pSession->pdDefault.toBehavior = pPdDefault->toBehavior;
        }

        if ((pSession->pdDefault.sendParam.qos == TRDP_PD_DEFAULT_QOS) &&
            (pPdDefault->sendParam.qos != TRDP_PD_DEFAULT_QOS) &&
            (pPdDefault->sendParam.qos != 0u))
        {
            pSession->pdDefault.sendParam.qos = pPdDefault->sendParam.qos;
        }

        if ((pSession->pdDefault.sendParam.ttl == TRDP_PD_DEFAULT_TTL) &&
            (pPdDefault->sendParam.ttl != TRDP_PD_DEFAULT_TTL) &&
            (pPdDefault->sendParam.ttl != 0u))
        {
            pSession->pdDefault.sendParam.ttl = pPdDefault->sendParam.ttl;
        }

        /*-对于apphandle PD配置中的flag 在apphandle原值基础上与上PDconfig中的有效flag选项 */
        if ((pPdDefault->flags != TRDP_FLAGS_DEFAULT) &&
            (!(pPdDefault->flags & TRDP_FLAGS_NONE)))
        {
            pSession->pdDefault.flags   |= pPdDefault->flags;
            pSession->pdDefault.flags   &= ~TRDP_FLAGS_NONE;   /* clear TRDP_FLAGS_NONE */
        }
    }

    /*- 用配置好的pd参数初始化统计值 */

	pSession->stats.pd.defQos       = pSession->pdDefault.sendParam.qos;
	pSession->stats.pd.defTtl       = pSession->pdDefault.sendParam.ttl;
	pSession->stats.pd.defTimeout   = pSession->pdDefault.timeout;


    return TRDP_NO_ERR;

}





/**********************************************************************************************************************/
/** Close a session.
 *  Clean up and release all resources of that session
 *
 *  @param[in]      appHandle             The handle returned by tlc_openSession
 *
 *  @retval         TRDP_NO_ERR           no error
 *  @retval         TRDP_NOINIT_ERR       handle invalid
 *  @retval         TRDP_PARAM_ERR        handle NULL
 */

EXT_DECL TRDP_ERR_T tlc_closeSession (
    TRDP_APP_SESSION_T appHandle)
{
    TRDP_SESSION_PT pSession = NULL;
    BOOL8 found = FALSE;
    TRDP_ERR_T      ret;

    /*    Find the session    */
    if (appHandle == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    ret = (TRDP_ERR_T) vos_mutexLock(sSessionMutex);

    if (ret != TRDP_NO_ERR)
    {
        vos_printLog(VOS_LOG_ERROR, "vos_mutexLock() failed (Err: %d)\n", ret);
    }
    else
    {
        pSession = sSession;

        if (sSession == (TRDP_SESSION_PT) appHandle)
        {
            sSession    = sSession->pNext;
            found       = TRUE;
        }
        else
        {
            while (pSession)
            {
                if (pSession->pNext == (TRDP_SESSION_PT) appHandle)
                {
                    pSession->pNext = pSession->pNext->pNext;
                    found = TRUE;
                    break;
                }
                pSession = pSession->pNext;
            }
        }

        /* We can release the global session mutex after removing the session from the list */
        if (vos_mutexUnlock(sSessionMutex) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_ERROR, "vos_mutexUnlock() failed\n");
        }

        /*    At this point we removed the session from the queue    */
        if (found)
        {
            pSession = (TRDP_SESSION_PT) appHandle;

            /*    Take the session mutex to prevent someone sitting on the branch while we cut it,
                    in case we can force leaving... */
            ret = trdp_getAccess(pSession, TRUE);

            if (ret != TRDP_NO_ERR)
            {
                vos_printLog(VOS_LOG_WARNING, "trdp_getAccess() failed while closing session (%s)\n",
                                                vos_getErrorString((VOS_ERR_T)ret));
            }
            else
            {
#ifdef HIGH_PERF_INDEXED
                trdp_indexDeInit(pSession);
#endif
                /*    Release all allocated sockets and memory    */
                vos_memFree(pSession->pNewFrame);

                while (pSession->pSndQueue != NULL)
                {
                    PD_ELE_T *pNext = pSession->pSndQueue->pNext;

                    trdp_releaseSocket(appHandle->ifacePD, pSession->pSndQueue->socketIdx, 0, FALSE, VOS_INADDR_ANY);

                    if (pSession->pSndQueue->pSeqCntList != NULL)
                    {
                        vos_memFree(pSession->pSndQueue->pSeqCntList);
                    }
                    vos_memFree(pSession->pSndQueue->pFrame);

                    /*    Only close socket if not used anymore    */
                    trdp_releaseSocket(pSession->ifacePD, pSession->pSndQueue->socketIdx, 0, FALSE, VOS_INADDR_ANY);

                    vos_memFree(pSession->pSndQueue);
                    pSession->pSndQueue = pNext;
                }

                while (pSession->pRcvQueue != NULL)
                {
                    PD_ELE_T *pNext = pSession->pRcvQueue->pNext;

                    /*  UnPublish our statistics packet   */
                    /*    Only close socket if not used anymore    */
                    trdp_releaseSocket(pSession->ifacePD, pSession->pRcvQueue->socketIdx, 0, FALSE, VOS_INADDR_ANY);
                    if (pSession->pRcvQueue->pSeqCntList != NULL)
                    {
                        vos_memFree(pSession->pRcvQueue->pSeqCntList);
                    }
                    if (pSession->pRcvQueue->pFrame != NULL)
                    {
                        vos_memFree(pSession->pRcvQueue->pFrame);
                    }
                    vos_memFree(pSession->pRcvQueue);
                    pSession->pRcvQueue = pNext;
                }

                trdp_releaseAccess(pSession);

                vos_mutexDelete(pSession->mutex);
                vos_mutexDelete(pSession->mutexTxPD);
                vos_mutexDelete(pSession->mutexRxPD);

                vos_memFree(pSession);
            }

        }
        else
        {
            ret = TRDP_NOINIT_ERR;
        }
    }

    return ret;
}

/**********************************************************************************************************************/
/** Un-Initialize.
 *  Clean up and close all sessions. Mainly used for debugging/test runs. No further calls to library allowed
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_INIT_ERR       no error
 *  @retval         TRDP_MEM_ERR        TrafficStore nothing
 *  @retval         TRDP_MUTEX_ERR      TrafficStore mutex err
 */
EXT_DECL TRDP_ERR_T tlc_terminate (void)
{
    TRDP_ERR_T ret = TRDP_NO_ERR;

    if (sInited == TRUE)
    {
        /*    Close all sessions    */
        while (sSession != NULL)
        {
            TRDP_ERR_T err;

            err = tlc_closeSession(sSession);
            if (err != TRDP_NO_ERR)
            {
                /* save the error code in case of an error */
                ret = err;
                vos_printLog(VOS_LOG_ERROR, "tlc_closeSession() failed (Err: %d)\n", ret);
            }
        }

        /* Delete SessionMutex and clear static variable */
        vos_mutexDelete(sSessionMutex);
        sSessionMutex = NULL;

        /* Close stop timers, release memory  */
        vos_terminate();
        sInited = FALSE;
    }
    else
    {
        ret = TRDP_NOINIT_ERR;
    }
    return ret;
}



/**********************************************************************************************************************/
/**
 *  @brief			返回指定的app最近下一任务间隔时间、app关联的文件描述符、文件描述符号个数
 *	@details		如果PD发送队列为空，返回interval为0
 *  @param[in]      appHandle          The handle returned by tlc_openSession
 *  @param[out]     pInterval          pointer to needed interval
 *  @param[in,out]  pFileDesc          pointer to file descriptor set
 *  @param[out]     pNoDesc            pointer to put no of highest used descriptors (for select())
 *
 *  @retval         TRDP_NO_ERR        no error
 *  @retval         TRDP_NOINIT_ERR    handle invalid
 */
EXT_DECL TRDP_ERR_T tlc_getInterval (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_TIME_T         *pInterval,
    TRDP_FDS_T          *pFileDesc,
    INT32               *pNoDesc)
{
#ifdef HIGH_PERF_INDEXED
    vos_printLogStr(VOS_LOG_ERROR, "####   tlc_getInterval() is not supported when using HIGH_PERF_INDEXED!  ####\n");
    vos_printLogStr(VOS_LOG_ERROR, "####           Use tlp_getInterval()/tlm_getInterval() instead!          ####\n");
    return TRDP_NOINIT_ERR; 
#else
    TRDP_TIME_T now;
    TRDP_ERR_T  ret = TRDP_UNKNOWN_ERR;

    /*- 传入句柄有效 */
    if (trdp_isValidSession(appHandle))
    {
        if ((NULL==pInterval) || (NULL==pFileDesc) || (NULL==pNoDesc))
        {
            ret = TRDP_PARAM_ERR;
        }
        else
        {
        	/*- 请求app总锁 */
            ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutex);

            if (ret != TRDP_NO_ERR)
            {
                vos_printLogStr(VOS_LOG_DBG, "vos_mutexLock() failed\n");
            }
            else
            {
                /*- 获取当前时间 */
                vos_getTime(&now);

                /*- 更新app下一任务时间，获取关联的文件描述符、最大socket句柄 */
                trdp_pdCheckPending(appHandle, pFileDesc, pNoDesc, TRUE);

                /*- app下一任务时间有效且未超时 */
                if (timerisset(&appHandle->nextJob) &&
                    timercmp(&now, &appHandle->nextJob, <))
                {
                	/*- 返回间隔时间至pInterval */
                    vos_subTime(&appHandle->nextJob, &now);
                    *pInterval = appHandle->nextJob;
                }
                /*- app下一任务时间有效但已超时 */
                else if (timerisset(&appHandle->nextJob))
                {
                	/*- 返回0至pInterval */
                    pInterval->tv_sec   = 0u;
                    pInterval->tv_usec  = 0;
                }
                /*- app下一任务时间无效 */
                else
                {
                	/*- 返回1s至pInterval */
                    pInterval->tv_sec   = 1u;
                    pInterval->tv_usec  = 0;
                }

                if (vos_mutexUnlock(appHandle->mutex) != VOS_NO_ERR)
                {
                    vos_printLogStr(VOS_LOG_ERROR, "vos_mutexUnlock() failed\n");
                }
            }
        }
    }
    else
    {
    	ret=TRDP_PARAM_ERR;
    }
    return ret;
#endif
}

/**********************************************************************************************************************/
/**
 *	@brief			处理appHandle中所有到时的的发布和订阅
 *	@note			如果使用本函数，不要再使用tlp_process和tlm_process
 *					建议单线程使用：tlc_getInterval,vos_select_tlc_process()
 *					建议多线程使用：	-线程1 tlp_getInterval(), vos_select(), tlp_processReceive()用于PD订阅处理
 *									-线程2 周期性调用tlp_processSend()用于PD发布处理
 *									-线程3 tlm_getInterval(), vos_select(), tlm_process()用于MD处理
 *  @param[in]      appHandle          The handle returned by tlc_openSession
 *  @param[in]      pRfds              pointer to set of ready descriptors
 *  @param[in,out]  pCount             pointer to number of ready descriptors
 *
 *  @retval         TRDP_NO_ERR        no error
 *  @retval         TRDP_NOINIT_ERR    handle invalid
 */
EXT_DECL TRDP_ERR_T tlc_process (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_FDS_T          *pRfds,
    INT32               *pCount)
{
#ifdef HIGH_PERF_INDEXED
    vos_printLogStr(VOS_LOG_ERROR, "####   tlc_process() is not supported when using HIGH_PERF_INDEXED!  ####\n");
    vos_printLogStr(VOS_LOG_ERROR, "#### Use tlp_processSend/tlp_processReceive()/tlm_process() instead! ####\n");
    return TRDP_NOINIT_ERR;
#else

    TRDP_ERR_T  result = TRDP_NO_ERR;
    TRDP_ERR_T  err;

    /*- 检查入参有效性 */
    if (!trdp_isValidSession(appHandle))
    {
        return TRDP_NOINIT_ERR;
    }

    /*- 获取appHandle总锁 */
    if (vos_mutexLock(appHandle->mutex) == VOS_NO_ERR)
    {
    	/*- 清空apphanle任务时间 */
        vos_clearTime(&appHandle->nextJob);

        /*- 成功获取appHandle发布锁 */
        if (vos_mutexTryLock(appHandle->mutexTxPD) == VOS_NO_ERR)
        {
        	/*- 处理发布 */
            err = trdp_pdSendQueued(appHandle);

            if (err != TRDP_NO_ERR)
            {
            	/*- 汇报并记录错误 */
                result = err;
                vos_printLog(VOS_LOG_ERROR, "trdp_pdSendQueued failed (Err: %d)\n", err);
            }

            /*- 释放appHandle发布锁 */
            if (vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR)
            {
                vos_printLogStr(VOS_LOG_ERROR, "vos_mutexUnlock() failed\n");
            }
        }
        /*- 获取appHandle发布锁失败 */
        else
        {
        	/*- 汇报并记录错误 */
        	vos_printLogStr(VOS_LOG_ERROR, "vos_mutexlock() failed\n");
        	result=TRDP_MUTEX_ERR;
        }

        /*- 成功获取appHandle订阅锁 */
        if (vos_mutexLock(appHandle->mutexRxPD) == VOS_NO_ERR)
        {
            /*- 处理订阅*/
            err = trdp_pdCheckListenSocks(appHandle, pRfds, pCount);

        	/*- 处理超时 */
            /* @note 原程序先处理超时再处理订阅，改为反过来 */
            trdp_pdHandleTimeOuts(appHandle);

            /*- 订阅失败*/
            if (err != TRDP_NO_ERR)
            {
            	/*- 汇报并记录错误 */
            	vos_printLog(VOS_LOG_ERROR, "trdp_pdCheckListenSocks() failed,error code:%d\n",err);
                result = err;
            }

            /*- 释放appHandle订阅锁 */
            if (vos_mutexUnlock(appHandle->mutexRxPD) != VOS_NO_ERR)
            {
                vos_printLogStr(VOS_LOG_DBG, "vos_mutexUnlock() failed\n");
            }
        }
        /*- 获取appHandle订阅锁失败 */
        else
        {
        	/*- 汇报并记录错误 */
        	vos_printLogStr(VOS_LOG_ERROR, "vos_mutexlock() failed\n");
        	result=TRDP_MUTEX_ERR;
        }

        /*- 释放appHandle总锁 */
        if (vos_mutexUnlock(appHandle->mutex) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_ERROR, "vos_mutexUnlock() failed\n");
        }

    }
    /*- 获取appHandle总锁失败 */
    else
    {
    	result=TRDP_NOINIT_ERR;
    }

    /*- 返回最后一个错误码 */
    return result;

#endif
}








#ifdef __cplusplus
}
#endif
