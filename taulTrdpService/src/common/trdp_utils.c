/**********************************************************************************************************************/
/**
 * @file            trdp_utils.c
 *
 * @brief           TRDP通信支持功能
 *
 * @details			trdp协议工具函数
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 */

/***********************************************************************************************************************
 * INCLUDES
 */

#include <string.h>

#include "trdp_utils.h"
#include "trdp_types.h"


#ifdef SOA_SUPPORT
#include "trdp_serviceRegistry.h"
#endif

/***********************************************************************************************************************
 * DEFINES
 */

/* 以下宏定义在trdp_serviceRegistry.h中定义，如果不支持（SOA_SUPPORT）则不关心serviceId */
#ifndef SOA_SAME_SERVICEID_OR0
#define SOA_SAME_SERVICEID_OR0(a,b) TRUE
#endif

#ifndef SOA_SAME_SERVICEID
#define SOA_SAME_SERVICEID(a,b) TRUE
#endif

#define SAME_SERVICE_COM_ID(a,b)    (((a).comId == (b).comId) && SOA_SAME_SERVICEID_OR0((a).serviceId,(b).serviceId))

/***********************************************************************************************************************
 * TYPEDEFS
 */

/***********************************************************************************************************************
 *   Locals
 */
static INT32 sCurrentMaxPDSocketCnt = 0;/* @global 使用的PDsocket计数 */
static INT32 sCurrentMaxMDSocketCnt = 0;/* @global 使用的MDsocket计数 */

/***********************************************************************************************************************
 *   Local Functions
 */

void    printSocketUsage (TRDP_SOCKETS_T iface[]);
void	printOneSocketUsage (TRDP_SOCKETS_T iface[],INT32 lIndex);
BOOL8   trdp_SockIsJoined (const TRDP_IP_ADDR_T mcList[VOS_MAX_MULTICAST_CNT],
                           TRDP_IP_ADDR_T       mcGroup);
BOOL8   trdp_SockAddJoin (TRDP_IP_ADDR_T    mcList[VOS_MAX_MULTICAST_CNT],
                          TRDP_IP_ADDR_T    mcGroup);
BOOL8   trdp_SockDelJoin (TRDP_IP_ADDR_T    mcList[VOS_MAX_MULTICAST_CNT],
                          TRDP_IP_ADDR_T    mcGroup);
TRDP_IP_ADDR_T  trdp_getOwnIP (void);

/**********************************************************************************************************************/
/**
 *	@brief			打印socket信息结构体列表中的所有socket信息到LOG（DEBUG级）
 *  @param[in]      iface            List of sockets
 *
 */
void printSocketUsage (
    TRDP_SOCKETS_T iface[])
{
    INT32 lIndex = 0;
    vos_printLogStr(VOS_LOG_DBG, "------- Socket usage -------\n");
    for (lIndex = 0; lIndex < trdp_getCurrentMaxSocketCnt(iface[0].type); lIndex++)
    {
        if (iface[lIndex].sock == -1)
        {
            continue;
        }
        vos_printLog(VOS_LOG_DBG, "iface[%d].sock = %d\n", lIndex, (int) iface[lIndex].sock);
        vos_printLog(VOS_LOG_DBG, "iface[%d].bindAddr = %x\n", lIndex, iface[lIndex].bindAddr);
        vos_printLog(VOS_LOG_DBG, "iface[%d].type = %s \n", lIndex, (iface[lIndex].type == TRDP_SOCK_PD ? "PD_UDP" :
                                                                     (iface[lIndex].type == TRDP_SOCK_MD_UDP ? "MD_UDP" :
                                                                      (iface[lIndex].type == TRDP_SOCK_MD_TCP ? "MD_TCP" : "PD_TSN"))));
        vos_printLog(VOS_LOG_DBG,
                     "iface[%d].sendParam.qos = %u, ttl = %u\n",
                     lIndex,
                     (unsigned) iface[lIndex].sendParam.qos,
                     (unsigned) iface[lIndex].sendParam.ttl);
        vos_printLog(VOS_LOG_DBG, "iface[%d].rcvMostly = %u\n", lIndex, (unsigned) iface[lIndex].rcvMostly);
        vos_printLog(VOS_LOG_DBG, "iface[%d].usrNum = %d\n", lIndex, iface[lIndex].usage);
    }
    vos_printLogStr(VOS_LOG_DBG, "----------------------------\n\n");
}

/**********************************************************************************************************************/
/**
 *	@brief			打印socket信息结构体列表中的一个socket信息到LOG（DEBUG级）
 *  @param[in]      iface            List of sockets
 *
 */
void printOneSocketUsage (TRDP_SOCKETS_T iface[],INT32 lIndex)
{
    vos_printLogStr(VOS_LOG_DBG, "------- Socket usage -------\n");

    if (iface[lIndex].sock == -1)
    {
        return;
    }
    vos_printLog(VOS_LOG_DBG, "iface[%d].sock = %d\n", lIndex, (int) iface[lIndex].sock);
    vos_printLog(VOS_LOG_DBG, "iface[%d].bindAddr = %x\n", lIndex, iface[lIndex].bindAddr);
    vos_printLog(VOS_LOG_DBG, "iface[%d].type = %s \n", lIndex, (iface[lIndex].type == TRDP_SOCK_PD ? "PD_UDP" :
                                                                 (iface[lIndex].type == TRDP_SOCK_MD_UDP ? "MD_UDP" :
                                                                  (iface[lIndex].type == TRDP_SOCK_MD_TCP ? "MD_TCP" : "PD_TSN"))));
    vos_printLog(VOS_LOG_DBG,
                 "iface[%d].sendParam.qos = %u, ttl = %u\n",
                 lIndex,
                 (unsigned) iface[lIndex].sendParam.qos,
                 (unsigned) iface[lIndex].sendParam.ttl);
    vos_printLog(VOS_LOG_DBG, "iface[%d].rcvMostly = %u\n", lIndex, (unsigned) iface[lIndex].rcvMostly);
    vos_printLog(VOS_LOG_DBG, "iface[%d].usrNum = %d\n", lIndex, iface[lIndex].usage);

    vos_printLogStr(VOS_LOG_DBG, "----------------------------\n\n");
}

/**********************************************************************************************************************/
/**
 *	@brief			检查传入的组播地址是否在传入的组播地址数组中
 *  @param[in]      mcList              List of multicast groups
 *  @param[in]      mcGroup             multicast group
 *
 *  @retval         1           if found
 *                  0           if not found
 */
BOOL8 trdp_SockIsJoined (
    const TRDP_IP_ADDR_T    mcList[VOS_MAX_MULTICAST_CNT],
    TRDP_IP_ADDR_T          mcGroup)
{
    int i = 0;

    for (i = 0; i < VOS_MAX_MULTICAST_CNT && mcList[i] != mcGroup; i++)
    {
        ;
    }

    return i < VOS_MAX_MULTICAST_CNT;
}

/**********************************************************************************************************************/
/**
 *	@brief			将传入的组播地址不重复地加入到传入的组播地址数组中
 *  @param[in]      mcList          List of multicast groups
 *  @param[in]      mcGroup         multicast group
 *
 *  @retval         1           if added
 *                  0           if list is full
 */
BOOL8 trdp_SockAddJoin (
    TRDP_IP_ADDR_T  mcList[VOS_MAX_MULTICAST_CNT],
    TRDP_IP_ADDR_T  mcGroup)
{
    int i = 0;

    for (i = 0; i < VOS_MAX_MULTICAST_CNT; i++)
    {
        if (0 == mcList[i] || mcGroup == mcList[i])
        {
            mcList[i] = mcGroup;
            return TRUE;
        }
    }

    return FALSE;
}

/**********************************************************************************************************************/
/**
 *	@brief			将传入的组播地址从传入的组播地址数组中删除
 *  @param[in]      mcList        List of multicast groups
 *  @param[in]      mcGroup         multicast group
 *
 *  @retval         1           if deleted
 *                  0           was not in list
 */
BOOL8 trdp_SockDelJoin (
    TRDP_IP_ADDR_T  mcList[VOS_MAX_MULTICAST_CNT],
    TRDP_IP_ADDR_T  mcGroup)
{
    int i = 0;

    for (i = 0; i < VOS_MAX_MULTICAST_CNT; i++)
    {
        if (mcGroup == mcList[i])
        {
            mcList[i] = 0;
            return TRUE;
        }
    }

    return FALSE;
}

/***********************************************************************************************************************
 *   Globals
 */

/**
 * @brief	返回指定类型的trdp创建的总socket数量
 * @details	各类型总socket数量保存在全局变量中
 */
INT32 trdp_getCurrentMaxSocketCnt (
 TRDP_SOCK_TYPE_T   type)
{
    switch (type)
    {
        case  TRDP_SOCK_PD:
        case  TRDP_SOCK_PD_TSN:
            return sCurrentMaxPDSocketCnt;
        case  TRDP_SOCK_MD_TCP:
        case  TRDP_SOCK_MD_UDP:
        default:
            break;
    }
    return sCurrentMaxMDSocketCnt;
}

/**
 * @brief	设置指定类型的trdp创建的总socket数量
 * @details	各类型总socket数量保存在全局变量中，本函数为全局变量赋值
 */
void trdp_setCurrentMaxSocketCnt (
    TRDP_SOCK_TYPE_T    type,
    INT32               currentMaxSocketCnt)
{
    switch (type)
    {
        case  TRDP_SOCK_PD:
        case  TRDP_SOCK_PD_TSN:
            sCurrentMaxPDSocketCnt = currentMaxSocketCnt;
            break;
        case  TRDP_SOCK_MD_TCP:
        case  TRDP_SOCK_MD_UDP:
        default:
            sCurrentMaxMDSocketCnt = currentMaxSocketCnt;
            break;
    }
}

/**********************************************************************************************************************/
/**
 *	@brief			检查传入的mc地址是否已经被指定app中的接收通道使用
 *  @param[in]      appHandle           the handle returned by tlc_openSession
 *  @param[in]      mcGroup             multicast group to look for
 *
 *  @retval         multi cast group if unused
 *                  VOS_INADDR_ANY if used
 */
TRDP_IP_ADDR_T trdp_findMCjoins (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_IP_ADDR_T      mcGroup)
{
    int         used = FALSE;
    PD_ELE_T    *pIter;
    for (pIter = appHandle->pRcvQueue; pIter != NULL; pIter = pIter->pNext)
    {
        if ((pIter->addr.mcGroup != VOS_INADDR_ANY) &&
            (pIter->addr.mcGroup == mcGroup))
        {
            used = TRUE;
            break;
        }
    }
    #if MD_SUPPORT
    {
        MD_LIS_ELE_T *pMDIter;
        for (pMDIter = appHandle->pMDListenQueue; pMDIter != NULL; pMDIter = pMDIter->pNext)
        {
            if ((pMDIter->addr.mcGroup != VOS_INADDR_ANY) &&
                (pMDIter->addr.mcGroup == mcGroup))
            {
                used = TRUE;
                break;
            }
        }
    }
    #endif
    return (used == TRUE) ? VOS_INADDR_ANY : mcGroup;
}

/**********************************************************************************************************************/
/**
 *	@breif			根据用户数据长度计算trdpPD包的长度，并进行四字节对齐
 *  @param[in]      dataSize            net data size (without padding)
 *
 *  @retval         packet size         the size of the complete packet to
 *                                      be sent or received
 */
UINT32 trdp_packetSizePD (
    UINT32 dataSize)
{
    UINT32 packetSize = sizeof(PD_HEADER_T) + dataSize;

    if (0 == dataSize)
    {
        /* Packet consists of header only  */
        return sizeof(PD_HEADER_T);
    }
    /*  padding to 4 */
    if ((dataSize & 0x3) > 0)
    {
        packetSize += 4 - dataSize % 4;
    }

    return packetSize;
}

#ifdef TSN_SUPPORT
/**********************************************************************************************************************/
/**
 *	@breif			根据用户数据长度计算trdpPD-TSN包的长度，并进行四字节对齐
 *  @param[in]      dataSize            net data size (without padding)
 *
 *  @retval         packet size         the size of the complete packet to
 *                                      be sent or received
 */
UINT32 trdp_packetSizePD2 (
    UINT32 dataSize)
{
    UINT32 packetSize = sizeof(PD2_HEADER_T) + dataSize;

    if (0 == dataSize)
    {
        /* Packet consists of header only  */
        return sizeof(PD2_HEADER_T);
    }
    /*  padding to 4 */
    if ((dataSize & 0x3) > 0)
    {
        packetSize += 4 - dataSize % 4;
    }

    return packetSize;
}
#endif

/**********************************************************************************************************************/
/**
 *	@breif			根据用户数据长度计算trdpMD包的长度，并进行四字节对齐
 *  @param[in]      dataSize            net data size (without padding)
 *
 *  @retval         packet size         the size of the complete packet to
 *                                      be sent or received
 */
UINT32 trdp_packetSizeMD (
    UINT32 dataSize)
{
    UINT32 packetSize = sizeof(MD_HEADER_T) + dataSize;

    if (0 == dataSize)
    {
        /* Packet consists of header only  */
        return sizeof(MD_HEADER_T);
    }
    /*  padding to 4 */
    if ((dataSize & 0x3) > 0)
    {
        packetSize += 4 - dataSize % 4;
    }

    return packetSize;
}

/**********************************************************************************************************************/
/**
 *	@brief			返回队列中指定comId的通道
 *	@note			处理拉模式回复时调用本函数
 *  @param[in]      pHead           pointer to head of queue
 *  @param[in]      comId           ComID to search for
 *
 *  @retval         != NULL         pointer to PD element
 *  @retval         NULL            No PD element found
 */
PD_ELE_T *trdp_queueFindComId (
    PD_ELE_T    *pHead,
    UINT32      comId)
{
    PD_ELE_T *iterPD;

    if (pHead == NULL)
    {
        return NULL;
    }

    for (iterPD = pHead; iterPD != NULL; iterPD = iterPD->pNext)
    {
        if (iterPD->addr.comId == comId)
        {
            return iterPD;
        }
    }
    return NULL;
}


/**********************************************************************************************************************/
/**
 *	@brief			用TRDP地址配置在发布通道链表中查询通道并返回，未找到则返回NULL
 *	@details		符合条件：
 *					-comid相同
 *					-serviceid相同
 *					-srcIp相同（或通道的Ip为任意）
 *					-destIp相同（或通道的Ip为任意）
 *					-组播Ip相同（或通道未配置组播）
 *	@note			创建发布通道检查有无重复通道时调用
 *  @param[in]      pHead           pointer to head of queue
 *  @param[in]      addr            Pub/Sub handle (Address, ComID, srcIP & dest IP, serviceId) to search for
 *
 *  @retval         != NULL         pointer to PD element
 *  @retval         NULL            No PD element found
 */
PD_ELE_T *trdp_queueFindPubAddr (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr)
{
    PD_ELE_T *iterPD;

    if (pHead == NULL || addr == NULL)
    {
        return NULL;
    }

    for (iterPD = pHead; iterPD != NULL; iterPD = iterPD->pNext)
    {
        /*  We match if src/dst/mc/service address is zero or matches */
        if ((iterPD->addr.comId == addr->comId)
            && ((iterPD->addr.srcIpAddr == 0) || (iterPD->addr.srcIpAddr == addr->srcIpAddr))
            && ((iterPD->addr.destIpAddr == 0) || (iterPD->addr.destIpAddr == addr->destIpAddr))
            && ((iterPD->addr.mcGroup == 0) || (iterPD->addr.mcGroup == addr->mcGroup))
            && SOA_SAME_SERVICEID_OR0(iterPD->addr.serviceId, addr->serviceId))
        {
            return iterPD;
        }
    }
    return NULL;
}


/**********************************************************************************************************************/
/**
 *	@brief			返回具有与传入的trdp地址配置相同的通道
 *	@details		查询通过trdp_findSubAddr()实现,屏蔽了额外指定comid
 *  @param[in]      pHead           pointer to head of queue
 *  @param[in]      addr            Pub/Sub handle (Address, ComID, srcIP & dest IP, serviceId) to search for
 *
 *  @retval         != NULL         pointer to PD element
 *  @retval         NULL            No PD element found
 */
PD_ELE_T *trdp_queueFindSubAddr (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr)
{
    return trdp_findSubAddr (pHead, addr, 0u);
}


/**********************************************************************************************************************/
/**
 *	@brief			返回具有与传入的trdp地址配置相同的通道
 *	@details		遍历通道链表，查找指定comid、srcIp、destIp的通道
 *					-额外规定comid作为匹配条件可选
 *					-查询到完全匹配的通道时中止查找，直接返回
 *					-查询到范匹配的通道时暂时保存，如果查找到多个范匹配的通道且未查找到完全匹配的通道，则最后一个范匹配的通道将被返回
 *					-若通道定义了两个srcIp,本函数视其含义为允许接收两Ip之间的srcIp,视其为完全匹配
 *	@note			在本程序中，该函数受trdp_queueFindSubAddr调用，屏蔽了另外指定comid选项，用于接收时查找包对应的接收通道
 *					修改了原程序配置两个srcIp时将通道0dest视为完全匹配的情况，这可以防止接收端配置了同comid，指定源范围的两个通道，一个指定destIp，一个不限制dest(允许发送端不知道目的Ip,可能是一个广播)，修改前返回两个通道中前面的，修改后稳定返回第一个
 *  @param[in]      pHead           通道链表头
 *  @param[in]      addr            trdp地址结构体，通道具有app内唯一（部分成员不起区分作用）的地址结构体
 *  @param[in]      comId           另外指定comId,为0时不另外指定comId
 *
 *  @retval         != NULL         pointer to PD element
 *  @retval         NULL            No PD element found
 */
PD_ELE_T *trdp_findSubAddr (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr,
    UINT32              comId)
{
    PD_ELE_T    *iterPD;
    PD_ELE_T    *pLastMatchedPD = NULL;

    /*- 空调用 */
    if (pHead == NULL || addr == NULL)
    {
        return NULL;
    }

    /*- 遍历app通道链表 */
    for (iterPD = pHead; iterPD != NULL; iterPD = iterPD->pNext)
    {
        /* 传入的comid非零且当前通道comid与之不符 */
        if ((comId != 0u) && (iterPD->addr.comId != comId))
        {
            continue;
        }

        /*- 当前通道的comid和serviceId符合传入的地址配置 */
        if (SAME_SERVICE_COM_ID(iterPD->addr, *addr))
        {
            /*- 完全匹配：当前通道的srcIp和destIp符合传入的地址配置 */
            if ((iterPD->addr.srcIpAddr == addr->srcIpAddr) &&
                (iterPD->addr.destIpAddr == addr->destIpAddr))
            {
            	/*- 返回当前通道 */
                return iterPD;
            }

            /*- 当前通道指定了第二个srcIp */
            if (iterPD->addr.srcIpAddr2 != VOS_INADDR_ANY)
            {
            	/*- 完全匹配：当前通道指定的srcIp范围和destIp符合传入的地址配置 */
                if ((addr->srcIpAddr >= iterPD->addr.srcIpAddr) &&
                    (addr->srcIpAddr <= iterPD->addr.srcIpAddr2) &&
					(iterPD->addr.destIpAddr == addr->destIpAddr))
                {
                	/*- 返回当前通道 */
                    return iterPD;
                }
            }

            /*- 范匹配：通道允许任意的srcIp接收，通道允许任意的destIp接收，或包没能获取到destIp的情况 */
            if (((iterPD->addr.srcIpAddr == VOS_INADDR_ANY) || (iterPD->addr.srcIpAddr == addr->srcIpAddr))
                && ((iterPD->addr.destIpAddr == VOS_INADDR_ANY) || (addr->destIpAddr == VOS_INADDR_ANY) ||
                    (iterPD->addr.destIpAddr == addr->destIpAddr)))
            {
            	/*- 记录当前通道 */
            	pLastMatchedPD = iterPD;
                break;
            }

            /*- 当前通道指定了第二个srcIp */
            if (iterPD->addr.srcIpAddr2 != VOS_INADDR_ANY)
            {
            	/*- 范匹配：当前通道指定的srcIp范围符合传入的地址配置，通道允许任意的destIp接收，或包没能获取到destIp的情况 */
                if ((addr->srcIpAddr >= iterPD->addr.srcIpAddr) && (addr->srcIpAddr <= iterPD->addr.srcIpAddr2)
						&& ((iterPD->addr.destIpAddr == VOS_INADDR_ANY) ||
								(addr->destIpAddr == VOS_INADDR_ANY) ||
								(iterPD->addr.destIpAddr == addr->destIpAddr)))
                {
                	/*- 记录当前通道 */
                	pLastMatchedPD = iterPD;
                    break;
                }
            }
        }
    }
    return pLastMatchedPD;
}


/**********************************************************************************************************************/
/**
 *	@brief			返回comId、servicedId、srcIp、destIp与传入的trdp地址相同的通道
 *	@details		当通道配置了srcIp2是，认为它配置了srcIp~srcIp2范围的所有srcIp
 *	@note			本函数在创建订阅通道时被调用，用于避免重复的通道配置,可以看到它的条件比trdp_findSubAddr更严格
 *					-由于当前版本将srcIp全部配置成0,因此实际判断条件为comId和destIp
 *  @param[in]      pHead           pointer to head of queue
 *  @param[in]      addr            Pub/Sub handle (Address, ComID, srcIP & dest IP) to search for
 *
 *  @retval         != NULL         pointer to PD element
 *  @retval         NULL            No PD element found
 */
PD_ELE_T *trdp_queueFindExistingSub (
    PD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr)
{
    PD_ELE_T *iterPD;

    if (pHead == NULL || addr == NULL)
    {
        return NULL;
    }

    for (iterPD = pHead; iterPD != NULL; iterPD = iterPD->pNext)
    {
        /*- comId和servicedId匹配 */
        if ((iterPD->addr.comId == addr->comId)
            && SOA_SAME_SERVICEID(iterPD->addr.serviceId, addr->serviceId)) /*lint !e506 meant to be true, if service support is off */
        {
        	/*- srcIp和destIp匹配 */
            if ((iterPD->addr.srcIpAddr == addr->srcIpAddr)
                && (iterPD->addr.destIpAddr == addr->destIpAddr))
            {
                return iterPD;
            }
            /*- srcIp被包含且destIp匹配 */
            if (iterPD->addr.srcIpAddr2 != VOS_INADDR_ANY)
            {
                if ((addr->srcIpAddr >= iterPD->addr.srcIpAddr) &&
                    (addr->srcIpAddr <= iterPD->addr.srcIpAddr2) &&
                    (iterPD->addr.destIpAddr == addr->destIpAddr))
                {
                    return iterPD;
                }
            }

        }
        else
        {

        }
    }
    return NULL;
}

/**********************************************************************************************************************/
/**
 *	@brief			从传入的trdp通道链表中删除传入的trdp通道
 *  @param[in]      ppHead          pointer to pointer to head of queue
 *  @param[in]      pDelete         pointer to element to delete
 */
void    trdp_queueDelElement (
    PD_ELE_T    * *ppHead,
    PD_ELE_T    *pDelete)
{
    PD_ELE_T *iterPD;

    if (ppHead == NULL || *ppHead == NULL || pDelete == NULL)
    {
        return;
    }

    /* handle removal of first element */
    if (pDelete == *ppHead)
    {
        *ppHead = pDelete->pNext;
        return;
    }

    for (iterPD = *ppHead; iterPD != NULL; iterPD = iterPD->pNext)
    {
        if (iterPD->pNext && iterPD->pNext == pDelete)
        {
            iterPD->pNext = pDelete->pNext;
            return;
        }
    }
}


/**********************************************************************************************************************/
/**
 * 	@brief			检查传入的本地拓扑值和待检查拓扑值是否符合
 * 	@details		当待检查拓扑值为零或等于本地拓扑值时判符合
 *  @param[in]      etbTopoCnt              ETB topography counter to be checked
 *  @param[in]      opTrnTopoCnt            Operational topography counter to be checked
 *  @param[in]      etbTopoCntFilter        ETB topography counter filter value
 *  @param[in]      opTrnTopoCntFilter      Operational topography counter filter value
 *
 *  @retval         TRUE           Filter criteria matched
 *                  FALSE          Filter criteria not matched
 */
BOOL8 trdp_validTopoCounters (
    UINT32  etbTopoCnt,
    UINT32  opTrnTopoCnt,
    UINT32  etbTopoCntFilter,
    UINT32  opTrnTopoCntFilter)
{
    if (((etbTopoCntFilter == 0) || (etbTopoCnt == etbTopoCntFilter))
        &&
        ((opTrnTopoCntFilter == 0) || (opTrnTopoCnt == opTrnTopoCntFilter)))
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

/**********************************************************************************************************************/
/**
 *	@brief			将传入的通道添加到传入的通道链表头
 *  @param[in]      ppHead          pointer to pointer to head of queue
 *  @param[in]      pNew            pointer to element to insert
 */
void    trdp_queueInsFirst (
    PD_ELE_T    * *ppHead,
    PD_ELE_T    *pNew)
{
    if (ppHead == NULL || pNew == NULL)
    {
        return;
    }

    pNew->pNext = *ppHead;
    *ppHead     = pNew;
}

/**********************************************************************************************************************/
/** @brief			初始化传入的socket清单
 *
 *  @param[in]      iface          pointer to the socket pool
 *  @param[in]      noOfEntries           entries in the socket pool
 */
void trdp_initSockets (
    TRDP_SOCKETS_T  iface[],
    UINT8           noOfEntries)
{
    UINT8 lIndex;
    /* Clear the socket pool */
    for (lIndex = 0; lIndex < noOfEntries; lIndex++)
    {
        iface[lIndex].sock = VOS_INVALID_SOCKET;
        iface[lIndex].type = TRDP_SOCK_INVAL;
    }
}

/**********************************************************************************************************************/
/**
 *  First we loop through the socket pool and check if there is already a socket
 *  which would suit us. If a multicast group should be joined, we do that on an otherwise suitable socket - up to 20
 *  multicast goups can be joined per socket.
 *  If a socket for multicast publishing is requested, we also use the source IP to determine the interface for outgoing
 *  multicast traffic.
 *	@brief			从socket池中请求一个socket
 *	@details		首先遍历socket池，如果已经存在合适的socket,为其加入组播后返回；
 *					如果应用请求一个组播发布socket，我们用sourceIp来确定用哪个网口发出组播包
 *  @param[in,out]  iface           socket池,实际是会话绑定的所有socket列表
 *  @param[in]      port            端口号
 *  @param[in]      params          发送参数，用于socket的option设置
 *  @param[in]      srcIP           源Ip
 *  @param[in]      mcGroup         组播Ip(0 = do not join)
 *  @param[in]      type            通道类型(PD, MD/UDP, MD/TCP)
 *  @param[in]      options         阻塞/非阻塞
 *  @param[in]      rcvMostly       true:订阅 false：发布
 *  @param[in]     	useSocket       -1:正常请求 已知socket:仅为socket记录请求数
 *  @param[out]     pIndex          本次请求返回的socket在socket池中的下标
 *  @param[in]      cornerIp        only used for receiving
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_PARAM_ERR
 */
TRDP_ERR_T  trdp_requestSocket (
    TRDP_SOCKETS_T          iface[],
    UINT16                  port,
    const TRDP_SEND_PARAM_T *params,
    TRDP_IP_ADDR_T          srcIP,
    TRDP_IP_ADDR_T          mcGroup,
    TRDP_SOCK_TYPE_T        type,
    TRDP_OPTION_T           options,
    BOOL8                   rcvMostly,
    SOCKET                  useSocket,
    INT32                   *pIndex,
    TRDP_IP_ADDR_T          cornerIp)
{
	/*- 创建并初始化临时变量 */

    INT32           lIndex;
    INT32           emptySockIdx = -1;
    INT32           sockMax;
    TRDP_ERR_T      err         = TRDP_UNKNOWN_ERR;

    VOS_SOCK_OPT_T  sock_options;
    memset(&sock_options, 0, sizeof(sock_options));

    TRDP_IP_ADDR_T  bindAddr    = vos_determineBindAddr(srcIP, mcGroup, rcvMostly);/* 根据绑定Ip策略处理srcIp,返回应绑定的Ip */

    /*- 入参存在检查 */
    if (iface == NULL || params == NULL || pIndex == NULL)
    {
        return TRDP_PARAM_ERR;
    }


    /* 如果找到一个可用的socket,获取它；如果请求的是一个组播socket，查找组播组列表执行需要的添加 */

    /* 遍历socket池，循环次数为trdp_unils模块申请的socket总数，这个数量大于等于socket池中有效socket的数量 */
    for (lIndex = 0; lIndex < trdp_getCurrentMaxSocketCnt(type); lIndex++)
    {
        /* case1:调用已经制定了socket,且这个socket存在于池中 */
        if (useSocket != VOS_INVALID_SOCKET &&
            useSocket == iface[lIndex].sock)
        {
            /* 返回指定的socket在池中的下标 */
            *pIndex = lIndex;
            /* 更新当前socket的请求者数量 */
            iface[lIndex].usage++;
            err = TRDP_NO_ERR;
            /* 跳转到程序末尾 */
            goto err_exit;
        }
        /* @note 总结：	1.发送和接收使用不同的socket
         * 				2.若为发送通道：不同的网口使用不同的socket；若为接收通道：所有组播共用一个socket,单播不同的网口使用不同的socket
         * 				3.其他配置检查当前版本都用不上
         */
        /*- 当前socket符合请求要求 */
        else if ((iface[lIndex].sock != VOS_INVALID_SOCKET)/* 当前socket有效 */
                 && !((mcGroup != 0u) && (bindAddr != iface[lIndex].bindAddr))  /* 若请求组播socket必须要求绑定Ip相同 */
                 && ((bindAddr == 0) || (iface[lIndex].bindAddr == bindAddr))/* 无绑定要求或绑定Ip相同 */
                 && (iface[lIndex].type == type)/* 通道类型相同 */
                 && ((rcvMostly) || (iface[lIndex].sendParam.qos == params->qos))/* 若请求发送socket必须要求qos和ttl参数相同 */
                 && ((rcvMostly) || (iface[lIndex].sendParam.ttl == params->ttl))
                 && (iface[lIndex].sendParam.tsn == params->tsn)/* tsn配置相同 */
                 && (iface[lIndex].sendParam.vlan == params->vlan)/* vlan配置相同 */
                 && (iface[lIndex].rcvMostly == rcvMostly)/* 发送/接收相同 */
                 && ((type != TRDP_SOCK_MD_TCP)/* 若为TCP-MDsocket必须要求cornerIp符合且通道独占socket */
                     || ((type == TRDP_SOCK_MD_TCP) && (iface[lIndex].tcpParams.cornerIp == cornerIp) &&
                         (iface[lIndex].usage == 0))))
        {
        	/* @note	在请求新socket时，仅为接收socket加入组播，仅为组播发送socket指定网口；
        	 * 			但在使用已存在的socket时，只要是组播都加入组播组，只要绑定了单播地址都指定网口；
        	 * 			修改上述的情况，统一为请求新socket时的处理方式
        	 */

            /*- 接收socket且需要另外加入组播  */
            if (rcvMostly && (mcGroup != 0) && (trdp_SockIsJoined(iface[lIndex].mcGroups, mcGroup) == FALSE))
            {
                /*- 执行加入组播列表失败 */
                if (trdp_SockAddJoin(iface[lIndex].mcGroups, mcGroup) == FALSE)
                {
                    continue;   /* 这是因为这个socket加入的组播到达了上限，继续遍历 */
                }
                /*- 执行加入组播列表成功 */
                else
                {
                	/*- 执行加入组播失败 */
                    if (vos_sockJoinMC(iface[lIndex].sock, mcGroup, srcIP) != VOS_NO_ERR)
                    {
                    	/*- 复原组播列表 */

                        if (trdp_SockDelJoin(iface[lIndex].mcGroups, mcGroup) == FALSE)
                        {
                            vos_printLogStr(VOS_LOG_ERROR, "trdp_SockDelJoin() failed!\n");
                        }

                        continue; /* 继续遍历 */
                    }
                    /*- 执行加入组播成功 */
                    else
                    {
                    	vos_printLog(VOS_LOG_INFO, "socket %d joined %s!\n", (int) iface[lIndex].sock, vos_ipDotted(mcGroup));
                    }
                }
            }
            else
            {

            }

            /* 绑定到了一个单播地址，就再为可能的组播发送指定网口 */
            if ((type != TRDP_SOCK_MD_TCP)
            	&&(!rcvMostly)
                && (iface[lIndex].bindAddr != 0)
                && !vos_isMulticast(iface[lIndex].bindAddr))
            {
                err = (TRDP_ERR_T) vos_sockSetMulticastIf(iface[lIndex].sock, iface[lIndex].bindAddr);
                if (err != TRDP_NO_ERR)
                {
                     vos_printLog(VOS_LOG_ERROR, "vos_sockSetMulticastIf() for UDP snd failed! (Err: %d)\n", err);
                }
            }

#ifdef MC_ISSUE_VXWORKS_2_INTERFACES_TBD
            /* 没看懂？？为啥给MD_UDP组播接收通道指定一个组播地址作为组播接收网口 */
            if ((type = TRDP_SOCK_MD_UDP)
                && (rcvMostly)
                && vos_isMulticast(srcIP))
            {
                err = (TRDP_ERR_T) vos_sockSetMulticastIf(iface[lIndex].sock, srcIP);
                if (err != TRDP_NO_ERR)
                {
                     vos_printLog(VOS_LOG_WARNING, "vos_sockSetMulticastIf() for UDP snd failed! (Err: %d)\n", err);
                }
            }
#endif

            /*- 返回当前socket所在的socket池下标 */
            *pIndex = lIndex;

            /*- 为socket更新申请者计数 */
            iface[lIndex].usage++;

            err = TRDP_NO_ERR;
            /*- 为合适的socket加入组播并指定网口后返回，已经完成所有功能，跳到函数结尾  */
            goto err_exit;
        }//池中找到符合要求的socket处理结束
        /*- socket池空了但还没记录下来 */
        else if ((iface[lIndex].sock == VOS_INVALID_SOCKET) && (emptySockIdx == -1))
        {
            /* 记录池中第一个空元素下表 */
            emptySockIdx = lIndex;
        }
    }//遍历socket池结束

    /* 函数执行到这里说明无法返回一个池中已经存在的socket,接下来新建一个 */

    /*- 获取socket数量上限*/
    switch (type)
    {
    case  TRDP_SOCK_PD:
    case  TRDP_SOCK_PD_TSN:
        sockMax = TRDP_MAX_PD_SOCKET_CNT;
        break;
    case  TRDP_SOCK_MD_TCP:
    case  TRDP_SOCK_MD_UDP:
        sockMax = TRDP_MAX_MD_SOCKET_CNT;
        break;
    default:
        sockMax = 0;
    }

    /*- trdp_unils模块申请的socket总数未到上限 */
    if (lIndex < sockMax)
    {
    	/*- 增加全局socket计数值 */
    	/* @note 这里修改了sCurrentMaxPDSocketCnt全局变量的含义,以前它的含义是单个apphandle下socket池的最大单元数 */
    	trdp_setCurrentMaxSocketCnt(type, lIndex + 1);

    	/*- 令lIndex获取首个socket池空元素下标，接下来它代表我们要创建的新socket在池中的下标 */
        if ((emptySockIdx != -1)
            && (lIndex != emptySockIdx))
        {
            lIndex = emptySockIdx;
        }

        /*- 为socket池新元素配置参数 */
        iface[lIndex].sock      = VOS_INVALID_SOCKET;
        iface[lIndex].bindAddr  = bindAddr;
        iface[lIndex].srcAddr   = srcIP;
        iface[lIndex].type      = type;
        iface[lIndex].sendParam = *params;
        iface[lIndex].rcvMostly = rcvMostly;

        /* @note 当前版本tcpParams这个字段将不会再任何地方被使用 */
        iface[lIndex].tcpParams.connectionTimeout.tv_sec    = 0;
        iface[lIndex].tcpParams.connectionTimeout.tv_usec   = 0;
        iface[lIndex].tcpParams.cornerIp    = cornerIp;
        iface[lIndex].tcpParams.sendNotOk   = FALSE;
        iface[lIndex].tcpParams.notSend     = FALSE;
        iface[lIndex].tcpParams.morituri    = FALSE;
        iface[lIndex].tcpParams.sendingTimeout.tv_sec   = 0;
        iface[lIndex].tcpParams.sendingTimeout.tv_usec  = 0;
        if (rcvMostly == TRUE)
        {
            iface[lIndex].tcpParams.addFileDesc = TRUE;
        }
        else
        {
            iface[lIndex].tcpParams.addFileDesc = FALSE;
        }

        /*- 为socket池新元素初始化动态参数 */
        iface[lIndex].usage = 0;
        memset(iface[lIndex].mcGroups, 0, sizeof(iface[lIndex].mcGroups));

        /* case2:调用已经指定了socket,但这个socket不存在于池中(不用本函数来创建socket，而是外部创建后，用本函数将socket记录到socket池，当前版本没有这种用法??考虑删除) */
        if (useSocket != VOS_INVALID_SOCKET)
        {
        	/*- 池新元素保存这个socket */
            iface[lIndex].sock  = useSocket;
            /*- 池新元素标记一个请求者 */
            iface[lIndex].usage = 1;
            /*- 返回池新元素下标 */
            *pIndex = lIndex;

            err = TRDP_NO_ERR;
            /*- 跳转到函数结尾 */
            goto err_exit;
        }

        /*- 配置socket选项 */
        sock_options.qos    = params->qos;
        sock_options.ttl    = params->ttl;
        sock_options.vlanId         = params->vlan;

        sock_options.reuseAddrPort  = (options & TRDP_OPTION_NO_REUSE_ADDR) ? FALSE : TRUE;
        sock_options.nonBlocking    = (options & TRDP_OPTION_BLOCK) ? FALSE : TRUE;

        sock_options.ttl_multicast  = (type != TRDP_SOCK_MD_TCP) ? params->ttl : 0;
        sock_options.no_mc_loop     = ((type != TRDP_SOCK_MD_TCP) && (options & TRDP_OPTION_NO_MC_LOOP_BACK)) ? 1 : 0;
        sock_options.no_udp_crc     = ((type != TRDP_SOCK_MD_TCP) && (options & TRDP_OPTION_NO_UDP_CHK)) ? 1 : 0;

        sock_options.ifName[0]      = 0;

        switch (type)
        {
#ifdef TSN_SUPPORT
            case TRDP_SOCK_PD_TSN:
                sock_options.no_udp_crc = 1;    /* To speed up UDP header generation */
                sock_options.txTime     = 1;
                sock_options.raw        = !rcvMostly; /* Raw sockets can send only!   */
#if __linux
                sock_options.reuseAddrPort = FALSE;
#endif
                err = (TRDP_ERR_T) vos_sockOpenTSN(&iface[lIndex].sock, &sock_options);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "vos_sockOpenTSN failed! (Err: %d)\n", err);
                    *pIndex = TRDP_INVALID_SOCKET_INDEX;
                }
                else
                {
                    iface[lIndex].usage = 1;
                    *pIndex = lIndex;
#ifndef SIM
                    /* @note 这片看不懂，翻译如下：
                     * 这个socket首先要绑到有着由options指定的VLAN ID的（虚拟）网口上,
                     * 具体实现取决于目标系统的组网方式
                     * planA:我们先尝试几种系统惯用的网口名:	Linux——ethx.y 其中x物理网口序号，y是VLAN ID；vlanyyy 其中y是VLAN ID扩展
													BSD(QNX,Darwin)——vlanx 其中x是虚拟网口序号(VLAN ID不在网口名中)
					 * planB:直接在系统输入指令，自己配置符合期望的网口
                     * 流程:	1. 遍历各网口，如果上述命名策略命中了,使用
                     *		2. 用planB再试一次
                     */

                    /* 我们想绑定到一个允许vlan的网口, 其ip地址必须唯一 */
                    {
                        VOS_IF_REC_T tempIF;

                        /* 用vlanId查找设备相应的网口名称,假设网口名称根据上述的命名方式 */
                        if (vos_ifnameFromVlanId(sock_options.vlanId, (CHAR8 *) sock_options.ifName) != VOS_NO_ERR)
                        {
                            /* ip地址必须唯一!
                             * !!! 注意 !!!
                           	 * 这是一个临时策略，仅用于概念验证阶段!最后必须另想一种方法来定义TSN网口
                             */

                            /* 添加设备ID （下式计算结果是“10.64.vlanId.实际设备第一个子网Ip的最后一个字段”）*/
                            VOS_IP4_ADDR_T rndIP = 0x0a400000u +
                                (unsigned) (sock_options.vlanId << 8u) +
                                (trdp_getOwnIP() & 0xFF);

                            /* 通过Linux系统指令来配TSN网口之后再尝试一次vos_ifnameFromVlanId */
                            if ((vos_createVlanIF(sock_options.vlanId, tempIF.name, rndIP) != VOS_NO_ERR) ||
                                (vos_ifnameFromVlanId(sock_options.vlanId, (CHAR8 *) sock_options.ifName) != VOS_NO_ERR))
                            {
                                /* 到这儿，我们得放弃了 - plan B也失败了 */
                                vos_printLogStr(VOS_LOG_ERROR,
                                                "Creating TSN Socket failed, vlan interface not available!\n");
                                err     = TRDP_SOCK_ERR;
                                *pIndex = TRDP_INVALID_SOCKET_INDEX;
                                break;
                            }
                        }

                        /* 到这里，策略成功了，拷贝网口名 */
                        strncpy(tempIF.name, sock_options.ifName, VOS_MAX_IF_NAME_SIZE);

                        /* 用网口名绑定特定的网口而不是用Ip, 仅为发送socket绑定 */

                        (void) vos_sockBind2IF(iface[lIndex].sock, &tempIF, sock_options.raw);

                        iface[lIndex].bindAddr = tempIF.ipAddr;
                    }
#endif
                    /* 对于接收socket,执行普通的绑定操作 */
                    if (rcvMostly)
                    {

                        err = (TRDP_ERR_T) vos_sockBind(iface[lIndex].sock, iface[lIndex].bindAddr, port);

                        if (err != TRDP_NO_ERR)
                        {
                            vos_printLog(VOS_LOG_ERROR, "vos_sockBind() for UDP rcv failed! (Err: %d)\n", err);
                            *pIndex = TRDP_INVALID_SOCKET_INDEX;
                            break;
                        }

                        if (0u != mcGroup)
                        {
                            err = (TRDP_ERR_T) vos_sockJoinMC(iface[lIndex].sock, mcGroup, iface[lIndex].bindAddr);

                            if (err != TRDP_NO_ERR)
                            {
                                vos_printLog(VOS_LOG_ERROR, "vos_sockJoinMC() for TSN rcv failed! (Err: %d)\n", err);
                                *pIndex = TRDP_INVALID_SOCKET_INDEX;
                                break;
                            }
                            else
                            {
                                if (trdp_SockAddJoin(iface[lIndex].mcGroups, mcGroup) == FALSE)
                                {
                                    vos_printLogStr(VOS_LOG_ERROR, "trdp_SockAddJoin() failed!\n");
                                }
                            }
                        }
                    }
                    else
                    {
                        ;
                    }
                }
                break;
#endif /* TSN */
            case TRDP_SOCK_MD_UDP:
                sock_options.nonBlocking = TRUE;  /* MD UDP sockets are always non blocking because they are polled */
            case TRDP_SOCK_PD:
            	/*- 创建socket */
                err = (TRDP_ERR_T) vos_sockOpenUDP(&iface[lIndex].sock, &sock_options);
                /*- 失败则返回无效下标-1 */
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "vos_sockOpenUDP failed! (Err: %d)\n", err);
                    *pIndex = TRDP_INVALID_SOCKET_INDEX;
                }
                /*- 成功 */
                else
                {
                	/*- 返回下标 */
                    iface[lIndex].usage = 1;
                    *pIndex = lIndex;

                    /*- 处理接收socket的绑定 */
                    if (rcvMostly)
                    {
                    	/*- 绑定 */
                        err = (TRDP_ERR_T) vos_sockBind(iface[lIndex].sock, iface[lIndex].bindAddr, port);

                        /*- 绑定失败则无效化返回值 */
                        if (err != TRDP_NO_ERR)
                        {
                            vos_printLog(VOS_LOG_ERROR, "vos_sockBind() for UDP rcv failed! (Err: %d)\n", err);
                            *pIndex = TRDP_INVALID_SOCKET_INDEX;
                            break;
                        }

                        /*- 若为组播接收端 */
                        if (0u != mcGroup)
                        {
                        	/*- 为socket加入组播 */
                            err = (TRDP_ERR_T) vos_sockJoinMC(iface[lIndex].sock, mcGroup, srcIP);

                            /*- 加入失败则无效化返回值 */
                            if (err != TRDP_NO_ERR)
                            {
                                vos_printLog(VOS_LOG_ERROR, "vos_sockJoinMC() for UDP rcv failed! (Err: %d)\n", err);
                                *pIndex = TRDP_INVALID_SOCKET_INDEX;
                                break;
                            }
                            else
                            {
                            	/*- 加入的组播记录到列表 */
                                if (trdp_SockAddJoin(iface[lIndex].mcGroups, mcGroup) == FALSE)
                                {
                                    vos_printLogStr(VOS_LOG_ERROR, "trdp_SockAddJoin() failed!\n");
                                }
                            }
                        }
                    }
                    /*- 处理指定了网口的发送socket的绑定 */
                    else if (iface[lIndex].bindAddr != 0)
                    {
                        (void) vos_sockBind(iface[lIndex].sock, iface[lIndex].bindAddr,0);
                    }

                    /* 若为绑定了单播的socket */
                    if (iface[lIndex].bindAddr != 0 && !vos_isMulticast(iface[lIndex].bindAddr))
                    {
                    	/* 为可能的组播发送指定网口 */
                        err = (TRDP_ERR_T) vos_sockSetMulticastIf(iface[lIndex].sock, iface[lIndex].bindAddr);
                        if (err != TRDP_NO_ERR)
                        {
                             vos_printLog(VOS_LOG_WARNING, "vos_sockSetMulticastIf() for UDP snd failed! (Err: %d)\n", err);
                             /* @note 这里就算指定失败，包也能发出去，所以就警告一下，不无效化返回值了，毕竟前面指定失败的时候原程序也不做错误处理 */
                        }
                    }

                }//创建socket成功后处理
                break;
            case TRDP_SOCK_MD_TCP:
                err = (TRDP_ERR_T) vos_sockOpenTCP(&iface[lIndex].sock, &sock_options);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "vos_sockOpenTCP() failed! (Err: %d)\n", err);
                    *pIndex = TRDP_INVALID_SOCKET_INDEX;
                }
                else
                {
                    iface[lIndex].usage = 1;
                    *pIndex = lIndex;
                }

                break;
            default:
                *pIndex = TRDP_INVALID_SOCKET_INDEX;
                err     = TRDP_SOCK_ERR;
                break;
        }//switch(type)结束

        if (err != TRDP_NO_ERR)
        {
            /* Release socket in case of error */
            trdp_releaseSocket(iface, lIndex, 0, FALSE, VOS_INADDR_ANY);
        }
    }
    /*- trdp_unils模块申请的socket总数已到上限 */
    else
    {
        err = TRDP_MEM_ERR;
    }

err_exit:

	/*- 每次请求socket都会打印池中所有socket信息没必要,改成打印当前的 */
    printOneSocketUsage(iface,lIndex);

    return err;
}

/**********************************************************************************************************************/
/** Handle the socket pool: if a received TCP socket is unused, the socket connection timeout is started.
 *  In Udp, Release a socket from our socket pool
 *  @brief			无效化一次trdp_requestSocket()
 *  @details		操作传入的池中指定的单元
					-为其减少请求者数量
					-如果传入了组播，为socket退出组播
					-如果单元已经没有请求者,释放socket并清除池单元
 *  @note			Handle the socket pool: if a received TCP socket is unused, the socket connection timeout is started.
 *  In Udp, Release a socket from our socket pool
 *  @param[in,out]  iface           socket pool
 *  @param[in]      lIndex          index of socket to release
 *  @param[in]      connectTimeout  time out
 *  @param[in]      checkAll        release all TCP pending sockets
 *  @param[in]      mcGroupUsed     release MC group subscription
 *
 */
void  trdp_releaseSocket (
    TRDP_SOCKETS_T  iface[],
    INT32           lIndex,
    UINT32          connectTimeout,
    BOOL8           checkAll,
    TRDP_IP_ADDR_T  mcGroupUsed)
{
    TRDP_ERR_T err = TRDP_PARAM_ERR;

    if (iface == NULL)
    {
        return;
    }

#if MD_SUPPORT
    if (checkAll == TRUE)
    {
        /* Check all the sockets */
        /* Close the morituri = TRUE sockets */
        for (lIndex = 0; lIndex < trdp_getCurrentMaxSocketCnt(iface[0].type); lIndex++)
        {
            if (iface[lIndex].tcpParams.morituri == TRUE)
            {
                vos_printLog(VOS_LOG_INFO, "The socket (Num = %d) will be closed\n", (int) iface[lIndex].sock);

                err = (TRDP_ERR_T) vos_sockClose(iface[lIndex].sock);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "vos_sockClose() failed (Err:%d)\n", err);
                }

                /* Delete the socket from the iface */
                vos_printLog(VOS_LOG_INFO,
                             "Deleting socket from the iface (Sock: %d, lIndex: %d)\n",
                             (int) iface[lIndex].sock, lIndex);
                iface[lIndex].sock = TRDP_INVALID_SOCKET;
                iface[lIndex].sendParam.qos = 0;
                iface[lIndex].sendParam.ttl = 0;
                iface[lIndex].usage         = 0;
                iface[lIndex].bindAddr      = 0;
                iface[lIndex].srcAddr       = 0;
                iface[lIndex].type      = TRDP_SOCK_INVAL;
                iface[lIndex].rcvMostly = FALSE;
                iface[lIndex].tcpParams.cornerIp = 0;
                iface[lIndex].tcpParams.connectionTimeout.tv_sec    = 0;
                iface[lIndex].tcpParams.connectionTimeout.tv_usec   = 0;
                iface[lIndex].tcpParams.addFileDesc = FALSE;
                iface[lIndex].tcpParams.morituri    = FALSE;
            }
        }

    }
    else
#endif
    {
        /* 指定的socket是一个有效的UDPsocket */
        if (iface[lIndex].sock != VOS_INVALID_SOCKET &&
            (iface[lIndex].type == TRDP_SOCK_MD_UDP ||
             iface[lIndex].type == TRDP_SOCK_PD ||
             iface[lIndex].type == TRDP_SOCK_PD_TSN))
        {
            vos_printLog(VOS_LOG_DBG,
                         "Decrement the socket %d usage = %d\n",
                         (int) iface[lIndex].sock,
                         iface[lIndex].usage);

            /* 减少一个请求者 */
            iface[lIndex].usage--;

            /* 调用时socket只有一个请求者 */
            if (iface[lIndex].usage <= 0)
            {
                /* 关闭socket并将它在池中置无效 */
                err = (TRDP_ERR_T) vos_sockClose(iface[lIndex].sock);
                if (err != TRDP_NO_ERR)
                {
                    vos_printLog(VOS_LOG_ERROR, "Close socket %d failed.\n", (int) iface[lIndex].sock);
                }
                else
                {
                    vos_printLog(VOS_LOG_DBG, "Closed socket %d\n", (int) iface[lIndex].sock);
                }

                iface[lIndex].sock = VOS_INVALID_SOCKET;
                iface[lIndex].type = TRDP_SOCK_INVAL;

                /* @note 这里不再清空其他配置，因为请求socket的时候会执行初始化清空配置工作 */
            }
            /* 不只有一个请求者且调用传入了组播地址 */
            else if (mcGroupUsed != VOS_INADDR_ANY)
            {
            	/*- 退出组播数组和组播组 */
                /* remove MC group from socket list:
                    we do that only if the caller is the only user of this MC group on this socket!
                    ??这里根本没有去判断调用是不是唯一的使用者，后续版本考虑增加，这需要在扩充MC数据成员并在findMCjoins函数中增加标记处理 */
                if (trdp_SockDelJoin(iface[lIndex].mcGroups, mcGroupUsed) == FALSE)
                {
                    vos_printLogStr(VOS_LOG_WARNING, "trdp_sockDelJoin() failed!\n");
                }
                else
                {
                    if (vos_sockLeaveMC(iface[lIndex].sock, mcGroupUsed, iface[lIndex].srcAddr) != VOS_NO_ERR)
                    {
                        vos_printLogStr(VOS_LOG_WARNING, "trdp_sockLeaveMC() failed!\n");
                    }
                }
            }
            else
            {

            }
        }
#if MD_SUPPORT
        else /* TCP socket */
        {
            if (iface[lIndex].sock != VOS_INVALID_SOCKET &&
                iface[lIndex].rcvMostly == FALSE)
            {
                vos_printLog(VOS_LOG_DBG,
                             "Decrement the socket %d usage = %d\n",
                             (int) iface[lIndex].sock,
                             iface[lIndex].usage);

                iface[lIndex].usage--;

                if (iface[lIndex].usage <= 0)
                {
                    /* Start the socket connection timeout */
                    TRDP_TIME_T tmpt_interval, tmpt_now;

                    iface[lIndex].usage = 0;

                    vos_printLog(VOS_LOG_INFO,
                                 "The Socket (Num = %d usage=0) ConnectionTimeout will be started\n",
                                 (int) iface[lIndex].sock);

                    tmpt_interval.tv_sec    = connectTimeout / 1000000;
                    tmpt_interval.tv_usec   = connectTimeout % 1000000;

                    vos_getTime(&tmpt_now);
                    vos_addTime(&tmpt_now, &tmpt_interval);

                    memcpy(&iface[lIndex].tcpParams.connectionTimeout,
                           &tmpt_now,
                           sizeof(TRDP_TIME_T));
                }
            }
        }
#endif
    }
}

/**********************************************************************************************************************/
/**
 *	@brief			重置通道序列号记录为0
 *	@details		通道为每个唯一的消息类型&源Ip组合（称为一个entry）维护一个序列号记录，调用时根据入参选择相应entry来重置其序列号记录,
 *  @param[in]      pElement            subscription element
 *  @param[in]      srcIP               Source IP address
 *  @param[in]      msgType             message type
 *
 *  @retval         none
 */

void trdp_resetSequenceCounter (
    PD_ELE_T        *pElement,
    TRDP_IP_ADDR_T  srcIP,
    TRDP_MSG_T      msgType)
{
    int l_index;

    if (pElement == NULL || pElement->pSeqCntList == NULL)
    {
        return;
    }
    /*- 遍历通道输入列表 */
    for (l_index = 0; l_index < pElement->pSeqCntList->curNoOfEntries; ++l_index)
    {
        if ((srcIP == pElement->pSeqCntList->seq[l_index].srcIpAddr) &&
            (msgType == pElement->pSeqCntList->seq[l_index].msgType))
        {
            pElement->pSeqCntList->seq[l_index].lastSeqCnt = 0;
        }
    }
}

/**********************************************************************************************************************/
/**
 *
 *	@brief			检查传入的序列号是否为新，并更新通道序列号记录
 *	@note			不同srcIp间序列号记录相互独立，而接收通道不限制srcIp,如果有两个不同的srcIp在使用同一comid和destIp发数据，则都将触发回调??引发问题
					在配置了sdt的情况下，接收端将识别到主备切换失败；但未配置sdt时两个数据源发送的数据都会被写入
 *  @param[in]      pElement            subscription element
 *  @param[in]      sequenceCounter     sequence counter to check
 *  @param[in]      srcIP               Source IP address
 *  @param[in]      msgType             type of the message
 *
 *  @retval         0 - no duplicate
 *                  1 - duplicate or old sequence counter
 *                 -1 - memory error
 */

int trdp_checkSequenceCounter (
    PD_ELE_T        *pElement,
    UINT32          sequenceCounter,
    TRDP_IP_ADDR_T  srcIP,
    TRDP_MSG_T      msgType)
{
    int l_index;

    if (pElement == NULL)
    {
        vos_printLogStr(VOS_LOG_DBG, "Parameter error\n");
        return -1;
    }

    if (pElement->pSeqCntList == NULL)
    {
        /* Allocate some space */
        pElement->pSeqCntList = (TRDP_SEQ_CNT_LIST_T *) vos_memAlloc(TRDP_SEQ_CNT_START_ARRAY_SIZE *
                                                                     sizeof(TRDP_SEQ_CNT_ENTRY_T) +
                                                                     sizeof(TRDP_SEQ_CNT_LIST_T));
        if (pElement->pSeqCntList == NULL)
        {
            return -1;
        }
        pElement->pSeqCntList->maxNoOfEntries   = TRDP_SEQ_CNT_START_ARRAY_SIZE;
        pElement->pSeqCntList->curNoOfEntries   = 0;
    }
    /* Loop over entries */
    for (l_index = 0; l_index < pElement->pSeqCntList->curNoOfEntries; ++l_index)
    {
        if ((srcIP == pElement->pSeqCntList->seq[l_index].srcIpAddr) &&
            (msgType == pElement->pSeqCntList->seq[l_index].msgType))
        {
            /*        Is this packet a duplicate?    */
            if ((pElement->pSeqCntList->seq[l_index].lastSeqCnt == 0) ||    /* first time after timeout */
                (sequenceCounter > pElement->pSeqCntList->seq[l_index].lastSeqCnt))
            {
                /*
                 vos_printLog(VOS_LOG_DBG,
                 "Rcv sequence: %u    last seq: %u\n",
                 sequenceCounter,
                 pElement->pSeqCntList->seq[l_index].lastSeqCnt);
                 vos_printLog(VOS_LOG_DBG, "-> new PD data found (SrcIp: %s comId %u)\n", vos_ipDotted(
                 srcIP), pElement->addr.comId);
                 */
                pElement->pSeqCntList->seq[l_index].lastSeqCnt = sequenceCounter;
                return 0;
            }
            else
            {
                vos_printLog(VOS_LOG_DBG,
                             "Rcv sequence: %u    last seq: %u\n",
                             sequenceCounter,
                             pElement->pSeqCntList->seq[l_index].lastSeqCnt);
                vos_printLog(VOS_LOG_DBG, "-> duplicated PD data ignored (SrcIp: %s comId %u)\n", vos_ipDotted(
                                 srcIP), pElement->addr.comId);
                return 1;
            }
        }
    }

    /* Not found in list, add new entry */
    if (pElement->pSeqCntList->curNoOfEntries >= pElement->pSeqCntList->maxNoOfEntries)
    {
        /* Allocate some more space */
        UINT16 newSize = 2 * pElement->pSeqCntList->curNoOfEntries;
        TRDP_SEQ_CNT_LIST_T *newList = (TRDP_SEQ_CNT_LIST_T *) vos_memAlloc(newSize * sizeof(TRDP_SEQ_CNT_ENTRY_T) +
                                                                            sizeof(TRDP_SEQ_CNT_LIST_T));
        if (newList == NULL)
        {
            return -1;
        }

        /* Copy old data into new, larger area */
        memcpy(newList, pElement->pSeqCntList, pElement->pSeqCntList->maxNoOfEntries *
               sizeof(TRDP_SEQ_CNT_ENTRY_T) +
               sizeof(TRDP_SEQ_CNT_LIST_T));
        vos_memFree(pElement->pSeqCntList);     /* Free old area */
        pElement->pSeqCntList = newList;
        pElement->pSeqCntList->maxNoOfEntries = newSize;
    }
    pElement->pSeqCntList->seq[pElement->pSeqCntList->curNoOfEntries].lastSeqCnt    = sequenceCounter;
    pElement->pSeqCntList->seq[pElement->pSeqCntList->curNoOfEntries].srcIpAddr     = srcIP;
    pElement->pSeqCntList->seq[pElement->pSeqCntList->curNoOfEntries].msgType       = msgType;
    pElement->pSeqCntList->curNoOfEntries++;
    vos_printLog(VOS_LOG_DBG, "Rcv sequence: %u\n", sequenceCounter);
    vos_printLog(VOS_LOG_DBG, "*** new sequence entry (SrcIp: %s comId %u)\n", vos_ipDotted(
                     srcIP), pElement->addr.comId);

    return 0;
}
/***********************************************************************************************************************
 *   当前版本没有使用的函数
 */
#if MD_SUPPORT
/**********************************************************************************************************************/
/** Return the element with same comId from MD queue
 *
 *  @param[in]      pHead           pointer to head of queue
 *  @param[in]      addr            Pub/Sub handle (Address, ComID, srcIP & dest IP) to search for
 *
 *  @retval         != NULL         pointer to PD element
 *  @retval         NULL            No PD element found
 */
MD_ELE_T *trdp_MDqueueFindAddr (
    MD_ELE_T            *pHead,
    TRDP_ADDRESSES_T    *addr)
{
    MD_ELE_T *iterMD;

    if (pHead == NULL || addr == NULL)
    {
        return NULL;
    }

    for (iterMD = pHead; iterMD != NULL; iterMD = iterMD->pNext)
    {
        /*  We match if src/dst address is zero or found */
        if ((iterMD->addr.comId == addr->comId)
            && ((addr->srcIpAddr == 0) || (iterMD->addr.srcIpAddr == addr->srcIpAddr))
            && ((addr->destIpAddr == 0) || (iterMD->addr.destIpAddr == addr->destIpAddr)))
        {
            return iterMD;
        }
    }
    return NULL;
}

/**********************************************************************************************************************/
/** Delete an element from MD queue
 *
 *  @param[in]      ppHead          pointer to pointer to head of queue
 *  @param[in]      pDelete         pointer to element to delete
 */
void    trdp_MDqueueDelElement (
    MD_ELE_T    * *ppHead,
    MD_ELE_T    *pDelete)
{
    MD_ELE_T *iterMD;

    if (ppHead == NULL || *ppHead == NULL || pDelete == NULL)
    {
        return;
    }

    /*    handle removal of first element    */
    if (pDelete == *ppHead)
    {
        *ppHead = pDelete->pNext;
        return;
    }

    for (iterMD = *ppHead; iterMD != NULL; iterMD = iterMD->pNext)
    {
        if (iterMD->pNext && iterMD->pNext == pDelete)
        {
            iterMD->pNext = pDelete->pNext;
            return;
        }
    }
}

/**********************************************************************************************************************/
/** Append an element at end of queue
 *
 *  @param[in]      ppHead          pointer to pointer to head of queue
 *  @param[in]      pNew            pointer to element to append
 */
void    trdp_MDqueueAppLast (
    MD_ELE_T    * *ppHead,
    MD_ELE_T    *pNew)
{
    MD_ELE_T *iterMD;

    if (ppHead == NULL || pNew == NULL)
    {
        return;
    }

    /* Ensure this element is last! */
    pNew->pNext = NULL;

    if (*ppHead == NULL)
    {
        *ppHead = pNew;
        return;
    }

    for (iterMD = *ppHead; iterMD->pNext != NULL; iterMD = iterMD->pNext)
    {
        ;
    }
    iterMD->pNext = pNew;
}

/**********************************************************************************************************************/
/** Insert an element at front of MD queue
 *
 *  @param[in]      ppHead          pointer to pointer to head of queue
 *  @param[in]      pNew            pointer to element to insert
 */
void    trdp_MDqueueInsFirst (
    MD_ELE_T    * *ppHead,
    MD_ELE_T    *pNew)
{
    if (ppHead == NULL || pNew == NULL)
    {
        return;
    }

    pNew->pNext = *ppHead;
    *ppHead     = pNew;
}

/**********************************************************************************************************************/
/** Initialize the UncompletedTCP pointers to null
 *
 *  @param[in]      appHandle           the handle returned by tlc_openSession
 */
void trdp_initUncompletedTCP (TRDP_APP_SESSION_T appHandle)
{
    int lIndex;
    /* Initialize the pointers to Null */
    for (lIndex = 0; lIndex < VOS_MAX_SOCKET_CNT; lIndex++)
    {
        appHandle->uncompletedTCP[lIndex] = NULL;
    }
}
#endif
/**********************************************************************************************************************/
/**
 *	@brief			将传入的通道添加到传入的通道链表尾
 *	@note			本来在tlp_subscribe()中使用本函数，修改为使用trdp_queueInsFirst()
 *  @param[in]      ppHead          pointer to pointer to head of queue
 *  @param[in]      pNew            pointer to element to append
 */
void    trdp_queueAppLast (
    PD_ELE_T    * *ppHead,
    PD_ELE_T    *pNew)
{
    PD_ELE_T *iterPD;

    if (ppHead == NULL || pNew == NULL)
    {
        return;
    }

    /* Ensure this element is last! */
    pNew->pNext = NULL;

    if (*ppHead == NULL)
    {
        *ppHead = pNew;
        return;
    }

    for (iterPD = *ppHead; iterPD->pNext != NULL; iterPD = iterPD->pNext)
    {
        ;
    }
    iterPD->pNext = pNew;
}
/**********************************************************************************************************************/
/**
 *	@brief			获取设备的第一个网口IP
 *	@note			当前版本不使用
 *  @param[in]      mcList        List of multicast groups
 *  @param[in]      mcGroup         multicast group
 *
 *  @retval         1           if deleted
 *                  0           was not in list
 */
TRDP_IP_ADDR_T trdp_getOwnIP ()
{
    UINT32          i;
    UINT32          addrCnt = 2 * VOS_MAX_NUM_IF;
    VOS_IF_REC_T    localIF[2 * VOS_MAX_NUM_IF];
    (void) vos_getInterfaces(&addrCnt, localIF);
    for (i = 0u; i < addrCnt; ++i)
    {
        if ((localIF[i].mac[0] ||        /* Take a MAC address as indicator for an ethernet interface */
             localIF[i].mac[1] ||
             localIF[i].mac[2] ||
             localIF[i].mac[3] ||
             localIF[i].mac[4] ||
             localIF[i].mac[5])
            &&
            (localIF[i].ipAddr != VOS_INADDR_ANY)
            &&
            (localIF[i].ipAddr != INADDR_LOOPBACK)
            )
        {
            vos_printLog(VOS_LOG_INFO, "Own IP determined as %s\n", vos_ipDotted(localIF[i].ipAddr));
            return localIF[i].ipAddr;   /* Could still be unset!    */
        }
    }
    vos_printLogStr(VOS_LOG_WARNING, "Own IP could not be determined!\n");
    return VOS_INADDR_ANY;
}

/**********************************************************************************************************************/
/** Check if listener URI is in addressing range of destination URI.
 *
 *  @param[in]      listUri       Null terminated listener URI string to compare
 *  @param[in]      destUri       Null terminated destination URI string to compare
 *
 *  @retval         FALSE - not in addressing range
 *  @retval         TRUE  - listener URI is in addressing range of destination URI
 */

BOOL8 trdp_isAddressed (const TRDP_URI_USER_T listUri, const TRDP_URI_USER_T destUri)
{
    return (vos_strnicmp(listUri, destUri, TRDP_USR_URI_SIZE) == 0);
}

/**********************************************************************************************************************/
/** Check if received IP is in addressing range of listener's IPs.
 *
 *  @param[in]      receivedSrcIP           Received IP address
 *  @param[in]      listenedSourceIPlow     Lower bound IP
 *  @param[in]      listenedSourceIPhigh    Upper bound IP
 *
 *  @retval         FALSE - not in addressing range
 *  @retval         TRUE  - received IP is in addressing range of listener
 */

BOOL8 trdp_isInIPrange (TRDP_IP_ADDR_T  receivedSrcIP,
                        TRDP_IP_ADDR_T  listenedSourceIPlow,
                        TRDP_IP_ADDR_T  listenedSourceIPhigh)
{
    if ((receivedSrcIP == VOS_INADDR_ANY) ||
        (listenedSourceIPlow == VOS_INADDR_ANY) ||
        (listenedSourceIPhigh == VOS_INADDR_ANY))
    {
        return FALSE;
    }
    return (receivedSrcIP >= listenedSourceIPlow) && (receivedSrcIP <= listenedSourceIPhigh);
}
