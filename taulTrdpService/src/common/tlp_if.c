/**********************************************************************************************************************/
/**
 * @file            tlp_if.c
 *
 * @brief           PD通信功能
 *
 * @details         TRDP Light接口的实现（之一）
 * 					PD通信的所有操作
 * @note
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 */


/***********************************************************************************************************************
 * INCLUDES
 */

#include <string.h>

#include "trdp_if_light.h"
#include "trdp_utils.h"
#include "trdp_pdcom.h"
#include "vos_sock.h"
#include "vos_mem.h"
#include "vos_utils.h"

#ifdef HIGH_PERF_INDEXED
#include "trdp_pdindex.h"
#endif

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 * TYPEDEFS
 */

/***********************************************************************************************************************
 * LOCALS
 */

/******************************************************************************
 * LOCAL FUNCTIONS
 */

/***********************************************************************************************************************
 * GLOBAL FUNCTIONS
 */

/**********************************************************************************************************************/


/**********************************************************************************************************************/


/**********************************************************************************************************************/


/**********************************************************************************************************************/
/**
 *	@breif			设置一个app下属于指定冗余组的所有通道设置或取消从标志位
 *	@details		如果传入的redId为0，则为所有配置了冗余组Id的通道设置或取消冗余主、从标志位
 *  @param[in]      appHandle           the handle returned by tlc_openSession
 *  @param[in]      redId               will be set for all ComID's with the given redId, 0 to change for all redId
 *  @param[in]      leader              TRUE if we send
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error / redId not existing
 *  @retval         TRDP_NOINIT_ERR     handle invalid
 */
EXT_DECL TRDP_ERR_T tlp_setRedundant (
    TRDP_APP_SESSION_T  appHandle,
    UINT32              redId,
    BOOL8               leader)
{
    TRDP_ERR_T  ret = TRDP_NOINIT_ERR;
    PD_ELE_T    *iterPD;
    BOOL8       found = FALSE;

    if (trdp_isValidSession(appHandle))
    {
        ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutexTxPD);
        if (TRDP_NO_ERR == ret)
        {
            /*    Set the redundancy flag for every PD with the specified ID */
            for (iterPD = appHandle->pSndQueue; NULL != iterPD; iterPD = iterPD->pNext)
            {
                if ((0u != iterPD->redId)                           /* packet has redundant ID       */
                    &&
                    ((0u == redId) || (iterPD->redId == redId)))    /* all set redundant ID are targeted if redId == 0
                                                                     or packet redundant ID matches       */
                {
                    if (TRUE == leader)
                    {
                        iterPD->privFlags = (TRDP_PRIV_FLAGS_T) (iterPD->privFlags & ~(TRDP_PRIV_FLAGS_T)TRDP_REDUNDANT);
                    }
                    else
                    {
                        iterPD->privFlags |= TRDP_REDUNDANT;
                    }
                    found = TRUE;
                }
            }

            if ((FALSE == found) && (0u != redId))
            {
                vos_printLogStr(VOS_LOG_DBG, "Redundant ID not found\n");
                ret = TRDP_PARAM_ERR;
            }

            if (vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR)
            {
                vos_printLogStr(VOS_LOG_DBG, "vos_mutexUnlock() failed\n");
            }
        }
    }

    return ret;
}

/**********************************************************************************************************************/
/**
 *	@brief			根据冗余Id获取主从状态
 *  @param[in]      appHandle           the handle returned by tlc_openSession
 *  @param[in]      redId               will be returned for all ComID's with the given redId
 *  @param[in,out]  pLeader             TRUE if we're sending this redundancy group (leader)
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      redId invalid or not existing
 *  @retval         TRDP_NOINIT_ERR     handle invalid
 */
EXT_DECL TRDP_ERR_T tlp_getRedundant (
    TRDP_APP_SESSION_T  appHandle,
    UINT32              redId,
    BOOL8               *pLeader)
{
    TRDP_ERR_T  ret = TRDP_NOINIT_ERR;
    PD_ELE_T    *iterPD;

    if ((pLeader == NULL) || (redId == 0u))
    {
        return TRDP_PARAM_ERR;
    }

    if (trdp_isValidSession(appHandle))
    {
        ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutexTxPD);
        if (ret == TRDP_NO_ERR)
        {
            /*    Search the redundancy flag for every PD with the specified ID */
            for (iterPD = appHandle->pSndQueue; NULL != iterPD; iterPD = iterPD->pNext)
            {
                if (iterPD->redId == redId)         /* packet redundant ID matches                      */
                {
                    if (iterPD->privFlags & TRDP_REDUNDANT)
                    {
                        *pLeader = FALSE;
                    }
                    else
                    {
                        *pLeader = TRUE;
                    }
                    break;
                }
            }

            if (vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR)
            {
                vos_printLogStr(VOS_LOG_DBG, "vos_mutexUnlock() failed\n");
            }
        }
    }

    return ret;
}

/**********************************************************************************************************************/
/**
 *	@brief			创建发布通道用于周期循环时期的发送
 *	@details		-在app发送锁下执行：
 *					-创建通道，为通道获取socket，初始化通道，将通道加入链表，用传入的buffer执行一次发布，对齐各发布通道周期
 *	@note			-若配置于现有通道重复则不再创建
 *					-处理tsn通道与普通通道的区别
 *  @param[in]      appHandle           必须配置
 *  @param[out]     pPubHandle          返回通道的指针，必须配置
 *  @param[in]      pUserRef            存放回调函数返回的的值，直接传递
 *  @param[in]      pfCbFunction        回调函数，未配则使用app的默认回调
 *  @param[in]      serviceId           直接传递
 *  @param[in]      comId               直接传递
 *  @param[in]      etbTopoCnt          ETB topocount to use, 0 if consist local communication
 *  @param[in]      opTrnTopoCnt        operational topocount, != 0 for orientation/direction sensitive communication
 *  @param[in]      srcIpAddr           用于socket绑定，传入0则使用app绑定的Ip
 *  @param[in]      destIpAddr          直接传递
 *  @param[in]      interval            周期us
 *  @param[in]      redId               0 - Non-redundant, > 0 valid redundancy group
 *  @param[in]      pktFlags            若为默认宏定义则使用app下的flag
 *  @param[in]      pSendParam          未配则使用app下的sendparm
 *  @param[in]      pData               用户数据地址，直接传递，可为NULL，代表调用仅创建通道，暂时不为通道准备用户数据
 *  @param[in]      dataSize            用户数据长度，直接传递，可为0，代表调用仅创建通道，暂时不为通道准备用户数据和配置数据长度
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_MEM_ERR        could not insert (out of memory)
 *  @retval         TRDP_NOINIT_ERR     handle invalid
 */
EXT_DECL TRDP_ERR_T tlp_publish (
    TRDP_APP_SESSION_T      appHandle,
    TRDP_PUB_T              *pPubHandle,
    const void              *pUserRef,
    TRDP_PD_CALLBACK_T      pfCbFunction,
    UINT32                  serviceId,
    UINT32                  comId,
    UINT32                  etbTopoCnt,
    UINT32                  opTrnTopoCnt,
    TRDP_IP_ADDR_T          srcIpAddr,
    TRDP_IP_ADDR_T          destIpAddr,
    UINT32                  interval,
    UINT32                  redId,
    TRDP_FLAGS_T            pktFlags,
    const TRDP_SEND_PARAM_T *pSendParam,
    const UINT8             *pData,
    UINT32                  dataSize)
{
    PD_ELE_T            *pNewElement = NULL;
    TRDP_TIME_T         nextTime;
    TRDP_TIME_T         tv_interval;
    TRDP_ERR_T          ret         = TRDP_NO_ERR;
    TRDP_MSG_T          msgType     = TRDP_MSG_PD; //默认为PD类
    TRDP_SOCK_TYPE_T    sockType    = TRDP_SOCK_PD;//默认为PD类
    TRDP_ADDRESSES_T pubHandle;
    const TRDP_SEND_PARAM_T *pCurrentSendParams;

    /*- 检查传入参数的有效性 */

    if ((interval != 0u && interval < TRDP_TIMER_GRANULARITY)
        || (pPubHandle == NULL))
    {
    	ret=TRDP_PARAM_ERR;
    }

    if(TRDP_NO_ERR==ret)
    {
        if (!trdp_isValidSession(appHandle))
        {
            ret=TRDP_NOINIT_ERR;
        }
    }

    if(TRDP_MAX_PD_DATA_SIZE<dataSize)
    {
    	ret=TRDP_PARAM_ERR;
    }

    /*- 参数有效 */
    if(TRDP_NO_ERR==ret)
    {
    	/*- 占用app的发送锁 */
    	ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutexTxPD);
    }

    /*- 获取锁成功 */
    if (TRDP_NO_ERR == ret)
    {
        /*- 配置TRDP地址结构体 */

        /*- 传入的srcIpAddr为0 */
        if (srcIpAddr == VOS_INADDR_ANY)
        {
        	/*- 使用app对应的设备子网ip */
            srcIpAddr = appHandle->realIP;
        }

        pubHandle.comId         = comId;
        pubHandle.destIpAddr    = destIpAddr;
        pubHandle.mcGroup       = vos_isMulticast(destIpAddr) ? destIpAddr : 0u;
        pubHandle.srcIpAddr     = srcIpAddr;
        pubHandle.serviceId     = serviceId;

        /*- 查询到发布通道中存在相同配置 */
        if (trdp_queueFindPubAddr(appHandle->pSndQueue, &pubHandle) != NULL)
        {
            ret = TRDP_NOPUB_ERR;
        }
    }
    /*- 获取锁成功 */
    else
    {
    	/*- 设置错误码 */
    	ret = VOS_MUTEX_ERR;
    }


    /*- 发布通道中不存在相同配置 */
    if(TRDP_NO_ERR==ret)
    {
    	/*- 为PD_ELE_T分配内存 */
        pNewElement = (PD_ELE_T *) vos_memAlloc(sizeof(PD_ELE_T));
        if (pNewElement == NULL)
        {
            ret = TRDP_MEM_ERR;
        }
    }

    /*- PD_ELE_T内存分配成功 */
    if(TRDP_NO_ERR==ret)
    {
    	/*- 处理请求socket所需配置 */

    	/*- 若未传入sendParm,则使用所在app的sendParm(所在app的sendParm.qos/ttl来自配置，若未配置则使用iec61375-2-3.h中的宏定义默认值) */
        pCurrentSendParams = (pSendParam != NULL) ? pSendParam :&appHandle->pdDefault.sendParam;

        /*- 若未传入pktFlags，则使用所在app的flags */
        pNewElement->pktFlags = (pktFlags == TRDP_FLAGS_DEFAULT) ? appHandle->pdDefault.flags : pktFlags;


#ifdef TSN_SUPPORT
        /* 检查tsn配置 */

        /*- 配置了tsn */
        if (pCurrentSendParams->tsn == TRUE)
        {
        	/*- 包类型为tsn */
            if ((pNewElement->pktFlags & (TRDP_FLAGS_TSN | TRDP_FLAGS_TSN_SDT | TRDP_FLAGS_TSN_MSDT)))
            {
            	/*- sock类型由默认PD改为PD_TSN */
                sockType    = TRDP_SOCK_PD_TSN;

            }
            /*- 包类型不为tsn */
            else
            {
            	/*- 处理配置冲突错误 */
                vos_printLogStr(VOS_LOG_ERROR, "Publish: Wrong send parameters for TSN!\n");
                ret = TRDP_PARAM_ERR;
            }
        }
        if (ret == TRDP_NO_ERR)
#endif
        {
            /*- 为当前通道请求socket */
            ret = trdp_requestSocket(
                    appHandle->ifacePD,
                    0,//标准P127（pdf）要求发送要使用任何不同于协议默认的端口号
                    pCurrentSendParams,
                    srcIpAddr,
                    0u,
                    sockType,
                    appHandle->option,
                    FALSE,
                    -1,
                    &pNewElement->socketIdx,
                    0u);
        }

        //printf("publish create sock appHandle:%d sockHandle:%d	sock:%d	comid:%d retval:%d\n",appHandle,pNewElement->socketIdx,appHandle->ifacePD[pNewElement->socketIdx].sock,comId,ret);

        /*- 获取socket失败 */
        if (ret != TRDP_NO_ERR)
        {
            vos_memFree(pNewElement);
            pNewElement = NULL;
        }
        else
        {
        	/*- 计算数据包长度 */
#ifdef TSN_SUPPORT
			/*- 配置了tsn（此处不检查pNewElement->pktFlags，在设置socktype时已检查过） */
			if (pCurrentSendParams->tsn == TRUE)
			{
				/* @note tsn消息与普通pd消息的trdp包头不同，前者去除了reply和拓扑相关的信息 */
				pNewElement->grossSize  = trdp_packetSizePD2(dataSize);
			}
			else
#else
			{
				pNewElement->grossSize = trdp_packetSizePD(dataSize);
			}
#endif

            /* 为trdp 数据包分配内存 */
            pNewElement->pFrame = (PD_PACKET_T *) vos_memAlloc(pNewElement->grossSize);
            if (pNewElement->pFrame == NULL)
            {
                vos_memFree(pNewElement);
                pNewElement = NULL;
                ret = TRDP_MEM_ERR;
            }
        }
    }


	if ((ret == TRDP_NO_ERR)
		&& (pNewElement != NULL))
	{
		/*- 至此，通道已成功创建，不会再被删除 */

		/*- 通道加入app发布通道链表、返回到句柄 */

#ifdef HIGH_PERF_INDEXED
		/*    Keep queue sorted    */
		trdp_queueInsThroughputAccending(&appHandle->pSndQueue, pNewElement);
#else
		trdp_queueInsFirst(&appHandle->pSndQueue, pNewElement);
#endif

		*pPubHandle = (TRDP_PUB_T) pNewElement;

		/*- 完成通道其他配置 */

		/*- 用传入的参数配置通道callBackFunc,若未配置则使用会话配置的 */
		if ((pktFlags == TRDP_FLAGS_DEFAULT) &&
			(pfCbFunction == NULL))
		{
			pNewElement->pfCbFunction = appHandle->pdDefault.pfCbFunction;
		}
		else
		{
			pNewElement->pfCbFunction = pfCbFunction;
		}

		pNewElement->addr = pubHandle;
		pNewElement->pullIpAddress  = 0u;
		pNewElement->redId          = redId;
		pNewElement->pCachedDS      = NULL;
		pNewElement->magic          = TRDP_MAGIC_PUB_HNDL_VALUE;
		pNewElement->pUserRef       = pUserRef;
		pNewElement->dataSize   	= dataSize;

#ifdef TSN_SUPPORT
		/*- ������tsn���˴������pNewElement->pktFlags��������socktypeʱ�Ѽ����� */
		if (pCurrentSendParams->tsn == TRUE)
		{
			/*- 修改 msgtype(trdp包头参数) */
            if (pNewElement->pktFlags & TRDP_FLAGS_TSN_SDT)
            {
                msgType = TRDP_MSG_TSN_PD_SDT;
            }
            else if (pNewElement->pktFlags & TRDP_FLAGS_TSN_MSDT)
            {
                msgType = TRDP_MSG_TSN_PD_MSDT;
            }
            else
            {
                msgType = TRDP_MSG_TSN_PD;
            }

			interval    = 0u;       /*- 强制零周期 */
			pNewElement->privFlags  |= TRDP_IS_TSN;

			/*-对于tsn发布通道，我们将源ip配置为socket实际绑定的ip */
			pNewElement->addr.srcIpAddr = appHandle->ifacePD[pNewElement->socketIdx].bindAddr;

		}
#endif

		/*- 配置trdp包头结构体，@note reply相关量置0 */
		trdp_pdInit(pNewElement, msgType, etbTopoCnt, opTrnTopoCnt, 0u, 0u, serviceId);

		/*- 初始化通道动态量 */

		/*- 初始化序列号记录为-1 */
		pNewElement->curSeqCnt = 0xFFFFFFFFu;

		/*- 初始化另一个序列号记录以备该通道被请求为拉模式 */
		pNewElement->curSeqCnt4Pull = 0xFFFFFFFFu;

		pNewElement->privFlags  |= TRDP_INVALID_DATA;

		/*- 如果设置了冗余组Id */
		if (0 != redId)
		{
			/*- 为通道私有标志位标记冗余从 */
			pNewElement->privFlags |= TRDP_REDUNDANT;
			
		}
		else
		{

		}

		/* interval为零或配置了tsn */
		if (0 == interval)
		{
			/*- 清空通道中时间相关动态量 */
			vos_clearTime(&pNewElement->interval);
			vos_clearTime(&pNewElement->timeToGo);
			vos_clearTime(&pNewElement->timeToPut);
		}
		else
		{
			/*- 初始化通道中时间相关动态量（下一任务时间设置为创建通道（即now）的一周期后） */
			vos_getTime(&nextTime);
			tv_interval.tv_sec  = interval / 1000000u;
			tv_interval.tv_usec = interval % 1000000;
			vos_addTime(&nextTime, &tv_interval);
			pNewElement->interval   = tv_interval;
			pNewElement->timeToGo   = nextTime;
			pNewElement->timeToPut   = nextTime;
		}

#ifdef TSN_SUPPORT
		if (pNewElement->privFlags & TRDP_IS_TSN)
		{
			/*- @note 对于tsn发布通道，无需在协议栈中准备数据，也无需对齐通道周期 */
		}
		else
#endif
		{
#ifndef HIGH_PERF_INDEXED
			/*- 若app定义了整形选项 */
			if (appHandle->option & TRDP_OPTION_TRAFFIC_SHAPING)
			{
				/*- 对齐会话已配通道的周期 */
				ret = trdp_pdDistribute(appHandle->pSndQueue);
			}
			if(TRDP_NO_ERR!=ret)
			{
				vos_printLog(VOS_LOG_ERROR,"tlp_publish():distribute pd schedule failed,error code:%d\n",ret);
			}

			/*- 由于tlp_put中申请锁，此处需要释放锁 */
			if (vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR)
			{
				vos_printLogStr(VOS_LOG_ERROR, "tlp_publish() vos_mutexUnlock() failed1\n");
			}

			/*- 当前调用准备了用户数据 */
			if ((dataSize != 0u) || (NULL==pData))
			{
				/*- 将传入的用户数据put到trdp协议栈 */
				ret = tlp_put(appHandle, *pPubHandle, pData, dataSize);
				//??需要看看原来函数这里 有没有失败，因为重复申请了锁
			}
#endif
		}
	}
	else if(TRDP_MUTEX_ERR==ret)
	{

	}
	else
	{
		/*- 无论如何，释放锁 */
		if (vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR)
		{
			vos_printLogStr(VOS_LOG_ERROR, "tlp_publish() vos_mutexUnlock() failed2\n");
		}
	}

    return ret;
}



/**********************************************************************************************************************/
/** Stop sending PD messages.
 *
 *  @param[in]      appHandle           the handle returned by tlc_openSession
 *  @param[in]      pubHandle           the handle returned by prepare
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_NOPUB_ERR      not published
 *  @retval         TRDP_NOINIT_ERR     handle invalid
 */

EXT_DECL TRDP_ERR_T  tlp_unpublish (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_PUB_T          pubHandle)
{
    PD_ELE_T    *pElement = (PD_ELE_T *)pubHandle;
    TRDP_ERR_T  ret;

    if (pElement == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    if (pElement->magic != TRDP_MAGIC_PUB_HNDL_VALUE)
    {
        return TRDP_NOPUB_ERR;
    }

    if (!trdp_isValidSession(appHandle))
    {
        return TRDP_NOINIT_ERR;
    }

    /*    Reserve mutual access    */
    ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutexTxPD);
    if (ret == TRDP_NO_ERR)
    {
        /*    Remove from queue?    */
        trdp_queueDelElement(&appHandle->pSndQueue, pElement);
        trdp_releaseSocket(appHandle->ifacePD, pElement->socketIdx, 0u, FALSE, VOS_INADDR_ANY);
        pElement->magic = 0u;
        if (pElement->pSeqCntList != NULL)
        {
            vos_memFree(pElement->pSeqCntList);
        }
        vos_memFree(pElement->pFrame);
        vos_memFree(pElement);

#ifndef HIGH_PERF_INDEXED
        /* Re-compute distribution times */
        if (appHandle->option & TRDP_OPTION_TRAFFIC_SHAPING)
        {
            ret = trdp_pdDistribute(appHandle->pSndQueue);
        }
#else
        /* We must check if this publisher is listed in our indexed arrays */
        trdp_indexRemovePub(appHandle, pElement);
#endif

        if (vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_INFO, "vos_mutexUnlock() failed\n");
        }
    }

    return ret;      /*    Not found    */
}

/**********************************************************************************************************************/
/**
 *	@brief			更新发送通道中的待发数据并标记数据有效
 *	@details		函数主体为入口检查和防护，功能通过调用trdp_pdPut实现
 *  @param[in]      appHandle          the handle returned by tlc_openSession
 *  @param[in]      pubHandle          the handle returned by publish
 *  @param[in,out]  pData              pointer to application's data buffer
 *  @param[in,out]  dataSize           size of data
 *
 *  @retval         TRDP_NO_ERR        no error
 *  @retval         TRDP_PARAM_ERR     parameter error on uninitialized parameter or changed dataSize compared to published one
 *  @retval         TRDP_NOPUB_ERR     not published
 *  @retval         TRDP_NOINIT_ERR    handle invalid
 *  @retval         TRDP_COMID_ERR     ComID not found when marshalling
 */
EXT_DECL TRDP_ERR_T tlp_put (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_PUB_T          pubHandle,
    const UINT8         *pData,
    UINT32              dataSize)
{
    PD_ELE_T    *pElement   = (PD_ELE_T *)pubHandle;
    TRDP_ERR_T  ret         = TRDP_NO_ERR;

    /*- appHandle有效 */
    if (!trdp_isValidSession(appHandle))
    {
        return TRDP_NOINIT_ERR;
    }

    /*- 通道存在 */
    if (pElement == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /*- 通道已初始化 */
    if (pElement->magic != TRDP_MAGIC_PUB_HNDL_VALUE)
    {
        return TRDP_NOPUB_ERR;
    }

#ifdef TSN_SUPPORT
    if ((pElement->pktFlags & TRDP_FLAGS_TSN) ||
        (pElement->pktFlags & TRDP_FLAGS_TSN_SDT) ||
        (pElement->pktFlags & TRDP_FLAGS_TSN_MSDT))
    {
        /* For TSN telegrams, use tlp_putImmediate! */
        vos_printLogStr(VOS_LOG_ERROR, "For TSN telegrams, use tlp_putImmediate()!\n");
        return TRDP_PARAM_ERR;
    }
#endif

    /*- 在app发布锁下执行操作 */
    ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutexTxPD);
    if ( ret == TRDP_NO_ERR )
    {
        /*- Find the published queue entry */
        ret = trdp_pdPut(pElement,
                         appHandle->marshall.pfCbMarshall,
                         appHandle->marshall.pRefCon,
                         pData,
                         dataSize);

        if ( vos_mutexUnlock(appHandle->mutexTxPD) != VOS_NO_ERR )
        {
            vos_printLogStr(VOS_LOG_DBG, "tlp_put vos_mutexUnlock() failed\n");
        }
    }
    else
    {
    	vos_printLogStr(VOS_LOG_DBG, "tlp_put vos_mutexLock() failed\n");
    }
    /*- 在app发布锁下执行操作 */

    return ret;
}




/**********************************************************************************************************************/
/** Prepare for receiving PD messages.
 *  Subscribe to a specific PD ComID and source IP.
 *
 *  @param[in]      appHandle           the handle returned by tlc_openSession
 *  @param[out]     pSubHandle          return a handle for this subscription
 *  @param[in]      pUserRef            user supplied value returned within the info structure
 *  @param[in]      pfCbFunction        Pointer to subscriber specific callback function, NULL to use default function
 *  @param[in]      serviceId           optional serviceId this telegram belongs to (default = 0)
 *  @param[in]      comId               comId of packet to receive
 *  @param[in]      etbTopoCnt          ETB topocount to use, 0 if consist local communication
 *  @param[in]      opTrnTopoCnt        operational topocount, != 0 for orientation/direction sensitive communication
 *  @param[in]      srcIpAddr1          Source IP address, lower address in case of address range, set to 0 if not used
 *  @param[in]      srcIpAddr2          upper address in case of address range, set to 0 if not used
 *  @param[in]      destIpAddr          IP address to join
 *  @param[in]      pktFlags            OPTION:
 *                                      TRDP_FLAGS_DEFAULT, TRDP_FLAGS_NONE, TRDP_FLAGS_MARSHALL, TRDP_FLAGS_CALLBACK
 *  @param[in]      pRecParams          optional pointer to send parameter, NULL - default parameters are used
 *  @param[in]      timeout             timeout (>= 10ms) in usec
 *  @param[in]      toBehavior          timeout behavior
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_MEM_ERR        could not reserve memory (out of memory)
 *  @retval         TRDP_NOINIT_ERR     handle invalid
 */
EXT_DECL TRDP_ERR_T tlp_subscribe (
    TRDP_APP_SESSION_T      appHandle,
    TRDP_SUB_T              *pSubHandle,
    const void              *pUserRef,
    TRDP_PD_CALLBACK_T      pfCbFunction,
    UINT32                  serviceId,
    UINT32                  comId,
    UINT32                  etbTopoCnt,
    UINT32                  opTrnTopoCnt,
    TRDP_IP_ADDR_T          srcIpAddr1,
    TRDP_IP_ADDR_T          srcIpAddr2,
    TRDP_IP_ADDR_T          destIpAddr,
    TRDP_FLAGS_T            pktFlags,
    const TRDP_COM_PARAM_T  *pRecParams,
    UINT32                  timeout,
    TRDP_TO_BEHAVIOR_T      toBehavior)
{
    TRDP_TIME_T         now;
    TRDP_ERR_T          ret = TRDP_NO_ERR;
    TRDP_ADDRESSES_T    subHandle;
    INT32 lIndex;

    /*- 必要参数检查 */
    if (pSubHandle == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    if (!trdp_isValidSession(appHandle))
    {
        return TRDP_NOINIT_ERR;
    }

    /*- 异常情况下修正timeout值 */
    if (timeout == 0u)
    {
        timeout = appHandle->pdDefault.timeout;
    }
    else if (timeout < TRDP_TIMER_GRANULARITY)
    {
        timeout = TRDP_TIMER_GRANULARITY;
    }

    /*- 请求app订阅锁 */
    if (vos_mutexLock(appHandle->mutexRxPD) != VOS_NO_ERR)
    {
        return TRDP_MUTEX_ERR;
    }

    /*- 获取当前时间 */
    vos_getTime(&now);

    /*- 用入参配置trdp地址结构体 */
    subHandle.comId         = comId;
    subHandle.srcIpAddr     = srcIpAddr1;
    subHandle.srcIpAddr2    = srcIpAddr2;
    subHandle.destIpAddr    = destIpAddr;
    subHandle.opTrnTopoCnt  = opTrnTopoCnt;
    subHandle.etbTopoCnt    = etbTopoCnt;
    subHandle.serviceId     = serviceId;

    if (vos_isMulticast(destIpAddr))
    {
        subHandle.mcGroup = destIpAddr;
    }
    else
    {
        subHandle.mcGroup = 0u;
    }

    /*- 检查当前地址配置是否已存在相应通道 */
    if (trdp_queueFindExistingSub(appHandle->pRcvQueue, &subHandle) != NULL)
    {
        ret = TRDP_PARAM_ERR;
    }
    else
    {
        TRDP_SOCK_TYPE_T sockType = TRDP_SOCK_PD;

        if (pktFlags & (TRDP_FLAGS_TSN | TRDP_FLAGS_TSN_SDT | TRDP_FLAGS_TSN_MSDT))
        {
        	sockType = TRDP_SOCK_PD_TSN;
        }

        /*- 请求socket */
        ret = trdp_requestSocket(appHandle->ifacePD,
                                 appHandle->pdDefault.port,
                                 (pRecParams != NULL) ? pRecParams : &appHandle->pdDefault.sendParam,
                                 appHandle->realIP,/* @note该Ip决定socket绑定的Ip（单播）,取realIP为最佳策略，取订阅通道的destIp也不是不行，反正订阅通道的destIp配错了依然收不到数据 */
                                 subHandle.mcGroup,
								 sockType,
                                 appHandle->option,
                                 TRUE,
                                 -1,
                                 &lIndex,
                                 0u);
        
        //printf("subscribe create sock appHandle:%d sockHandle:%d	sock:%d	comid:%d realIp:%x result code :%d\n",appHandle,lIndex,appHandle->ifacePD[lIndex].sock,comId,appHandle->realIP,ret);

        if (ret == TRDP_NO_ERR)
        {
            PD_ELE_T *newPD;

            /*    buffer size is PD_ELEMENT plus max. payload size    */

            /*    Allocate a buffer for this kind of packets    */
            newPD = (PD_ELE_T *) vos_memAlloc(sizeof(PD_ELE_T));

            if (newPD == NULL)
            {
                ret = TRDP_MEM_ERR;
                trdp_releaseSocket(appHandle->ifacePD, lIndex, 0u, FALSE, VOS_INADDR_ANY);
            }
            else
            {
                /*  Alloc the corresponding data buffer  */
                newPD->pFrame = (PD_PACKET_T *) vos_memAlloc(TRDP_MAX_PD_PACKET_SIZE);
                if (newPD->pFrame == NULL)
                {
                    vos_memFree(newPD);
                    newPD   = NULL;
                    ret     = TRDP_MEM_ERR;
                }
                else
                {
                    /*    Initialize some fields    */
                    if (vos_isMulticast(destIpAddr))
                    {
                        newPD->addr.mcGroup     = destIpAddr;
                        newPD->privFlags        |= TRDP_MC_JOINT;
                        newPD->addr.destIpAddr  = destIpAddr;
                    }
                    else
                    {
                        newPD->addr.mcGroup     = 0u;
                        newPD->addr.destIpAddr  = 0u;
                    }

                    newPD->lastErr           = TRDP_TIMEOUT_ERR;

                    newPD->addr.comId           = comId;
                    newPD->addr.srcIpAddr       = srcIpAddr1;
                    newPD->addr.srcIpAddr2      = srcIpAddr2;
                    newPD->addr.serviceId       = serviceId;
                    newPD->addr.etbTopoCnt      = etbTopoCnt;
                    newPD->addr.opTrnTopoCnt    = opTrnTopoCnt;

                    /* @note 对于订阅通道，使用timeout值来定义周期,这意味着通道的下一任务时间每次跳跃timeout值
                     * 对于接收，每个while周期将会把所有app下所有socket全部处理一遍
                     * while周期小于等于最小发送周期或最小订阅timeout,有接收时也会立即进入处理
                     */
                    newPD->interval.tv_sec      = timeout / 1000000u;
                    newPD->interval.tv_usec     = timeout % 1000000u;
                    newPD->toBehavior           =
                        (toBehavior == TRDP_TO_DEFAULT) ? appHandle->pdDefault.toBehavior : toBehavior;
                    newPD->grossSize    = TRDP_MAX_PD_PACKET_SIZE;
                    newPD->pUserRef     = pUserRef;
                    newPD->socketIdx    = lIndex;
                    newPD->privFlags    |= TRDP_INVALID_DATA;
                    newPD->privFlags  |= TRDP_TIMED_OUT;
                    newPD->pktFlags     =
                        (pktFlags == TRDP_FLAGS_DEFAULT) ? appHandle->pdDefault.flags : pktFlags;
                    newPD->pfCbFunction =
                        (pfCbFunction == NULL) ? appHandle->pdDefault.pfCbFunction : pfCbFunction;
                    newPD->pCachedDS    = NULL;
                    newPD->magic        = TRDP_MAGIC_SUB_HNDL_VALUE;

                    if (timeout == TRDP_INFINITE_TIMEOUT)
                    {
                        vos_clearTime(&newPD->timeToGo);
                        vos_clearTime(&newPD->interval);
                    }
                    else
                    {
                        vos_getTime(&newPD->timeToGo);
                        vos_addTime(&newPD->timeToGo, &newPD->interval);
                    }

                    /*  append this subscription to our receive queue */
                    trdp_queueInsFirst(&appHandle->pRcvQueue, newPD);

                    *pSubHandle = (TRDP_SUB_T) newPD;
                }
            }
        }
    }

    if (vos_mutexUnlock(appHandle->mutexRxPD) != VOS_NO_ERR)
    {
        vos_printLogStr(VOS_LOG_INFO, "vos_mutexUnlock() failed\n");
    }

    return ret;
}

/**********************************************************************************************************************/
/** Stop receiving PD messages.
 *  Unsubscribe to a specific PD ComID
 *
 *  @param[in]      appHandle            the handle returned by tlc_openSession
 *  @param[in]      subHandle            the handle for this subscription
 *
 *  @retval         TRDP_NO_ERR          no error
 *  @retval         TRDP_PARAM_ERR       parameter error
 *  @retval         TRDP_NOSUB_ERR       not subscribed
 *  @retval         TRDP_NOINIT_ERR      handle invalid
 */
EXT_DECL TRDP_ERR_T tlp_unsubscribe (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_SUB_T          subHandle)
{
    PD_ELE_T    *pElement = (PD_ELE_T *) subHandle;
    TRDP_ERR_T  ret;

    if (pElement == NULL )
    {
        return TRDP_PARAM_ERR;
    }

    if (pElement->magic != TRDP_MAGIC_SUB_HNDL_VALUE)
    {
        return TRDP_NOSUB_ERR;
    }

    if (!trdp_isValidSession(appHandle))
    {
        return TRDP_NOINIT_ERR;
    }

    /*    Reserve mutual access    */
    ret = (TRDP_ERR_T) vos_mutexLock(appHandle->mutexRxPD);
    if (ret == TRDP_NO_ERR)
    {
        TRDP_IP_ADDR_T mcGroup = pElement->addr.mcGroup;
        /*    Remove from queue?    */
        trdp_queueDelElement(&appHandle->pRcvQueue, pElement);
        /*    if we subscribed to an MC-group, check if anyone else did too: */
        if (mcGroup != VOS_INADDR_ANY)
        {
            mcGroup = trdp_findMCjoins(appHandle, mcGroup);
        }
        trdp_releaseSocket(appHandle->ifacePD, pElement->socketIdx, 0u, FALSE, mcGroup);
        pElement->magic = 0u;
        if (pElement->pFrame != NULL)
        {
            vos_memFree(pElement->pFrame);
        }
        if (pElement->pSeqCntList != NULL)
        {
            vos_memFree(pElement->pSeqCntList);
        }
        vos_memFree(pElement);

#ifdef HIGH_PERF_INDEXED
        /* We must check if this publisher is listed in our indexed arrays */
        trdp_indexRemoveSub(appHandle, pElement);
#endif

        ret = TRDP_NO_ERR;
        if (vos_mutexUnlock(appHandle->mutexRxPD) != VOS_NO_ERR)
        {
            vos_printLogStr(VOS_LOG_INFO, "vos_mutexUnlock() failed\n");
        }
    }

    return ret;      /*    Not found    */
}







#ifdef __cplusplus
}
#endif
