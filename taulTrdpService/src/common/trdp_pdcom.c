/******************************************************************************/
/**
 * @file            trdp_pdcom.c
 *
 * @brief           PD通信功能
 *
 * @details			处理trdp协议栈（PD_ELE_T）
 *
 * @note
 *
 * @author          Bernd Loehr, NewTec GmbH
 *
 */

/*******************************************************************************
 * INCLUDES
 */

#include <string.h>

#include "trdp_types.h"
#include "trdp_utils.h"
#include "trdp_pdcom.h"
#include "trdp_if_light.h"
#include "vos_sock.h"
#include "vos_mem.h"
#include "trdp_stats.h"

#ifdef HIGH_PERF_INDEXED
#include "trdp_pdindex.h"
#endif

#ifdef	LADDER_COMPARE
#include "tau_ldLadder_config.h"
#endif

#ifdef TSN_COMMU_TIME_TEST
#include "tsn_stat.h"
#endif
/*******************************************************************************
 * DEFINES
 */

#ifndef UINT32_MAX
#define UINT32_MAX  4294967295U
#endif

/*******************************************************************************
 * TYPEDEFS
 */


/******************************************************************************
 *   GLOBALS
 */

UINT8 SUBNET1_index=TRDP_UINT8_INVALID_VALUE;	/* @gloabal 保存子网1关联的网口索引 @note 这两个变量仅在trdp_checkIfindex中读取和修改 */
UINT8 SUBNET2_index=TRDP_UINT8_INVALID_VALUE;	/* @gloabal 保存子网2关联的网口索引 */

/******************************************************************************
 *   Local Functions
 */
TRDP_ERR_T trdp_checkIfindex(TRDP_SESSION_PT  appHandle,UINT8 recvIfindex);

#ifdef LADDER_COMPARE
int compareSeqWithPeerSub(TRDP_SESSION_PT appHandle,PD_ELE_T* pElement);
int checkTimeoutOfPeerSub(TRDP_SESSION_PT appHandle,PD_ELE_T* pElement);
#endif

/******************************************************************************/
/**
 *	@brief			更新包头序列号和校验码
 *  @param[in]      pPacket         pointer to the packet to update
 */
void    trdp_pdUpdate (PD_ELE_T *pPacket);



/**
 *	@brief			更新tsn延迟测试时间戳，更新包头序列号和校验码
 *  @param[in]      pPacket         pointer to the packet to update
 */
void    trdp_pdUpdateWithPtpStamp(PD_ELE_T *pPacket);


/******************************************************************************/
/**
 *	@brief			发送一个PD包
 *	@details		用通道关联的socket将Frame中的数据包发送到通道配置的destIp
					-如果设置了拉模式Ip则优先使用拉模式Ip
 *  @param[in]      pdSock          socket descriptor
 *  @param[in]      pPacket         pointer to packet to be sent
 *  @param[in]      port            port on which to send
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_IO_ERR
 */
TRDP_ERR_T  trdp_pdSend(SOCKET pdSock,PD_ELE_T *pPacket,UINT16 port);

/******************************************************************************/
/**
 *  @brief			处理PD包的接收
 *	@details		-从接收socket读取PD包，将包拷贝到通道中的新包存放结构体（pNewFrame）
 *					-检查包是否符合协议
 *					-将包与通道中的已接收包做比较
 *					-对于新包，检查其是否为PD request包
 *					-对于新包，用临时PD_ELE_T结构体代替原有的
 *					-调用回调函数处理写共享内存操作
 *  @param[in]      appHandle           session pointer
 *  @param[in]      sock                the socket to read from
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_WIRE_ERR       protocol error (late packet, version mismatch)
 *  @retval         TRDP_QUEUE_ERR      not in queue
 *  @retval         TRDP_CRC_ERR        header checksum
 *  @retval         TRDP_TOPOCOUNT_ERR  invalid topocount
 *  @retval			TRDP_NOSUB_ERR		收到的数据包符合协议，符合app拓扑，但是没有对应的订阅通道
 */
TRDP_ERR_T  trdp_pdReceive (TRDP_SESSION_PT appHandle,SOCKET sock);

/******************************************************************************/
/**
 *	@brief			检查trdp包头各字段是否符合协议
 *	@details		符合条件
 *					-包长度不小于trdp包头长度，不大于最大trdp包长度
 *					-包头CRC校验正确
 *					-协议版本字段符合本程序协议版本（0x0100）
 *					-消息类型字段为PD
 *  @param[in]      pPacket         pointer to the packet to check
 *  @param[in]      packetSize      max size to check
 *  @param[out]     pIsTSN          set to TRUE on return if PD2 frame
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_CRC_ERR
 */
TRDP_ERR_T trdp_pdCheck (PD_HEADER_T *pPacket,UINT32 packetSize,int *pIsTSN);



/******************************************************************************/
/**
 *	@brief			用传入参数设置通道的trdp包头结构体
 *	@details		tsn和非tsn包的包头不同
 *  @param[in]      pPacket         pointer to the packet element to init
 *  @param[in]      type            type the packet
 *  @param[in]      etbTopoCnt      topocount to use for PD frame
 *  @param[in]      opTrnTopoCnt    topocount to use for PD frame
 *  @param[in]      replyComId      Pull request comId
 *  @param[in]      replyIpAddress  Pull request Ip
 *  @param[in]      serviceId       Service Id
 */
void    trdp_pdInit (
    PD_ELE_T    *pPacket,
    TRDP_MSG_T  type,
    UINT32      etbTopoCnt,
    UINT32      opTrnTopoCnt,
    UINT32      replyComId,
    UINT32      replyIpAddress,
    UINT32      serviceId)
{
    if (pPacket == NULL || pPacket->pFrame == NULL)
    {
        return;
    }
#ifdef TSN_SUPPORT
    /* If TSN is set, use the smaller header */
    if (pPacket->privFlags & TRDP_IS_TSN)
    {
        PD2_HEADER_T *pFrameHead = (PD2_HEADER_T *) &pPacket->pFrame->frameHead;
        pFrameHead->sequenceCounter = 0u;
        pFrameHead->protocolVersion = TRDP_VER_TSN_PROTO;
        pFrameHead->msgType         = (UINT8) (type & 0xFF);
        pFrameHead->comId           = vos_htonl(pPacket->addr.comId);
        pFrameHead->datasetLength   = vos_htons((UINT16) pPacket->dataSize);
        pFrameHead->reserved        = vos_htonl(serviceId);
    }
    else
#endif
    {   /* This is the standard header */
        pPacket->pFrame->frameHead.protocolVersion  = vos_htons(TRDP_PROTO_VER);

        pPacket->pFrame->frameHead.etbTopoCnt       = vos_htonl(etbTopoCnt);
        pPacket->pFrame->frameHead.opTrnTopoCnt     = vos_htonl(opTrnTopoCnt);
        pPacket->pFrame->frameHead.comId            = vos_htonl(pPacket->addr.comId);
        pPacket->pFrame->frameHead.msgType          = vos_htons((UINT16)type);
        pPacket->pFrame->frameHead.datasetLength    = vos_htonl(pPacket->dataSize);
        pPacket->pFrame->frameHead.reserved         = vos_htonl(serviceId);
        pPacket->pFrame->frameHead.replyComId       = vos_htonl(replyComId);
        pPacket->pFrame->frameHead.replyIpAddress   = vos_htonl(replyIpAddress);

    }
}

/******************************************************************************/
/**
 *  @brief			用户数据buffer更新到通道（若配置了编组在这里完成）并标记数据有效
 *	@details		传入的用户数据编组后（如果配置了编组）拷贝到通道;若通道未配置数据长度，用当前调用传入的数据长度重新配置
 *  @param[in]      pPacket         pointer to the packet element to send
 *  @param[in]      marshall        pointer to marshalling function
 *  @param[in]      refCon          reference for marshalling function
 *  @param[in]      pData           pointer to data
 *  @param[in]      dataSize        size of data
 *
 *  @retval         TRDP_NO_ERR     no error
 *                  TRDP_PARAM_ERR	传入通道不存在或buffer不存在、或数据长度不合理
 */
TRDP_ERR_T trdp_pdPut (
    PD_ELE_T        *pPacket,
    TRDP_MARSHALL_T marshall,
    void            *refCon,
    const UINT8     *pData,
    UINT32          dataSize)
{
    TRDP_ERR_T ret = TRDP_NO_ERR;
    UINT32          marshalledDataSize=0;

    if (pPacket == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /*- 数据长度无效 */
    if((TRDP_MAX_PD_DATA_SIZE<dataSize) || (0==dataSize))
    {
    	return TRDP_PARAM_ERR;
    }

    /*- 当前调用有待发数据 */
    if (NULL!= pData)
    {
    	/*- 通道尚未配置数据长度 */
        if (pPacket->dataSize == 0u)
        {
            /*- 当前通道的Frame不可用（为用户数据分配的大小为0），创建新的Frame */
            PD_PACKET_T *pTemp;

            /*- 用本次调用传入的dataSize配置通道 */
            pPacket->dataSize   = dataSize;
            pPacket->grossSize  = trdp_packetSizePD(dataSize);

            pTemp = (PD_PACKET_T *) vos_memAlloc(pPacket->grossSize);
            if (pTemp == NULL)
            {
                return TRDP_MEM_ERR;
            }

            /*-拷贝原Frame的包头部数据 */
            memcpy(pTemp, pPacket->pFrame, trdp_packetSizePD(0u));

            /*- 更正包头部数据中的datasetLength */
            pPacket->pFrame->frameHead.datasetLength = vos_htonl(pPacket->dataSize);

            /*- 释放原Frame */
            vos_memFree(pPacket->pFrame);

            /*- 通道绑定新Frame */
            pPacket->pFrame = pTemp;

        }

        /*- 当前通道无需编组 */
        if (!(pPacket->pktFlags & TRDP_FLAGS_MARSHALL) || (marshall == NULL))
        {

            /*- 检查数据长度匹配（当前调用传入的数据长度需等于通道配置的数据长度）  */
        	if (dataSize != pPacket->dataSize)
            {
                return TRDP_PARAM_ERR;
            }

            /*- buffer中的用户数据拷贝到通道协议栈 */
            memcpy(pPacket->pFrame->data, pData, dataSize);
        }
        /*- 当前通道需编组 */
        else
        {
            /*- 检查数据长度匹配（当前调用传入的数据长度需等于通道配置的数据长度）  */
        	if (dataSize != pPacket->dataSize)
            {
                return TRDP_PARAM_ERR;
            }

        	/*- buffer中的用户数据编组后拷贝到通道协议栈 */
            ret = tau_marshall(refCon,
                           pPacket->addr.comId,
                           (UINT8 *) pData,
                           dataSize,
                           pPacket->pFrame->data,
                           &marshalledDataSize,
                           &pPacket->pCachedDS);

            /*- 用编组后的数据长度dataSize计算并配置grossSize和包头数据长度字段 */
            pPacket->grossSize  = trdp_packetSizePD(marshalledDataSize);

            pPacket->pFrame->frameHead.datasetLength = vos_htonl(marshalledDataSize);
            /* @note 不将编组后的dataSize变化配置道通道dataSize */
        }

        if (TRDP_NO_ERR == ret)
        {
        	/*- 标记通道数据有效 */
            pPacket->privFlags = (TRDP_PRIV_FLAGS_T) (pPacket->privFlags & ~(TRDP_PRIV_FLAGS_T)TRDP_INVALID_DATA);

            /*- 更新待发数据更新次数 */
            pPacket->updPkts++;
        }
    }
    /*- 无效调用（datasize为0或超限或buffer为NULL）*/
    else
    {
    	/*- 返回参数错误 */
    	ret=TRDP_PARAM_ERR;
    }

    return ret;
}

#ifdef TSN_SUPPORT
/******************************************************************************/
/** Send TSN PD message immediately
 *
 *  @param[in]      appHandle           session pointer
 *  @param[in]      pSendPD             pointer to element to be sent
 *  @param[in]      pTxTime             time to send
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_IO_ERR         socket I/O error
 */
TRDP_ERR_T  trdp_pdSendImmediateTSN (
    TRDP_SESSION_PT appHandle,
    PD_ELE_T        *pSendPD,
    VOS_TIMEVAL_T   *pTxTime)
{
    VOS_ERR_T       err;
    PD2_PACKET_T    *pFrame = (PD2_PACKET_T *) pSendPD->pFrame;

    /*  Update the sequence counter and re-compute CRC    */
    trdp_pdUpdate(pSendPD);

    /* We pass the error to the application, but we keep on going    */
    pSendPD->sendSize = pSendPD->grossSize;

    err = vos_sockSendTSN(appHandle->ifacePD[pSendPD->socketIdx].sock,
                          (UINT8 *)&pFrame->frameHead,
                          &pSendPD->sendSize,
                          pSendPD->addr.srcIpAddr,
                          pSendPD->addr.destIpAddr,
                          appHandle->pdDefault.port,
                          pTxTime);

    if (err == VOS_NO_ERR)
    {
        appHandle->stats.pd.numSend++;
        pSendPD->numRxTx++;
    }
    return (TRDP_ERR_T) err;
}
#endif

/******************************************************************************/
/**
 *	@brief			不做任何判断地处理发布通道的发送
 *  @param[in]      appHandle           session pointer
 *  @param[in]      pSendPD             pointer to element to be sent
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_IO_ERR         socket I/O error
 */
TRDP_ERR_T  trdp_pdSendImmediate (
    TRDP_SESSION_PT appHandle,
    PD_ELE_T        *pSendPD)
{
    TRDP_ERR_T  err;
    PD_PACKET_T *pFrame = (PD_PACKET_T *) pSendPD->pFrame;

    /*  Update the sequence counter and re-compute CRC    */
    trdp_pdUpdate(pSendPD);

    /* Publisher check from Table A.5:
     Actual topography counter values <-> Locally stored with publish */
    if ( !trdp_validTopoCounters(appHandle->etbTopoCnt,
                                 appHandle->opTrnTopoCnt,
                                 vos_ntohl(pSendPD->pFrame->frameHead.etbTopoCnt),
                                 vos_ntohl(pSendPD->pFrame->frameHead.opTrnTopoCnt)))
    {
        err = TRDP_TOPO_ERR;
        vos_printLogStr(VOS_LOG_DBG, "Sending PD: TopoCount is out of date!\n");
        /* We pass the error to the application, and do not send!    */
    }
    else
    {

        pSendPD->sendSize = pSendPD->grossSize;

        err = (TRDP_ERR_T) vos_sockSendUDP(appHandle->ifacePD[pSendPD->socketIdx].sock,
                                           (UINT8 *)&pFrame->frameHead,
                                           &pSendPD->sendSize,
                                           pSendPD->addr.destIpAddr,
                                           appHandle->pdDefault.port);

        if (err == TRDP_NO_ERR)
        {
            appHandle->stats.pd.numSend++;
            pSendPD->numRxTx++;
        }
    }

    return err;
}

/******************************************************************************/
/**
 *	@brief			为指定的PD发送通道处理发送
 *	@note			不判断通道是否到时
 *  @param[in]      appHandle           session pointer
 *  @param[in]      ppElement           pointer to pointer of the element to send
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_IO_ERR         socket I/O error
 */
TRDP_ERR_T  trdp_pdSendElement (
    TRDP_SESSION_PT appHandle,
    PD_ELE_T        * *ppElement)
{
    TRDP_ERR_T  err     = TRDP_NO_ERR;
    PD_ELE_T    *iterPD = *ppElement;

    /*- 以下所有处理和trdp_pdSendQueued()对到时通道的处理一模一样 */

    if (!(iterPD->privFlags & TRDP_INVALID_DATA))
    {
        if ((iterPD->privFlags & TRDP_REQ_2B_SENT) &&
            (iterPD->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PD)))       /*  PULL packet?  */
        {
            iterPD->pFrame->frameHead.msgType = vos_htons(TRDP_MSG_PP);
        }

        trdp_pdUpdate(iterPD);

        if ( !trdp_validTopoCounters( appHandle->etbTopoCnt,
                                      appHandle->opTrnTopoCnt,
                                      vos_ntohl(iterPD->pFrame->frameHead.etbTopoCnt),
                                      vos_ntohl(iterPD->pFrame->frameHead.opTrnTopoCnt)))
        {
            err = TRDP_TOPO_ERR;
            vos_printLogStr(VOS_LOG_DBG, "Sending PD: TopoCount is out of date!\n");
        }

        else if (iterPD->socketIdx == TRDP_INVALID_SOCKET_INDEX)
        {
            vos_printLogStr(VOS_LOG_DBG, "Sending PD: Socket invalid!\n");
        }
        /*    Send the packet if it is not redundant    */
        else if (!(iterPD->privFlags & TRDP_REDUNDANT))
        {
            err = trdp_pdSend(appHandle->ifacePD[iterPD->socketIdx].sock, iterPD, appHandle->pdDefault.port);
            if (err == TRDP_NO_ERR)
            {
                appHandle->stats.pd.numSend++;
                iterPD->numRxTx++;
            }
            else
            {

            }
        }
    }

    if ((iterPD->privFlags & TRDP_REQ_2B_SENT) &&
        (iterPD->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PP)))
    {
        iterPD->pFrame->frameHead.msgType = vos_htons(TRDP_MSG_PD);
    }
#ifndef HIGH_PERF_INDEXED
    else if (timerisset(&iterPD->interval))
    {
        TRDP_TIME_T now;

        vos_getTime(&now);

        vos_addTime(&iterPD->timeToGo, &iterPD->interval);

        if (vos_cmpTime(&iterPD->timeToGo, &now) <= 0)
        {
            iterPD->timeToGo = now;
            vos_addTime(&iterPD->timeToGo, &iterPD->interval);
        }
    }
#endif

    if (iterPD->privFlags & TRDP_REQ_2B_SENT)
    {
        iterPD->privFlags = (TRDP_PRIV_FLAGS_T) (iterPD->privFlags & ~(TRDP_PRIV_FLAGS_T)TRDP_REQ_2B_SENT);
    }

    if (iterPD->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PR))
    {
        PD_ELE_T *pTemp;

        trdp_releaseSocket(appHandle->ifacePD, iterPD->socketIdx, 0u, FALSE, VOS_INADDR_ANY);

        pTemp = iterPD->pNext;

        trdp_queueDelElement(&appHandle->pSndQueue, iterPD);
        iterPD->magic = 0u;
        if (iterPD->pSeqCntList != NULL)
        {
            vos_memFree(iterPD->pSeqCntList);
        }
        vos_memFree(iterPD->pFrame);
        vos_memFree(iterPD);

        *ppElement = pTemp;
    }

    return err;
}

/******************************************************************************/
/**
 *	@brief			为所有到时的pd发送通道处理发送
 *	@details		遍历app发送队列中所有通道，处理到时或标记了立即发送请求的通道：
 *					- 若数据有效，更新包序列号、检查发送条件（socket有效、拓扑正确、非备机）后发送数据包
 *					- 更新任务时间
 *	@note			-不处理TSN通道
 *					-对于标记了立即发送请求的通道，将其包类型字段改为PP后发送，发送完成后将包类型字段恢复为PD并清除立即发送请求标志，该处理不更新任务时间
 *					-发送处理完毕后对于包类型字段为PR的通道进行删除
 *
 *  @param[in]      appHandle           session pointer
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_IO_ERR         socket I/O error
 */
TRDP_ERR_T  trdp_pdSendQueued (
    TRDP_SESSION_PT appHandle)
{
	/*- 获取发送队列中的第一个通道 */
    PD_ELE_T    *iterPD = appHandle->pSndQueue;

    /*- 初始化临时变量 */
    TRDP_TIME_T now;
    TRDP_ERR_T  err = TRDP_NO_ERR;

    /*- 遍历app发送队列中所有通道 */
    while (iterPD != NULL)
    {
        /*- 获取当前时间 */
        vos_getTime(&now);

        /*- 当前通道配置了TSN */
        if (iterPD->privFlags & TRDP_IS_TSN)
        {
        	/*- 跳过后续处理 */
            iterPD = iterPD->pNext;
            continue;
        }

        /*- 到时的周期通道或标记了立即发送请求的通道 */
        if ((timerisset(&iterPD->interval) &&
             !timercmp(&iterPD->timeToGo, &now, >)) ||
            (iterPD->privFlags & TRDP_REQ_2B_SENT))
        {
            /*- 无效数据标志位已置否 */
            if (!(iterPD->privFlags & TRDP_INVALID_DATA))
            {
            	/*- 处理立即发送请求 */
                if ((iterPD->privFlags & TRDP_REQ_2B_SENT) &&
                    (iterPD->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PD)))
                {
                    iterPD->pFrame->frameHead.msgType = vos_htons(TRDP_MSG_PP);
                }

                /*- 更新包头序列号和校验码 */
#ifndef TSN_COMMU_TIME_TEST
                trdp_pdUpdate(iterPD);
#else
                trdp_pdUpdateWithPtpStamp(iterPD);
#endif

                /*- 检查包头拓扑是否与app拓扑匹配（相等或为0） */
                if ( !trdp_validTopoCounters( appHandle->etbTopoCnt,
                                              appHandle->opTrnTopoCnt,
                                              vos_ntohl(iterPD->pFrame->frameHead.etbTopoCnt),
                                              vos_ntohl(iterPD->pFrame->frameHead.opTrnTopoCnt)))
                {
                    err = TRDP_TOPO_ERR;
                    vos_printLogStr(VOS_LOG_DBG, "Sending PD: TopoCount is out of date!\n");
                }
                /*- 检查到通道使用的socket无效 */
                else if (iterPD->socketIdx == TRDP_INVALID_SOCKET_INDEX)
                {
                    vos_printLogStr(VOS_LOG_DBG, "Sending PD: Socket invalid!\n");

                    /*- 获取下一通道 */
                    PD_ELE_T *pTemp;
                    pTemp = iterPD->pNext;

                    /*- 从发送队列中去除当前通道 */
                    trdp_queueDelElement(&appHandle->pSndQueue, iterPD);

                    /*- 清除当前通道的初始化标志 */
                    iterPD->magic = 0u;

                    /*- 释放通道所有内存资源 */
                    if (iterPD->pSeqCntList != NULL)
                    {
                        vos_memFree(iterPD->pSeqCntList);
                    }

                    vos_memFree(iterPD->pFrame);
                    vos_memFree(iterPD);

                    /*- 继续处理下一通道 */
                    iterPD = pTemp;
                    continue;
                }
                /*- 通道未标记冗余 */
                else if (!(iterPD->privFlags & TRDP_REDUNDANT))
                {
                	/* @note	原程序在此处保留了可配的回调，即如果通道配置了回调函数把发送包的数据写入共享内存
                	 * 			当前版本不为发送数据写共享内存，故去除
                	 */

                    /*- 处理udp发送 */
                    err = trdp_pdSend(appHandle->ifacePD[iterPD->socketIdx].sock, iterPD, appHandle->pdDefault.port);
                    /*- 发送成功 */
                    if (TRDP_NO_ERR == err)
                    {
                    	/*- 更新统计数据 */
                        appHandle->stats.pd.numSend++;
                        iterPD->numRxTx++;
                    }
                    /*- 发送失败 */
                    else
                    {
                    	//无处理
                    }
                }
            }
            /*- 无效标志位未置否 */
            else
            {

            }

            //更新通道任务时间

            /*- 通道标记了立即发送请求且当前包类型为拉模式回复 */
            if ((iterPD->privFlags & TRDP_REQ_2B_SENT) &&
                (iterPD->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PP)))
            {
                /*- 重置数据包类型字段 */
                iterPD->pFrame->frameHead.msgType = vos_htons(TRDP_MSG_PD);

                /* @note 不更新拉模式回复的任务时间 */
            }
            /*- 通道配置了周期 */
            else if (timerisset(&iterPD->interval))
            {
            	/*- 更新通道下一任务时间 */
                vos_addTime(&iterPD->timeToGo, &iterPD->interval);

                if (vos_cmpTime(&iterPD->timeToGo, &now) <= 0)
                {
                    /* in case of a delay of more than one interval - avoid sending it in the next cycle again */
                    iterPD->timeToGo = now;
                    vos_addTime(&iterPD->timeToGo, &iterPD->interval);
                }
            }

            /*- 清除立即发送请求标志 */

            if (iterPD->privFlags & TRDP_REQ_2B_SENT)
            {
                iterPD->privFlags = (TRDP_PRIV_FLAGS_T) (iterPD->privFlags & ~(TRDP_PRIV_FLAGS_T)TRDP_REQ_2B_SENT);
            }


            /*- 通道当前包类型为拉模式请求 */
            if (iterPD->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PR))
            {
            	/*- 删除该通道 */
                PD_ELE_T *pTemp;
                /*- 释放socket */
                trdp_releaseSocket(appHandle->ifacePD, iterPD->socketIdx, 0u, FALSE, VOS_INADDR_ANY);

                /*- 获取下一通道 */
                pTemp = iterPD->pNext;

                /*- 从发送队列中去除当前通道 */
                trdp_queueDelElement(&appHandle->pSndQueue, iterPD);

                /*- 清除当前通道的初始化标志 */
                iterPD->magic = 0u;

                /*- 释放通道所有内存资源 */
                if (iterPD->pSeqCntList != NULL)
                {
                    vos_memFree(iterPD->pSeqCntList);
                }

                vos_memFree(iterPD->pFrame);
                vos_memFree(iterPD);

                /*- 继续处理下一通道 */
                iterPD = pTemp;
                continue;
            }
        }
        //未到时通道且未标记立即发送请求
        else
        {

        }

        iterPD = iterPD->pNext;
    }
    return err;
}

/******************************************************************************/
/**
 *  @brief			处理PD包的接收
 *	@details		-从接收socket读取PD包，将包拷贝到通道中的新包存放结构体（pNewFrame）
 *					-检查包是否符合协议
 *					-将包与通道中的已接收包做比较
 *					-对于新包，检查其是否为PD request包
 *					-对于新包，用临时PD_ELE_T结构体代替原有的
 *					-调用回调函数处理写共享内存操作
 *  @param[in]      appHandle           session pointer
 *  @param[in]      sock                the socket to read from
 *
 *  @retval         TRDP_NO_ERR         no error
 *  @retval         TRDP_PARAM_ERR      parameter error
 *  @retval         TRDP_WIRE_ERR       protocol error (late packet, version mismatch)
 *  @retval         TRDP_QUEUE_ERR      not in queue
 *  @retval         TRDP_CRC_ERR        header checksum
 *  @retval         TRDP_TOPOCOUNT_ERR  invalid topocount
 *  @retval			TRDP_NOSUB_ERR		收到的数据包符合协议，符合app拓扑，但是没有对应的订阅通道
 */
TRDP_ERR_T  trdp_pdReceive (
    TRDP_SESSION_PT appHandle,
    SOCKET          sock)
{
	/*- 获取app下的轮空frame */
    PD_HEADER_T         *pNewFrameHead      = &appHandle->pNewFrame->frameHead;

#ifdef TSN_SUPPORT
    PD2_HEADER_T        *pTSNFrameHead = (PD2_HEADER_T *) pNewFrameHead;
#endif

    /*- 初始化临时变量 */
    PD_ELE_T            *pExistingElement   = NULL;
    PD_ELE_T            *pPulledElement;
    TRDP_MSG_T          msgType;
    UINT8				recvIfindex=0;
    UINT32 				newSeqCnt=1u;//取一个尽量小的非0值
    int checkSequenceCounterRt=-1;

#ifdef	TSN_COMMU_TIME_TEST
    UINT64				recvedSendTick=0;
    UINT64				recvTick=0;
    UINT64				commuDelay=0;
	UINT32 				tempNumMissed=0;
	UINT8				missFlag=0;

#endif

    TRDP_PD_INFO_T theMessage;/* 回调函数的参数结构体 */
    memset(&theMessage, 0, sizeof(TRDP_PD_INFO_T));

    /*- 初始化标志位 */
    TRDP_ERR_T          err             = TRDP_UNKNOWN_ERR;
    int                 informUser      = FALSE;
    int                 isTSN           = FALSE;

    /*- 初始化返回参数 */
    UINT32              recSize         = TRDP_MAX_PD_PACKET_SIZE;
    TRDP_ADDRESSES_T    subAddresses    = { 0u, 0u, 0u, 0u, 0u, 0u, 0u, 0u};

    /*- 接收数据包到pNewFrame开始的buffer */
    /*- @note vos_sockReceiveUDP()的返回类型为VOS_ERR_T，该枚举被TRDP_ERR_T包含，可以直接使用强制转换 */
    err = (TRDP_ERR_T) vos_sockReceiveUDP(sock,
                                          (UINT8 *) pNewFrameHead,
                                          &recSize,
                                          &subAddresses.srcIpAddr,
                                          NULL,
                                          &subAddresses.destIpAddr,
                                          FALSE,
										  &recvIfindex);

    /*- 接收成功 */
    if (err == TRDP_NO_ERR)
    {
#ifdef INTERFACE_SEPARATE
//printf("app:%d recv.comid:%u if:%u\n",appHandle==appHandle1,vos_ntohl(pNewFrameHead->comId),recvIfindex);
    	/*- 检查返回的索引是否对应当前调用的app */
    	err=trdp_checkIfindex(appHandle,recvIfindex);
#endif
    }


    /*- 索引与app对应正确 */
    if(err == TRDP_NO_ERR)
    {

#ifdef	TSN_COMMU_TIME_TEST
    	recvTick=vos_getPtpTime(appHandle->ptpClockId);
    	//printf("[TESTCHECK]recv ptp time:%llu,id:%d,app:%d,sock:%d\n",recvTick,appHandle->ptpClockId,appHandle,sock);
#endif

        /*- 检查包头内容是否符合协议 */
        err = trdp_pdCheck(pNewFrameHead, recSize, &isTSN);

        /*- 根据检查结果更新统计量 */
        switch (err)
        {
        	/*- 当前包符合协议 */
            case TRDP_NO_ERR:
            	/*- 更新统计量 */
                appHandle->stats.pd.numRcv++;
                break;
            /*- 当前包错误（trdp包头CRC校验失败） */
            case TRDP_CRC_ERR:
            	/*- 更新统计量后退出 */
                appHandle->stats.pd.numCrcErr++;
            /*- 当前包不符合协议 */
            case TRDP_WIRE_ERR:
            	/*- 更新统计量后退出 */
                appHandle->stats.pd.numProtErr++;
            /*- 检查到其他错误 */
            default:
            	;
        }
    }

    /*- 当前包符合协议 */
    if(err == TRDP_NO_ERR)
    {
    	//printf("[TRDP]ifindex:%d comid:%d destIp:%s \n",recvIfindex,vos_ntohl(pNewFrameHead->comId),vos_ipDotted(subAddresses.destIpAddr));
    	/*- 提取包头信息 */

#ifdef TSN_SUPPORT
		if (TRUE == isTSN)
		{
			/*  Compute the subscription handle */
			subAddresses.comId          = vos_ntohl(pTSNFrameHead->comId);
			subAddresses.etbTopoCnt     = 0u;   /* they do not matter */
			subAddresses.opTrnTopoCnt   = 0u;
			subAddresses.serviceId      = vos_ntohl(pTSNFrameHead->reserved);
			msgType = pTSNFrameHead->msgType;
		}
		else    /* no PULL on TSN */
#endif
		{
			subAddresses.comId          = vos_ntohl(pNewFrameHead->comId);
			subAddresses.etbTopoCnt     = vos_ntohl(pNewFrameHead->etbTopoCnt);
			subAddresses.opTrnTopoCnt   = vos_ntohl(pNewFrameHead->opTrnTopoCnt);
			subAddresses.serviceId      = vos_ntohl(pNewFrameHead->reserved);
			msgType = vos_ntohs(pNewFrameHead->msgType);

#ifdef	TSN_COMMU_TIME_TEST

			//使用pd包中无效的replyComId和replyIpAdderss来装tsn发送时间戳，使用reserved字段来标记数据包是否加入了tsn测试时间戳
			if(0x07D8==subAddresses.serviceId)
			{
				recvedSendTick=vos_ntohl(pNewFrameHead->replyComId)*0x100000000u+vos_ntohl(pNewFrameHead->replyIpAddress);
			}
#endif
		}

	    /*- 为当前包查找app下相应的通道 */

	#ifdef HIGH_PERF_INDEXED
	    if ((appHandle->pSlot == NULL) ||
	        (appHandle->pSlot->noOfRxEntries == 0))
	    {
	        /*  If not set up until now, we issue a warning, but handle the data...   */
	        vos_printLogStr(VOS_LOG_WARNING, "Receiving PD while tlc_updateSession() not yet called or rcvIdx empty.\n");
	        pExistingElement = trdp_queueFindSubAddr(appHandle->pRcvQueue, &subAddresses);
	    }
	    else
	    {
	        /*  This is the fast, indexed access to our subscriptions!   */
	        pExistingElement = trdp_indexedFindSubAddr(appHandle, &subAddresses);
	    }
	#else
	    pExistingElement = trdp_queueFindSubAddr(appHandle->pRcvQueue, &subAddresses);
	#endif

	    /*- 当前包有对应通道 */
	    if (pExistingElement != NULL)
	    {

	    	//printf("element match!packet comid:%d sub comid:%d\n",subAddresses.comId,pExistingElement->addr.comId);
	    	/* 原程序中，前面已经用app的拓扑值进行了拓扑检查，当当前包拓扑为0时，两次检查都能通过；当不为0时，需满足包=app=通道 或 包=app且通道=0
	    	 * 再进行通道中的拓扑配置有何意义?对于通道记录拓扑错误来说，如果一个包通过了前面的拓扑检查，那么它一定能通过后一个拓扑检查，else永远不会被经过
	    	 * 除非将通道拓扑配成和app不同且非0，那将没有包可以通过两个检查
	    	 * 修改：去掉第一次检查，第二次检查用包字段做fillter */

	    	/*- 当前包拓扑字段为0或通道拓扑值与当前包吻合 */
	    	if (((subAddresses.etbTopoCnt == 0) && (subAddresses.opTrnTopoCnt == 0)&&
	    			(trdp_validTopoCounters(appHandle->etbTopoCnt,
	    					appHandle->opTrnTopoCnt,
							subAddresses.etbTopoCnt,
							subAddresses.opTrnTopoCnt))) ||
					trdp_validTopoCounters(pExistingElement->addr.etbTopoCnt,
	                                   pExistingElement->addr.opTrnTopoCnt,
									   subAddresses.etbTopoCnt,
									   subAddresses.opTrnTopoCnt))
	        {
	    		/*- 提取当前包序列号 */
	            newSeqCnt = vos_ntohl(pNewFrameHead->sequenceCounter);

	            /* 本次接收的srcIp记录到通道 */
	            pExistingElement->lastSrcIP = subAddresses.srcIpAddr;
	            /* 本次接收的destIp保存到通道 */
	            pExistingElement->addr.destIpAddr = subAddresses.destIpAddr;

	            /*- 当前包为起始包或为通道超时后的首次接收 */
	            if ((newSeqCnt == 0u) || (pExistingElement->privFlags & TRDP_TIMED_OUT))
	            {
	            	/*- 重置通道序列号记录 */
	                trdp_resetSequenceCounter(pExistingElement, subAddresses.srcIpAddr, msgType);
	            }

	            /*
	             * @note trdp允许一个通道（comId）同时接收来自多个发送端（srcIp不同）的数据，且数据相同，序列号同步（应该是考虑到发送端双网口冗余）
	             * 当序列号不同步时，trdp仍然会接收两个源数据数据，此时通道numMissed会暴增
	             * 当数据不相同时，trdp仍然会接收数据,但sdt层如果没有不同步，两个网口的数据仍然会被呈现给用户
	             * 总的来看不会带来危险
	             */

	            /*- 检查当前包是否为来自一个源数据的最新包 */
	            checkSequenceCounterRt=trdp_checkSequenceCounter(pExistingElement,
                        newSeqCnt,
                        subAddresses.srcIpAddr, msgType);
	            switch (checkSequenceCounterRt)
	            {
	                case 0:
	                    break;
	                case -1:
	                	err=TRDP_MEM_ERR;
	                	break;
	                case 1:
	                    vos_printLog(VOS_LOG_DBG, "Old PD data ignored (SrcIp: %s comId %u)\n", vos_ipDotted(
	                                     subAddresses.srcIpAddr), subAddresses.comId);
	                    err=TRDP_NO_ERR;//此处需要的是直接返回无错误
	            }

	        }
	    	/*- 包拓扑与通道不匹配 */
	        else
	        {
	        	//printf("no element match!packet comid:%d\n",subAddresses.comId);
	        	/*- 统计并汇报拓扑错误 */
	            appHandle->stats.pd.numTopoErr++;
	            pExistingElement->lastErr = TRDP_TOPO_ERR;
	            err = TRDP_TOPO_ERR;

	            /* @note 原程序在此处打开了回调开关，该处理将导致拓扑不符的的包无需经过序列号比较就能调用回调，但又不进行任何通道记录和更新处理，不符合逻辑
	             * 改动后去掉了这个操作，拓扑错误将和无订阅错误一样，仅作统计和报告错误处理
	             */
	        }

	    }//当前包有对应的接收通道
	    /*- 当前包没有对应接收通道 */
	    else
	    {
	    	/*- 统计并汇报无订阅错误 */
	        vos_printLog(VOS_LOG_DBG, "No subscription (SrcIp: %s comId %u)\n", vos_ipDotted(subAddresses.srcIpAddr),vos_ntohl(pNewFrameHead->comId));
	        appHandle->stats.pd.numNoSubs++;
	        err = TRDP_NOSUB_ERR;
	    }
    }//符合协议的包处理

#ifdef	TSN_COMMU_TIME_TEST

    if(TRDP_NO_ERR==err)
    {
		if(0u!=recvedSendTick && 0u!=recvTick && recvTick>=recvedSendTick)
		{
			/*- 有对应订阅、拓扑正确且tsn延迟记录完成的包（不考虑新旧）*/

			/*- 1.更新tsn延迟统计量 */
			commuDelay=recvTick-recvedSendTick;
			if(0u==appHandle->stats.tsnPd.maxDelay){appHandle->stats.tsnPd.maxDelay=commuDelay;}
			else if(appHandle->stats.tsnPd.maxDelay<commuDelay){appHandle->stats.tsnPd.maxDelay=commuDelay;}
			if(0u==appHandle->stats.tsnPd.minDelay){appHandle->stats.tsnPd.minDelay=commuDelay;}
			else if(appHandle->stats.tsnPd.minDelay>commuDelay)
			{appHandle->stats.tsnPd.minDelay=commuDelay;}
			//appHandle->stats.tsnPd.avgDelay=(((REAL64)appHandle->stats.tsnPd.numRecv / (appHandle->stats.tsnPd.numRecv+1)) * (REAL64)appHandle->stats.tsnPd.avgDelay + (REAL64)commuDelay/ (appHandle->stats.tsnPd.numRecv+1));
			appHandle->stats.tsnPd.numRecv++;

			/*- 2.输出本次统计量 */

			if (0==checkSequenceCounterRt && (newSeqCnt > 0u) && (newSeqCnt > (pExistingElement->curSeqCnt + 1u)))
			{tempNumMissed=pExistingElement->numMissed + newSeqCnt - pExistingElement->curSeqCnt - 1u;missFlag=1;}
			else if (0==checkSequenceCounterRt && pExistingElement->curSeqCnt > newSeqCnt)
			{tempNumMissed=pExistingElement->numMissed + UINT32_MAX - pExistingElement->curSeqCnt + newSeqCnt;missFlag=1;}

			tsn_outputTestInfo(appHandle->realIP,recvIfindex,subAddresses.comId,missFlag,tempNumMissed,newSeqCnt,commuDelay,recvTick);
		}
		else if(0u!=recvedSendTick || (0x07D8==subAddresses.serviceId))
		{
			/*- 1.不更新tsn延迟统计量 */
			commuDelay=0;

			/*- 2.输出本次统计量 */
			if (0==checkSequenceCounterRt && (newSeqCnt > 0u) && (newSeqCnt > (pExistingElement->curSeqCnt + 1u)))
			{tempNumMissed=pExistingElement->numMissed + newSeqCnt - pExistingElement->curSeqCnt - 1u;missFlag=1;}
			else if (0==checkSequenceCounterRt && pExistingElement->curSeqCnt > newSeqCnt)
			{tempNumMissed=pExistingElement->numMissed + UINT32_MAX - pExistingElement->curSeqCnt + newSeqCnt;missFlag=1;}

			tsn_outputTestInfo(appHandle->realIP,recvIfindex,subAddresses.comId,missFlag,tempNumMissed,newSeqCnt,commuDelay,recvTick);
		}
		else
		{
			/*- 1.不更新tsn延迟统计量 */
		}
    }

#endif

    /*- 有对应订阅且拓扑正确的新包 */
    if((TRDP_NO_ERR==err) && (0==checkSequenceCounterRt))
    {
        /* @note 至此，本次接收成功,接下来进行的是对通道的更新和处理回调（需判断是否需要启动回调） */

        /*- 如果包序列号发生了跳跃 */
        if ((newSeqCnt > 0u) && (newSeqCnt > (pExistingElement->curSeqCnt + 1u)))
        {
        	/*- 更新通道漏包数 */
            pExistingElement->numMissed += newSeqCnt - pExistingElement->curSeqCnt - 1u;
        }
        /*- 如果包序列号发生了向后跳跃 */
        else if (pExistingElement->curSeqCnt > newSeqCnt)
        {
        	/*- 更新通道漏包数 */
            pExistingElement->numMissed += UINT32_MAX - pExistingElement->curSeqCnt + newSeqCnt;
        }

        /*- 记录当前包序列号到通道 */
        pExistingElement->curSeqCnt = vos_ntohl(pNewFrameHead->sequenceCounter);

        /* 回调的判断与相关配置 */

#ifdef TSN_SUPPORT
        /* tsn无论如何都回调 */
        if (TRUE == isTSN)
        {
            pExistingElement->dataSize = vos_ntohs(pTSNFrameHead->datasetLength);
            pExistingElement->grossSize = trdp_packetSizePD2(pExistingElement->dataSize);

            informUser = TRUE;

            theMessage.resultCode   = (TRDP_SIGNAL_T)err;
        }
        /* 非tsn要根据通道回调功能启用标志、数据是否更新、是不是超时重连、双网冗余策略（取更新或取单网）回调 */
        else
#endif
        {
        	/*- 将本次接收的数据长度更新到通道,以便写共享内存环节使用 ??需要讨论需求，当前版本是否指定数据长度 */
            pExistingElement->dataSize = vos_ntohl(pNewFrameHead->datasetLength);
            pExistingElement->grossSize = trdp_packetSizePD(pExistingElement->dataSize);

            /*- 当前通道启用回调  */
            if (pExistingElement->pktFlags & TRDP_FLAGS_CALLBACK)
            {

            	/* 回调判断1：用户数据重复检查 */
#if 1
                /*- 当前版本不进行数据重复检查 */
                informUser = TRUE;
#else
                /*- 通道配置了强制回调选项或通道超时 */
                if ((pExistingElement->pktFlags & TRDP_FLAGS_FORCE_CB) ||
                    (pExistingElement->privFlags & TRDP_TIMED_OUT))
                {
                    informUser = TRUE;
                }
                /*- 用户数据更新 */
                else if (0 != memcmp(appHandle->pNewFrame->data,
                                     pExistingElement->pFrame->data,
                                     pExistingElement->dataSize))
                {
                    informUser = TRUE;
                }
                else
                {
                	;
                }
#endif

                /* 回调判断2：双网取新检查 */

#ifdef LADDER_COMPARE

                /* 在使用双网取新策略时，比较另一会话，禁用非双网更新包的回调 */

                if(compareSeqWithPeerSub(appHandle,pExistingElement)<0)
                {
                	informUser  = FALSE;
                }

#endif
				/* 回调判断3：超时重连检查 */

            	/*- 通道超时后的首次接收 */
            	if(TRDP_TIMEOUT_ERR==pExistingElement->lastErr)
            	{
            		/*- 向应用通知重新连接 */
            		theMessage.resultCode   = TRDP_RECONNECT_SIG;

            		/*- 重新连接时无视条件1、2启用回调 */
            		informUser  = TRUE;
            		//printf("app:%d reconect.comid:%u if:%u callback:%d\n",appHandle==appHandle1,subAddresses.comId,recvIfindex,informUser == TRUE);
            	}
            	/*- 不是通道超时后的首次接收 */
            	else
            	{
            		theMessage.resultCode   = (TRDP_SIGNAL_T)err;// 若无出错，那么本err来自check_pd()
            		//printf("app:%d not reconect.comid:%u if:%u callback:%d\n",appHandle==appHandle1,subAddresses.comId,recvIfindex,informUser == TRUE);
            	}
            }
            /*- 当前通道禁用回调 */
            else
            {
            	/*- 不启用informUser */
            }

        }

        /*-将下一任务时间更新至一个周期后 */
        vos_getTime(&pExistingElement->timeToGo);
        vos_addTime(&pExistingElement->timeToGo, &pExistingElement->interval);

        /*- 更新统计计数器 */
        pExistingElement->numRxTx++;
        /*- 更新通道上一错误 */
        pExistingElement->lastErr   = TRDP_NO_ERR;
        /*- 置否超时标志位 */
        pExistingElement->privFlags =
            (TRDP_PRIV_FLAGS_T) (pExistingElement->privFlags & ~(TRDP_PRIV_FLAGS_T)TRDP_TIMED_OUT);

        /*- 置否通道无效数据标志位 */
        pExistingElement->privFlags =
            (TRDP_PRIV_FLAGS_T) (pExistingElement->privFlags & ~(TRDP_PRIV_FLAGS_T)TRDP_INVALID_DATA);

        /*- 交换通道Frame(数据包结构体)与apphandle的NewFrame指针 */
        /* @note 相当于app下一共有n个通道，n+1个frame空间，通道轮流链接（用指针）这些空间，它们的大小都是最大包大小 */
        {
            PD_PACKET_T *pTemp = pExistingElement->pFrame;
            pExistingElement->pFrame    = appHandle->pNewFrame;
            appHandle->pNewFrame        = pTemp;
        }

        /*- 如果是拉模式的请求数据包 */
        if ((msgType == TRDP_MSG_PR) && (FALSE == isTSN))/* @note 目前TSN不支持拉模式 */
        {
            /*- 请求发送锁 */
            if (vos_mutexLock(appHandle->mutexTxPD) != VOS_NO_ERR)
            {
                vos_printLogStr(VOS_LOG_DBG, "A pull request could not get the TxPd mutex!\n");
            }

            /*- 如果当前包来自统计拉模式的专用通道 */
            if (vos_ntohl(pNewFrameHead->comId) == TRDP_STATISTICS_PULL_COMID)
            {
            	/*- 查找己方的统计回复专用通道 */
                pPulledElement = trdp_queueFindComId(appHandle->pSndQueue, TRDP_GLOBAL_STATS_REPLY_COMID);
                /*- 己方配置了统计回复专用通道  */
                if (pPulledElement != NULL)
                {
                    pPulledElement->addr.destIpAddr = vos_ntohl(pNewFrameHead->replyIpAddress);

                    trdp_pdInit(pPulledElement, TRDP_MSG_PP, appHandle->etbTopoCnt, appHandle->opTrnTopoCnt,
                                0u, 0u, vos_ntohl(pNewFrameHead->reserved));

                    trdp_pdPrepareStats(appHandle, pPulledElement);
                }
                /*- 己方未配置统计回复专用通道  */
                else
                {
                    vos_printLogStr(VOS_LOG_DBG, "Statistics request failed, not published!\n");
                }
            }
            /*- 如果当前包来自其他拉模式通道 */
            else
            {

                UINT32 replyComId = vos_ntohl(pNewFrameHead->replyComId);

                if (replyComId == 0u)
                {
                    replyComId = vos_ntohl(pNewFrameHead->comId);
                }

                /*  Find requested publish element  */
                pPulledElement = trdp_queueFindComId(appHandle->pSndQueue, replyComId);
            }

            if (pPulledElement != NULL)
            {
                /*  Set the destination address of the requested telegram either to the replyIp or the source Ip of the
                 requester   */

                if (pNewFrameHead->replyIpAddress != 0u)
                {
                    pPulledElement->pullIpAddress = vos_ntohl(pNewFrameHead->replyIpAddress);
                }
                else
                {
                    pPulledElement->pullIpAddress = subAddresses.srcIpAddr;
                }

                /* trigger immediate sending of PD  */
                pPulledElement->privFlags |= TRDP_REQ_2B_SENT;

                if (trdp_pdSendElement(appHandle, &pPulledElement) != TRDP_NO_ERR)
                {
                    /*  We do not break here, only report error */
                    vos_printLogStr(VOS_LOG_DBG, "Error sending one or more PD packets\n");
                }
                informUser = TRUE;
            }
            /* We should release the mutex as soon as possible! */
            (void) vos_mutexUnlock(appHandle->mutexTxPD);
        }
        else
        {
        	;
        }
    }

    /*- 若当前包应开启回调 */
    if ((pExistingElement != NULL) &&
        (informUser == TRUE))
    {
        /*- 若配置了回调且提供了回调函数 */
        if ((pExistingElement->pktFlags & TRDP_FLAGS_CALLBACK)
            && (pExistingElement->pfCbFunction != NULL))
        {
        	/*- 填充回调函数的参数结构体 */

            theMessage.comId        = pExistingElement->addr.comId;
            theMessage.srcIpAddr    = pExistingElement->lastSrcIP;
            theMessage.destIpAddr   = subAddresses.destIpAddr;
            theMessage.msgType      = msgType;
            theMessage.seqCount     = pExistingElement->curSeqCnt;
            theMessage.pUserRef     = pExistingElement->pUserRef; /* User reference given with the local subscribe? */

#ifdef TSN_SUPPORT
            if (TRUE == isTSN)
            {
                theMessage.etbTopoCnt   = 0u;
                theMessage.opTrnTopoCnt = 0u;
                theMessage.replyComId   = 0u;
                theMessage.replyIpAddr  = VOS_INADDR_ANY;
                theMessage.protVersion  = pTSNFrameHead->protocolVersion;
                theMessage.serviceId    = pTSNFrameHead->reserved;
                pExistingElement->pfCbFunction(appHandle->pdDefault.pRefCon,
                                               appHandle,
                                               &theMessage,
                                               ((PD2_PACKET_T *)pExistingElement->pFrame)->data,
                                               (UINT32) vos_ntohs(((PD2_PACKET_T *)pExistingElement->pFrame)->frameHead
                                                                  .datasetLength));
            }
            else
#endif
            {
                theMessage.etbTopoCnt   = vos_ntohl(pExistingElement->pFrame->frameHead.etbTopoCnt);
                theMessage.opTrnTopoCnt = vos_ntohl(pExistingElement->pFrame->frameHead.opTrnTopoCnt);
                theMessage.protVersion  = vos_ntohs(pExistingElement->pFrame->frameHead.protocolVersion);
                theMessage.replyComId   = vos_ntohl(pExistingElement->pFrame->frameHead.replyComId);
                theMessage.replyIpAddr  = vos_ntohl(pExistingElement->pFrame->frameHead.replyIpAddress);
                theMessage.serviceId    = vos_ntohl(pExistingElement->pFrame->frameHead.reserved);

                /*- 调用回调 */
                pExistingElement->pfCbFunction(appHandle->pdDefault.pRefCon,
                                               appHandle,
                                               &theMessage,
                                               pExistingElement->pFrame->data,
                                               vos_ntohl(pExistingElement->pFrame->frameHead.datasetLength));
                /* @note	trdp不要求数据长度与通道中配置的数据长度匹配，无论数据有多长都接收并写入共享内存（此处传入的长度是包的长度）
                 * 			并且会用新的数据长度覆盖通道中的配置，但是目前提供给平台的数据长度仅在初始化完成，还需要考虑这里怎么操作
                 */
            }
        }
    }//回调处理结束

    return err;
}

/******************************************************************************/
/**
 *	@brief			获取各通道下一任务时间中的最小值作为app下一任务时间，将所有有效的接收socket返回到pFileDesc参数,将接收socket句柄最大值返回到pNoDesc
 *	@details		调用时设置checkSend可以决定是否检查发送通道
 *  @param[in]      appHandle           session pointer
 *  @param[in,out]  pFileDesc           pointer to set of ready descriptors
 *  @param[in,out]  pNoDesc             pointer to number of ready descriptors
 *  @param[in]      checkSend           check send queue, too
 */
void trdp_pdCheckPending (
    TRDP_APP_SESSION_T  appHandle,
    TRDP_FDS_T          *pFileDesc,
    INT32               *pNoDesc,
    int                 checkSend)
{
    PD_ELE_T *iterPD;

    vos_clearTime(&appHandle->nextJob);

    /*-遍历app接收通道 */
    for (iterPD = appHandle->pRcvQueue; iterPD != NULL; iterPD = iterPD->pNext)
    {
        if ((!(iterPD->privFlags & TRDP_TIMED_OUT)) &&              /* 不是标记了超时的通道 */
            timerisset(&iterPD->interval) &&                        /* 周期通道 */
            (timercmp(&iterPD->timeToGo, &appHandle->nextJob, <) || !timerisset(&appHandle->nextJob)))/* 通道下一任务时间小于app下一任务时间（或app下一任务时间还未设置） */
        {
        	/*- 令app获取通道的下一任务时间 */
            appHandle->nextJob = iterPD->timeToGo;
        }

        /*- 通道关联的socket有效且未在文件描述符集中设置 */
        if (iterPD->socketIdx != -1 &&
            appHandle->ifacePD[iterPD->socketIdx].sock != -1 &&
            !FD_ISSET(appHandle->ifacePD[iterPD->socketIdx].sock, (fd_set *)pFileDesc))
        {
        	/*- 在文件描述符集中设置该socket */
            FD_SET(appHandle->ifacePD[iterPD->socketIdx].sock, (fd_set *)pFileDesc);

            /*- 更新最大socket值 */

            if (appHandle->ifacePD[iterPD->socketIdx].sock > *pNoDesc)
            {
                *pNoDesc = (INT32) appHandle->ifacePD[iterPD->socketIdx].sock;
            }
        }
    }

    /*- 调用要求检查发送 */
    if (checkSend)
    {
    	/*-遍历app发送通道 */
        for (iterPD = appHandle->pSndQueue; iterPD != NULL; iterPD = iterPD->pNext)
        {

            if (timerisset(&iterPD->interval) &&   /* 周期通道 */
                (timercmp(&iterPD->timeToGo, &appHandle->nextJob, <) || !timerisset(&appHandle->nextJob)))/* 通道下一任务时间小于app下一任务时间（或app下一任务时间还未设置） */
            {
            	/*- 令app获取通道的下一任务时间 */
                appHandle->nextJob = iterPD->timeToGo;
            }
        }
    }
}

/******************************************************************************/
/**
 *	@brief			处理apphanle超时通道
 *  @param[in]      appHandle         application handle
 */
void trdp_pdHandleTimeOuts (
    TRDP_SESSION_PT appHandle)
{
    PD_ELE_T *iterPD = NULL;

    /*- 遍历接收通道 */
    for (iterPD = appHandle->pRcvQueue; iterPD != NULL; iterPD = iterPD->pNext)
    {
    	/*- 处理接收通道超时 */
        trdp_handleTimeout(appHandle, iterPD);
    }
}

/******************************************************************************/
/**
 *	@brief			判断通道是否超时并进行超时处理
 *	@details		超时处理即调用回调函数
 *  @param[in]      appHandle       Session handle
 *  @param[in]      pPacket         pointer to the packet element to check
 */
void trdp_handleTimeout (
    TRDP_SESSION_PT appHandle,
    PD_ELE_T        *pPacket)
{
    TRDP_TIME_T now;

    /*- 获取当前时间 */
    vos_getTime(&now);

    /*- 条件：初始化完成的常规pd通道，已到时但未处理超时  */
    if (timerisset(&pPacket->interval) &&						/* 通道周期非0 */
        timerisset(&pPacket->timeToGo) &&                        /* 通道任务时间非0 */
        !timercmp(&pPacket->timeToGo, &now, >) &&                /* 通道已到时 */
        !(pPacket->privFlags & TRDP_TIMED_OUT) &&                /* 通道未标记超时 */
        !(pPacket->addr.comId == TRDP_STATISTICS_PULL_COMID)) /* 通道不是统计拉专用 */
    {
        /*- 更新app超时计数 */
        appHandle->stats.pd.numTimeout++;

        /*- 通道标记超时 */
        pPacket->privFlags |= TRDP_TIMED_OUT;

        /*- 标记通道上一错误为超时 */
        pPacket->lastErr = TRDP_TIMEOUT_ERR;

        /*- 清空最近接收序列号 */
        pPacket->curSeqCnt=0u;

        /*- 通道配置了回调函数 */
        if (pPacket->pfCbFunction != NULL)
        {
    		/* 回调 */

        	/*- 创建PD信息结构体 */
            TRDP_PD_INFO_T theMessage;
            memset(&theMessage, 0, sizeof(TRDP_PD_INFO_T));

            /*- 获取通道参数 */
            theMessage.comId        = pPacket->addr.comId;
            theMessage.srcIpAddr    = pPacket->addr.srcIpAddr;
            theMessage.destIpAddr   = pPacket->addr.destIpAddr;
            theMessage.pUserRef     = pPacket->pUserRef;

            /* @note 这里传递的是订阅通道句柄结构体指针,该指针来自：
             * -subscribeTelegram()中创建订阅通道句柄结构体时将自身指针装入pUserRef成员
             * -subscribeTelegram()中调用tlp_subscribe()时将pUserRef成员作为参数传入
             * -tlp_subscribe()中创建通道结构体时将参数填入pUserRef
             *
             * -对于发布，通道句柄结构体中pUserRef成员不配置，通道结构体中pUserRef成员为NULL（调用tlp_subscribe()时为参数传入NULL）
             */

#ifdef LADDER_COMPARE
        	/*- 另一appHandle中的对应通道没有超时 */
        	if(0==checkTimeoutOfPeerSub(appHandle,pPacket))
        	{
                /*- 设置错误码 */
                theMessage.resultCode   = TRDP_SINGLE_LINKDOWN_SIG;

        	}
        	/*- 另一appHandle中的对应通道也超时了或者没有冗余通道 */
        	else
#endif
        	{
                /*- 设置错误码 */
                theMessage.resultCode   = TRDP_TIMEOUT_ERR_SIG;
        	}

			/*- 通道中有待发数据 */
			if (pPacket->pFrame != NULL)
			{
#ifdef TSN_SUPPORT
				if (pPacket->privFlags & TRDP_IS_TSN)
				{
					PD2_PACKET_T *pFrame = (PD2_PACKET_T *) pPacket->pFrame;
					theMessage.msgType      = (TRDP_MSG_T)pFrame->frameHead.msgType;
					theMessage.seqCount     = vos_ntohl(pFrame->frameHead.sequenceCounter);
					theMessage.protVersion  = pFrame->frameHead.protocolVersion;
				}
				else
#endif
				{
					/*- 获取通道trdp包头参数 */
					theMessage.etbTopoCnt   = vos_ntohl(pPacket->pFrame->frameHead.etbTopoCnt);
					theMessage.opTrnTopoCnt = vos_ntohl(pPacket->pFrame->frameHead.opTrnTopoCnt);
					theMessage.msgType      = (TRDP_MSG_T) vos_ntohs(pPacket->pFrame->frameHead.msgType);
					theMessage.seqCount     = vos_ntohl(pPacket->pFrame->frameHead.sequenceCounter);
					theMessage.protVersion  = vos_ntohs(pPacket->pFrame->frameHead.protocolVersion);
					theMessage.replyComId   = vos_ntohl(pPacket->pFrame->frameHead.replyComId);
					theMessage.replyIpAddr  = vos_ntohl(pPacket->pFrame->frameHead.replyIpAddress);
				}

				/*- 调用回调函数处理共享内存 */
				pPacket->pfCbFunction(appHandle->pdDefault.pRefCon,
									  appHandle,
									  &theMessage,
									  pPacket->pFrame->data,
									  pPacket->dataSize);
			}
			/*- 通道中没有待发数据 */
			else
			{
				/*- 调用回调函数处理共享内存(无用户数据) */
				pPacket->pfCbFunction(appHandle->pdDefault.pRefCon,
									  appHandle,
									  &theMessage,
									  NULL,
									  pPacket->dataSize);
			}
        }//回调处理结束
        /*- 通道未配置回调函数 */
        else
        {

        }
    }
    /*- 通道不需要处理超时 */
    else
    {
    	;
    }

}

/**********************************************************************************************************************/
/**
 *	@brief			处理当前appHandle下所有待处理的socket中的PD接收操作
 *	@details		对每个待处理的socket接收其现有的全部数据
 *	@note			一次接收处理，即trdp_pdReceive()的耗时如果超过socket接收数据的间隔，将导致永远无法接收完数据，程序将会停滞在本函数内部
 *  @param[in]      appHandle           session pointer
 *  @param[in]      pRfds               已准备好的文件描述符集（此处传入的值由select得到）
 *  @param[in,out]  pCount              已准备好的文件描述符的数量（此处传入的值由select得到）
 */
TRDP_ERR_T   trdp_pdCheckListenSocks (
    TRDP_SESSION_PT appHandle,
    TRDP_FDS_T      *pRfds,
    INT32           *pCount)
{
    TRDP_ERR_T result = TRDP_NO_ERR;

    /*- 空调用 */
    if ((pRfds == NULL) || (pCount == NULL))
    {

    }
    /*- 有准备好的文件描述符 */
    else if ((pCount != NULL) && (*pCount > 0))
    {
    	/*- 检查sockets来获取收到的PD包 */
        UINT32      idx;
        TRDP_ERR_T  err;

        /*- 获取app阻塞策略 */
        BOOL8       nonBlocking = !(appHandle->option & TRDP_OPTION_BLOCK);

        /*- 遍历app的socket列表，使用全局socket总数作为循环上限  */
        for (idx = 0; idx < (UINT32) trdp_getCurrentMaxSocketCnt(TRDP_SOCK_PD); idx++)
        {
        	/*- socket有效且socket存在于准备好的文件描述符集中 */
            if ((appHandle->ifacePD[idx].sock != -1) &&
                (FD_ISSET(appHandle->ifacePD[idx].sock, (fd_set *) pRfds)))
            {
                do
                {
                    /*- 处理接收 */
                    err = trdp_pdReceive(appHandle, appHandle->ifacePD[idx].sock);

                }
                /*- 无阻塞模式下接收成功 */
                while ((err == TRDP_NO_ERR) && (nonBlocking == TRUE));

                /*- 处理报错 @note 接收处理失败只会中断当前socket的接收处理，不执行退出，后续的socket仍然会正常处理，如果后面的socket
                 * 	处理时发生了需返回错误，将会覆盖之前的返回值
                 */
                switch (err)
                {
                    case TRDP_NO_ERR:
                    case TRDP_NODATA_ERR:		/* @note 发生无数据错误不返回错误 */
                    case TRDP_BLOCK_ERR:		/* @note 发生阻塞错误不返回错误 */
                    case TRDP_NOSUB_ERR:        /* @note 发生无订阅错误不返回错误 */
                        break;
                    case TRDP_TOPO_ERR:
                    case TRDP_TIMEOUT_ERR:
                    default:
                        result  = err;
                        vos_printLog(VOS_LOG_DBG, "trdp_pdReceive() failed (Err: %d)\n", err);
                        break;
                }

                /*- 更新已准备好的socket数量和文件描述符集，去除当前处理完成的socket */
                (*pCount)--;
                FD_CLR(appHandle->ifacePD[idx].sock, (fd_set *)pRfds);

            }//一个socket处理完成
        }
    }

    return result;
}

/******************************************************************************/
/**
 *	@brief			更新包头序列号和校验码
 *  @param[in]      pPacket         pointer to the packet to update
 */
void    trdp_pdUpdate (
    PD_ELE_T *pPacket)
{
	/*- 初始化局部变量 */
    UINT32 myCRC=0;

#ifdef TSN_SUPPORT
    /* If TSN is set, use the smaller header */
    if (pPacket->privFlags & TRDP_IS_TSN)
    {
        PD2_HEADER_T *pFrameHead = (PD2_HEADER_T *) &pPacket->pFrame->frameHead;

        /* increment counter with each telegram */
        pPacket->curSeqCnt++;
        pFrameHead->sequenceCounter = vos_htonl(pPacket->curSeqCnt);

        /* Compute CRC32   */
        myCRC = vos_crc32(INITFCS, (UINT8 *)pFrameHead, sizeof(PD2_HEADER_T) - SIZE_OF_FCS);
        pFrameHead->frameCheckSum = MAKE_LE(myCRC);
    }
    else
#endif
    {
        /*- 当前包为拉模式回复pd */
        if (pPacket->pFrame->frameHead.msgType == vos_htons(TRDP_MSG_PP))
        {
            pPacket->curSeqCnt4Pull++;
            pPacket->pFrame->frameHead.sequenceCounter = vos_htonl(pPacket->curSeqCnt4Pull);
        }
        else
        {
        	/*- 更新序列号并装入包头 */
            pPacket->curSeqCnt++;
            pPacket->pFrame->frameHead.sequenceCounter = vos_htonl(pPacket->curSeqCnt);
        }

        /* 重新计算包头校验码并装入包头 */
        myCRC = vos_crc32(INITFCS, (UINT8 *)&pPacket->pFrame->frameHead, sizeof(PD_HEADER_T) - SIZE_OF_FCS);
        pPacket->pFrame->frameHead.frameCheckSum = MAKE_LE(myCRC);
    }
}

/******************************************************************************/
/**
 *	@brief			更新tsn延迟测试时间戳，更新包头序列号和校验码
 *  @param[in]      pPacket         pointer to the packet to update
 */
void    trdp_pdUpdateWithPtpStamp (PD_ELE_T *pPacket)
{
	/*- 初始化局部变量 */
    UINT32 myCRC=0;

#ifdef	TSN_COMMU_TIME_TEST
    UINT64 tsnTick= 0;
    PUBLISH_TELEGRAM_T* pPublishTelegram=NULL;

    /*- 通道头在初始化时在reserved字段标记了tsn网络延迟测试 */
	if(0x07D8==vos_ntohl(pPacket->pFrame->frameHead.reserved))
	{
	    pPublishTelegram=(PUBLISH_TELEGRAM_T*)pPacket->pUserRef;

	    tsnTick=vos_getPtpTime(pPublishTelegram->appHandle->ptpClockId);

	    pPacket->pFrame->frameHead.replyComId=vos_ntohl(tsnTick/0x100000000u);
	    pPacket->pFrame->frameHead.replyIpAddress=vos_ntohl(tsnTick%0x100000000u);
	}
	else
	{
		//不额外更新时间戳
	}

#endif

	/*- 更新序列号并装入包头 */
	pPacket->curSeqCnt++;
	pPacket->pFrame->frameHead.sequenceCounter = vos_htonl(pPacket->curSeqCnt);

	/* 重新计算包头校验码并装入包头 */
	myCRC = vos_crc32(INITFCS, (UINT8 *)&pPacket->pFrame->frameHead, sizeof(PD_HEADER_T) - SIZE_OF_FCS);
	pPacket->pFrame->frameHead.frameCheckSum = MAKE_LE(myCRC);

}


/******************************************************************************/
/**
 *	@brief			检查trdp包头各字段是否符合协议
 *	@details		符合条件
 *					-包长度不小于trdp包头长度，不大于最大trdp包长度
 *					-包头CRC校验正确
 *					-协议版本字段符合本程序协议版本（0x0100）
 *					-消息类型字段为PD
 *  @param[in]      pPacket         pointer to the packet to check
 *  @param[in]      packetSize      max size to check
 *  @param[out]     pIsTSN          set to TRUE on return if PD2 frame
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_CRC_ERR
 */
TRDP_ERR_T trdp_pdCheck (
    PD_HEADER_T *pPacket,
    UINT32      packetSize,
    int         *pIsTSN)
{
    UINT32          myCRC;
    TRDP_ERR_T      err = TRDP_NO_ERR;

#ifdef TSN_SUPPORT
    PD2_HEADER_T    *pPacket2 = (PD2_HEADER_T *) pPacket;

    /*  Check for TSN small header first    */
    if (pPacket2->protocolVersion == 0x2)
    {
        *pIsTSN = TRUE;
        if ((packetSize < TRDP_MIN_PD2_HEADER_SIZE) ||
            (packetSize > TRDP_MAX_PD2_PACKET_SIZE))
        {
            vos_printLog(VOS_LOG_INFO, "PDframe size error (%u))\n", packetSize);
            err = TRDP_WIRE_ERR;
        }
        else
        {
            /*    Check Header CRC (FCS)  */
            myCRC = vos_crc32(INITFCS, (UINT8 *) pPacket2, sizeof(PD2_HEADER_T) - SIZE_OF_FCS);

            if (pPacket2->frameCheckSum != MAKE_LE(myCRC))
            {
                vos_printLog(VOS_LOG_INFO, "PDframe crc error (%08x != %08x))\n", pPacket2->frameCheckSum,
                             MAKE_LE(myCRC));
                err = TRDP_CRC_ERR;
            }
            /*  Check type  */
            else if ((pPacket2->msgType != TRDP_MSG_TSN_PD) &&
                     (pPacket2->msgType != TRDP_MSG_TSN_PD_SDT) &&
                     (pPacket2->msgType != TRDP_MSG_TSN_PD_MSDT) &&
                     (pPacket2->msgType != TRDP_MSG_TSN_PD_RES))
            {
                vos_printLog(VOS_LOG_INFO, "PDframe type error, received %02x\n", pPacket2->msgType);
                err = TRDP_WIRE_ERR;
            }
            /*  Check size  */
            else if (vos_ntohs(pPacket2->datasetLength) > TRDP_MAX_PD2_DATA_SIZE)
            {
                vos_printLog(VOS_LOG_INFO, "PDframe datalength error, received %04x\n", pPacket2->msgType);
                err = TRDP_WIRE_ERR;
            }
        }
    }
    else
#endif
    {   /* Version 1 TRDP (non-TSN) */
        *pIsTSN = FALSE;
        /*- 传入的包长度超限 */
        if ((packetSize < TRDP_MIN_PD_HEADER_SIZE) ||
            (packetSize > TRDP_MAX_PD_PACKET_SIZE))
        {
            vos_printLog(VOS_LOG_DBG, "PDframe size error (%u))\n", packetSize);
            err = TRDP_WIRE_ERR;
        }
        else
        {
            /*- 计算trdp包头FCS */
            myCRC = vos_crc32(INITFCS, (UINT8 *) pPacket, sizeof(PD_HEADER_T) - SIZE_OF_FCS);

            /*- 计算的FCS与包中的FCS字段不相等 */
            if (pPacket->frameCheckSum != MAKE_LE(myCRC))/* @note MAKE_LE() 条件编译的大小端转换宏定义 */
            {
                vos_printLog(VOS_LOG_DBG, "PDframe crc error (%08x != %08x))\n", pPacket->frameCheckSum, MAKE_LE(myCRC));
                err = TRDP_CRC_ERR;
            }
            /*- 包头的协议版本字段不正确或数据长度字段超限 */
            else if (((vos_ntohs(pPacket->protocolVersion) & TRDP_PROTOCOL_VERSION_CHECK_MASK)
                      != (TRDP_PROTO_VER & TRDP_PROTOCOL_VERSION_CHECK_MASK)) ||
                     (vos_ntohl(pPacket->datasetLength) > TRDP_MAX_PD_DATA_SIZE))
            {
                vos_printLog(VOS_LOG_DBG, "PDframe protocol error (%04x != %04x))\n",
                             vos_ntohs(pPacket->protocolVersion),
                             TRDP_PROTO_VER);
                err = TRDP_WIRE_ERR;
            }
            /*- 包头的类型字段不为任何一种PD类型 */
            else if ((vos_ntohs(pPacket->msgType) != (UINT16) TRDP_MSG_PD) &&
                     (vos_ntohs(pPacket->msgType) != (UINT16) TRDP_MSG_PP) &&
                     (vos_ntohs(pPacket->msgType) != (UINT16) TRDP_MSG_PR) &&
                     (vos_ntohs(pPacket->msgType) != (UINT16) TRDP_MSG_PE))
            {
                vos_printLog(VOS_LOG_DBG, "PDframe type error, received %04x\n", vos_ntohs(pPacket->msgType));
                err = TRDP_WIRE_ERR;
            }
        }
    }
    return err;
}


/******************************************************************************/
/**
 *	@brief			发送一个PD包
 *	@details		用通道关联的socket将Frame中的数据包发送到通道配置的destIp
					-如果设置了拉模式Ip则优先使用拉模式Ip
 *  @param[in]      pdSock          socket descriptor
 *  @param[in]      pPacket         pointer to packet to be sent
 *  @param[in]      port            port on which to send
 *
 *  @retval         TRDP_NO_ERR
 *  @retval         TRDP_IO_ERR
 */
TRDP_ERR_T  trdp_pdSend (
    SOCKET      pdSock,
    PD_ELE_T    *pPacket,
    UINT16      port)
{
    VOS_ERR_T   err     = VOS_NO_ERR;
    UINT32      destIp  = pPacket->addr.destIpAddr;

    /*- 通道配置了拉模式Ip */
    if (pPacket->pullIpAddress != 0u)
    {
    	/*- 获取该Ip后清除原Ip */
        destIp = pPacket->pullIpAddress;
        pPacket->pullIpAddress = 0u;
    }

    pPacket->sendSize = pPacket->grossSize;

    err = vos_sockSendUDP(pdSock,
                          (UINT8 *)&pPacket->pFrame->frameHead,
                          &pPacket->sendSize,
                          destIp,
                          port);

    if (err != VOS_NO_ERR)
    {
        vos_printLog(VOS_LOG_WARNING, "trdp_pdSend failed,err:%d\n",err);
        return TRDP_IO_ERR;
    }

    if (pPacket->sendSize != pPacket->grossSize)
    {
        vos_printLog(VOS_LOG_WARNING, "trdp_pdSend incomplete,send size:%u,expect size:%u\n",pPacket->sendSize,pPacket->grossSize);
        return TRDP_IO_ERR;
    }

    return TRDP_NO_ERR;
}

#ifndef HIGH_PERF_INDEXED

/* Note: This function is not necessary for the high performance version; see trdp_pdindex.c */

/******************************************************************************/
/**
 *	@brief		将分散的通道任务时间尽量整合到一起
 *	@details	遍历所有发送通道，将（周期较长的）通道任务时间延迟到最远下一任务时间的一最小周期后
 *  			-由于已经限制了最小周期和最大包长度，可以无视包长度带来的阻塞（即预定义的最小周期足以处理1432长度数据）
 *  			-我们对通道下一任务时间的修改需要在1/2周期以内，否则为通道增加延迟可能会导致既有通道的超时
 *  			-时间表基于最小周期计算
 *  @param[in]      pSndQueue       pointer to send queue
 *
 *  @retval         TRDP_NO_ERR
 */
TRDP_ERR_T  trdp_pdDistribute (
    PD_ELE_T *pSndQueue)
{
    PD_ELE_T    *pPacket    = pSndQueue;
    TRDP_TIME_T deltaTmax   = {1000u, 0}; /*    Preset to highest value    */
    TRDP_TIME_T tNull       = {0u, 0};
    TRDP_TIME_T temp        = {0u, 0};
    TRDP_TIME_T nextTime2Go;
    UINT32      noOfPackets = 0u;
    UINT32      packetIndex = 0u;

    if (pSndQueue == NULL)
    {
        return TRDP_PARAM_ERR;
    }

    /*  Do nothing if only one packet is pending */
    if (pSndQueue->pNext == NULL)
    {
        return TRDP_NO_ERR;
    }

    /*- 获取浮动时间 - 我们能够扰动通道任务时间的最大时长。该值等于最小通道周期。统计该时间段内发送的包数量，并找到下一个发包时间 */

    while (pPacket)
    {
        /*- 过滤PULL-only通道（interval为0的） */
        if ((pPacket->interval.tv_sec != 0u) ||
            (pPacket->interval.tv_usec != 0))
        {
            if (vos_cmpTime(&deltaTmax, &pPacket->interval) > 0)
            {
            	/*- deltaTmax获取最小周期 */
                deltaTmax = pPacket->interval;
            }
            if (vos_cmpTime(&tNull, &pPacket->timeToGo) < 0)
            {	/*- tNull获取最远的任务 */
                tNull = pPacket->timeToGo;
            }
            noOfPackets++;
        }
        pPacket = pPacket->pNext;
    }

    /*- 未查找到需要编组的通道 */
    if ((vos_cmpTime(&deltaTmax, &temp) == 0) ||
        (noOfPackets == 0))
    {
    	/*- 目前无需处理，退出（无报错） */
        vos_printLog(VOS_LOG_DBG, "trdp_pdDistribute: no minimal interval in %d packets found!\n", noOfPackets);
        return TRDP_NO_ERR;
    }

    /*- 计算允许浮动，为最小周期除以总通道数量 */
    vos_divTime(&deltaTmax, noOfPackets);

    vos_printLog(VOS_LOG_DBG,
                 "trdp_pdDistribute: deltaTmax   = %ld.%06u\n",
                 (long) deltaTmax.tv_sec,
                 (unsigned int)deltaTmax.tv_usec);
    vos_printLog(VOS_LOG_DBG,
                 "trdp_pdDistribute: tNull       = %ld.%06u\n",
                 (long) tNull.tv_sec,
                 (unsigned int)tNull.tv_usec);
    vos_printLog(VOS_LOG_DBG, "trdp_pdDistribute: noOfPackets = %d\n", noOfPackets);

    for ((void)(packetIndex = 0), pPacket = pSndQueue; packetIndex < noOfPackets && pPacket != NULL; )
    {
    	/*- 过滤PULL-only通道（interval为0的） */
        if ((pPacket->interval.tv_sec != 0u) ||
            (pPacket->interval.tv_usec != 0))
        {

            nextTime2Go = tNull;
            temp        = deltaTmax;
            vos_mulTime(&temp, packetIndex);

            vos_addTime(&nextTime2Go, &temp);	//令nextTime2Go为最远任务+最小周期
            vos_mulTime(&temp, 2u);				//令temp为2*最小周期

            /*- 若当前通道周期小于二倍最小周期 */
            if (vos_cmpTime(&temp, &pPacket->interval) > 0)
            {
            	/*- 不改变该通道下一任务时间 */
                vos_printLog(VOS_LOG_DBG, "trdp_pdDistribute: packet [%d] with interval %lu.%06u could timeout...\n",
                             packetIndex, (long) temp.tv_sec, (unsigned int)temp.tv_usec);
                vos_printLogStr(VOS_LOG_DBG, "...no change in send time!\n");
            }
            /*- 若当前通道周期大于二倍最小周期 */
            else
            {
            	/*- 将下一任务时间延迟到nextTime2Go */
            	pPacket->timeToPut = nextTime2Go;
                pPacket->timeToGo = nextTime2Go;
                vos_printLog(VOS_LOG_DBG, "trdp_pdDistribute: nextTime2Go[%d] = %lu.%06u\n",
                             packetIndex, (unsigned long) nextTime2Go.tv_sec, (unsigned int)nextTime2Go.tv_usec);
            }
            packetIndex++;
        }
        pPacket = pPacket->pNext;
    }

    return TRDP_NO_ERR;
}
#endif


/**
 * @brief		检查传入的网口索引是否是指定app在使用的
 * @parms		TRDP_SESSION_PT		appHandle
 * 				UINT8				recvIfindex
 * @return		TRDP_ERR_T
 * @retval		TRDP_NOSUB_ERR		传入的网口索引不由指定的app处理
 * 				TRDP_NO_ERR			传入的网口索引由指定的app处理
 */
TRDP_ERR_T trdp_checkIfindex(TRDP_SESSION_PT  appHandle,UINT8 recvIfindex)
{
	TRDP_ERR_T err=TRDP_UNKNOWN_ERR;

    if((UINT32)recvIfindex==appHandle->ifIndex)
    {
    	err=TRDP_NO_ERR;
    }
    else
    {
    	err=TRDP_NOSUB_ERR;
    }


    return err;
}

#ifdef LADDER_COMPARE
int compareSeqWithPeerSub(TRDP_SESSION_PT appHandle,PD_ELE_T* pElement)
{

	/* 在使用双网取新策略时，比较另一会话，禁用非双网更新包的回调 */
	PD_ELE_T            *pAnotherExistingElement   = NULL;
	TRDP_SESSION_PT		panotherApphandle=NULL;
	TRDP_ADDRESSES_T	*pSubAddresses;

	pSubAddresses=&pElement->addr;

	/* 获取另一appHandle */

	panotherApphandle=((appHandle==appHandle1)?appHandle2:appHandle1);

	/*- 非单session */
	if((TRDP_APP_SESSION_T)LADDER_TOPOLOGY_DISABLE!=appHandle2)
	{
		pAnotherExistingElement = trdp_queueFindSubAddr(panotherApphandle->pRcvQueue, pSubAddresses);
		if(NULL==pAnotherExistingElement)
		{
			/* 没有冗余通道，无法比较 */
			return 0;
		}
		else if(pAnotherExistingElement->curSeqCnt >= pElement->curSeqCnt)
		{
			/* 当前通道的包是旧的 */
			return -1;
		}
		else
		{
			/* 当前通道的包是新的 */
			return 1;
		}
	}
	else
	{
		/* 没有启用冗余网口，无法比较 */
		return 0;
	}
}


/**
 * @brief 查看冗余订阅通道是否超时
 * @retval	-0：冗余通道未超时
 * 			-1：冗余通道超时
 * 			-2：没有冗余通道
 */
int checkTimeoutOfPeerSub(TRDP_SESSION_PT appHandle,PD_ELE_T* pElement)
{
	PD_ELE_T            *pAnotherExistingElement   = NULL;
	TRDP_SESSION_PT		panotherApphandle=NULL;
	TRDP_ADDRESSES_T	*pSubAddresses;

	pSubAddresses=&pElement->addr;

	/* 获取另一appHandle */
	panotherApphandle=((appHandle==appHandle1)?appHandle2:appHandle1);


	if((TRDP_APP_SESSION_T)LADDER_TOPOLOGY_DISABLE!=appHandle2)
	{
		pAnotherExistingElement = trdp_queueFindSubAddr(panotherApphandle->pRcvQueue, pSubAddresses);
		if(NULL==pAnotherExistingElement)
		{
			/* 没有冗余通道，无法比较 */
			return 2;
		}
		/*- 冗余通道超时 */
		else if(pAnotherExistingElement->privFlags & TRDP_TIMED_OUT)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 2;
	}
}
#endif
