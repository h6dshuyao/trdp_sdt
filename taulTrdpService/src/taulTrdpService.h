/**
 * @file				taulApp.h
 *
 * @brief				Global Variables for TRDP Ladder Topology Support TAUL Application
 *
 * @details
 *
 * @note				Project: TCNOpen TRDP prototype stack
 *
 * @author				Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id$
 *
 */

#ifndef TAULAPP_H_
#define TAULAPP_H_
#ifdef TRDP_OPTION_LADDER
/*******************************************************************************
 * INCLUDES
 */
#include "trdp_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************************************************************
 * DEFINES
 */

/* Application Thread List Size Max */
#define APPLICATION_THREAD_LIST_MAX				200

/* Message Queue Descriptor Table Size Max */
#define MESSAGE_QUEUE_DESCRIPTOR_TABLE_MAX		1000

/* Message Queue */
#define MESSAGE_QUEUE_NAME_SIZE						24			/* Message Queue Name Size */
#define THREAD_COUNTER_CHARACTER_SIZE				10			/* Thread Counter Character Size */
#define TRDP_QUEUE_MAX_SIZE							(sizeof(trdp_apl_cbenv_t)-2)
#define TRDP_QUEUE_MAX_MESG 						128

#define TLC_PROCESS_CYCLE_TIME						10000			/* default MD tlc_process cycle time for tlm_delListener wait */

#define REQUEST_HANDLE_TABLE_MAX					1000			/* MD Send Request (Mr) Handle Table Size Max */
#define RECEIVE_REPLY_HANDLE_TABLE_MAX				1000			/* Caller Receive Reply Handle Table Size Max */

#define REPLIERS_UNKNOWN								0				/* Repliiers unknown kind */

/* LOG CATEGORY */
#define LOG_CATEGORY_ERROR							0x02				/**< This is a critical error                 */
#define LOG_CATEGORY_WARNING						0x04				/**< This is a warning                        */
#define LOG_CATEGORY_INFO							0x08				/**< This is an info                          */
#define LOG_CATEGORY_DEBUG							0x10				/**< This is a debug info                     */

/* Default PD Application Parameter */
#define DEFAULT_PD_APP_CYCLE_TIME					500000			/* Ladder Application cycle in us */
//#define DEFAULT_PD_APP_CYCLE_TIME					5000			/* Ladder Application cycle in us */
#define DEFAULT_PD_SEND_CYCLE_NUMBER				0				/* Publish Send Cycle Number */
#define DEFAULT_PD_RECEIVE_CYCLE_NUMBER			0				/* Subscribe Receive Cycle Number */
#define DEFAULT_WRITE_TRAFFIC_STORE_SUBNET		0				/* Traffic Store Using Subnet */
/* Default MD Application Parameter */
/* for MD Notify/Request Destination Parameter */
/* Point to Multi point */
//const TRDP_IP_ADDR_T		DEFAULT_CALLER_DEST_IP_ADDRESS	= 0xefff0101;		/* Destination IP Address for Caller Application 239.255.1.1 */
//const TRDP_URI_HOST_T	DEFAULT_CALLER_DEST_URI = "239.255.1.1";			/* Destination URI for Caller Application */
/* Point to Point */
const TRDP_IP_ADDR_T		DEFAULT_CALLER_DEST_IP_ADDRESS	= 0xa000111;		/* Destination IP Address for Caller Application 10.0.1.17 */
const TRDP_URI_HOST_T	DEFAULT_CALLER_DEST_URI = "10.0.1.17";				/* Destination URI for Caller Application */
#define DEFAULT_MD_APP_CYCLE_NUMBER				0				/* Number of MD Application Cycle : Caller Send Number, Replier Receive Number */
#define DEFAULT_MD_APP_CYCLE_TIME					5000000		/* Request Send Cycle Interval Time */
/* for tau_ldRequest() numReplies Parameter */
#define DEFAULT_CALLER_NUMBER_OF_REPLIER			0				/* Number of Repliers for Caller Application send Request for unKnown */
//#define DEFAULT_CALLER_NUMBER_OF_REPLIER			1				/* Number of Repliers for Caller Application send Request for Known Replier:1 (point to point) */
//#define DEFAULT_CALLER_NUMBER_OF_REPLIER			2				/* Number of Repliers for Caller Application send Request for Known Replier:2 (point to point) */
#define DEFAULT_CALLER_SEND_INTERVAL_TYPE			REQUEST_REQUEST				/* Caller Application Request send interval type value */

/* Input Command */
#define GET_COMMAND_MAX				1000			/* INPUT COMMAND MAX */
#define SPACE							 ' '			/* SPACE Character */

/***********************************************************************************************************************
 * TYPEDEFS
 */

/* TAUL Application Error Type definition */
typedef enum
{
    TAUL_APP_NO_ERR					= 0,			/**< TAUL Application No Error */
    TAUL_APP_ERR						= -1,			/**< TAUL Application Error */
    TAUL_INIT_ERR          			= -2,			/**< TAUL Call without valid initialization */
    TAUL_APP_PARAM_ERR				= -3,			/**< TAUL Application Parameter Error */
    TAUL_APP_MEM_ERR					= -4,			/**< TAUL Application Memory Error */
    TAUL_APP_THREAD_ERR				= -5,			/**< TAUL Application Thread Error */
    TAUL_APP_MUTEX_ERR				= -6,			/**< TAUL Application Thread Mutex Error */
    TAUL_APP_MQ_ERR					= -7,			/**< TAUL Application Thread Message Queue Error */
    TAUL_APP_MRMP_ONE_CYCLE_ERR		= -8,			/**< TAUL Application Repliers unknown Mr-Mp One Cycle End (Receive Reply Timeout) */
    TAUL_QUEUE_ERR					= -10,			/**< Queue empty */
    TAUL_QUEUE_FULL_ERR				= -11,			/**< Queue full */
    TAUL_APP_COMMAND_ERR				= -12,			/**< TAUL Application Command Error */
    TAUL_APP_QUIT_ERR				= -13,			/**< TAUL Application Quit Command */
    TAUL_APP_REBOOT_ERR				= -14,			/**< TAUL Application TAUL Reboot Command */
} TAUL_APP_ERR_TYPE;



/***********************************************************************************************************************
 * GLOBAL VARIABLES
 */



/***********************************************************************************************************************
 * PROTOTYPES
 */

/**********************************************************************************************************************/
/** Call back Function
 */

/**********************************************************************************************************************/
/** callback routine for TRDP logging/error output
 *
 *  @param[in]		pRefCon		user supplied context pointer
 *  @param[in]		category		Log category (Error, Warning, Info etc.)
 *  @param[in]		pTime			pointer to NULL-terminated string of time stamp
 *  @param[in]		pFile			pointer to NULL-terminated string of source module
 *  @param[in]		LineNumber		line
 *  @param[in]		pMsgStr       pointer to NULL-terminated string
 *  @retval         none
 */
void dbgOut (
    void        *pRefCon,
    TRDP_LOG_T  category,
    const CHAR8 *pTime,
    const CHAR8 *pFile,
    UINT16      LineNumber,
    const CHAR8 *pMsgStr);

/**********************************************************************************************************************/


/**********************************************************************************************************************/
/** init TAUL Application
 *
 *  @retval         0		no error
 *  @retval         1		some error
 */
TAUL_APP_ERR_TYPE initTaulApp (
		void);

#ifdef __cplusplus
}
#endif
#endif	/* TRDP_OPTION_LADDER */
#endif /* TAULAPP_H_ */
