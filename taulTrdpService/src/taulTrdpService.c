/**********************************************************************************************************************/
/**
 * @file		taulApp.c
 *
 * @brief		Demo TAUL application for TRDP
 *
 * @details	TRDP Ladder Topology Support TAUL Application
 *
 * @note		Project: TCNOpen TRDP prototype stack
 *
 * @author		Kazumasa Aiba, Toshiba Corporation
 *
 * @remarks This Source Code Form is subject to the terms of the Mozilla Public License, v. 2.0.
 *          If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *          Copyright Toshiba Corporation, Japan, 2013. All rights reserved.
 *
 * $Id: taulApp_PD_sample.c 2054 2019-08-28 11:03:37Z bloehr $
 *
 */

#ifdef TRDP_OPTION_LADDER
/***********************************************************************************************************************
 * INCLUDES
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <errno.h>
#include <unistd.h>
#include <sys/stat.h>


#include "taulTrdpService.h"
#include "tau_ldLadder_config.h"
#include "tau_ldLadder.h"
#include "2x2shrmem.h"

/*******************************************************************************
 * DEFINES
 */
#define TRDP_INTEGRATION_VER	"v0.95.1.9"
#define TRDP_INTEGRATION_VERSION_NOTE ""
#define SOURCE_URI "user@host"
#define DEST_URI "user@host"
#define TDRP_VAR_SIZE 0
#define DDS_MESSAGE_HEADLENTH 6
#define DDS_DATALENTH_MAX 1432



#define APP_TO_REDUN_DDS_DATALENTH 27
//#pragma pack(1)

/*******************************************************************************
 * TYPEDEFS
 */
typedef struct
{
    UINT32                  comId;                    /* handle from related subscribe */
    UINT32                  offset;                                
    UINT16     				datasize;                 /* dataset (size, buffer) */
    UINT8					subnetState1;				/* 子网1接收数据的情况 */
	UINT8					subnetState2;				/* 子网2接收数据的情况 */
}TRDP_APP_INDEX;

typedef struct
{
    UINT32                  comId;                              /* handle from related subscribe */
    UINT32                  datasize;             
    PUBLISH_TELEGRAM_T*		pPublishTelegram;/* pointer to corresponding PublishTelegram */    
}PUBLISHER_ELE;


#define IF_STATE_INVALID 0u
#define	IF_STATE_ON 1u
#define	IF_STATE_OFF 2u


/******************************************************************************
 *   Locals
 */

TAUL_APP_ERR_TYPE myCreateDataset (
		TRDP_DATASET_T			*pDatasetDesc,
		UINT8					*pDstEnd,
		UINT8					*Buffer);

void indicateIfState (
    void                    *pRefCon,
    TRDP_APP_SESSION_T      argAppHandle,
    const TRDP_PD_INFO_T    *pPDInfo,
    UINT8                   *pData,
    UINT32                  dataSize);
void getRedunState(REDUN_INFO_T* pRedunStateList,UINT32* listLen);

/******************************************************************************
 *   Globals
 */

#ifdef XML_CONFIG_ENABLE

#if defined(linux)
const CHAR8 appXmlConfigFileName[FILE_NAME_MAX_SIZE] = "xmlconfig.xml";      /* XML Config File Name */
#elif defined(VXWORKS)
const CHAR8 appXmlConfigFileName[FILE_NAME_MAX_SIZE] = "/mmc0a/xmlconfig.xml";
#endif

#endif /* ifdef XML_CONFIG_ENABLE */





TRDP_APP_SESSION_T appHandleList[2];

/* Create Role Application Thread */
TRDP_APP_INDEX comidOffsetList[APPLICATION_THREAD_LIST_MAX]={0};


/******************************************************************************
 *   Function
 */
/**********************************************************************************************************************/
/** callback routine for TRDP logging/error output
 *
 *  @param[in]		pRefCon		user supplied context pointer
 *  @param[in]		category		Log category (Error, Warning, Info etc.)
 *  @param[in]		pTime			pointer to NULL-terminated string of time stamp
 *  @param[in]		pFile			pointer to NULL-terminated string of source module
 *  @param[in]		LineNumber		line
 *  @param[in]		pMsgStr       pointer to NULL-terminated string
 *  @retval         none
 */
void dbgOut (
    void        *pRefCon,
    TRDP_LOG_T  category,
    const CHAR8 *pTime,
    const CHAR8 *pFile,
    UINT16      LineNumber,
    const CHAR8 *pMsgStr)
{
    const char *catStr[] = {"**Error:", "Warning:", "   Info:", "  Debug:"};
    BOOL8 logPrintOnFlag = FALSE;							/* FALSE is not print */
    FILE *fpOutputLog = NULL;								/* pointer to Output Log */
    const TRDP_FILE_NAME_T logFileNameNothing = {0};		/* Log File Name Nothing */
    struct stat logStatBuf;

    /* Check Output Log Enable from Debug Config */
    switch(category)
    {
    	case VOS_LOG_ERROR:
			/* Debug Config Option : ERROR or WARNING or INFO or DEBUG */
			if (((debugConfigTAUL.option & TRDP_DBG_ERR) == TRDP_DBG_ERR)
				|| ((debugConfigTAUL.option & TRDP_DBG_WARN) == TRDP_DBG_WARN)
				|| ((debugConfigTAUL.option & TRDP_DBG_INFO) == TRDP_DBG_INFO)
				|| ((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG))
			{
				logPrintOnFlag = TRUE;
			}
		break;
    	case VOS_LOG_WARNING:
			/* Debug Config Option : WARNING or INFO or DEBUG */
    		if (((debugConfigTAUL.option & TRDP_DBG_WARN) == TRDP_DBG_WARN)
    			|| ((debugConfigTAUL.option & TRDP_DBG_INFO) == TRDP_DBG_INFO)
				|| ((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG))
			{
				logPrintOnFlag = TRUE;
			}
    	break;
    	case VOS_LOG_INFO:
			/* Debug Config Option : INFO or DBUG */
    		if (((debugConfigTAUL.option & TRDP_DBG_INFO) == TRDP_DBG_INFO)
    			|| ((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG))
			{
				logPrintOnFlag = TRUE;
			}
    	break;
    	case VOS_LOG_DBG:
			/* Debug Config Option : DEBUG */
			if((debugConfigTAUL.option & TRDP_DBG_DBG) == TRDP_DBG_DBG)
			{
				logPrintOnFlag = TRUE;
			}
		break;
    	default:
    	break;
    }

    /* Check log Print */
    if (logPrintOnFlag == TRUE)
    {   
    	/*- 配置了log文件名称 */
    	if (memcmp(debugConfigTAUL.fileName, logFileNameNothing,sizeof(TRDP_FILE_NAME_T)) != 0)
    	{
    		/*- 获取文件信息成功 */
    	    if(stat(debugConfigTAUL.fileName, &logStatBuf)==0)
    	    {
    	    	/*- 文件超过配置大小 */
        	    if((UINT32)logStatBuf.st_size >= debugConfigTAUL.maxFileSize)
        	    {
            	    /*- write 模式打开文件 */
            		fpOutputLog = fopen(debugConfigTAUL.fileName, "w");
            	    if (fpOutputLog == NULL)
            	    {
            	    	vos_printLog(VOS_LOG_ERROR, "dbgOut() Log File Open (mode:w) Err\n");

            			return;
            	    }
        	    }
        	    /*- 文件未超过配置大小 */
        	    else
        	    {
            	    /*- append 模式打开文件 */
            		fpOutputLog = fopen(debugConfigTAUL.fileName, "a");
            	    if (fpOutputLog == NULL)
            	    {
            	    	vos_printLog(VOS_LOG_ERROR, "dbgOut() Log File Open (mode:a) Err\n");
            			return;
            	    }
        	    }
    	    }
    	    /*- 获取文件信息失败 */
    	    else
    	    {
        	    /*- append 模式打开文件 */
        		fpOutputLog = fopen(debugConfigTAUL.fileName, "a");
        	    if (fpOutputLog == NULL)
        	    {
        	    	vos_printLog(VOS_LOG_ERROR, "dbgOut() Log File Open (mode:a) Err\n");
        			return;
        	    }
    	    }
    	}
    	else
    	{
    		/* Set Output place to Display */
    		fpOutputLog = stdout;
    	}

    	/* Output Log Information */
		/*  Log Date and Time */
		if (debugConfigTAUL.option & TRDP_DBG_TIME)
			fprintf(fpOutputLog, "%s ", pTime);
		/*  Log category */
		if (debugConfigTAUL.option & TRDP_DBG_CAT)
			fprintf(fpOutputLog, "%s ", catStr[category]);
		/*  Log Source File Name and Line */
		if (debugConfigTAUL.option & TRDP_DBG_LOC)
			fprintf(fpOutputLog, "%s:%d ", pFile, LineNumber);

		/*  Log message */
		fprintf(fpOutputLog, "%s", pMsgStr);

		if (fpOutputLog != stdout)
		{
			/* Close Log File */
			fclose(fpOutputLog);
		}
    }
}









/**********************************************************************************************************************/
/** Application Thread Common Function
 */

/**********************************************************************************************************************/
/** TAUL Application initialize
 *
 *  @retval         0		no error
 *  @retval         1		some error
 */
TAUL_APP_ERR_TYPE initTaulApp (
		void)
{
	TRDP_ERR_T							err = TRDP_NO_ERR;
	UINT32								sessionIndex=0;
	PUBLISH_TELEGRAM_T					*iterPublishTelegram = NULL;
	SUBSCRIBE_TELEGRAM_T				*iterSubscribeTelegram = NULL;

	extern PUBLISH_TELEGRAM_T			*pHeadPublishTelegram[MAX_SESSIONS];		/** @global 发布通道链表头 */
	extern SUBSCRIBE_TELEGRAM_T			*pHeadSubscribeTelegram[MAX_SESSIONS];		/** @global 订阅通道链表头 */

	extern UINT8        				*pTrafficStoreAddr;

	UINT32								publisherApplicationId = 0;					/* Publisher Thread Application Id */
	UINT32								subscriberApplicationId = 0;				/* Subscriber Thread Application Id */
	UINT32								taulApplicationUserdataIndex = 0;			/* 	用户数据下标，每条用户数据（可由唯一的comid区分）具有唯一的用户数据下标
																							由服务层提供，用来关联提供给应用的用户数据信息列表单元与taul_ld层提供的通道句柄 */

	UINT32 pubTelegramNuber=0;
	UINT32 subTelegramNuber=0;

	UINT32 getNoOfIfaces = NUM_ED_INTERFACES;
	VOS_IF_REC_T ifAddressTable[NUM_ED_INTERFACES];
	TRDP_IP_ADDR_T ownIpAddress = 0;
	UINT32 i = 0;

	UINT32 tempp=0u;
	UINT32 temps=0u;
	UINT32 listLenth=0u;

#ifdef XML_CONFIG_ENABLE
	/* Set XML Config */
	strncpy(xmlConfigFileName, appXmlConfigFileName, sizeof(xmlConfigFileName));
#endif

	/*- 初始化双冗余trdp服务，传入用户层定义，下层管理的回调函数，主备查询服务和接口状态汇服务 */
	err = tau_ldInit(dbgOut,indicateIfState,getRedunState);
	if (err != TRDP_NO_ERR)
	{
		printf("initTaulApp() failed. tau_ldInit faied ,err code: %d \n", err);
		return TAUL_INIT_ERR;
	}

	/*- 用默认策略设置双网冗余主子网 */
    err = tau_ldSetNetworkContext(SUBNET_AUTO);
    if (err != TRDP_NO_ERR)
    {
    	vos_printLog(VOS_LOG_ERROR, "Set Writing Traffic Store Sub-network error\n");
        return TAUL_APP_ERR;
    }

    /* 读取taul_trdp为服务层提供的通道句柄中的信息，提供给应用平台调度 */

	/*- 读取首个session的订阅通道句柄链表 */
	for (iterSubscribeTelegram = pHeadSubscribeTelegram[0];
			iterSubscribeTelegram != NULL;
			iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram)
	{
		/*- 从服务层返回列表下标到通道句柄结构体 */
		iterSubscribeTelegram->userIndex=taulApplicationUserdataIndex;

		/*- 设置comidOffsetList列表 */
		comidOffsetList[taulApplicationUserdataIndex].comId=htonl(iterSubscribeTelegram->comId);
		comidOffsetList[taulApplicationUserdataIndex].offset=htonl(iterSubscribeTelegram->pPdParameter->offset);
		comidOffsetList[taulApplicationUserdataIndex].datasize=htons((UINT16)iterSubscribeTelegram->dataset.size);

		comidOffsetList[taulApplicationUserdataIndex].subnetState1=IF_STATE_INVALID;
		comidOffsetList[taulApplicationUserdataIndex].subnetState2=IF_STATE_INVALID;

		/*- 更新用户数据下标 */
		taulApplicationUserdataIndex++;
		subscriberApplicationId++;
	}

	subTelegramNuber=subscriberApplicationId;

	printf("read subsriber finished,sub telegram number:%d\n",subTelegramNuber);

	/*- 为其余session的订阅通道句柄结构体返回列表下标 */

	/*- 遍历其余session的订阅通道句柄链表 */
	for(sessionIndex=1u;sessionIndex<MAX_SESSIONS;sessionIndex++)
	{
		for (iterSubscribeTelegram = pHeadSubscribeTelegram[sessionIndex];
				iterSubscribeTelegram != NULL;
				iterSubscribeTelegram = iterSubscribeTelegram->pNextSubscribeTelegram)
		{
			/*- 遍历comid-offset-dataset列表 */
			for(i=0;i<taulApplicationUserdataIndex;i++)
			{
				/*- 寻找与当前通道句柄匹配的单元 */
				if(iterSubscribeTelegram->comId==htonl(comidOffsetList[i].comId))
				{
					/* 从服务层返回列表下标到通道句柄结构体 */
					iterSubscribeTelegram->userIndex=i;
					break;
				}
				else
				{
					iterSubscribeTelegram->userIndex=0xffffffffu;
				}
			}
		}
	}

	/*- 读取首个session的发布通道句柄链表 */
	for (iterPublishTelegram = pHeadPublishTelegram[0];
			iterPublishTelegram != NULL;
			iterPublishTelegram = iterPublishTelegram->pNextPublishTelegram)
	{
		/* Set comidOffsetList */
		comidOffsetList[taulApplicationUserdataIndex].comId=htonl(iterPublishTelegram->comId);
		comidOffsetList[taulApplicationUserdataIndex].offset=htonl(iterPublishTelegram->pPdParameter->offset);
		comidOffsetList[taulApplicationUserdataIndex].datasize=htons((UINT16)iterPublishTelegram->dataset.size);

		comidOffsetList[taulApplicationUserdataIndex].subnetState1=IF_STATE_INVALID;
		comidOffsetList[taulApplicationUserdataIndex].subnetState2=IF_STATE_INVALID;

		publisherApplicationId++;
		taulApplicationUserdataIndex++;
	}

	pubTelegramNuber=publisherApplicationId;
	printf("read publisher finished,pub telegram number:%d\n",pubTelegramNuber);
		
	listLenth=sizeof(TRDP_APP_INDEX)*taulApplicationUserdataIndex;
	
	if((listLenth+8+sharememConfigTAUL.indexOffset)>sharememConfigTAUL.size)
	{
		printf("not enough space for comid-offset list,requare %d bytes,prepare %d bytes,tail data loss\n",listLenth,sharememConfigTAUL.size-sharememConfigTAUL.indexOffset-8);
		return TAUL_APP_PARAM_ERR;
	}

	/* Set comid-offset List in sharemem*/

	tempp=htonl(pubTelegramNuber);
	temps=htonl(subTelegramNuber);

    err=tau_ldLockTrafficStore();
    if(TRDP_NO_ERR==err)
    {
        memcpy((void *)(pTrafficStoreAddr + sharememConfigTAUL.indexOffset), (const void *)&temps, 4);
        memcpy((void *)(pTrafficStoreAddr + sharememConfigTAUL.indexOffset+4),(const void *)&tempp, 4);
        memcpy((void *)(pTrafficStoreAddr + sharememConfigTAUL.indexOffset+8), (const void *)comidOffsetList, listLenth);

        tau_ldUnlockTrafficStore();
    	if(TRDP_NO_ERR!=err)
    	{
    		printf("tau_ldUnlockTrafficStore failed\n");
    		return TAUL_APP_MUTEX_ERR;
    	}
    }
    else
    {
    	printf("LockTrafficStore failed\n");
    	return TAUL_APP_MUTEX_ERR;
    }

    
    printf("set comid-offset-datasize finished:\n");
    for(int qq=0;qq<taulApplicationUserdataIndex;qq++)
    {
    	printf("comid:%08d	offset:%08d	datasize:%04d\n",htonl(comidOffsetList[qq].comId),htonl(comidOffsetList[qq].offset),htons(comidOffsetList[qq].datasize));
    }
    printf("\n");

	return TAUL_APP_NO_ERR;
}

/**********************************************************************************************************************/
/** main entry
 *
 *  @retval         0		no error
 *  @retval         1		some error
 */


int main (INT32 argc, CHAR8 *argv[])
{

#ifdef TSN_COMMU_TIME_TEST
	TRDP_ERR_T rt=TRDP_UNKNOWN_ERR;
	CHAR8 IF_NAME1[] = "eth1";
	CHAR8 IF_NAME2[] = "eth2";
	TRDP_TSN_STATISTICS_T tsnCommuDelayStatics1={0};
	TRDP_TSN_STATISTICS_T tsnCommuDelayStatics2={0};
	UINT64 maxDelay=0;
	UINT32 gateRecvNum=6000u;
#endif

	TAUL_APP_ERR_TYPE								err = TAUL_APP_NO_ERR;

	printf("##################### %s %s##################### \r\n",TRDP_INTEGRATION_VER,TRDP_INTEGRATION_VERSION_NOTE);
	
	/*- 打开与2x2服务的共享内存 */
	shrmem_2x2_open();

	/* Taul Application Init */
	err = initTaulApp();
	
	vos_printLog(VOS_LOG_ERROR,"##################### %s %s##################### \r\n",TRDP_INTEGRATION_VER,TRDP_INTEGRATION_VERSION_NOTE);

	if (err != TAUL_APP_NO_ERR)
	{
		printf("initTaulApp() failed,rt=0x%x\n",err);
		return 0;
	}
	/*for(int j=subTelegramNuber;j<subTelegramNuber+pubTelegramNuber;j++)
	{
		UINT8 checkBuffer[1223]={0};
		
		tau_ldLockTrafficStore();
		memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset),2,htons(comidOffsetList[j].datasize)-16);
		tau_ldUnlockTrafficStore();

	}*/
	
	vos_threadDelay(3000000);
	//CHAR8 ddsReaderThreadName[] ="ddsReaderThread";

	//createDDSThread((VOS_THREAD_FUNC_T)testLock,ddsReaderThreadName,NULL,0);
	int counter=0;

	while(1)
	{
		vos_threadDelay(5000000);

#ifdef TSN_COMMU_TIME_TEST
		memset(&tsnCommuDelayStatics1,0,sizeof(tsnCommuDelayStatics1));
		rt=tau_getTsnCommuTimeTestStatics(IF_NAME1,&tsnCommuDelayStatics1);
		if(TRDP_NO_ERR==rt)
		{
			//printf("[TESTCHECK]tsn communication delay stacics,recv num:%d,max delay:%lld,min delay:%lld,avg delay:%lld",tsnCommuDelayStatics1.numRecv,tsnCommuDelayStatics1.maxDelay,tsnCommuDelayStatics1.minDelay,tsnCommuDelayStatics1.avgDelay);
			if(gateRecvNum>tsnCommuDelayStatics1.numRecv)
			{
				gateRecvNum-=tsnCommuDelayStatics1.numRecv;
			}
			else
			{
				if(0u==maxDelay)
				{
					vos_printLog(VOS_LOG_ERROR,"[DELAY_RECORD]max delay update from %lld	us to %lld	us\n",maxDelay/1000,tsnCommuDelayStatics1.maxDelay/1000);
					maxDelay=tsnCommuDelayStatics1.maxDelay;

				}
				else if(maxDelay<tsnCommuDelayStatics1.maxDelay)
				{
					vos_printLog(VOS_LOG_ERROR,"[DELAY_RECORD]max delay update from %lld	us to %lld	us\n",maxDelay/1000,tsnCommuDelayStatics1.maxDelay/1000);
					maxDelay=tsnCommuDelayStatics1.maxDelay;
				}
			}

			rt=tau_clearTsnCommuTimeTestStatics(IF_NAME1);
			if(TRDP_NO_ERR!=rt)
			{
				printf("clear communication delay stacics failed,error code:%d",rt);
			}
		}
		else
		{
			printf("get communication delay stacics failed,error code:%d",rt);
		}

		//@note 至此，已经获取到指定网口的tsn统计结果,在这里进行输出


		//@note 及时清空以获取最近一段时间的统计量

#endif

//		for(int j=0;j<subTelegramNuber/*+pubTelegramNuber*/;j++)
//		{
//			UINT8 checkBuffer[1223]={0};
//
//			tau_ldLockTrafficStore();
//			memcpy(checkBuffer,pTrafficStoreAddr+htonl(comidOffsetList[j].offset),htons(comidOffsetList[j].datasize));
//			tau_ldUnlockTrafficStore();
//
//			printf("buffer[%d;%d;%d]:",htonl(comidOffsetList[j].comId),htonl(comidOffsetList[j].offset),htons(comidOffsetList[j].datasize));
//			for(int q=0;q<htons(comidOffsetList[j].datasize);q++)
//				printf("%02x ",checkBuffer[q]);
//			printf("\n");
//		}
//
//		for(int j=0;j<subTelegramNuber;j++)
//		{
//			TRDP_APP_INDEX readList;
//			UINT32	currentOffset=0;
//
//			currentOffset=8+sharememConfigTAUL.indexOffset+j*sizeof(TRDP_APP_INDEX);
//			tau_ldLockTrafficStore();
//			memcpy((void *)&readList,(void *)(pTrafficStoreAddr+currentOffset),sizeof(TRDP_APP_INDEX));
//			tau_ldUnlockTrafficStore();
//
//			printf("index comId:%d	state1%d	state2%d\n",htonl(readList.comId),readList.subnetState1,readList.subnetState2);
//		}

//		for(int j=subTelegramNuber;j<subTelegramNuber+pubTelegramNuber;j++)
//		{
//			tau_ldLockTrafficStore();
//			memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset),counter,htons(comidOffsetList[j].datasize)-16);
//			memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset)+10,0,1);
//			memset(pTrafficStoreAddr+htonl(comidOffsetList[j].offset)+11,15,1);
//			tau_ldUnlockTrafficStore();
//		}
//		counter++;
	}
}



/**********************************************************************************************************************/
/**
 *	@brief			为通道的超时和重联处理共享内存
 *	@details
 *  @param[in]      pRefCon         user supplied context pointer
 *  @param[in]      argAppHandle           application handle returned by tlc_opneSession
 *  @param[in]      pPDInfo         pointer to PDInformation
 *  @param[in]      pData               pointer to receive PD Data
 *  @param[in]      dataSize        receive PD Data Size
 *
 */
void indicateIfState (
    void                    *pRefCon,
    TRDP_APP_SESSION_T      argAppHandle,
    const TRDP_PD_INFO_T    *pPDInfo,
    UINT8                   *pData,
    UINT32                  dataSize)
{
	extern UINT8	*pTrafficStoreAddr;
	UINT8* pState=NULL;
	UINT8	currentState=IF_STATE_INVALID;
	UINT32	stateOffset=0;
	SUBSCRIBE_TELEGRAM_T *pSubscribeTelegram;
	UINT32 appIndex=2;

	//printf("app:%d\n"
			//,argAppHandle==appHandle1);

	if(argAppHandle==appHandle1)
	{
		appIndex=0;
	}
	else if(argAppHandle==appHandle2)
	{
		appIndex=1;
	}
	else
	{
		return;
	}

    /*- 获取通道句柄结构体指针失败 */
    if ((pSubscribeTelegram = (SUBSCRIBE_TELEGRAM_T *)pPDInfo->pUserRef) == NULL)
    {
    	return;
    }


	//printf("app handle:%llucode:%d(timeout:%d single down:%d reconect:%d) comid:%u userIndex:%u\n"
			//,argAppHandle==appHandle1,pPDInfo->resultCode,TRDP_TIMEOUT_ERR_SIG,TRDP_SINGLE_LINKDOWN_SIG,TRDP_RECONNECT_SIG
			//,pSubscribeTelegram->comId,pSubscribeTelegram->userIndex);

	/*- 判断currentState */
	if(TRDP_TIMEOUT_ERR_SIG==pPDInfo->resultCode || TRDP_SINGLE_LINKDOWN_SIG==pPDInfo->resultCode)
	{
		currentState=IF_STATE_INVALID;
	}
	else if(TRDP_RECONNECT_SIG==pPDInfo->resultCode)
	{
		currentState=IF_STATE_ON;
	}
	else
	{
		return;
	}

	/*- 计算offset */
	stateOffset=pSubscribeTelegram->userIndex;
	stateOffset=sharememConfigTAUL.indexOffset+8+sizeof(TRDP_APP_INDEX)*stateOffset+10+appIndex;

	pState=(UINT8*)pTrafficStoreAddr+stateOffset;

    tau_ldLockTrafficStore();
    *pState=currentState;
    tau_ldUnlockTrafficStore();

    return;
}

/**
 *	@brief			从共享内存获取冗余组状态，处理后返回至出参
 *	@details		从共享内存读取接口，判断时间戳有效性
 *					-时间戳无效时，认为冗余服务处于宕机状态，返回一个统一备单元
 *					-时间戳有效时，返回读取到的各冗余单元
 *  @param[out]      pRedunStateList 列表空间
 *  @param[in,out]   listLen 读取列表可写单元数量，返回已写列表单元数量
 *
 */
void getRedunState(REDUN_INFO_T* pRedunStateList,UINT32* pListLen)
{
	UINT8 err=1;
	STU_2X2SVC_SHRMEMINFO readedInfo;
	UINT32 i=0;

	if(NULL!=pListLen)
	{
		if(NULL!=pRedunStateList)
		{
			/*- 读取共享内存 */
			err=(UINT8)shrmem_2x2_trdpRead(&readedInfo);
			if(0u!=err)
			{
				*pListLen=0;
			}
		}
		else
		{
			*pListLen=0;
		}
	}

	if (0u==err)
	{
		/*- 更新时间有效 */
		if(0u!=readedInfo.timestamp && CM_GetTick()-readedInfo.timestamp<=readedInfo.cycle*CM_MILLION + TOLERABLE_DELAY)
		{
			if((UINT32)readedInfo.redIdNum<=*pListLen)
			{
				for(i=0;i<readedInfo.redIdNum;i++)
				{
					pRedunStateList[i].redId=(UINT32)readedInfo.redIdStateList[i].redId;
					pRedunStateList[i].leader=(BOOL8)readedInfo.redIdStateList[i].leder;
				}

				*pListLen=(UINT32)readedInfo.redIdNum;
			}
			else
			{
				for(i=0;i<*pListLen;i++)
				{
					pRedunStateList[i].redId=(UINT32)readedInfo.redIdStateList[i].redId;
					pRedunStateList[i].leader=(BOOL8)readedInfo.redIdStateList[i].leder;
				}

				vos_printLog(VOS_LOG_DBG, "read %d redundant ids more than defined %d.\n",(UINT32)readedInfo.redIdNum, *pListLen);
			}
		}
		/*- 更新时间失效 */
		else
		{
			/*- 返回统一置slave */
			pRedunStateList[0].redId=0;
			pRedunStateList[0].leader=FALSE;
			*pListLen=1;
		}
	}
	else
	{

	}
}

#endif /* TRDP_OPTION_LADDER */
