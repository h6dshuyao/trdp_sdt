/**********************************************************************************************************************/
/**	@file			sdsrc.c
 *	@brief			发送端sdsrc通道
 *	@author			杨凯
 *	@date			2021-11-06
 *	@version		v1.0.1
 */

/***********************************************************************************************************************
 * INCLUDES
 */

#include "sdsrc.h"
#include "sdt_utils.h"

/*	---------------------------------------内部变量定义--------------------------------------------------	*/
/*  发送端定义  */
SDSRC g_sdsrc = {.channelCapacity = SDSRC_CHANNEL_AMOUNT_MAX};		/* 全局变量, 保存当前接发送端通道的信息 */
/*  测试时使用的变量  */
INT32 test_cycCount = 0;
UINT8 test_stepCount=0;

/*	---------------------------------------内部函数定义--------------------------------------------------	*/
/* 配置发送端通道 */
SDT_RC sdsrc_config_sdsrc_channel(P_SDSRC_CHANNEL pSdsrcChannel, P_SDT_CONFIG_T const pSdsrcConfig);

/* 添加VDP尾部							*/ 
SDT_RC sdsrc_add_vdp_tail(UINT8* vdpBuffer, UINT32 tailOffset, P_VDP_INFO_T pVDPInfo, UINT32 sid);

/* 打印vpd信息							*/
void sdsrc_printf_test_info(P_SDSRC_CHANNEL const pSdsrcChannel, UINT8 const* vpdBuffer, UINT32 const vpdBufferLength);

/*	-----------------------------------------接口实现---------------------------------------------------	*/

/** 
*	@brief		增加指定通道SSC值
*	@details	SSC值加上入参
*	@param[in]	SDT_HANDLE	sdsrcHandle		通道句柄
*	@param[in]	UINT32		value			增加的值
*	@return		SDT_RC					执行情况
*	@retval		SDT_OK					成功
*               SDT_INVALID_HANDLE		无效句柄
*/

SDT_RC sdsrc_SSCvalue_add_value(SDT_HANDLE sdsrcHandle,UINT32 value)
{
	/*- 检查传入的句柄有效性 */
	SDT_RC result=SDT_OK;
	 
	if (SDSRC_HANDLE_IS_INVALIDE(sdsrcHandle))
	{
		SDT_DEBUG_MSG("invalid sdsrc handle! \n");
		result = SDT_INVALID_HANDLE;
	}
	
	/*- 为句柄相应通道的SSC加上输入的值  */
	g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount=g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount+value;

	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		创建 并 配置 发送端通道
*	@details	创建 并 配置 发送端通道
*	@param[in]	P_SDT_HANDLE			pSdsrcHandle	发送端句柄指针
*	@param[in]	P_SDT_CONFIG_T		pSdsrcConfig	发送端通道配置信息
*	@return		SDT_RC					执行情况
*	@retval		SDT_OK					通道创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER	无效参数
*				SDSRC_CHANNEL_FULL		通道数量达到上限
*				SDT_INVALID_SID			SID计算错误
*/

SDT_RC sdsrc_create_channel(P_SDT_HANDLE pSdsrcHandle, P_SDT_CONFIG_T pSdsrcConfig)
{
	/*- 初始化相关局部变量 */
	
	SDT_RC			result = SDT_OK;
	SDT_HANDLE		tempHandle = g_sdsrc.channelNum;
	P_SDSRC_CHANNEL tempSdsrcChannel = NULL;
	UINT32 i=0;
	
	/*- 初次调用通道创建函数  */
	if(0u==g_sdsrc.channelNum)
	{
		/*- 初始化SDSINK所有通道 */
		SDT_Memset(g_sdsrc.channelArray,sizeof(SDSRC_CHANNEL) * SDT_CHANNEL_AMOUNT_MAX,0u,sizeof(SDSRC_CHANNEL) * g_sdsrc.channelCapacity);

		for(i=0u;i<g_sdsrc.channelCapacity;i++)
			g_sdsrc.channelArray[i].m_handleEffective=SDT_UNEFFECTIVE;
	}
	
	/*- 当前使用通道数量已经达到上限 */
	if (tempHandle >= g_sdsrc.channelCapacity)
	{
		/*- 返回值赋错误码 */
		SDT_DEBUG_MSG("sdsrc channel create failed, channel array is full!\n");
		result = SDSRC_CHANNEL_FULL;
	}

	/*- 返回值为SDT_OK */
	if (SDT_OK == result)
	{
		/*- 传入指针不存在 */
		if (PT_IS_NULL(pSdsrcHandle) || PT_IS_NULL(pSdsrcConfig))
		{
			/*- 返回值赋错误码 */
			SDT_DEBUG_MSG("sdsrc channel create error, pointer does not exist!\n");
			result = SDT_PT_NOT_EXIST;
		}
	}

	/*- 通道数量未达上限且传入指针存在 */
	if (SDT_OK == result)
	{
		/*- 检查配置参数有效性 */	
		result = sdt_check_config(pSdsrcConfig);
	}
	
	/*- 配置参数有效 */
	if (SDT_OK == result)
	{
		/*- 获取SDSINK当前通道，激活并配置，成功后返回通道句柄 */
		
		/* 获取发送端通道 */
		tempSdsrcChannel = &(g_sdsrc.channelArray[tempHandle]);
		/*- 获取发送端通道失败 */
		if (PT_IS_NULL(tempSdsrcChannel))
		{
			/*- 返回值赋错误码 */
			SDT_DEBUG_MSG("sdsrc create failed, can not get channel pointer!");
			result = SDT_PT_NOT_EXIST;		/* 未获取到发送端通道指针 */
		}
		/*- 获取通道指针成功但通道已被占用 */
		else if(SDT_EFFECTIVE==tempSdsrcChannel->m_handleEffective)
		{
			/*- 返回值赋错误码 */
			/* 待定：此处应添加一个应对通道激活标志位与当前通道数量不匹配的错误处理函数*/
			SDT_DEBUG_MSG("sdsrc create failed, channel counter disturbed!\n");
			result = SDT_OUT_OF_BOUND;
		}
		else
		{
			/*- 激活并配置当前SDSINK通道 */
			result = sdsrc_config_sdsrc_channel(tempSdsrcChannel, pSdsrcConfig);
		}
		
		/*- 通道配置失败 */
		if (SDT_OK != result)
		{
			/*- 重初始化当前SDSINK通道 */
			SDT_Memset(tempSdsrcChannel,sizeof(SDSRC_CHANNEL), 0u, sizeof(SDSRC_CHANNEL));
			tempSdsrcChannel->m_handleEffective=SDT_UNEFFECTIVE;
			SDT_DEBUG_MSG("SDSRC : channel %u create failed! \n", tempHandle);
		}
		else
		{/*- 增加通道数量并返回通道句柄*/
			g_sdsrc.channelNum++;
			*pSdsrcHandle = tempHandle;			
			SDT_DEBUG_MSG("SDSRC : channel %u create success! \n", tempHandle);
		}
	}

	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		发送端数据处理
*	@details	发送端数据处理
*	@param[in]	SDT_HANDLE*		pSdsrcHandle	发送端句柄指针
*	@param[in]	UINT8*			vpdBuffer		vpd信息缓存
*	@param[in]	UINT32			vpdLength		vpd信息长度
*	@param[out]	UINT8*			vdpBuffer		vdp信息缓存
*	@param[out]	UINT32*			vdpLength		vdp信息长度
*	@return		SDT_RC					执行情况
*	@retval		SDT_OK					VDP生成成功
*				SDT_INVALID_HANDLE		句柄无效
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度无效
*				SDT_OUT_OF_BOUND		访问越界误
*/

SDT_RC sdsrc_generate_VDP(SDT_HANDLE sdsrcHandle, UINT8* vpdBuffer, UINT32 userDataLenth, UINT8* vdpBuffer, UINT32* vdpLength)
{
	/*- 入参有效性检查 */
	
	SDT_RC	result = SDT_OK;
	UINT32	tempVpdLength = 0;
	UINT32	vdpSize = 0;
	UINT8	tempVdpBuffer[SDT_VPDMAX+SDT_TAIL_SIZE];		/* 984+16 = 1000 字节*/
	SDT_Memset(tempVdpBuffer, sizeof(tempVdpBuffer),0u, sizeof(tempVdpBuffer));

	/*	传入的句柄无效	*/ 
	if (SDSRC_HANDLE_IS_INVALIDE(sdsrcHandle))
	{
		/*- 返回值赋错误码 */
		SDT_DEBUG_MSG("invalid sdsrc handle! \n");
		result = SDT_INVALID_HANDLE;
	}

	/*- 返回值为SDT_OK */
	if(SDT_OK == result)
	{
		/*	检测传入的指针是否存在	 */
		if (PT_IS_NULL(vpdBuffer) || PT_IS_NULL(vdpBuffer) || PT_IS_NULL(vdpLength))
		{
			SDT_DEBUG_MSG("[vpd buffer || vdp buffer || vdp length]pointer does not exist!");
			result = SDT_PT_NOT_EXIST;
		}
	}

	/* 指针存在 */
	if(SDT_OK == result)
	{
		/*  检测传入的长度是否合理  */
		if ((SDT_VPDMIN >= userDataLenth) || (SDT_VPDMAX < userDataLenth) || (*vdpLength<(userDataLenth + SDT_TAIL_SIZE)))
		{
			result = SDT_INVALID_VPD_LEN;
			SDT_DEBUG_MSG("user data length is unreasonable!");
		}
	}
	
	/*- 输入参数均合理 */ /* vdp长度合理 */
	if(SDT_OK == result)
	{	
		/*- 拷贝用户数据到VDP缓存 */
		SDT_Memcpy(tempVdpBuffer,SDT_VPDMAX+SDT_TAIL_SIZE, (const void*)vpdBuffer, userDataLenth);
		
		/*- 计算协议规定的vpd长度  */
		tempVpdLength = sdsrc_four_byte_alignment(userDataLenth);
		
		/*- 生成VDP包尾 */	
		result = sdsrc_add_vdp_tail(tempVdpBuffer, tempVpdLength, &(g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo), g_sdsrc.channelArray[sdsrcHandle].m_SID);
		if (SDT_OK != result)
		{
			SDT_DEBUG_MSG("vdp tail add failed!\n");
		}
	}
	
	/*- 生成VDP包尾成功 */	
	if(SDT_OK == result)
	{
		/*-	返回生成的VDP数据并递增SSC */ 

		//首先将用户数据部分拷贝到vdpBuffer中，所使用的长度是未经四字节对齐处理的vpd长度，也就是纯用户数据部分
		SDT_Memcpy((void*)vdpBuffer,*vdpLength, tempVdpBuffer, userDataLenth);

		//返回的vpd长度是纯用户数据的长度再加上16
		vdpSize = userDataLenth + SDT_TAIL_SIZE;
		*vdpLength = vdpSize;

		//然后将vdp尾部16个字节的校验数据拷贝到vdpBuffer中，其中vdpBuffer的偏移是刚才的vpd长度，tempvdpBuffer的偏移是经过四字节对齐处理后的vpd长度
		SDT_Memcpy(&vdpBuffer[userDataLenth],16u, (const void *)(tempVdpBuffer+tempVpdLength), SDT_TAIL_SIZE);

	#ifdef _TEST_INFO
		sdsrc_printf_test_info(&(g_sdsrc.channelArray[sdsrcHandle]), vpdBuffer, userDataLenth);
	#endif

		g_sdsrc.channelArray[sdsrcHandle].m_VDPInfo.m_SafeSequCount++;
		g_sdsrc.channelArray[sdsrcHandle].m_VDPNum++;
	}
	else
	{
		*vdpLength = 0u;
	}

	/*- 返回函数执行情况 */
	return result;
}

/**
*	@brief		获取指定SDSRC通道已经生成的VDP数量
*	@details	获取指定SDSRC通道已经生成的VDP数量
*	@param[in]	SDT_HANDLE	sdsrcHandle		通道句柄
*	@return		UINT32	已经生成的VDP数量
*	@retval		非0		查询成功
*               0		无效句柄
*/

UINT32 sdsrc_get_VDP_number(SDT_HANDLE sdsrcHandle)
{
	/*- 初始化返回值 */
	UINT32 vdpNumber=0;

	/*- 传入的句柄有效 */
	if (!SDSRC_HANDLE_IS_INVALIDE(sdsrcHandle))
	{
		/*- 获取当前通道的VDP计数  */
		vdpNumber=g_sdsrc.channelArray[sdsrcHandle].m_VDPNum;
	}

	/*- 返回结果 */
	return vdpNumber;
}


/*	-----------------------------内部函数实现--------------------------------------------------	*/


/** 
*	@brief		配置发送端通道
*	@details	配置发送端通道
*	@param[in]	SDT_HANDLE*				pSdsrcHandle	发送端句柄指针
*	@param[in]	SDT_CONFIG_T const *	pSdsrcConfig	配置结构体指针
*	@return		SDT_RC					执行情况
*	@retval		SDT_OK					通道配置成功
*				SDT_INVALID_HANDLE		句柄无效
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度无效
*				SDT_OUT_OF_BOUND		访问越界误
*/

SDT_RC sdsrc_config_sdsrc_channel(P_SDSRC_CHANNEL pSdsrcChannel, P_SDT_CONFIG_T const pSdsrcConfig)
{
	/*-	将通道状态标志位置为激活 */
	pSdsrcChannel->m_handleEffective = SDT_EFFECTIVE;

	
	/*- 根据设备身份配置SMI */
	
	SDT_RC result = SDT_OK;

	if (STA_SDSRC_CHANNEL_A == pSdsrcConfig->channelIdentity)
	{
		pSdsrcChannel->m_SMI = pSdsrcConfig->smi1;
	}
	else if (STA_SDSRC_CHANNEL_B == pSdsrcConfig->channelIdentity)
	{
		pSdsrcChannel->m_SMI = pSdsrcConfig->smi2;
	}
	else
	{
		SDT_DEBUG_MSG("channel identity error! sdsrc identity should be [STA_SDSRC_CHANNEL_A|STA_SDSRC_CHANNEL_B]\n");
		result = SDT_INVALID_PARAMETER;
	}
	
	/*- 设备身份为有效值 */
	if(SDT_OK==result)
	{
		/*- 计算SID */
		
		/* 设置SID结构体参数 */
		SDT_Memset(&(pSdsrcChannel->m_SIDInfo),sizeof(SID_INFO_T), 0u, sizeof(SID_INFO_T));
		pSdsrcChannel->m_SIDInfo.SDTProtVers = SDT_VERSION;

		/* 计算发送端通道的SID */
		pSdsrcChannel->m_SID = sdt_calculate_sid(&(pSdsrcChannel->m_SIDInfo), pSdsrcChannel->m_SMI);
		if (0u == pSdsrcChannel->m_SID)
		{
			result = SDT_INVALID_SID;
		}
	}
	
	/*- SID为有效值 */
	if(SDT_OK==result)
	{
		/*- 设置其他配置量 */
		
		/*  设置VDP结构体参数			*/
		SDT_Memset(&(pSdsrcChannel->m_VDPInfo),sizeof(VDP_INFO_T), 0u, sizeof(VDP_INFO_T));
		pSdsrcChannel->m_VDPInfo.m_UserDataVersion = pSdsrcConfig->udv;
		
		pSdsrcChannel->m_VDPInfo.m_SafeSequCount = 1u; //待定：SSC从1开始
		
		/*	配置时间参数 */
		pSdsrcChannel->m_rxPeriod = pSdsrcConfig->rxPeriod;
		pSdsrcChannel->m_txPeriod = pSdsrcConfig->txPeriod;
	}

	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		添加VDP尾
*	@details	将vdp包尾信息结构体中的信息写到vdp数据缓存的相应位置，其中包括计算SC
*	@note		此处tailOffset的实参应为未经四字节对齐的用户数据长度
*	@param[in]	UINT8*			vdpBuffer		vdp数据缓存
*	@param[in]	UINT32			tailOffset		开始写入vdp包尾的地址偏移
*	@param[in]	P_VDP_INFO_T	pVDPInfo		vdp包尾信息结构体
*	@param[out]	UINT32			sid				SID
*	@return		SDT_RC					执行情况
*	@retval		SDT_OK					VDP生成成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_OUT_OF_BOUND		访问越界误
*/	

SDT_RC sdsrc_add_vdp_tail(UINT8* vdpBuffer, UINT32 tailOffset, P_VDP_INFO_T pVDPInfo, UINT32 sid)
{
	/*- 检查传入指针存在性 */
	SDT_RC result = SDT_OK;
	UINT32 pos = 0u;

	if ((PT_IS_NULL(vdpBuffer)) || (PT_IS_NULL(pVDPInfo)))
	{
		SDT_DEBUG_MSG("[vdpBuffer || pVdpInfo ]pointer dose not exist!\n");
		result = SDT_PT_NOT_EXIST;
	}
	
	/*- 传入指针存在 */
	if(SDT_OK==result)
	{
		/*- 将vdp包尾信息结构体中的信息写到vdp数据缓存的相应位置并计算SC */
		
		pos = tailOffset;
		/*  添加reserved01	*/
		LongToChar(pVDPInfo->m_reserved01, &vdpBuffer[pos]);
		pos += sizeof(pVDPInfo->m_reserved01);
		/*  添加reserved02	*/
		ShortToChar(pVDPInfo->m_reserved02, &vdpBuffer[pos]);
		pos += sizeof(pVDPInfo->m_reserved02);

		/*  添加UDV			*/
		ShortToChar(pVDPInfo->m_UserDataVersion, &vdpBuffer[pos]);
		pos += sizeof(pVDPInfo->m_UserDataVersion);

		/*  添加SSC			*/
		LongToChar(pVDPInfo->m_SafeSequCount, &vdpBuffer[pos]);
		pos += sizeof(pVDPInfo->m_SafeSequCount);
		
		/*  计算SC			*/
		pVDPInfo->m_SafetyCode = sdt_calculate_sc(vdpBuffer, tailOffset, sid);

		/*	添加SC			*/
		LongToChar(pVDPInfo->m_SafetyCode, &vdpBuffer[pos]);
		pos += sizeof(pVDPInfo->m_SafetyCode);

		if (pos != tailOffset + SDT_TAIL_SIZE)
		{
			SDT_DEBUG_MSG("memory access out of bounds!\n");
			SDT_Memset((void*)(&vdpBuffer[tailOffset]),SDT_TAIL_SIZE, 0u, SDT_TAIL_SIZE);
			result = SDT_OUT_OF_BOUND;
		}
	}

	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		打印发送端测试信息
*	@details	打印发送端测试信息
*	@param[in]	P_SDSRC_CHANNEL	const	pSdsrcChannel		发送端通道句柄
*	@param[in]	UINT8	const*			vpdBuffer			vpd信息缓存指针
*	@param[in]	UINT32	const			vpdBufferLength 	vpd信息长度
*/	

void sdsrc_printf_test_info(P_SDSRC_CHANNEL const pSdsrcChannel, UINT8 const* vpdBuffer, UINT32 const vpdBufferLength)
{
	
	/*- 打印发送端测试信息 */
	fprintf(stdout, "-----------------------------------------------------------\n");
	fprintf(stdout, "当前发送端通道信息\n");
	fprintf(stdout, "发送内容: %u字节\n%s\n", vpdBufferLength, vpdBuffer);
	fprintf(stdout, "SMI : %u\tSID : %x\n", pSdsrcChannel->m_SMI, pSdsrcChannel->m_SID);
	fprintf(stdout, "UDV : %u\tSSC : %u\n", pSdsrcChannel->m_VDPInfo.m_UserDataVersion, pSdsrcChannel->m_VDPInfo.m_SafeSequCount);
	fprintf(stdout, "SC  : %x\n", pSdsrcChannel->m_VDPInfo.m_SafetyCode);
	fprintf(stdout, "-----------------------------------------------------------\n");

}

