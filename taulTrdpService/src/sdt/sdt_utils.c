/**********************************************************************************************************************/
/**	@file			sdt_utils.c
 *	@brief			sdt工具函数
 *	@author			杨凯
 *	@date			2021-11-06
 *	@version		v1.0.1
 */

/***********************************************************************************************************************
 * INCLUDES
 */


#include "sdt_utils.h"
#include <time.h>
/*------------------------------------------------- 内部函数声明------------------------------------------------------*/
/* 将SID结构体转换为UINT8数组	*/
SDT_RC sdt_convert_sid_struct_to_array(UINT8* ptSIDArray, UINT32 SIDArraySize, const P_SID_INFO_T ptSIDStruct, UINT32 smi);

/* CRC32算法 */ 
UINT32 sdt_calculate_crc32(UINT8* array, UINT32 arraySize, UINT32 crcSeed);

/* 检测sdt参数存在性 */
SDT_RC sdt_check_sdt_config_parameter_exist(P_SDT_CONFIG_T pSdtConfig);

/* 检测参数合理性 */
SDT_RC sdt_check_sdt_config_parameter_valid(P_SDT_CONFIG_T pSdtConfig);


/*-------------------------------------------------- 接口定义-----------------------------------------------------*/


/** 
*	@brief		计算SID
*	@details	通过输入的SID信息结构体和SMI计算SID
*	@param[in]	SID_INFO_T*			pSIDStruct			SID信息结构体指针
*	@param[in]	UINT32				smi					SMI
*	@return		UINT32				SMI
*	@retval		非0		返回计算得到的SID值
*				0		计算SID值错误
*/

UINT32 sdt_calculate_sid(P_SID_INFO_T pSIDStruct, UINT32 smi)
{
	/*- 局部变量初始化 */
	UINT32	sidValue = 0;
	UINT32	sidArraySize = sizeof(SID_INFO_T) + sizeof(smi);
	UINT8	sidArray[sizeof(SID_INFO_T) + sizeof(smi)];

	/*- 将SID结构体转换为UINT8数组成功 */
	if (SDT_OK == sdt_convert_sid_struct_to_array(sidArray, sidArraySize, pSIDStruct, smi))
	{
		/*- 通过CRC算法计算SID的值 */
		sidValue = sdt_calculate_crc32(sidArray, sidArraySize, SDT_SID_SEED);
	}

	/*- 返回sid */
	return  sidValue;
}

/** 
*	@brief		计算SC
*	@details	通过给定的VDP数组，计算SC
*	@note		本函数需传入未经四字节对齐处理的用户数据长度，对用户数据的四字节对齐及补零处理在这里进行，符合协议规范的VDP也仅在此处产生
*	@param[in]	UINT8*			vdpArray			vdp数组
*	@param[in]	UINT32			userDataLenth		vdp数组中用户数据部分的长度
*	@param[in]	UINT32			sid					种子值sid
*	@return		UINT32			SC
*	@retval		非0		返回计算得到的SC值
*				0		计算SC值错误
*/

UINT32 sdt_calculate_sc(UINT8* vdpArray, UINT32 userDataLenth, UINT32 sid)
{
	
	/*- 根据用户数据长度为SC计算Buffer分配四字节对齐后的内存 */
	UINT32 scValue = 0;
	UINT32 SCstructLenth=0;
	UINT32 vpdLenth=0;
	
	vpdLenth=sdsrc_four_byte_alignment(userDataLenth);
	SCstructLenth= vpdLenth+SDT_TAIL_SIZE- sizeof(UINT32);
	
	UINT8* ptempArray = (UINT8*)malloc(SCstructLenth);
	
	/*- 分配失败 */
	if (PT_IS_NULL(ptempArray))
	{
		/*- 返回值赋零 */
		scValue = 0u;
		SDT_DEBUG_MSG("calculate safety code error!");
	}
	else
	{
		
		/*- 将vdp缓存中的用户数据部分和VDP包尾分别拷贝到SC计算数据Buffer */
		SDT_Memset((void*)ptempArray,SCstructLenth,0u,SCstructLenth);
		
		SDT_Memcpy((void*)ptempArray,SCstructLenth ,(const void*)vdpArray, userDataLenth);
		SDT_Memcpy((void*)(&ptempArray[vpdLenth]),SCstructLenth-vpdLenth ,(const void*)(&vdpArray[userDataLenth]), SDT_TAIL_SIZE - sizeof(UINT32));
		
		/*- 计算SC */
		scValue = sdt_calculate_crc32(ptempArray, SCstructLenth, sid);

	}

	/*- 释放内存 */
	free((void*)ptempArray);
	ptempArray = NULL;

	/*- 返回sc */
	return scValue;
}

/*
*	函数名称:   sdt_get_rand_number
*	函数功能:   生成给定范围内([min, max])的随机数
*
*	参数说明:   UINT32			min					随机数下限
*				UINT32			max					随机数上限
*
*	返回结果:   UINT32			生成的随机数
* 
*/

#if 0
/** 
*	@brief		生成随机数
*	@details	生成给定范围内([min, max])的随机数
*	@param[in]	UINT32			min					随机数下限				
*	@param[in]	UINT32			max					随机数上限
*	@return		UINT32			生成的随机数
*	@retval		生成的随机数
*/

UINT32 sdt_get_rand_number(UINT32 min, UINT32 max)
{
	/*- 计算随机数 */
	srand((unsigned)time(NULL));
	
	/*- 返回随机数 */
	return (UINT32)(rand()%(max-min+1))+min;
}
#endif
/** 
*	@brief		检测配置信息有效性
*	@details	检测配置信息有效性
*	@note		未检测传入指针存在性，应仅在sdt模块内部调用该函数
*	@param[in]	P_SDT_CONFIG_T			pSdtConfig				配置参数指针
*	@return		SDT_RC					sdt状态
*	@retval		SDT_OK					配置信息有效
*               SDT_PT_NOT_EXIST		配置信息指针不存在
*               SDT_INVALID_PARAMETER   相关配置参数错误
*/
SDT_RC sdt_check_config(P_SDT_CONFIG_T pSdtConfig)
{
	/*- 检查各参数是否赋值 */
	
	SDT_RC result = SDT_ERROR;
	result = sdt_check_sdt_config_parameter_exist(pSdtConfig);

	/*- 各必要参数均有赋值 */
	if(SDT_OK==result)
	{
		/*- 检查各参数值合理性 */
		result = sdt_check_sdt_config_parameter_valid(pSdtConfig);
	}
	else{}

	/*- 返回检查情况 */
	return result;

}

/** 
*	@brief		获取系统上电时钟(ms)
*	@details	获取当前程序运行时间
*	@return		SDT_CLOCK		当前程序运行时间
*	@retval		SDT_CLOCK		当前程序运行时间
*/

SDT_CLOCK sdt_get_current_time(void)
{

#if ((defined(WIN32))|| (defined(WIN64)))
	/*- 获取当前系统时间 */
	return clock();

	#elif(defined(VXWORKS))
    struct timespec currentTime = {(time_t)NULL,(long)NULL};   
#elif(defined(linux))
    struct timespec currentTime;
#endif
    
    /*- 返回上电时钟 */
    SDT_CLOCK retval;

    (void)clock_gettime(CLOCK_MONOTONIC, &currentTime);
    retval=(currentTime.tv_sec*1000) + (currentTime.tv_nsec/1000000);

    return retval;
}



/*---------------------------------------------内部函数实现---------------------------------------------*/


/** 
*	@brief		将SID结构体转换为UINT8数组
*	@details	将SID结构体转换为UINT8数组
*	@param[out]	UINT8 *				pSIDArray		返回SID数组的指针
*	@param[in]	UINT32				SIDArraySize	SID数组的大小
*	@param[in]	SID_INFO_T const *	ptSIDStruct		SID结构体指针
*	@param[in]	UINT32				smi				smi
*/
SDT_RC sdt_convert_sid_struct_to_array(UINT8* pSIDArray, UINT32 SIDArraySize, const P_SID_INFO_T ptSIDStruct, UINT32 smi)
{
	
	/*- 将SID结构体转换为UINT8数组 */
	SDT_RC result = SDT_OK;
	UINT32 pos = 0u;

	if (NULL == pSIDArray)
	{
		SDT_DEBUG_MSG("[sdt_convert_sid_struct_to_array()]:sid info array pointer dose not exist!\n");
		result = SDT_PT_NOT_EXIST;
	}
	else
	{
		/* 清空数组 */
		SDT_Memset((void*)pSIDArray,SIDArraySize,0u, SIDArraySize);

		/* 写入smi */
		LongToChar(smi, &pSIDArray[pos]);
		pos += sizeof(smi);

		/* 写入保留值01 */
		ShortToChar(ptSIDStruct->reserved01, &pSIDArray[pos]);
		pos += sizeof(ptSIDStruct->reserved01);

		/* 写入SDT版本号 */
		ShortToChar(ptSIDStruct->SDTProtVers, &pSIDArray[pos]);
		pos += sizeof(ptSIDStruct->SDTProtVers);

		/* 写入UUID */
		SDT_Memcpy((void*)&pSIDArray[pos],SIDArraySize-pos, ptSIDStruct->cstUUID, SDT_UUID_SIZE);
		pos += SDT_UUID_SIZE;

		/* 写入拓扑操作计数 */
		LongToChar(ptSIDStruct->safeTopoCount, &pSIDArray[pos]);
		pos += sizeof(ptSIDStruct->safeTopoCount);

		/* 写入保留值02 */
		LongToChar(ptSIDStruct->reserved02, &pSIDArray[pos]);
		pos += sizeof(ptSIDStruct->reserved02);

		/* 越界检查 */
		if (pos != SIDArraySize)
		{
			SDT_DEBUG_MSG("[sdt_convert_sid_struct_to_array()]:Out of bounds occurred during array conversion!\n");
			result = SDT_OUT_OF_BOUND;
		}
	}

	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		CRC32算法计算
*	@details	CRC32算法
*	@param[in]	UINT8*			array				数组
*	@param[in]	UINT32			arraySize			数组长度
*	@param[in]	UINT32			crcSeed				种子值
*	@return		UINT32			计算结果
*	@retval		非0				返回计算得到的值
*				0				计算值错误
*/
UINT32 sdt_calculate_crc32(UINT8* array, UINT32 arraySize, UINT32 crcSeed)
{
	/*- CRC32计算 */
	UINT32 i=0u; /* 计数 */
	UINT32 crcValue = crcSeed;
	for (i = 0u; i < arraySize; ++i)
	{
		crcValue = g_tct_sdt_crc32_table[((UINT32)(crcValue >> 24) ^ array[i]) & 0xffu] ^ (crcValue << 8);
	}
	
	/*- 返回计算结果 */
	return crcValue;
}

/** 
*	@brief		检查sdt参数是否赋值
*	@details	检查参数是否赋值，必要参数未赋值则报错，非必要参数未赋值则赋默认值
*	@note		未检测传入指针存在性，应仅在sdt模块内部调用该函数
*	@param[out]	SDT_HANDLE*				pSdsinkHandle	发送端句柄指针
*	@param[in]	SDT_CONFIG_T*			pSdsinkConfig	发送端通道配置信息
*	@return		SDT_RC					sdt状态
*	@retval		SDT_OK					必要参数赋值检测通过
*				SDT_INVALID_PARAMETER   必要参数未赋值
*/
SDT_RC sdt_check_sdt_config_parameter_exist(P_SDT_CONFIG_T pSdtConfig)
{
	/*- 必要参数赋值检测 */
	SDT_RC result = SDT_OK;

	/* smi1 赋值检测 */
	if (SDT_INVALID_VALUE == pSdtConfig->smi1)
	{
		SDT_DEBUG_MSG("parameter: \"smi1\" does not exist!\n");
		result |= SDT_INVALID_PARAMETER;
	}

	/* udv 检测*/
	if (SDT_INVALID_VALUE == pSdtConfig->udv)
	{
		SDT_DEBUG_MSG("parameter: \"udv\" does not exist!\n");
		result |= SDT_INVALID_PARAMETER;
	}

	/* rxPeriod 检测 */
	if (SDT_INVALID_VALUE == pSdtConfig->rxPeriod)
	{
		SDT_DEBUG_MSG("parameter: \"rxPeriod\" does not exist!\n");
		result |= SDT_INVALID_PARAMETER;
	}

	/* txPeriod 检测 */
	if (SDT_INVALID_VALUE == pSdtConfig->txPeriod)
	{
		SDT_DEBUG_MSG("parameter: \"txPeriod\" does not exist!\n");
		result |= SDT_INVALID_PARAMETER;
	}

	/*- 可选参数赋值检测, 未赋值则赋默认值 */
	if (SDT_INIT_VALUE == pSdtConfig->smi2)		pSdtConfig->smi2 =	SDT_DEFAULT_CHANNEL_B_SMI;
	if (SDT_INIT_VALUE == pSdtConfig->nRxSafe)	pSdtConfig->nRxSafe = SDT_DEFAULT_N_RXSAFE;
	if (SDT_INIT_VALUE == pSdtConfig->nGuard)	pSdtConfig->nGuard = SDT_DEFAULT_N_GUARD;
	if (SDT_INIT_VALUE == pSdtConfig->K4)		pSdtConfig->K4 = SDT_DEFAULT_K4;
	if (SDT_INIT_VALUE == pSdtConfig->channelIdentity)	pSdtConfig->channelIdentity = STA_SDSINK_CHANNEL;

	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		检查配置信息合理性
*	@details	检查配置信息合理性
*	@note		未检测传入指针存在性，应仅在sdt模块内部调用该函数
*	@param[in]	P_SDT_CONFIG_T			pSdtConfig				配置参数指针
*	@return		SDT_RC					sdt状态
*	@retval		SDT_OK					相关配置参数合理
*               SDT_INVALID_PARAMETER   相关配置参数无效
*/

SDT_RC sdt_check_sdt_config_parameter_valid(P_SDT_CONFIG_T pSdtConfig)
{
	/*- smi1 和 smi2 合理性检查 */
	
	SDT_RC result = SDT_OK;
	
	/* smi1 和 smi2 不能同时为0, 参数赋值检查已经覆盖 */
	/* smi1 和 smi2 应当互斥 */
	if (pSdtConfig->smi1 == pSdtConfig->smi2)
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"smi1\" and \"pSdtConfig->smi2\" have the same value!\n");
		result |= SDT_INVALID_PARAMETER;
	}
	
	/* 不得使用1-999范围内的smi值 */
	if ((1u <= pSdtConfig->smi1) && (999u >= pSdtConfig->smi1))
	{
		SDT_DEBUG_MSG("Warning: smi1 is %u, in the range [1-999]\n", pSdtConfig->smi1);
		result |= SDT_INVALID_PARAMETER;
	}
	if ((1u <= pSdtConfig->smi2) && (999u >= pSdtConfig->smi2))
	{
		SDT_DEBUG_MSG("Warning: smi2 is %u, in the range [1-999]\n", pSdtConfig->smi2);
		result |= SDT_INVALID_PARAMETER;
	}

	/*- udv 合理性检查 */
	if ((SDT_UDV_MIN > pSdtConfig->udv) || (SDT_UDV_MAX < pSdtConfig->udv))
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"udv\" should be in the range %u - %u\n", SDT_UDV_MIN, SDT_UDV_MAX);
		result |= SDT_INVALID_PARAMETER;
	}

	/*- 时间参数 合理性检测 */
	if ((SDT_PERIOD_MIN > pSdtConfig->rxPeriod) || (SDT_PERIOD_MAX < pSdtConfig->rxPeriod))
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"rxPeriod\" should be in the range %u - %u\n", SDT_PERIOD_MIN, SDT_PERIOD_MAX);
		result |= SDT_INVALID_PARAMETER;
	}
	if ((SDT_PERIOD_MIN > pSdtConfig->txPeriod) || (SDT_PERIOD_MAX < pSdtConfig->txPeriod))
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"txPeriod\" should be in the range %u - %u\n", SDT_PERIOD_MIN, SDT_PERIOD_MAX);
		result |= SDT_INVALID_PARAMETER;
	}
	if (pSdtConfig->rxPeriod > pSdtConfig->txPeriod)
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"txPeriod\" should be bigger than \"rxPeriod\", \"txPeriod\": %u , \"rxPeriod\": %u\n", pSdtConfig->txPeriod, pSdtConfig->rxPeriod);
		result |= SDT_INVALID_PARAMETER;
	}

	/* nRxSafe 合理性检查 */
	if ((SDT_N_SAFE_MIN > pSdtConfig->nRxSafe) || (SDT_N_SAFE_MAX < pSdtConfig->nRxSafe))
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"nRxSafe\" should be in the range %u - %u\n", SDT_N_SAFE_MIN, SDT_N_SAFE_MAX);
		result |= SDT_INVALID_PARAMETER;
	}

	/* nGuard 合理性检查*/
	if ((SDT_N_GUARD_MIN > pSdtConfig->nGuard) || (SDT_N_GUARD_MAX < pSdtConfig->nGuard))
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"nGuard\" should be in the range %u - %u\n", SDT_N_GUARD_MIN, SDT_N_GUARD_MAX);
		result |= SDT_INVALID_PARAMETER;
	}

	/* @note 由于没有合适的方案，不进行通道监视阈值合理性检查
	if (SDT_DEFAULT_CM_THR > pSdtConfig->cmThr)
	{
		SDT_DEBUG_MSG("Recommend more than %u, it is not recommended to modify at this time\n", SDT_DEFAULT_CM_THR);
		result |= SDT_INVALID_PARAMETER;
	}*/

	/*- 通道身份非法 */
	if ((STA_SDSINK_CHANNEL != pSdtConfig->channelIdentity) &&
		(STA_SDSRC_CHANNEL_A != pSdtConfig->channelIdentity) &&
		(STA_SDSRC_CHANNEL_B != pSdtConfig->channelIdentity))
	{
		SDT_DEBUG_MSG("unreasonable parameter: \"channelIdentity\" channel identify should be: [STA_SDSINK_CHANNEL|STA_SDSRC_CHANNEL_A|STA_SDSRC_CHANNEL_B]\n");
		result |= SDT_INVALID_PARAMETER;
	}
	
	/*- 通道身份为 发送端通道B */
	if ((STA_SDSRC_CHANNEL_B == pSdtConfig->channelIdentity) && (SDT_INIT_VALUE == pSdtConfig->smi2))
	{
		/*- smi2存在检查 */
		SDT_DEBUG_MSG("unreasonable parameter: \"channelIdentity\" , smi2 does not exist!\n");
		result |= SDT_INVALID_PARAMETER;
	}
	
	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		数据长度四字节对齐
*	@details	根据输入的数据长度返回对齐后的数据长度
*	@param[in]	UINT32	length	原始数据长度
*	@return		UINT32	length	四字节对齐后的数据长度
*	@retval		四字节对齐后的数据长度
*/

UINT32 sdsrc_four_byte_alignment(UINT32 length)
{
	/*- 计算四字节对齐后的数值 */
	
	/*- 返回计算结果 */
	return length + ((4u - (length % 4u)) % 4u);
}

/** 
*	@brief		ShortFromChar
*	@details	用于将2字节数据变为UINT16， BIG endian
*	@param[in]	const UINT8 *		pInput		输入
*	@return		UINT16	变换后的UINT16值
*	@retval		变换后的UINT16值
*/

UINT16 ShortFromChar(const UINT8 *pInput)
{
	/*- 计算short型数据 */
    UINT16 Tempshort;
    Tempshort = ( *(pInput) );
    Tempshort = (UINT16)( Tempshort<<8 ) + pInput[1] ;
    
    /*- 返回计算结果 */
    return Tempshort;
}


/** 
*	@brief		LongFromChar
*	@details	用于将4字节数据变为UINT32， BIG endian
*	@param[in]	const UINT8 *		pInput		输入
*	@return		UINT32	变换后的UINT32值
*	@retval		变换后的UINT32值
*/

UINT32 LongFromChar(const UINT8 *pInput)
{
	/*- 计算long型数据 */
    UINT32 Templong;

    Templong = (UINT32)pInput[0];
    Templong = (UINT32)( Templong<<8 ) + pInput[1];
    Templong = (UINT32)( Templong<<8 ) + pInput[2];
    Templong = (UINT32)( Templong<<8 ) + pInput[3];

    /*- 返回计算结果 */
    return Templong;
}

/** 
*	@brief		ShortToChar
*	@details	将2个字节长的整型变为字节表示  BIG endian
*	@param[in]	UINT16				Input		输入
*	@param[out]	const UINT8 *		pOutput		输出
*/

void ShortToChar ( UINT16 Input, UINT8 *pOutput)
{
	/*- 写输出Buffer */
    pOutput[0] =  (Input>>8) & 0xff;
    pOutput[1] = Input & 0xff;
}

/** 
*	@brief		LongToChar
*	@details	将4个字节长的整型变为字节表示  BIG endian
*	@param[in]	UINT32				Input		输入
*	@param[out]	const UINT8 *		pOutput		输出
*/

void LongToChar(UINT32 Input, UINT8 *pOutput)
{
	/*- 写输出Buffer */
    pOutput[0] =  (UINT8)((Input>>24) & 0xff);
    pOutput[1] =  (UINT8)((Input>>16) & 0xff);
    pOutput[2] =  (UINT8)((Input>>8) & 0xff);
    pOutput[3] = (UINT8)(Input & 0xff);
}

/**
* @brief 用指定值填充内存
* @param destination 待填充的内存区
* @param value 填充值
* @param len 内存区大小
*/
void SDT_Memset(void * destination,SDT_SIZE_T destSize,UINT8 value,SDT_SIZE_T len)
{
	/*- 输出缓冲空间足够 */
	if(destSize >= len)
	{
		/*- 填充 */
		memset(destination, (int)value, len);
	}
	else
	{
		;
	}
}
/**
* @brief 内存复制
* @param destination 目的地址
* @param destSize 目的地址大小
* @param source 源地址
* @param count 复制数据大小
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
* @details 源地址段和目的地址段不能有重叠
*/
INT32 SDT_Memcpy(void* destination,SDT_SIZE_T destSize,const void * source,SDT_SIZE_T count)
{
	/*- 变量初始化 */
	INT32 ret = -1; /* 函数返回值 */

	/*- 输出缓冲空间足够 */
	if(destSize >= count)
	{
		/*- 拷贝 */
		memcpy(destination, source, count);
		/*- 记执行成功 */
		ret = 0;
	}
	else
	{
		/*- 记执行失败 */;
	}

	/*- 返回执行结果 */
	return ret;
}
