/**********************************************************************************************************************/
/**	@file			sdsink.c
 *	@brief			接收端sdsink通道
 *	@author			杨凯
 *	@date			2021-11-06
 *	@version		v1.0.1
 */

/***********************************************************************************************************************
 * INCLUDES
 */

#include "sdsink.h"
#include "sdt_utils.h"
//#define SDT_STATE_REPORT
#ifdef SDT_STATE_REPORT
#include "tsn_stat.h"
#endif

#ifdef CM_API
#define SDT_LOG_MSG(...) MSCP_LOG_MSG(...)
#else
#include "vos_thread.h"
#include "vos_utils.h"
#define	LOG_ERROR VOS_LOG_ERROR
#define SDT_LOG_MSG(level, format, args ...) vos_printLog((level), (format), ## args)
#endif



/*---------------------------------------内部变量定义-------------------------------------------------*/
/**
 * @brief	全局变量，保存当前接收端的全部通道信息
 * @note	channelCapacity绝对不得超过SDSINK_CHANNEL_AMOUNT_MAX
 */
SDSINK	g_sdsink = { .channelCapacity = SDSINK_CHANNEL_AMOUNT_MAX,.channelNum = 0 };	
VOS_MUTEX_T pmyPrintMutex;	/* 解析信息打印锁，多线程解析调试时使用 */

/*---------------------------------------内部函数声明--------------------------------------------------*/

SDT_RC sdsink_config_sdsink_channel(P_SDSINK_CHANNEL pSdsinkChannel, const P_SDT_CONFIG_T pSdsinkConfig);	/* 配置发送端通道 */	

UINT32 sdsink_get_last_VDP_SSC(P_SDSINK_CHANNEL pSdsinkChannel);	/* 获取上一VDP的SSC */ 
UINT32 sdsink_get_current_sid(P_SDSINK_CHANNEL pSdsinkChannel);	/* 获取当前的SID */

SDT_RC sdsink_prepare_parsing(P_SDSINK_CHANNEL pSdsinkChannel, UINT8 * vdpBuffer, UINT32 vpdLength);	/* 解析vdp信息 为校验做准备*/ 
void sdsink_update_parsing_environment(P_SDSINK_CHANNEL pSdsinkChannel);	/* 准备解析环境 为解析做准备 */
void sdsink_reset_vdp_parsing_environment(P_SDSINK_CHANNEL pSdsinkChannel);	/* 重置sdsink 上电/重启后调用该函数, 宿时间监视发生不安全后调用该函数 */
SDT_RC sdsink_get_vdp_info(P_VDP_INFO_T vdpInfoStruct, UINT8 * vdpBuffer, UINT32 vdpTailOffset);	/* 提取VDP信息 */ 

void sdsink_check_vdp_type(P_SDSINK_CHANNEL pSdsinkChannel);				/* 判断VDP类型 */
void sdsink_check_vdp_is_correct(P_SDSINK_CHANNEL pSdsinkChannel);			/* 判断 VDP 是否为正确的VDP */
void sdsink_check_vdp_is_repeated(P_SDSINK_CHANNEL pSdsinkChannel);			/* 判断 VDP 是否为重复的VDP */
void sdsink_check_vdp_is_initial(P_SDSINK_CHANNEL pSdsinkChannel);			/* 判断 VDP 是否为初始的VDP */
void sdsink_check_vdp_is_new(P_SDSINK_CHANNEL pSdsinkChannel);				/* 判断 VDP 是否为新的VDP */
void sdsink_check_vdp_is_valid(P_SDSINK_CHANNEL pSdsinkChannel);			/* 判断 VDP 是否为有效的VDP */

void sdsink_check_communication_safe_state(P_SDSINK_CHANNEL pSdsinkChannel);	/* SDT通信状态判断 */
void sdsink_check_rts_communication_safe(P_SDSINK_CHANNEL pSdsinkChannel);	/* 宿时间监视的通信安全判断 */ 
void sdsink_check_gtc_communication_safe(P_SDSINK_CHANNEL  pSdsinkChannel);	/* 守护时间监视的通信安全判断 */
void sdsink_check_lmc_communication_safe(P_SDSINK_CHANNEL pSdsinkChannel);	/* 延迟时间监视的通信安全判断 */
void sdsink_check_cmc_communication_safe(P_SDSINK_CHANNEL pSdsinkChannel);	/* 通道监视的通信安全判断 */ 

void sdsink_print_test_info(SDT_HANDLE sdsinkHandle);/* 打印一行测试信息*/
#ifdef SDT_STATE_REPORT
void sdsink_make_test_info(SDT_HANDLE sdsinkHandle,UINT8* pVdpData);/* 生成一行测试信息字符串 */
#endif
UINT32 sdsink_update_statistic_info(P_SDSINK_CHANNEL pSdsinkChanne);/* 更新统计信息 */
void sdsink_reset_statistic_info(P_SDSINK_STATISTIC_INFO_T pStatInfo);/* 重置统计信息 */
void sdsink_print_statistic_info(SDT_HANDLE sdsinkHandle);/* 打印当前一个通道的统计信息*/


/*---------------------------------------公共接口实现---------------------------------------------------*/

/** 
*	@brief		创建并配置接收端通道
*
*	@details	创建并配置接收端通道
*	@param[out]	P_SDT_HANDLE				pSdsinkHandle	发送端句柄指针
*	@param[in]	P_SDT_CONFIG_T			pSdsinkConfig	发送端通道配置信息
*	@return		SDT_RC					sdt状态
*	@retval		SDT_OK					创建成功
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_PARAMETER   相关配置参数错误
*				SDSINK_CHANNEL_FULL		当前使用通道到达上限
*				SDT_INVALID_SID			计算SID错误
* 
* 	@retval		SDT_OK					创建成功
*/
SDT_RC sdsink_create_channel(P_SDT_HANDLE pSdsinkHandle, P_SDT_CONFIG_T pSdsinkConfig)
{	
	/*- 初始化相关局部变量 */
	
	SDT_RC		result = SDT_ERROR;
	SDT_HANDLE	tempHandle = g_sdsink.channelNum;	
	P_SDSINK_CHANNEL tempSdsinkChannel = NULL;
	
	/*- 初次调用通道创建函数  */
	if(0u==g_sdsink.channelNum)
	{
		/*- 初始化SDSINK所有通道 */
		SDT_Memset(g_sdsink.channelArray,sizeof(SDSINK_CHANNEL) * SDSINK_CHANNEL_AMOUNT_MAX,0u,sizeof(SDSINK_CHANNEL) * g_sdsink.channelCapacity);
		for(UINT32 i=0;i<g_sdsink.channelCapacity;i++)
			g_sdsink.channelArray[i].m_handleEffective=SDT_UNEFFECTIVE;
	}
	else{}

	
	/*- 检查当前使用通道数量是否已经达到上限 */
	if (tempHandle >= g_sdsink.channelCapacity)
	{
		SDT_DEBUG_MSG("sdsink channel create error, channel array is full!\n");
		result = SDSINK_CHANNEL_FULL;
	}
	else
	{
		result = SDT_OK;
	}


	/*- 传入指针存在性检查 */
	if(SDT_OK==result)
	{
		if ((PT_IS_NULL(pSdsinkHandle)) || (PT_IS_NULL(pSdsinkConfig)))
		{
			SDT_DEBUG_MSG("sdsink channel create failed, [ pSdsinkHandle || pSdsinkConfig ] pointer does not exist!\n");
			result = SDT_PT_NOT_EXIST;
		}
		else{}
	}
	else{}

	/*- 通道数量未达上限且传入指针存在 */
	if(SDT_OK==result)
	{
		/*- 检查配置参数有效性 */
		result = sdt_check_config(pSdsinkConfig);
		if (SDT_OK != result)
		{
			SDT_DEBUG_MSG("sdsink channel create error, configuation parameters are invalide!\n");
		}
		else{}
	}
	else{}

	/*- 配置参数有效 */
	if(SDT_OK==result)
	{
		/*- 获取SDSINK当前通道，激活并配置，成功后返回通道句柄 */
		
		/* 获取SDSINK通道指针 */
		tempSdsinkChannel = &(g_sdsink.channelArray[tempHandle]);
		
		/* 获取通道指针失败 */
		if (PT_IS_NULL(tempSdsinkChannel))
		{
			SDT_DEBUG_MSG("sdsink create failed, can not get channel pointer!\n");
			result = SDT_PT_NOT_EXIST;		
		}
		/* 获取通道指针成功但通道已被占用 */
		else if(SDT_EFFECTIVE==tempSdsinkChannel->m_handleEffective)
		{
			/* 待定：此处应添加一个应对通道激活标志位与当前通道数量不匹配的错误处理函数*/
			SDT_DEBUG_MSG("sdsink create failed, channel counter disturbed!\n");
			result = SDT_OUT_OF_BOUND;
		}
		else
		{
			/* 激活并配置当前SDSINK通道 */
			result = sdsink_config_sdsink_channel(tempSdsinkChannel, pSdsinkConfig);
		}

		/* 通道配置失败 */
		if (SDT_OK != result)
		{
			/* 重初始化当前SDSINK通道 */
			SDT_Memset((void*)tempSdsinkChannel,sizeof(SDSINK_CHANNEL), 0u, sizeof(SDSINK_CHANNEL));
			tempSdsinkChannel->m_handleEffective=SDT_UNEFFECTIVE;
			SDT_DEBUG_MSG("SDSINK : channel %u create failed! \n", tempHandle);
		}
		else
		{
			/* 增加通道数量并返回通道句柄*/
			g_sdsink.channelNum++;			
			*pSdsinkHandle = tempHandle;
			SDT_DEBUG_MSG("SDSINK : channel %u create success! \n", tempHandle);
		}
	}
	else
	{

	}

	/*- 返回通道创建情况 */
	return result;
}

/** 
*	@brief		根据句柄对应的通道, 解析收到的vdp数据
*	@details	根据句柄对应的通道, 解析收到的vdp数据
*	@note		函数支持解析未经四字节对齐的vdp数据，视vdp的末16个字节之前数据为有效用户数据，
*				进行SC校验值计算时对用户数据不足4字节的部分补零处理，返回的vpd数据和vpd长度为未经四字节对齐的原始用户数据部分
*				修改：支持空调用，即vdpBuffer为NULL;
*	@param[in]	SDT_HANDLE		pSdsinkHandle			接收端句柄
*	@param[in]	UINT8*			vdpBuffer				vdp数据
*	@param[in]	UINT32			vdpLength				vdp的长度
*	@param[out]	UINT8*			vpdBuffer				解析后的vpd数据
*	@param[out]	UINT8*			pVpdLength				解析后数据的长度
*	@return		SDT_RC					sdt状态
*	@retval		SDT_OK					解析成功
*				SDT_INVALID_HANDLE		无效句柄
*				SDT_PT_NOT_EXIST		指针不存在
*				SDT_INVALID_VPD_LEN		VPD长度不合理
*				SDT_INVALID_SC			计算SC错误
*				SDT_OUT_OF_BOUND		提取信息过程中访问越界
*/

SDT_RC sdsink_parse_VDP(SDT_HANDLE sdsinkHandle, UINT8 * vdpBuffer, UINT32 vdpLength, UINT8 * vpdBuffer, UINT32* pVpdLength)
{
	/*- 入参有效性检查 */
	
	SDT_RC	result = SDT_OK;	
	UINT32	userDataLength = 0;
	P_SDSINK_CHANNEL tempSdsinkChannel = NULL;
#ifdef SDT_STATE_REPORT
	SDT_SAFE_STATE safeStateBeforeParse=SDT_SAFE;
#endif


	/* 判断传入的接收端句柄是否有效 */ 
	if (SDSINK_HANDLE_IS_INVALIDE(sdsinkHandle))
	{
		SDT_DEBUG_MSG("invalid sdsink handle!\n");
		result = SDT_INVALID_HANDLE;
	}

	/* 句柄有效 */
	if(SDT_OK == result)
	{
		/* 获取相应的SDSINK通道指针 */
		tempSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);
		
		/* 检测传入的指针是否存在 */
		if (PT_IS_NULL(vpdBuffer) || PT_IS_NULL(pVpdLength))
		{
			SDT_DEBUG_MSG("[vpdBuffer || pVpdLength] pointer does not exist!\n");
			result = SDT_PT_NOT_EXIST;
		}
	}
#ifdef SDT_STATE_REPORT
	safeStateBeforeParse=tempSdsinkChannel->m_safeComFlag;
#endif
	/* 指针存在 */
	if(SDT_OK == result)
	{
		/* 判断传入的vdp长度是否合理 */
		if (((SDT_VPDMIN + SDT_TAIL_SIZE) > vdpLength) || ((SDT_VPDMAX + SDT_TAIL_SIZE) < vdpLength) || ((vdpLength - SDT_TAIL_SIZE) > *pVpdLength))
		{
			SDT_DEBUG_MSG("vdp length invalide! should be [16-1000], current is %u\n", vdpLength);
			result = SDT_INVALID_VPD_LEN; 
		}
	}


	/*- 输入参数均合理 */ /* vdp长度合理 */
	if(SDT_OK == result)
	{
		/*- 在通道中准备分析所需的变量环境 */ 
		userDataLength = vdpLength - SDT_TAIL_SIZE;
		result = sdsink_prepare_parsing(tempSdsinkChannel, vdpBuffer, userDataLength);
	}

	

	/*- 变量环境准备成功 */
	if(SDT_OK == result)
	{
		/*- VDP类型判断		*/ 
		sdsink_check_vdp_type(tempSdsinkChannel);

		/*- SDT通信状态判断	*/ 
		sdsink_check_communication_safe_state(tempSdsinkChannel);
		
		/*- 更新统计信息    */
		UINT32 telpackcounter=sdsink_update_statistic_info(tempSdsinkChannel);

		if (5000u==telpackcounter)
		{
			sdsink_print_statistic_info(sdsinkHandle);
			sdsink_reset_statistic_info(&(tempSdsinkChannel->m_statisticInfo));
		}
		
		/*- 通道安全且数据有效 */ 
		if ((SDT_SAFE == tempSdsinkChannel->m_safeComFlag) && (SDT_TRUE == tempSdsinkChannel->m_validVDPFlag))
		{
			/*- 将接收到的VPD信息返回 */ 
			SDT_Memcpy((void*)vpdBuffer,*pVpdLength, (const void*)vdpBuffer, (SDT_SIZE_T)userDataLength);
			*pVpdLength = userDataLength;
		}
		else
		{
			/*- 返回空数据包 */
			SDT_Memset((void*)vpdBuffer,*pVpdLength, SDT_INVALID_VALUE, userDataLength);
			*pVpdLength = SDT_INVALID_VALUE;
			result = SDT_INVALID_VPD;
		}
	}
	else
	{
		*pVpdLength = SDT_INVALID_VALUE;
	}
	
   // struct timespec tpStartTick;
   // struct timespec tpEndTick;
    //clock_gettime(CLOCK_REALTIME, &tpStartTick);
	//if(16==sdsinkHandle)
	//sdsink_print_test_info(sdsinkHandle);
   // clock_gettime(CLOCK_REALTIME, &tpEndTick);
   // long long costTimeMs = (tpEndTick.tv_sec-tpStartTick.tv_sec)*1000 + (tpEndTick.tv_nsec-tpStartTick.tv_nsec)/1000000;
   // printf("[%d]print cost time %lld ms \n",sdsinkHandle,costTimeMs);
#ifdef SDT_STATE_REPORT
	/*- 输出解析情况 */
	if(10301<=tempSdsinkChannel->m_channelASMI && 10304>=tempSdsinkChannel->m_channelASMI)
	{
		sdsink_make_test_info(sdsinkHandle,vdpBuffer);
	}
//	if(SDT_SAFE!=safeStateBeforeParse || SDT_SAFE!=tempSdsinkChannel->m_safeComFlag)
//	{
//
//	}
#endif
	/*- 返回解析情况 */
	return result;
}


/*------------------------------------- 内部函数实现--------------------------------------------------*/

/** 
*	@brief		激活并配置当前SDSINK通道
*	@details	激活并配置当前SDSINK通道
*	@param[out]	P_SDSINK_CHANNEL		pSdsinkChannel	发送端句柄指针
*	@param[in]	P_SDT_CONFIG_T			pSdsinkConfig	发送端通道配置信息
*	@return		SDT_RC					sdt状态
*	@retval		SDT_OK					配置成功
*				SDT_INVALID_SID			计算SID错误
*/

SDT_RC sdsink_config_sdsink_channel(P_SDSINK_CHANNEL pSdsinkChannel, const P_SDT_CONFIG_T pSdsinkConfig)
{
	/*-	将通道状态标志位 置为激活	*/
	SDT_RC result = SDT_OK;

	pSdsinkChannel->m_handleEffective = SDT_EFFECTIVE;

	/*- 设置通道配置变量 */
	
	/* 时间参数设置 */
	pSdsinkChannel->m_txPeriod	= pSdsinkConfig->txPeriod;
	pSdsinkChannel->m_rxPeriod	= pSdsinkConfig->rxPeriod;
	pSdsinkChannel->m_rxSafe	= pSdsinkConfig->txPeriod * pSdsinkConfig->nRxSafe;
	pSdsinkChannel->m_tGuard	= pSdsinkConfig->txPeriod * pSdsinkConfig->nGuard;



	/* 预期用户数据版本 */
	pSdsinkChannel->m_udv = pSdsinkConfig->udv;

	/* smi设置 */
	pSdsinkChannel->m_channelASMI = pSdsinkConfig->smi1;
	pSdsinkChannel->m_channelBSMI = pSdsinkConfig->smi2;
	if (SDT_DEFAULT_CHANNEL_B_SMI == pSdsinkChannel->m_channelBSMI)
	{
		pSdsinkChannel->m_redundancyOpenFlag = SDT_FALSE;
	}
	else
	{
		pSdsinkChannel->m_redundancyOpenFlag = SDT_TRUE;
	}

	/* 计算期望接收SSC的窗口大小, 向上取整 */
	pSdsinkChannel->m_recvWindowSize = (UINT32)((pSdsinkChannel->m_rxSafe + pSdsinkChannel->m_txPeriod - 1u) / pSdsinkChannel->m_txPeriod);

	/* 通道监视参数设置 */
	pSdsinkChannel->m_K4 = pSdsinkConfig->K4;
	pSdsinkChannel->m_K3 = (UINT32)((REAL32)pSdsinkChannel->m_K4 * SDT_CMC_CHECKRATE + 1u );

	/* 计算通道监视 VDP每小时发送频率 */
	pSdsinkChannel->m_sendFrequencyPerHour	= (UINT32)(3600000u / pSdsinkChannel->m_txPeriod);			/* 3600000ms/tx_period */
	pSdsinkChannel->m_wrongIncreaseStep		= (UINT32)(pSdsinkChannel->m_sendFrequencyPerHour / pSdsinkChannel->m_K4);
	pSdsinkChannel->m_wrongCounterUpper		= (UINT32)((pSdsinkChannel->m_sendFrequencyPerHour / pSdsinkChannel->m_K4) * pSdsinkChannel->m_K3);
	pSdsinkChannel->m_cmThr					= (UINT32)(UINT32)((pSdsinkChannel->m_sendFrequencyPerHour / pSdsinkChannel->m_K4) * (pSdsinkChannel->m_K3 - pSdsinkChannel->m_K4));
	pSdsinkChannel->m_currentProcessStartTime = 0;

	/*- 重置vdp解析环境 */
	sdsink_reset_vdp_parsing_environment(pSdsinkChannel);
	pSdsinkChannel->m_wrongVDPCounter	= SDT_INIT_VALUE;//@note 发生宿时间监视不安全时不重置用于通道安全监视的错误vdp计数器
	pSdsinkChannel->m_lastValidVDPSSC	= SDT_INIT_VALUE;//@note 发生宿时间监视不安全时不重置用于判断重复接收的上一有效SSC
	pSdsinkChannel->m_lastSdsrcIdentity = STA_SDSRC_CHANNEL_A; //@note 发生宿时间监视不安全时不重置用于判断换端的上一源身份

	/*- 重置统计信息 */
	sdsink_reset_statistic_info(&(pSdsinkChannel->m_statisticInfo));
	
	/*- 计算SID */
	
	/* 设置SID结构体参数	*/
	SDT_Memset((void*)&(pSdsinkChannel->m_SIDInfo),sizeof(SID_INFO_T), 0u, sizeof(SID_INFO_T));
	pSdsinkChannel->m_SIDInfo.SDTProtVers = SDT_VERSION;

	/* 设置了冗余发送端 */
	if (SDT_TRUE == pSdsinkChannel->m_redundancyOpenFlag)
	{
		/* 计算AB两个SID */
		pSdsinkChannel->m_channelASID = sdt_calculate_sid(&(pSdsinkChannel->m_SIDInfo), pSdsinkChannel->m_channelASMI);
		pSdsinkChannel->m_channelBSID = sdt_calculate_sid(&(pSdsinkChannel->m_SIDInfo), pSdsinkChannel->m_channelBSMI);
		
		if ((0u == pSdsinkChannel->m_channelASID) || (0u == pSdsinkChannel->m_channelBSID))
		{
			result = SDT_INVALID_SID;
		}
	}
	/* 未设置冗余发送端 */
	else
	{
		/* 只计算ASID */
		pSdsinkChannel->m_channelASID = sdt_calculate_sid(&(pSdsinkChannel->m_SIDInfo), pSdsinkChannel->m_channelASMI);
		pSdsinkChannel->m_channelBSID = SDT_INVALID_VALUE;
		
		if (0u == pSdsinkChannel->m_channelASID)
		{
			result = SDT_INVALID_SID;
		}
	}

	/*- 返回通道配置情况 */
	return result;
}

/** 
*	@brief		准备分析
*	@details	准备分析环境, 更新分析环境的相关变量, 提取VDP中的相关信息
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel			接收端通道指针
*	@param[in]	UINT8*				vdpBuffer				VDP数据缓存
*	@param[in]	UINT32				userDataLenth			用户数据长度
*	@return		SDT_RC				sdt状态
*	@retval		SDT_OK				解析准备成功
*				SDT_PT_NOT_EXIST	指针不存在
*				SDT_OUT_OF_BOUND	提取信息过程中访问越界
*				SDT_INVALID_SC		计算SC错误
*/

SDT_RC sdsink_prepare_parsing(P_SDSINK_CHANNEL pSdsinkChannel, UINT8 * vdpBuffer, UINT32 userDataLenth)
{
	/*- 准备解析环境，包括更新和重置通道中的周期动态变量等	*/
	
	SDT_RC result = SDT_OK;

	sdsink_update_parsing_environment(pSdsinkChannel);

	if(PT_IS_NULL(vdpBuffer))
	{
		/*- 沿用上次的解析结果 */
	}
	else
	{
		/*- 提取vdp中的信息到通道 */
		result = sdsink_get_vdp_info(&(pSdsinkChannel->m_VDPInfo), vdpBuffer, userDataLenth);

		/*- 提取VDP信息成功 */
		if(SDT_OK==result)
		{
			/*- 根据vdp计算SC校验值 */
			
			/* 配置了冗余发送端 */
			if (SDT_TRUE == pSdsinkChannel->m_redundancyOpenFlag)
			{
				/* 计算 通道A、B的SC */
				pSdsinkChannel->m_channelASC = sdt_calculate_sc(vdpBuffer, userDataLenth, pSdsinkChannel->m_channelASID);

				pSdsinkChannel->m_channelBSC = sdt_calculate_sc(vdpBuffer, userDataLenth, pSdsinkChannel->m_channelBSID);

				if ((0u == pSdsinkChannel->m_channelASC) || (0u == pSdsinkChannel->m_channelBSC))
				{
					SDT_DEBUG_MSG("channel A or B : safety code calculate failed!\n");
					result = SDT_INVALID_SC;
				}
			}
			else
			/* 未配置冗余发送端 */
			{
				/* 只计算 通道A的SC */
				pSdsinkChannel->m_channelASC = sdt_calculate_sc(vdpBuffer, userDataLenth, pSdsinkChannel->m_channelASID);
				pSdsinkChannel->m_channelBSC = SDT_INVALID_VALUE;

				if (0u == pSdsinkChannel->m_channelASC)
				{
					SDT_DEBUG_MSG("channel A or B : safety code calculate failed!\n");
					result = SDT_INVALID_SC;
				}
			}
		}
		/*- 提取VDP信息未成功 */
		else
		{
			/*- 告警 */
			SDT_DEBUG_MSG("get vdp information failed!\n");
		}
	}

	/*- 返回准备情况 */
	return result;
}

/** 
*	@brief		更新解析环境
*	@details	准备解析环境，包括更新和重置通道中的周期动态变量等
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel			接收端通道指针
*/

void sdsink_update_parsing_environment(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 获取当前时间作为本次解析vdp的时间 */
	pSdsinkChannel->m_currentProcessStartTime = sdt_get_current_time();

	/* 此处不检查宿时间监视和上电标志位进行通道重置，因为如果发生了宿时间超时，则当时已经重置了通道；
	 如果是上电后首次解析，那么在初始化流程中已经进行了通道重置 */

	/* 保存解析结果变量到动态记录变量后重置解析变量 */
	
	/*- 上一VDP是初始VDP */
	if (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag)
	{
		/*- 保存相关变量 */
															
		/* 保存 初始SID */
		if (STA_SDSRC_CHANNEL_A == pSdsinkChannel->m_sdsrcIdentity)
		{
			pSdsinkChannel->m_initialSID = pSdsinkChannel->m_channelASID;
		}
		else if (STA_SDSRC_CHANNEL_B == pSdsinkChannel->m_sdsrcIdentity)
		{
			pSdsinkChannel->m_initialSID = pSdsinkChannel->m_channelBSID;
		}
		
		pSdsinkChannel->m_lastSdsrcIdentity = pSdsinkChannel->m_sdsrcIdentity;	/* 保存上一VDP接收端通道识别身份	*/
		
		pSdsinkChannel->m_initialVDPSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;	/* 保存初始SSC */
		
		pSdsinkChannel->m_lastValidVDPSSC = SDT_INIT_VALUE;
		
	}
	/*- 上一VDP是有效VDP */
	else if(SDT_TRUE == pSdsinkChannel->m_validVDPFlag)
	{
		/*- 保存相关变量 */
		
		pSdsinkChannel->m_lastVDPIsNewFlag	= pSdsinkChannel->m_newVDPFlag;				/* 保存上一VDP的新VDP的标志位 */
		pSdsinkChannel->m_lastSdsrcIdentity = pSdsinkChannel->m_sdsrcIdentity;			/* 保存上一VDP接收端通道识别身份 */
		pSdsinkChannel->m_lastValidVDPSSC	= pSdsinkChannel->m_VDPInfo.m_SafeSequCount;/* 保存上一VDP的SSC */
	}

	/*- 重置解析相关变量 */
	
	/* 此时不更新接收端通道识别身份，这使得接下来如果收到错误VDP，通道身份将保持在最近的正确vdp的通道身份 */
	SDT_Memset((void*)&(pSdsinkChannel->m_VDPInfo),sizeof(VDP_INFO_T), 0u, sizeof(VDP_INFO_T));

	pSdsinkChannel->m_lastChannelASC = pSdsinkChannel->m_channelASC;
	pSdsinkChannel->m_channelASC = SDT_INIT_VALUE;
	pSdsinkChannel->m_channelBSC = SDT_INIT_VALUE;
}

/** 
*	@brief		提取VDP信息
*	@details	提取VDP尾中的信息到通道
*	@param[out]	VDP_INFO_T*			vdpInfoStruct		vdp信息结构体
*	@param[in]	UINT8*				vdpBuffer			VDP信息缓存
*	@param[in]	UINT32				vdpTailOffset		VDP尾偏移
*	
*	@return		SDT_RC				SDT状态
*	
*	@retval		SDT_OK				提取VDP信息成功
*				SDT_PT_NOT_EXIST	指针不存在
*				SDT_OUT_OF_BOUND	提取信息过程中访问越界
*/

SDT_RC sdsink_get_vdp_info(P_VDP_INFO_T vdpInfoStruct, UINT8* vdpBuffer, UINT32 vdpTailOffset)
{
	/*- 初始化相关局部变量 */
	SDT_RC result = SDT_OK;
	UINT32 pos = 0u;
	
	/*- 检查出入参指针有效性 */
	if (PT_IS_NULL(vdpInfoStruct) || PT_IS_NULL(vdpBuffer))
	{
		SDT_DEBUG_MSG("[vdpInfoStruct || vdpBuffer ]pointer does not exist!\n");
		result = SDT_PT_NOT_EXIST;
	}

	/*- 指针有效 */
	if(SDT_OK == result)
	{
		/*- 拷贝vdpBuffer尾部数据至通道结构体并计算SC */

		pos = vdpTailOffset;

		/* 提取reserved01 */
		vdpInfoStruct->m_reserved01 = LongFromChar(&vdpBuffer[pos]);
		pos += sizeof(vdpInfoStruct->m_reserved01);

		/* 提取reserved02 */
		vdpInfoStruct->m_reserved02 = ShortFromChar(&vdpBuffer[pos]);
		pos += sizeof(vdpInfoStruct->m_reserved02);

		/* 提取UDV*/
		vdpInfoStruct->m_UserDataVersion = ShortFromChar(&vdpBuffer[pos]);
		pos += sizeof(vdpInfoStruct->m_UserDataVersion);

		/* 提取SSC*/
		vdpInfoStruct->m_SafeSequCount = LongFromChar(&vdpBuffer[pos]);
		pos += sizeof(vdpInfoStruct->m_SafeSequCount);

		/* 提取SC*/
		vdpInfoStruct->m_SafetyCode = LongFromChar(&vdpBuffer[pos]);
		pos += sizeof(vdpInfoStruct->m_SafetyCode);

		/* 越界检测 */
		if (pos != vdpTailOffset + SDT_TAIL_SIZE)
		{
			SDT_DEBUG_MSG("access out of bound!\n");
			result = SDT_OUT_OF_BOUND;
		}
	}
	
	/*- 返回函数执行情况 */
	return result;
}

/** 
*	@brief		判断接收到的VDP的类型
*	@details	判断接收到的VDP的类型
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_vdp_type(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 判断 VDP是否为正确VDP */
	sdsink_check_vdp_is_correct(pSdsinkChannel);

	/*- 判断 VDP是否为重复VDP */
	sdsink_check_vdp_is_repeated(pSdsinkChannel);

	/*- 判断 VDP是否为初始VDP */
	sdsink_check_vdp_is_initial(pSdsinkChannel);

	/*- 判断 VDP是否为新VDP   */
	sdsink_check_vdp_is_new(pSdsinkChannel);

	/*- 判断 VDP是否为有效VDP */
	sdsink_check_vdp_is_valid(pSdsinkChannel);

}

/** 
*	@brief		判断VDP是否为正确VDP
*	@details	比较SC接收值与校验值，判断VDP正确性和发送端身份，置通道正确标志位和通道身份标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/
void sdsink_check_vdp_is_correct(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 接收到的SC和通道A计算得到的SC相等 */
	if ((pSdsinkChannel->m_channelASC == pSdsinkChannel->m_VDPInfo.m_SafetyCode) &&
		(pSdsinkChannel->m_udv == pSdsinkChannel->m_VDPInfo.m_UserDataVersion))
	{
		/*- 置通道正确标志位 */ 
		pSdsinkChannel->m_correctVDPFlag = SDT_TRUE;

		/*- 置通道身份标志位为A */ 
		pSdsinkChannel->m_sdsrcIdentity = STA_SDSRC_CHANNEL_A;
	}
	
	/*- 接收到的SC和通道B计算得到的SC相等 */
	else if ((pSdsinkChannel->m_channelBSC == pSdsinkChannel->m_VDPInfo.m_SafetyCode) &&
			 (pSdsinkChannel->m_udv == pSdsinkChannel->m_VDPInfo.m_UserDataVersion))
	{
		/*- 置通道正确标志位 */
		pSdsinkChannel->m_correctVDPFlag = SDT_TRUE;
		
		/*- 置通道身份标志位为B */
		pSdsinkChannel->m_sdsrcIdentity = STA_SDSRC_CHANNEL_B;

	}
	/* 接收到的VDP是错误的VDP */
	else
	{
		/*- 置否通道正确VDP标志位 */
		pSdsinkChannel->m_correctVDPFlag = SDT_FALSE;
		
	}
}

/** 
*	@brief		判断VDP是否为重复VDP
*	@details	通过比较 SSC是否和上一有效或初始VDP的SSC相同来判断VDP是否重复，置通道重复vdp标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_vdp_is_repeated(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 初始化判断所需变量 */
	UINT32 tempSSC = 0;
	
	/*- 当前VDP为正确的VDP */ 
	if (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag)
	{
		/* 当前vdp发生源身份切换 */
		if(pSdsinkChannel->m_sdsrcIdentity!=pSdsinkChannel->m_lastSdsrcIdentity)
		{
			/*- 置否通道重复VDP标志位 */
			pSdsinkChannel->m_repeatedVDPFlag = SDT_FALSE;
		}
		else
		{
			/* 获取上一有效VDPSSC或初始SSC */
			tempSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel);

			/*-  SSC是和上一有效或初始VDP的SSC相同 */
			if (tempSSC== pSdsinkChannel->m_VDPInfo.m_SafeSequCount)
			{
				/*- 置通道重复VDP标志位 */
				pSdsinkChannel->m_repeatedVDPFlag = SDT_TRUE;
			}
			else
			{
				/*- 置否通道重复VDP标志位 */
				pSdsinkChannel->m_repeatedVDPFlag = SDT_FALSE;
			}
		}
	}
	/* 当前接收到的VDP不是正确的VDP */
	else
	{
		/*- 上一VDP SCA等于当前VDP SCA */
		if(pSdsinkChannel->m_lastChannelASC==pSdsinkChannel->m_channelASC)
		{
			/* 当前VDP是重复的VDP */
			pSdsinkChannel->m_repeatedVDPFlag = SDT_TRUE;
		}
		else
		{
			/* 当前VDP不是重复的VDP*/
			pSdsinkChannel->m_repeatedVDPFlag = SDT_FALSE;
		}
	}

	return;
}

/** 
*	@brief		判断VDP是否为初始VDP
*	@details	判断后置通道初始vdp标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_vdp_is_initial(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 初始化判断所需变量 */
	UINT8  flag = 0;

	/*- 当前VDP是非重复且正确的 */ 
	if ((SDT_FALSE == pSdsinkChannel->m_repeatedVDPFlag) && (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag))
	{
		/*-	当前VDP是上电/重置后的第一个VDP */ 
		if (SDT_TRUE == pSdsinkChannel->m_powerOnResetFlag)
		{
			/*- 置通道初始VDP标志位 */ 
			pSdsinkChannel->m_initialVDPFlag = SDT_TRUE;

			/*- 将上电重置标志位置为否 */
			pSdsinkChannel->m_powerOnResetFlag = SDT_FALSE;
			
			flag = 1u;
		}
	
		/*- 当前VDP是宿时间丢失后收到的第一个VDP */ 
		if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
		{
			/*- 置通道初始VDP标志位 */
			pSdsinkChannel->m_initialVDPFlag = SDT_TRUE;
			
			flag = 1u;

		}
	
		/*-发生发送端通道A/通道B切换 */
		if (pSdsinkChannel->m_lastSdsrcIdentity != pSdsinkChannel->m_sdsrcIdentity)
		{
			/*- 置通道初始VDP标志位 */
			pSdsinkChannel->m_initialVDPFlag = SDT_TRUE;

			flag = 1u;
		}

		/*- 不符合上述三种中的任何情况 */
		if (0u == flag)
		{
			/*- 置否通道初始VDP标志位 */
			pSdsinkChannel->m_initialVDPFlag = SDT_FALSE;
		}
	}
	/* 当前VDP是重复或不正确的 */ 
	else
	{
		/*- 置否通道初始VDP标志位 */
		pSdsinkChannel->m_initialVDPFlag = SDT_FALSE;
	}
}

/** 
*	@brief		判断vdp是否为新
*	@details	判断后置通道新vdp标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_vdp_is_new(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 当前VDP是是正确VDP且不是初始VDP */ 
	if ((SDT_TRUE == pSdsinkChannel->m_correctVDPFlag) && (SDT_FALSE == pSdsinkChannel->m_initialVDPFlag))
	{
		/*- 获取当前的SID和上一VDP的SSC */
		
		UINT32 tempSID = 0;
		UINT32 lastVDPSSC = 0;

		tempSID = sdsink_get_current_sid(pSdsinkChannel);
		lastVDPSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel);

		/* 待定：该情况在程序逻辑完善时不会出现 */
		if (0u == tempSID)
		{
			SDT_DEBUG_MSG("SID equal 0, maybe something is error, please check configuation!\n");
		}

		/*-  当前SID与初始SID相同, 且SSC落在新VDP SSC窗口内*/
		if ((pSdsinkChannel->m_initialSID == tempSID) 
			&&(((lastVDPSSC + 1u) <= pSdsinkChannel->m_VDPInfo.m_SafeSequCount)
				&& ((lastVDPSSC + pSdsinkChannel->m_recvWindowSize) >= pSdsinkChannel->m_VDPInfo.m_SafeSequCount)))
		{
			/*- 置通道新VDP标志位 */
			pSdsinkChannel->m_newVDPFlag = SDT_TRUE;
		}
		else
		{
			/*- 置否通道新VDP标志位 */
			pSdsinkChannel->m_newVDPFlag = SDT_FALSE;
		}
	}
	/* 当前VDP是错误VDP或初始VDP */
	else
	{
		/*- 置否通道新VDP标志位 */
		pSdsinkChannel->m_newVDPFlag = SDT_FALSE;
	}
}

/** 
*	@brief		判断VDP是否为有效VDP
*	@details	判断VDP是否为有效VDP
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_vdp_is_valid(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/* 当前VDP是新VDP		*/ 
	if (SDT_TRUE == pSdsinkChannel->m_newVDPFlag)
	{
		/* 置通道有效VDP标志位 */
		pSdsinkChannel->m_validVDPFlag = SDT_TRUE;
	}
	/* 当前VDP是新VDP的重复VDP，既：当前VDP正确且非初始, 且SSC和上一有效VDP相同*/ 
	else if ((SDT_TRUE == pSdsinkChannel->m_correctVDPFlag) &&
			 (SDT_FALSE == pSdsinkChannel->m_initialVDPFlag) &&
			 (pSdsinkChannel->m_lastValidVDPSSC == pSdsinkChannel->m_VDPInfo.m_SafeSequCount))
	{
		/* 置通道有效VDP标志位 */
		pSdsinkChannel->m_validVDPFlag = SDT_TRUE;
	}
	else
	{
		/* 置否通道有效VDP标志位 */
		pSdsinkChannel->m_validVDPFlag = SDT_FALSE;
	}

	/*- 返回 */
	return;
}

/** 
*	@brief		获取当前新VDP窗口的基准SSC
*	@details	返回上一有效VDP的SSC，如果上一有效VDP不存在（初始vdp之后还未接收到有效vdp）则返回初始VDP的SSC
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*	@return		UINT32	当前作为新VDP窗口基准的SSC
*	@retval		非0		有效的新VDP窗口基准SSC
*				0		无效
*/
UINT32 sdsink_get_last_VDP_SSC(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 返回值c初始化 */
	UINT32 tempSSC = 0;
	
	/*- 上一有效VDP SSC还未被赋值过 */
	if(SDT_INIT_VALUE == pSdsinkChannel->m_lastValidVDPSSC)
	{
		/*- tempSSC赋初始SSC */
		tempSSC = pSdsinkChannel->m_initialVDPSSC;	
	}		
	else	
	{
		/*- tempSSC赋上一有效VDP SSC */
		tempSSC = pSdsinkChannel->m_lastValidVDPSSC;
	}	
	
	/*- 返回SSC */
	return tempSSC;
}

/** 
*	@brief		判断当前接收端通信安全状态
*	@details	判断当前接收端通信安全状态
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_communication_safe_state(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 宿时间监视的安全状态判断 */ 
	sdsink_check_rts_communication_safe(pSdsinkChannel);

	/*- 宿时间监视不安全 */
	if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
	{
		/*- 立即重置通道 */
		sdsink_reset_vdp_parsing_environment(pSdsinkChannel);
	}
	else
	{
		/*-	守护时间监视的通信安全判断	*/
		sdsink_check_gtc_communication_safe(pSdsinkChannel);
		/*-	延迟时间监视的通信安全判断	*/
		sdsink_check_lmc_communication_safe(pSdsinkChannel);
		/*-	通道监视的通信安全判断		*/
		sdsink_check_cmc_communication_safe(pSdsinkChannel);
	}

	/*- 三项安全监视状态全部安全 */	/* 条件中包含了宿时间监视也需安全的要求，因为如果宿时间超时则重置通道，使三项安全监视状态全部置非安全 */ 
	if ((SDT_SAFE == pSdsinkChannel->m_gtcSafeCom) &&
		(SDT_SAFE == pSdsinkChannel->m_lmcSafeCom) &&
		(SDT_SAFE == pSdsinkChannel->m_cmcSafeCom))
	{
		/*- 置通道安全标志位为安全 */
		pSdsinkChannel->m_safeComFlag = SDT_SAFE;
	}
	else
	{
		/*- 置通道安全标志位为不安全 */
		pSdsinkChannel->m_safeComFlag = SDT_UNSAFE;
	}
}

/** 
*	@brief		宿时间监视的通信安全判断
*	@details	判断宿时间监视的安全状态并置rts标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/
void sdsink_check_rts_communication_safe(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 当前VDP为初始VDP时 */
	if (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag)
	{
		/*- 直接更新宿时间监视计时器 */ //这将使宿时间超时检查一定通过
		pSdsinkChannel->m_rtsTimer = pSdsinkChannel->m_currentProcessStartTime;
	}

	/* 宿时间超时检查 */
	
	/*- 未发生超时 */
	if ( (0 != pSdsinkChannel->m_rtsTimer) &&
		 (0 != pSdsinkChannel->m_currentProcessStartTime)&&
		 ((SDT_CLOCK)pSdsinkChannel->m_rxSafe >= (pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_rtsTimer)))
	{
		/*- 将宿时间监视的通道安全状态置为安全 */
		pSdsinkChannel->m_rtsSafeCom = SDT_SAFE;
	}
	else
	{
		/*- 将宿时间监视的通道安全状态置为不安全 */
		pSdsinkChannel->m_rtsSafeCom = SDT_UNSAFE;
	}
	
	/*- 当前VDP为新VDP */
	if(SDT_TRUE == pSdsinkChannel->m_newVDPFlag)
	{
		/*- 更新宿时间监视计时器 */ 
		pSdsinkChannel->m_rtsTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
		
#if 0
	printf("/-*******************宿时间监控测试信息*******************-/\n");
	printf("初始VDP: %s\n", SDT_TRUE == pSdsinkChannel->m_initialVDPFlag?"是":"否");
	printf("新  VDP: %s\n", SDT_TRUE == pSdsinkChannel->m_newVDPFlag ? "是" : "否");
	printf("rts计时器: %u\n", pSdsinkChannel->m_rtsTimer);
	printf("当前进程计时器: %u\n", pSdsinkChannel->m_currentProcessStartTime);
	printf("宿时间监控状态: %s\n", SDT_SAFE == pSdsinkChannel->m_rtsSafeCom ? "安全" : "不安全");
	printf("/-*******************宿时间监控测试信息*******************-/\n");
#endif
}

/** 
*	@brief		守护时间监视的通信安全判断
*	@details	判断守护时间监视的安全状态并置gtc标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_gtc_communication_safe(P_SDSINK_CHANNEL  pSdsinkChannel)
{
	/*- 获取当前VDP的SID */
	UINT32 tempSID = sdsink_get_current_sid(pSdsinkChannel);		 
	
	if (0u == tempSID)
	{
		SDT_DEBUG_MSG("SID equal 0, maybe something is error, please check configuation!\n");
	}

	/*- 上电或宿时间超时重置后还未接收到初始vdp */
	if (SDT_INIT_VALUE == pSdsinkChannel->m_initialSID)
	{
		/*- 将守护时间监视的通道安全状态置为不安全并更新守护时间计时器 */
		pSdsinkChannel->m_gtcSafeCom = SDT_UNSAFE;
		pSdsinkChannel->m_gtcTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
	/*- 当前包发生了通道切换 */
	else if ((SDT_INIT_VALUE != pSdsinkChannel->m_initialSID) &&
		(tempSID != pSdsinkChannel->m_initialSID))
	{
		/*- 守护时间计时器已关闭 */
		if (0 == pSdsinkChannel->m_gtcTimer)
		{
			/*- 将守护时间监视的通道安全状态置为安全 */
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
		}
		else
		{
			/*- 将守护时间监视的通道安全状态置为不安全 */
			pSdsinkChannel->m_gtcSafeCom = SDT_UNSAFE;
		}
		/*- 更新守护时间监视计时器 */
		pSdsinkChannel->m_gtcTimer = pSdsinkChannel->m_currentProcessStartTime;
	}
	/*- 没有发生通道切换 */
	else if ((SDT_INIT_VALUE != pSdsinkChannel->m_initialSID) &&
		(tempSID == pSdsinkChannel->m_initialSID))
	{
		/*- 守护时间重置标志打开 */
		if (SDT_TRUE == pSdsinkChannel->m_gtcResetFlag)
		{
			/*- 将守护时间监视的通道安全状态置为安全并关闭守护时间重置标志  */ 
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
			pSdsinkChannel->m_gtcResetFlag = SDT_FALSE;
		}
		/*- 守护时间计时器已关闭 */
		if (0 == pSdsinkChannel->m_gtcTimer)
		{
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
		}
		/*- 满足守护时间结束条件 */
		else if ((SDT_CLOCK)pSdsinkChannel->m_tGuard < (pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_gtcTimer))
		{
			/*- 将守护时间监视的通道安全状态置为安全并关闭守护时间计时器  */ 
			pSdsinkChannel->m_gtcTimer = 0;
			pSdsinkChannel->m_gtcSafeCom = SDT_SAFE;
		}

	}
	
	/*printf("/-*******************守护时间监控测试信息*******************-/\n");
	printf("当前接收端通道: %s\n", STA_SDSRC_CHANNEL_A == pSdsinkChannel->m_sdsrcIdentity ? "A" : "B");
	printf("接收端A通道SC: %x\n", pSdsinkChannel->m_channelASC);
	printf("接收端B通道SC: %x\n", pSdsinkChannel->m_channelBSC);
	printf("接收到VDP的SC: %x\n", pSdsinkChannel->m_VDPInfo.m_SafetyCode);
	printf("计时器时间: %u\n", pSdsinkChannel->m_gtcTimer);
	printf("计时器过期剩余时间: %d\n", (SDT_CLOCK)pSdsinkChannel->m_tGuard - (pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_gtcTimer));
	printf("初始SID: %u\n", pSdsinkChannel->m_initialSID);
	printf("当前SID: %u\n", sdsink_get_current_sid(pSdsinkChannel));
	printf("/-*******************守护时间监控测试信息*******************-/\n");*/
}

/** 
*	@brief		时延监视的通信安全判断
*	@details	判断时延监视的安全状态并置lmc标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_check_lmc_communication_safe(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 时延监视相关通道动态变量的更新 */
	UINT32 tempSSC = 0u;
	UINT32 differTime=0U;
	UINT32 diffSSC=0u;

	/*- 当前包为初始VDP或到达测量间隔 */
	if ((SDT_TRUE == pSdsinkChannel->m_initialVDPFlag)
			|| ((UINT32)(pSdsinkChannel->m_currentProcessStartTime-pSdsinkChannel->m_lmcTimer) > (1000u*pSdsinkChannel->m_txPeriod)))
	{
		/*- 更新时延监视计时器 */
		pSdsinkChannel->m_lmcTimer = pSdsinkChannel->m_currentProcessStartTime;

		/*- 更新基准SSC为但前包的SSC */
		pSdsinkChannel->m_BaseSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;

		/*- 置预期SSC为但前包的SSC */
		pSdsinkChannel->m_ExceptSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;
	}
	else
	{
		/*- 计算预期SSC */
		differTime = pSdsinkChannel->m_currentProcessStartTime - pSdsinkChannel->m_lmcTimer;
		pSdsinkChannel->m_ExceptSSC = pSdsinkChannel->m_BaseSSC + ((differTime + pSdsinkChannel->m_txPeriod - 1) / pSdsinkChannel->m_txPeriod);

		pSdsinkChannel->m_lmcResetFlag = SDT_FALSE;
	}
	
	/*- 上电后已接收到初始VDP */
	if ( SDT_INIT_VALUE != pSdsinkChannel->m_BaseSSC)
	{
		/*- 当前VDP有效或初始 */
		if ((SDT_TRUE == pSdsinkChannel->m_validVDPFlag) || (SDT_TRUE == pSdsinkChannel->m_initialVDPFlag))
		{
			/*- 获取当前包的SSC作为当前SSC */
			tempSSC = pSdsinkChannel->m_VDPInfo.m_SafeSequCount;
		}
		else
		{
			/*- 获取上一有效VDP的下一SSC作为当前SSC */	
			tempSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel) + 1u;	/* 待定：这使对于没有落在新VDP窗口内的包，不进行时延判断 */
		}

		/*- 计算当前SSC与预期的差值 */
		diffSSC = (pSdsinkChannel->m_ExceptSSC >= tempSSC) ? (pSdsinkChannel->m_ExceptSSC - tempSSC) : (tempSSC - pSdsinkChannel->m_ExceptSSC);

		/*- 时延超过安全时间 */ 
		if ((diffSSC * pSdsinkChannel->m_txPeriod) >= pSdsinkChannel->m_rxSafe)
		{
			/*- 时延监视安全状态置为不安全 */
			pSdsinkChannel->m_lmcSafeCom = SDT_UNSAFE;
		}
		else
		{
			/*- 时延监视安全状态置为安全 */
			pSdsinkChannel->m_lmcSafeCom = SDT_SAFE;
		}
	}
	else
	{
		/* 当 基准SSC不存在时 意味着没有接收到VDP 将安全状态置为不安全 */ 
		/*- 时延监视安全状态置为不安全 */
		pSdsinkChannel->m_lmcSafeCom = SDT_UNSAFE;
	}
#if 0

	printf("smi %u LMC info: \t",pSdsinkChannel->m_channelASMI);
	printf("SSCi : %u\t", pSdsinkChannel->m_BaseSSC);
	printf("SSC : %u\t", pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
	printf("SSCE : %u\t", pSdsinkChannel->m_ExceptSSC);
	printf("SSCdiffer : %u\n", pSdsinkChannel->m_ExceptSSC >= pSdsinkChannel->m_VDPInfo.m_SafeSequCount ? (pSdsinkChannel->m_ExceptSSC - pSdsinkChannel->m_VDPInfo.m_SafeSequCount) : (pSdsinkChannel->m_VDPInfo.m_SafeSequCount - pSdsinkChannel->m_ExceptSSC));

#endif
	
}

/** 
*	@brief		通道监视的通信安全判断
*	@details	判断通道监视的安全状态并置cmc标志位
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/
void sdsink_check_cmc_communication_safe(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 更新错误VDP计数 */ 
	
	/*- 非重复VDP	*/
	if (SDT_FALSE == pSdsinkChannel->m_repeatedVDPFlag)
	{	
		/*- 当前VDP正确	*/
		if (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag)
		{
			/*- 错误VDP计数减1（下限为0） */
			pSdsinkChannel->m_wrongVDPCounter = ((0u < pSdsinkChannel->m_wrongVDPCounter) ? (pSdsinkChannel->m_wrongVDPCounter - 1u) : 0u);
		}
		else
		{
			/*- 错误VDP计数增长一个步长（上限为错误计数上限） */
			pSdsinkChannel->m_wrongVDPCounter =
				(((pSdsinkChannel->m_wrongVDPCounter + pSdsinkChannel->m_wrongIncreaseStep) < pSdsinkChannel->m_wrongCounterUpper) ?
				(pSdsinkChannel->m_wrongVDPCounter + pSdsinkChannel->m_wrongIncreaseStep) :
				pSdsinkChannel->m_wrongCounterUpper);
		}
	}

	/*- 通道安全状态判断 */
	
	/*- 达到错误阈值 */ 
	if (pSdsinkChannel->m_cmThr <= pSdsinkChannel->m_wrongVDPCounter)
	{
		/*- 通道监视通信安全状态置为不安全 */
		pSdsinkChannel->m_cmcSafeCom = SDT_UNSAFE;
	}
	/*- 错误计数归0 */
	else if (0u == pSdsinkChannel->m_wrongVDPCounter)
	{
		/*- 通道监视通信安全状态 置为安全 */ 
		pSdsinkChannel->m_cmcSafeCom = SDT_SAFE;
	}
}

/** 
*	@brief		获取当前的SID
*	@details	返回当前用于SC校验的SID，若SC校验不通过，即当前接收的是错误VDP，则返回0
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*	@result		UINT32				SID
*	@retval		非0					当前通道身份对应的SID值
*				0					当前vdp不是正确VDP
*/

UINT32 sdsink_get_current_sid(P_SDSINK_CHANNEL pSdsinkChannel)
{	
	/*- 获取当前通道身份对应的SID值 */
	UINT32 tempSid = 0;

	if (SDT_TRUE == pSdsinkChannel->m_correctVDPFlag)
	{
		tempSid = ((STA_SDSRC_CHANNEL_A == pSdsinkChannel->m_sdsrcIdentity) ? pSdsinkChannel->m_channelASID : pSdsinkChannel->m_channelBSID);
	}

	/*- 返回SID */
	return tempSid;
}
#ifdef SDT_STATE_REPORT
/** 
*	@brief		制造vdp解析结果字符串
*	@param[in]	SDT_HANDLE sdsinkHandle		接收端通道句柄
*/

void sdsink_make_test_info(SDT_HANDLE sdsinkHandle,UINT8* pVdpData)
{
	P_SDSINK_CHANNEL pSdsinkChannel = NULL;
	CHAR8 infoStr[500]={0};
	UINT16 offset=0u;
	INT32 len=0;
	
	pSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);

	len=sprintf(infoStr+offset,"Handle:%02u\t", sdsinkHandle);
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"SSC:%06u\t",  pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"srcId:%u\t", pSdsinkChannel->m_sdsrcIdentity);
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t %s\t", pSdsinkChannel->m_correctVDPFlag == SDT_TRUE ? "CORRECT" : "correct", pSdsinkChannel->m_repeatedVDPFlag == SDT_TRUE ? "REPEAT" : "repeat");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t%s\t", pSdsinkChannel->m_initialVDPFlag == SDT_TRUE ? "INITIAL" : "initial", pSdsinkChannel->m_newVDPFlag == SDT_TRUE ? "NEW" : "new");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t", pSdsinkChannel->m_validVDPFlag == SDT_TRUE ? "VALID" : "invalid");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t", SDT_SAFE == pSdsinkChannel->m_safeComFlag ? " SAFE " : "UNSAFE");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t", SDT_SAFE == pSdsinkChannel->m_rtsSafeCom ? "RTS" : "rts");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t", SDT_SAFE == pSdsinkChannel->m_gtcSafeCom ? "GTC" : "gtc");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t", SDT_SAFE == pSdsinkChannel->m_lmcSafeCom ? "LMC" : "lmc");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"%s\t", SDT_SAFE == pSdsinkChannel->m_cmcSafeCom ? "CMC" : "cmc");
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"now:%ld\t", pSdsinkChannel->m_currentProcessStartTime);
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	len=sprintf(infoStr+offset,"LMC info:SSCi:%u SSCE:%u SSCdiffer:%u lmcTimer:%ld curTimer:%ld whatifNewOk:%d\t"
			,pSdsinkChannel->m_BaseSSC,pSdsinkChannel->m_ExceptSSC, pSdsinkChannel->m_ExceptSSC >= pSdsinkChannel->m_VDPInfo.m_SafeSequCount ? (pSdsinkChannel->m_ExceptSSC - pSdsinkChannel->m_VDPInfo.m_SafeSequCount) : (pSdsinkChannel->m_VDPInfo.m_SafeSequCount - pSdsinkChannel->m_ExceptSSC),pSdsinkChannel->m_lmcTimer,pSdsinkChannel->m_currentProcessStartTime
					,((pSdsinkChannel->m_ExceptSSC >= pSdsinkChannel->m_VDPInfo.m_SafeSequCount ? (pSdsinkChannel->m_ExceptSSC - pSdsinkChannel->m_VDPInfo.m_SafeSequCount) : (pSdsinkChannel->m_VDPInfo.m_SafeSequCount - pSdsinkChannel->m_ExceptSSC))*pSdsinkChannel->m_txPeriod)<(pSdsinkChannel->m_rxSafe+pSdsinkChannel->m_rxPeriod));
	if(len>0)
	{
		offset+=(UINT16)len;
	}

	if(NULL!=pVdpData)
	{
		len=sprintf(infoStr+offset,"data:%u%u%u%u%u%u%u%u%u%u...",pVdpData[0],pVdpData[1],pVdpData[2],pVdpData[3],pVdpData[4],pVdpData[5],pVdpData[6],pVdpData[7],pVdpData[8],pVdpData[9]);
		if(len>0)
		{
			offset+=(UINT16)len;
		}
	}

	offset++;

	LOGMSG_outputSdtInfo((UINT8*)infoStr,offset);


	return;

}
#endif
#ifdef DEBUG
/** 
*	@brief		打印vdp解析结果
*	@details	打印vdp解析结果
*	@param[in]	SDT_HANDLE sdsinkHandle		接收端通道句柄
*/
void sdsink_print_test_info(SDT_HANDLE sdsinkHandle)
{
	/*- 打印vdp解析结果 */

	P_SDSINK_CHANNEL pSdsinkChannel = NULL;
	pSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);

	printf("Handle : %01d\t", sdsinkHandle);
	printf("SMI : %04d\t", pSdsinkChannel->m_channelASMI);
	printf("SSC : %04d\t",  pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
	printf("SSCi : %04d\t", pSdsinkChannel->m_lastValidVDPSSC);
	printf("%s\t %s\t", pSdsinkChannel->m_correctVDPFlag == SDT_TRUE ? "CORRECT" : "correct", pSdsinkChannel->m_repeatedVDPFlag == SDT_TRUE ? "REPEAT" : "repeat");
	printf("%s\t%s\t", pSdsinkChannel->m_initialVDPFlag == SDT_TRUE ? "INITIAL" : "initial", pSdsinkChannel->m_newVDPFlag == SDT_TRUE ? "NEW" : "new");
	printf("%s\t", pSdsinkChannel->m_validVDPFlag == SDT_TRUE ? "valid" : "invalid");

	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_safeComFlag ? " SAFE " : "UNSAFE");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_rtsSafeCom ? "RTS" : "rts");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_gtcSafeCom ? "GTC" : "gtc");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_lmcSafeCom ? "LMC" : "lmc");
	printf("%s\t", SDT_SAFE == pSdsinkChannel->m_cmcSafeCom ? "CMC" : "cmc");

	printf("now:%lld\t", pSdsinkChannel->m_currentProcessStartTime);
	printf("rtsTimer:%lld\n", pSdsinkChannel->m_rtsTimer);
}
#endif
/**
*	@brief		打印指定通道的SDSINK质量统计量至log
*	@details	打印指定通道的SDSINK质量统计量至log
*	@param[in]	SDT_HANDLE sdsinkHandle		接收端通道句柄
*/

void sdsink_print_statistic_info(SDT_HANDLE sdsinkHandle)
{
	/*- 打印指定通道的SDSINK质量统计量至log */
	
	P_SDSINK_CHANNEL pSdsinkChannel = NULL;
	pSdsinkChannel = &(g_sdsink.channelArray[sdsinkHandle]);

	SDT_LOG_MSG(LOG_ERROR, "sdsinkHandle:%02d(%u)\tAVALIABLE:%03u\tNEW:%03u\tINVALID:%03u\tOUT OF ORDER:%03u\tDELAY CYCLES:%03u\tSWICH:%03u\tBaceSSC:%04u\tExceptSSC:%04u\tSSC:%04u\n"
			, sdsinkHandle,pSdsinkChannel->m_channelASMI,pSdsinkChannel->m_statisticInfo.m_availableVDPCounter,pSdsinkChannel->m_statisticInfo.m_newVDPCounter
			, pSdsinkChannel->m_statisticInfo.m_invalidVDPCounter,pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter
			, pSdsinkChannel->m_statisticInfo.m_lantencyCounter,pSdsinkChannel->m_statisticInfo.m_switchChannelCounter,pSdsinkChannel->m_BaseSSC, pSdsinkChannel->m_ExceptSSC
			, pSdsinkChannel->m_VDPInfo.m_SafeSequCount);
	/*vos_printLog(VOS_LOG_ERROR, "sdsinkHandle:%02d\t", sdsinkHandle);
	vos_printLog(VOS_LOG_ERROR, "NEW: %03u\t", pSdsinkChannel->m_statisticInfo.m_newVDPCounter);
	vos_printLog(VOS_LOG_ERROR, "REPEAT%03u\t", pSdsinkChannel->m_statisticInfo.m_repeatedVDPVounter);
	vos_printLog(VOS_LOG_ERROR, "INVALID: %03u\t", pSdsinkChannel->m_statisticInfo.m_invalidVDPCounter);
	vos_printLog(VOS_LOG_ERROR, "OUT OF ORDER: %03u\t", pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter);
	vos_printLog(VOS_LOG_ERROR, "DELAY CYCLES: %03u\t", pSdsinkChannel->m_statisticInfo.m_lantencyCounter);
	vos_printLog(VOS_LOG_ERROR, "BaceSSC: %04u\t", pSdsinkChannel->m_BaseSSC);
	vos_printLog(VOS_LOG_ERROR, "ExceptSSC: %04u\t", pSdsinkChannel->m_ExceptSSC);
	vos_printLog(VOS_LOG_ERROR, "SSC: %04u\n", pSdsinkChannel->m_VDPInfo.m_SafeSequCount);*/
	
}

/** 
*	@brief		重置当前sdsink通道
*	@details	上电/重启后的通道配置环节调用该函数, 宿时间监视发生不安全后调用该函数
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*/

void sdsink_reset_vdp_parsing_environment(P_SDSINK_CHANNEL pSdsinkChannel)
{
	/*- 重置通道动态变量为初值 */
	
	/* 上电相关标志位初始化 */
	pSdsinkChannel->m_powerOnResetFlag = SDT_TRUE;
	pSdsinkChannel->m_gtcResetFlag		= SDT_TRUE;		
	pSdsinkChannel->m_lmcResetFlag		= SDT_TRUE;

	/* vdp类型标志位全部置否 */
	pSdsinkChannel->m_correctVDPFlag	= SDT_FALSE;
	pSdsinkChannel->m_repeatedVDPFlag	= SDT_FALSE;
	pSdsinkChannel->m_initialVDPFlag	= SDT_FALSE;
	pSdsinkChannel->m_newVDPFlag		= SDT_FALSE;
	pSdsinkChannel->m_validVDPFlag		= SDT_FALSE;
	pSdsinkChannel->m_lastVDPIsNewFlag	= SDT_FALSE;
	
	/* vdp解析数据全部清零 */	
	
	SDT_Memset((void*)&(pSdsinkChannel->m_VDPInfo),sizeof(VDP_INFO_T), 0u, sizeof(VDP_INFO_T));
	pSdsinkChannel->m_channelASC = SDT_INIT_VALUE;
	pSdsinkChannel->m_channelBSC = SDT_INIT_VALUE;
	pSdsinkChannel->m_lastChannelASC = SDT_INIT_VALUE;
	pSdsinkChannel->m_sdsrcIdentity = STA_SDSINK_CHANNEL;

	/* 安全状态全部置不安全 */
	
	pSdsinkChannel->m_safeComFlag	= SDT_UNSAFE;
	pSdsinkChannel->m_rtsSafeCom	= SDT_UNSAFE;
	pSdsinkChannel->m_gtcSafeCom	= SDT_UNSAFE;
	pSdsinkChannel->m_lmcSafeCom	= SDT_UNSAFE;
	pSdsinkChannel->m_cmcSafeCom	= SDT_UNSAFE;
	
	/* 通道历史记录全部置零  */
	

	/* 待定：此处将lastSdsrcIdentity置A，这使如果收到的第一条消息来自B机，则静态计数值中的切换次数会多一次（虽然并没有发生切换），其他功能不受影响 */
	
	/* 为什么不影响其他功能：使用lastSdsrcIdentity进行判断仅在初始vdp判断时进行，调用当前函数的情况（上电和宿时间超时）必使初始vdp判断为TRUE
	 	 lastSdsrcIdentity初值是什么没有影响，初始vdp接收的下一周期初，lastSdsrcIdentity即被更新，初值的影响随之消失 */


	pSdsinkChannel->m_initialVDPSSC		= SDT_INIT_VALUE;
	pSdsinkChannel->m_initialSID		= SDT_INIT_VALUE;
	pSdsinkChannel->m_rtsTimer			= 0;
	pSdsinkChannel->m_gtcTimer			= 0;
	pSdsinkChannel->m_lmcTimer			= 0;
	pSdsinkChannel->m_BaseSSC			= SDT_INIT_VALUE;
	pSdsinkChannel->m_ExceptSSC			= SDT_INIT_VALUE;
	
}

/** 
*	@brief		更新通道SDSINK质量统计量
*	@details	更新通道SDSINK质量统计量
*	@param[in]	P_SDSINK_CHANNEL		pSdsinkChannel		接收端通道指针
*	@return		UINT32				记录信息的vdp数量
*	@retval		记录信息的vdp数量
*/

UINT32 sdsink_update_statistic_info(P_SDSINK_CHANNEL pSdsinkChannel)
{	
	/* 更新通道SDSINK质量计数值 */
	
	/* 更新 新VDP计数器*/
	if (SDT_TRUE == pSdsinkChannel->m_newVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_newVDPCounter);
	}
	else{}

	/* 更新 错误VDP计数器*/
	if (SDT_FALSE == pSdsinkChannel->m_correctVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_errorVDPCounter);
	}
	else{}

	/* 更新 通道切换计数器*/
	if (pSdsinkChannel->m_lastSdsrcIdentity != pSdsinkChannel->m_sdsrcIdentity)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_switchChannelCounter);
	}
	else{}
	
	/* 更新 乱序接收计数器*/
	/* 不对重复VDP进行统计 */
	UINT32 tempSSC = sdsink_get_last_VDP_SSC(pSdsinkChannel);
	if ((SDT_FALSE == pSdsinkChannel->m_repeatedVDPFlag) 
		&&(0u != tempSSC)
		&&((tempSSC + 1u) != pSdsinkChannel->m_VDPInfo.m_SafeSequCount))
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_wrongOrderCounter);
	}
	else{}

	/* 更新 重复VDP计数器*/
	if (SDT_TRUE == pSdsinkChannel->m_repeatedVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_repeatedVDPVounter);
	}
	else{}

	/* 更新 发生延迟接收周期计数器*/
	if ((SDT_FALSE == pSdsinkChannel->m_initialVDPFlag)&&(SDT_UNSAFE == pSdsinkChannel->m_lmcSafeCom))
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_lantencyCounter);
	}
	else{}

	/* 更新 错误UDV计数器*/
	if (pSdsinkChannel->m_udv != pSdsinkChannel->m_VDPInfo.m_UserDataVersion)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_wrongUDVCounter);
	}

	/* 更新 cmc非安全周期计数器 */
	if (SDT_UNSAFE == pSdsinkChannel->m_cmcSafeCom)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_cmcUnsafeCounter);
	}
	
	/* 更新测试用的一些计数值，标准里没有，后续可以去掉 */
	if (SDT_UNSAFE == pSdsinkChannel->m_rtsSafeCom)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_rtsUnsafeCounter);
	}
	
	if ((SDT_SAFE == pSdsinkChannel->m_safeComFlag)&&(SDT_TRUE == pSdsinkChannel->m_validVDPFlag))
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_availableVDPCounter);
	}
	
	if (SDT_TRUE != pSdsinkChannel->m_validVDPFlag)
	{
		SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_invalidVDPCounter);
	}
	
	SDSINK_COUNTER_ADD(pSdsinkChannel->m_statisticInfo.m_VDPAmountCounter);
	
	/*- 返回记录数量 */
	return pSdsinkChannel->m_statisticInfo.m_VDPAmountCounter;
	
}

/** 
*	@brief		重置通道SDSINK质量统计量
*	@details	重置通道SDSINK质量统计量
*	@param[in]	SDSINK_STATISTIC_INFO_T*	pStatInfo		统计量结构体指针
*/

void sdsink_reset_statistic_info(P_SDSINK_STATISTIC_INFO_T pStatInfo)
{
	/*- 重置通道SDSINK质量统计量 */
	pStatInfo->m_newVDPCounter			= SDT_INIT_VALUE;
	pStatInfo->m_errorVDPCounter		= SDT_INIT_VALUE;
	pStatInfo->m_switchChannelCounter	= SDT_INIT_VALUE;
	pStatInfo->m_wrongOrderCounter		= SDT_INIT_VALUE;
	pStatInfo->m_repeatedVDPVounter		= SDT_INIT_VALUE;
	pStatInfo->m_lantencyCounter		= SDT_INIT_VALUE;
	pStatInfo->m_wrongUDVCounter		= SDT_INIT_VALUE;
	pStatInfo->m_cmcUnsafeCounter		= SDT_INIT_VALUE;
	
	pStatInfo->m_rtsUnsafeCounter		= SDT_INIT_VALUE;
	pStatInfo->m_VDPAmountCounter		= SDT_INIT_VALUE;
	pStatInfo->m_availableVDPCounter	= SDT_INIT_VALUE;
	pStatInfo->m_invalidVDPCounter		= SDT_INIT_VALUE;
}
