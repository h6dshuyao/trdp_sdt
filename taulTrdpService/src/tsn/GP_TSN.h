/**
 * @file       GP_TSN.h
 * @brief      TSN相关函数封装
 * @details    主要包括TSN网络配置初始化等功能
 * @author     GT-BU
 * @date       2022-03-16
 * @version    V1.0
 * @copyright  交控科技股份有限公司
 */

#ifndef API_GP_TSN_H_
#define API_GP_TSN_H_

#include "common.h"

#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */

/**
 * @struct TSNINFO
 * @brief  TSN网络信息结构体
 */
typedef struct
{
	CHAR8   ifname[16];  /* 网络名称 */
	UINT32 srcIP;       /* 本地IP,网络端序 */
	UINT16 srcPort;     /* 本地端口 */
	CHAR8   srcMAC[6];   /* 本地MAC */
	UINT32 dstIP;       /* 远端IP,网络端序 */
	UINT16 dstPort;     /* 远端端口 */
	CHAR8   dstMAC[6];   /* 远端MAC */
	INT32  vlanID;      /* VLAN ID */
	INT32  pcp;         /* TSN优先级 */
} TSNINFO;

/**
 * @brief      初始化TSN网络
 * @details    初始化TSN网络，配置相关参数
 * @param[inout]  info   TSN网络信息指针
 * @return     STATUS_T  函数执行结果
 *     - RET_NO_ERR  成功
 *     - ohter       失败
 */
TSN_STATUS_T TSN_Init(TSNINFO *info);

/**
 * @brief      获取PTP时钟ID
 * @details    获取PTP时钟ID
 * @param[in]  name   网络名称
 * @return     INT32  时钟ID
 *     - -1    获取失败
 */
INT32 GetPTPClockID(const CHAR8 *name);




#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /* API_GP_TSN_H_ */
