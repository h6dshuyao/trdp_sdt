/**
* @file CM_UDP.h
* @brief UDP通信的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#ifndef _CM_UDP_H
#define _CM_UDP_H

#include "CM_Socket.h"

#ifdef VXWORKS
#include <ioLib.h>
#endif
#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */

    /** @brief UDP传输时分包的大小 */
#define CM_UDP_LAN_MSG_SIZE_MAX (1472) /**< 局域网下每条UDP消息大小 */
#define CM_UDP_WAN_MSG_SIZE_MAX (548)  /**< 广域网下每条UDP消息大小 */

    /**
    * @brief CM_UDP_Init
    *
    * UDP初始化，仅需初始化一次.
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_UDP_Init(void);

    /**
    * @brief CM_UDP_Create_Socket
    *
    * Detailed description.
    * @param[out] pSocket
    * @param[in] isNonBlockMode
    * @param[in] pAddrIP
    * @param[in] portNumber
    * @param[in] maddr
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_UDP_Socket_Create(OUT CM_SOCKET* pSock, IN CM_BOOL isNonBlockMode, IN CM_UINT32 addrIP, IN CM_UINT16 portNumber, IN CM_UINT32 maddr);

    /**
    * @brief CM_UDP_Send_Msg
    *
    * Detailed description.
    * @param[in] pSock
    * @param[in] addrDest
    * @param[in] nPortDest
    * @param[in] pBuf
    * @param[in] bufSize
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_UDP_Send_Msg(IN CM_SOCKET sock, IN const CM_UINT8* pBuf, IN CM_INT32 bufSize, IN CM_UINT32 addrDest, IN CM_UINT16 nPortDest);

    /**
    * @brief CM_UDP_Recv_Msg
    *
    * Detailed description.
    * @param[in] sock
    * @param[in] pBuf
    * @param[in] BufSize传入时：表示传入的消息buf大小
    * @param[in] pAddrSrc
    * @param[in] pPortSrc
    * @return CM_INT32 返回接收消息大小， 返回-1表示失败
    */
    CM_INT32 CM_UDP_Recv_Msg(IN CM_SOCKET sock, OUT CM_UINT8 *pBuf, IN CM_INT32 BufSize, OUT CM_UINT32* pAddrSrc, OUT CM_UINT16* pPortSrc);

    /**
    * @brief CM_UDP_Is_Read_Ready
    *
    * Sokcet是否可读.
    * @param[in] sock
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_UDP_Is_Read_Ready(IN CM_SOCKET sock);

    /**
    * @brief CM_UDP_Is_Write_Ready
    *
    * Sokcet是否可写.
    * @param[in] sock
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_UDP_Is_Write_Ready(IN CM_SOCKET sock);


#ifdef __cplusplus
}

#endif /**< __cplusplus */

#endif /**< _CM_UDP_H */

