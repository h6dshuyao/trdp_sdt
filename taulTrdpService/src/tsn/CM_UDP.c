/**
* @file CM_UDP.c
* @brief UDP通信的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#include "CM_UDP.h"
#include "CM_Memory.h"
#ifdef WIN32
#include <ws2tcpip.h> /* 组播 */
#endif

/** UDP服务已初始化标志 */
static CM_BOOL s_UDP_Initialized = CM_FALSE;

/**
* @brief 初始化UDP服务
* @details 仅需初始化一次
*/
ENUM_CM_RETURN CM_UDP_Init(void)
{
    /*- 变量初始化 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE; /* 函数返回值 */

    /*- 初始化socket服务 */
    rt = CM_Socket_Init();

    /*- 是否车初始化成功 */
    if (ENUM_CM_TRUE == rt)
    {
        /*- UDP初始化成功 */
        s_UDP_Initialized = CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}

    /*- 返回初始化结果 */
    return rt;
}

/**
* @brief socket是否可读
* @param sock soket
* @preturn 可读状态
* - ENUM_CM_TRUE 可读
* - ENUM_CM_FALSE 不可读
*/
ENUM_CM_RETURN CM_UDP_Is_Read_Ready(IN CM_SOCKET sock)
{
    /*- 变量初始化 */
    CM_INT32 sFd = 0;                    /* 文件描述符 */
    CM_INT32 sFlag = 0;                  /* 临时变量 */
    struct timeval timeover1 = { 0, 0 }; /* 时间 */
    fd_set readok;                       /* 临时变量 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;   /* 函数返回值 */
    /*- UDP服务是否已经初始化 */
    if (CM_TRUE == s_UDP_Initialized)
    {
        /*- select查询sock的读状态 */
        CM_Memset((void*)(&readok), (CM_UINT8)0U, (size_t)(sizeof(fd_set)));
        timeover1.tv_sec = 0;

        timeover1.tv_usec = 0;

        FD_ZERO(&readok);
        FD_SET(sock, &readok);

        sFd = (CM_INT32)select((sock + 1), &readok, NULL, NULL, &timeover1);

        /*- @alias 判断sock是否可读*/
        if (sFd > 0)
        {
        	sFlag = FD_ISSET(sock, &readok);
            if (0 != sFlag)
            {
                rt = ENUM_CM_TRUE;
            }
            else
            {
                rt = ENUM_CM_FALSE;
            }
        }
        else
        {
            rt = ENUM_CM_FALSE;
        }
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回sock的可读状态 */
    return rt;
}

/**
* @brief socket是否可写
* @param sock soket
* @preturn 可写状态
* - ENUM_CM_TRUE 可写
* - ENUM_CM_FALSE 不可写
*/
ENUM_CM_RETURN CM_UDP_Is_Write_Ready(IN CM_SOCKET sock)
{
    /*- 变量初始化 */
    CM_INT32 sFd = 0;                    /* 文件描述符 */
    CM_INT32 sFlag = 0;                  /* 临时变量 */
    struct timeval timeover1 = { 0, 0 }; /* 时间 */
    fd_set writeok;                      /* 临时变量 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;   /* 函数返回值 */

    /*- UDP服务是否已经初始化 */
    if (CM_TRUE == s_UDP_Initialized)
    {
        /*- select查询sock的写状态 */
        CM_Memset((void*)(&writeok), (CM_UINT8)0U, (size_t)(sizeof(fd_set)));
        timeover1.tv_sec = 0;
        timeover1.tv_usec = 0;
        FD_ZERO(&writeok);
        FD_SET(sock, &writeok);

        sFd = (CM_INT32)select((CM_INT32)(sock + 1), NULL, &writeok, NULL, &timeover1);

        /*- @alias 判断sock是否可写 */
        if (sFd > 0)
        {
        	sFlag = FD_ISSET(sock, &writeok);
            if (0 != sFlag)
            {
                rt = ENUM_CM_TRUE;
            }
            else
            {
                rt = ENUM_CM_FALSE;
            }
        }
        else
        {
            rt = ENUM_CM_FALSE;
        }
    }
    else
	{
		/*- 无处理 */
	}
    //printf("=====UDP SND FLAG: sFd =%d, sFlag = %d\r\n", sFd, sFlag );
    /*- 返回sock的可写状态 */
    return rt;
}

/**
* @brief 创建UDP socket
* @param pSock socket指针
* @param isNonBlockMode 是否是非阻塞模式 CM_TRUE:非阻塞，CM_FALSE:阻塞
* @param addrIP IP地址
* @param portNumber 端口号
* @param maddr 所属组播地址，0为不加入组播
* @return 创建结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_UDP_Socket_Create(OUT CM_SOCKET *pSock, IN CM_BOOL isNonBlockMode, IN CM_UINT32 addrIP, IN CM_UINT16 portNumber, IN CM_UINT32 maddr)
{
    /*- 变量初始化 */
    CM_SOCKADDR_T addrHost;           /* 主机地址 */
    CM_INT32 ret = 0;                 /* 临时结果 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE; /* 函数返回值 */
    struct ip_mreq multiCast;         /* 组播配置 */           

    /*- 是否UDP服务未初始化或输入非法 */
    if ((CM_TRUE != s_UDP_Initialized) || (NULL == pSock))
    {
        /*- 创建失败 */
        rt = ENUM_CM_FALSE;
    }
    else
    {
        /*- 创建UDP socket*/
        CM_Memset((void*)(&addrHost), (CM_UINT8)0U, (size_t)(sizeof(CM_SOCKADDR_T)));        
        rt = CM_Socket_Create(pSock, CM_TRUE);
        /*- 创建socket是否成功 */
        if (ENUM_CM_TRUE == rt)
        {
            /*- 绑定IP端口 */
            CM_Memset((void*)(&addrHost), (CM_UINT8)0U, (size_t)(sizeof(CM_SOCKADDR_T)));            
            addrHost.sin_family = (CM_UINT16)AF_INET;             
            addrHost.sin_port = htons(portNumber);
#ifdef WIN32
            addrHost.sin_addr.s_addr = addrIP;
#endif
#ifdef LINUX
            addrHost.sin_addr.s_addr = addrIP;
#else
            addrHost.sin_addr.s_addr = INADDR_ANY;
#endif
            ret = (CM_INT32)bind(*pSock, (CM_SOCKADDR*)(&addrHost), (unsigned int)sizeof(CM_SOCKADDR));
            /*- 绑定是否成功 */
            if (ret != CM_SOCKET_ERROR)
            {
            	/*- 设置socket的阻塞模式 */
#ifdef WIN32
            	ret = ioctlsocket(*pSock, (long)FIONBIO, (unsigned long *)&isNonBlockMode);
#else/*Linux Vxworks常用PIOX标准*/
            	ret = ioctl(*pSock, FIONBIO, &isNonBlockMode);
#endif
                /*- 设置socket的阻塞模式是否失败 */
                if (CM_SOCKET_ERROR == ret)
                {
                    /*- 创建失败 */
                    rt = ENUM_CM_FALSE;
                }
                else
                {
                    /*- 是否需要加入组播 */
                    if (0u != maddr)
                    {
                        /*- 配置组播地址 */
                        multiCast.imr_interface.s_addr = addrIP;
                        multiCast.imr_multiaddr.s_addr = maddr;
                        ret = setsockopt(*pSock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void*)(&multiCast), (unsigned int)sizeof(multiCast));
                        /*- 是否配置成功 */
                        if (0 == ret)
                        {
                            /*- 创建成功 */
                            rt = ENUM_CM_TRUE;
                        }
                        else
                        {
                            /*- 创建失败 */
                            printf("%s加入组播%s失败\n", inet_ntoa(multiCast.imr_interface), inet_ntoa(multiCast.imr_multiaddr));
                            rt = ENUM_CM_FALSE;
                        }
                    }
                    else
                    { 
                        /*- 创建成功 */
                        rt = ENUM_CM_TRUE;
                    }
                    
                }
            }
            else
            {
                /*- 创建失败 */
                printf("绑定IP:%s:%u失败\n", inet_ntoa(addrHost.sin_addr), portNumber); /* parasoft-suppress BD-PB-NOTINIT "WIN API 导致的误报" */
                rt = ENUM_CM_FALSE;
            }
        }
        else
		{
			/*- 无处理 */
		}
    }
    /*- 返回创建结果 */
    return rt;
}

/**
* @brief UDP发送（指定IP端口）
* @param sock socket
* @param pBuf 发送报文指针
* @param bufSize 发送报文长度
* @param addrDest 目的IP
* @param nPortDest 目的端口
* @return 发送结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_UDP_Send_Msg(IN CM_SOCKET sock, IN const CM_UINT8* pBuf, IN CM_INT32 bufSize, IN CM_UINT32 addrDest, IN CM_UINT16 nPortDest)
{
    /*- 变量初始化 */
    CM_SOCKADDR_T addr;                /* 目的地址 */
#ifdef WIN32
    size_t ret = 0;                    /* 函数返回值 */
#else
    ssize_t ret = 0;                  /* 函数返回值 */
#endif
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;  /* 函数返回值 */
    ENUM_CM_RETURN isReady = ENUM_CM_FALSE; /* 可写标志 */

    /*- UDP协议是否已经初始化 */
    if (CM_TRUE == s_UDP_Initialized)
    {
    	/*- 获取socket可写状态 */
    	isReady = CM_UDP_Is_Write_Ready(sock);
        /*- socket是否可写 */
        if (ENUM_CM_TRUE == isReady)
        {
            /*- 生成目的地址 */
            CM_Memset((void *)(&addr), (CM_UINT8)0U, (size_t)(sizeof(addr)));

            addr.sin_family = (CM_UINT16)AF_INET;
            addr.sin_port = htons(nPortDest);
            addr.sin_addr.s_addr = addrDest;
//            printf("socket buf [0x%x] [0x%x] [0x%x] [0x%x] [0x%x]\r\n",pBuf[10],pBuf[11],pBuf[12],pBuf[13],pBuf[14]);
            /*- 向目的地址发送报文 */
            ret = sendto(sock, (void *)pBuf, (size_t)bufSize, 0, (const struct sockaddr*)(&addr), (unsigned int)sizeof(CM_SOCKADDR_T));

            /*- @alias 判断报文是否发送成功 */
            if (ret == CM_SOCKET_ERROR)
            {
                rt = ENUM_CM_FALSE;
            }
            else if (ret != bufSize)
            {
                rt = ENUM_CM_FALSE;
            }
            else
            {
                rt = ENUM_CM_TRUE;
            }
        }
        else
        {
        	printf("send error socket buf [0x%x] [0x%x] [0x%x] [0x%x] [0x%x]\r\n",pBuf[0],pBuf[1],pBuf[2],pBuf[3],pBuf[4]);
            /*- 发送失败 */
            rt = ENUM_CM_FALSE;
        }
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回发送结果 */
    return rt;
}

/**
* @brief UDP发送（源IP、端口）
* @param sock socket
* @param pBuf 接收报文指针
* @param BufSize 接收报文缓冲区大小
* @param pAddrSrc 源IP
* @param pPortSrc 源端口
* @return 收到报文长度
* - 正整数 收到的报文长度
* - 0 未收到报文
* - -1 接收异常
*/
CM_INT32 CM_UDP_Recv_Msg(IN CM_SOCKET sock, OUT CM_UINT8 *pBuf, IN CM_INT32 BufSize, OUT CM_UINT32* pAddrSrc, OUT CM_UINT16* pPortSrc)
{
    /*- 变量初始化 */
    CM_SOCKADDR_T addr;   /* 临时变量 */
#ifdef WIN32
    int nFromLen = (int)sizeof(CM_SOCKADDR_T); /* 临时变量 */
#else
    CM_UINT32 nFromLen = sizeof(CM_SOCKADDR_T); /* 临时变量 */
#endif
    CM_INT32 recvNum = 0; /* 接收长度 */

    /*- 是否UDP协议未初始化或输入非法 */
    if ((CM_TRUE != s_UDP_Initialized) || (NULL == pBuf) || (0 >= BufSize))
    {
        /*- 接收异常 */
        recvNum = -1;
    }
    else
    {
        CM_Memset((void*)(&addr), (CM_UINT8)0U, (unsigned long)(sizeof(addr)));
        
        /*- UDP接收 */
        recvNum = (CM_INT32)recvfrom(sock, (void *)pBuf, (unsigned long)BufSize, 0, (struct sockaddr*)(&addr), &nFromLen);
        /*- 接收长度是否大于0 */
        if (recvNum > 0)
        {
            /*- @alias 输出源IP */
            if (NULL != pAddrSrc)
            {
                *pAddrSrc = addr.sin_addr.s_addr;
            }
            else
    		{
    			/* 无处理 */
    		}

            /*- @alias 出书源端口 */
            if (NULL != pPortSrc)
            {
                *pPortSrc = ntohs(addr.sin_port);
            }
            else
    		{
    			/* 无处理 */
    		}
        }
        else
		{
			/* 无处理 */
		}
    }
    /*- 返回接收长度 */
    return recvNum;
}
