/**
* @file CM_Socket.h
* @brief socket操作的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#ifndef _CM_SOCKET_H
#define _CM_SOCKET_H

#include "CM_Types.h"

#ifdef WIN32
#pragma comment(lib, "Ws2_32.lib")
#endif
#ifdef VXWORKS
#include <signal.h>
#include <socket.h>
#include <sockLib.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#endif

#ifdef QNX
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <signal.h>
#endif

#ifdef LINUX
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <signal.h>
#endif

typedef struct sockaddr CM_SOCKADDR;  /* socket地址结构体 */

#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */

    typedef struct sockaddr_in CM_SOCKADDR_T;  /* 地址类型 */
#define CM_SOCKET_ERROR (-1)

#ifdef WIN32
    typedef SOCKET CM_SOCKET;   /* socket类型 */
#endif

#ifdef VXWORKS
    typedef int CM_SOCKET;  /* socket类型 */
#endif

#ifdef QNX
    typedef int CM_SOCKET; /* socket类型 */
#endif

#ifdef LINUX
    typedef int CM_SOCKET; /* socket类型 */
#endif

    /**
    * @brief CM_Socket_Init
    *
    * Socket初始化，仅需初始化一次.
    * @return CM_BOOL
    */
	ENUM_CM_RETURN CM_Socket_Init(void);

    /**
    * @brief CM_Socket_Create
    *
    * 创建Socket.
    * @param[in] sock
    * @param[in] isUDP
    * @return CM_BOOL
    */
	ENUM_CM_RETURN CM_Socket_Create(INOUT CM_SOCKET* sock,IN CM_BOOL isUDP);

    /**
    * @brief CM_Socket_Close_Socket
    *
    * 关闭Socket.
    * @param[in] sock
    * @return CM_BOOL
    */
	ENUM_CM_RETURN CM_Socket_Close(INOUT CM_SOCKET* pSock);

#ifdef __cplusplus
}

#endif /**< __cplusplus */

#endif /**< _CM_SOCKET_H */

