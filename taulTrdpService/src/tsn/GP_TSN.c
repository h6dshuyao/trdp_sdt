/**
 * @file       GP_TSN.c
 * @brief      TSN相关函数封装
 * @details    主要包括TSN网络配置初始化等功能
 * @author     GT-BU
 * @date       2022-03-16
 * @version    V1.0
 * @copyright  交控科技股份有限公司
 */

#include <GP_TSN.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <linux/if_packet.h>
#include <linux/if_ether.h>
#include <netinet/if_ether.h>
#include <fcntl.h>
#include <time.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <unistd.h>

/**
 * @struct tsn_stream
 * @brief  tsn配置帧结构体
 */
struct tsn_stream
{
	uint32_t src;      /* 本地IP */
	uint32_t dst;      /* 远程IP */
	uint16_t sport;    /* 本地端口 */
	uint16_t dport;    /* 远程端口 */
	uint8_t  proto;    /* 控制协议 */
	uint8_t  dscp;

	uint16_t vid;      /* VLAN ID */
	uint8_t  pcp;      /* TSN 优先级 */
	uint8_t  smac[6];  /* 本地mac */
	uint8_t  dmac[6];  /* 远程mac */
}__attribute__((__packed__));

#define CLOCKFD 3
#define FD_TO_CLOCKID(fd)	((~(clockid_t) (fd) << 3) | CLOCKFD)

#define INLEN 4  /* IP地址长度 */
#define MAC_BCAST_ADDR  (UINT8 *) "\xff\xff\xff\xff\xff\xff" /* MAC广播地址 */
#define SET_IPV4_TSN_INTERCEPT 0x100


/**
 * @brief      获取网络地址信息
 * @details    获取本地IP/MAC地址,远端IP/MAC地址
 * @param[inout]  info   TSN网络信息指针
 * @return     STATUS_T  函数执行结果
 *     - RET_NO_ERR		成功
 *     - RET_TIMEOUT	收不到arp reply,自己计算对方mac地址
 *     - 其他			失败
 */
static TSN_STATUS_T arp_get_mac(TSNINFO *info)
{
	TSN_STATUS_T ret = RET_UNKNOWN_ERR;

    INT32 sk = 0;
    struct sockaddr_ll reqsa;
    ssize_t bytes = 0;

    /* arp请求帧/回复帧 */
    struct arp_pkt
    {
		struct ether_header eh;    /* 以太网头部 */
		struct ether_arp ea;       /* arp数据 */
		u_char padding[18];        /* 填充数据 */
    } req, rep;

    /* 参数有效 */
    if (NULL != info)
    {
        /* 创建链路层socket */
        sk = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
        /* 创建成功 */
        if(-1 != sk)
    	{
    		/* 设置接口名称 */
    		struct ifreq ifr1, ifr2;
    		memset(&ifr1, 0, sizeof(struct ifreq));
    		memset(&ifr2, 0, sizeof(struct ifreq));
    		strncpy(ifr1.ifr_name, info->ifname, sizeof(ifr1.ifr_name) - 1);
    		strncpy(ifr2.ifr_name, info->ifname, sizeof(ifr2.ifr_name) - 1);

    		/* 获取接口ip mac地址 */
    		INT32 rt1 = ioctl(sk, SIOCGIFADDR, &ifr1);
    		INT32 rt2 = ioctl(sk, SIOCGIFHWADDR, &ifr2);

    		/* 获取成功 */
    		if ((-1 != rt1) && (-1 != rt2))
    		{
    			INT32 rt;
    			fd_set readfds;
    			struct timeval tv;
    			struct sockaddr_in *paddr = (struct sockaddr_in *)&ifr1.ifr_addr;

    			/* 获取本地32位网络字节序IP地址 */
    			info->srcIP = paddr->sin_addr.s_addr;
    			/* 获取本地6字节网络MAC地址 */
    			memcpy(info->srcMAC, ifr2.ifr_hwaddr.sa_data, ETH_ALEN);

    	        bzero(&req, sizeof(req));

    	        /* 填写以太网头部*/
    	        memcpy(req.eh.ether_dhost, MAC_BCAST_ADDR, ETH_ALEN);//目的mac
    	        memcpy(req.eh.ether_shost, ifr2.ifr_hwaddr.sa_data, ETH_ALEN);//源mac
    	        req.eh.ether_type = htons(ETHERTYPE_ARP);//以太网类型

    	        /* 填写arp数据 */
    	        req.ea.arp_hrd = htons(ARPHRD_ETHER);//设备地址 类型
    	        req.ea.arp_pro = htons(ETHERTYPE_IP);//网络地址 类型
    	        req.ea.arp_hln = ETH_ALEN;
    	        req.ea.arp_pln = INLEN;
    	        req.ea.arp_op = htons(ARPOP_REQUEST);

    	        memcpy(req.ea.arp_sha, ifr2.ifr_hwaddr.sa_data, ETH_ALEN);//源mac
    	        memcpy(req.ea.arp_spa, &paddr->sin_addr, INLEN);//源ip
    	        memcpy(req.ea.arp_tpa, &info->dstIP, INLEN);//目的ip

    	    	/* 配置目的端arp地址 */
    	        bzero(&reqsa, sizeof(reqsa));
    	        reqsa.sll_family = PF_PACKET;
    	        reqsa.sll_ifindex = if_nametoindex(info->ifname);

    	        /* 发送arp请求帧 */
    	        bytes = sendto(sk, &req, sizeof(req), 0, (struct sockaddr *)&reqsa, sizeof(reqsa));

    	        /* 发送arp请求帧失败 */
    	        if(bytes<=0)
    	        {
    	        	printf("get mac send arp reply failed\n");
    	        	ret = RET_SOCK_ERR;
    	        }

	    		/* 设置超时时间 */
	    		tv.tv_sec  = 0;
	    		tv.tv_usec = 10000;   /* 总超时时间 1s */

    	        /* 发送成功 */
    	        while(bytes > 0)
    	        {
    	    		/* 清零读文件描述符 */
    	    		FD_ZERO(&readfds);
    	    		/* 设置读文件描述符 */
    	    		FD_SET(sk, &readfds);

    	    		/* 等待可读套接字 */
    	    		rt=select(sk + 1, &readfds, NULL, NULL, &tv);

    	    		/* 有未读数据 */
    	    		if (0 < rt)
    	    		{
        	        	bzero(&rep, sizeof(rep));
        	        	/* 接收arp回复帧 */
        	        	bytes = recv(sk, &rep, sizeof(rep), 0);
        	        	/* 接收成功 */
        	        	if (0 < bytes)
        	        	{
        	        		rt = memcmp(req.ea.arp_tpa, rep.ea.arp_spa, 4);

        	        		/* 接收为指定远端arp回复帧 */
        	        		if ((ARPOP_REPLY == ntohs(rep.ea.arp_op)) && (0 == rt))
        	        		{
        	        			/* 获取远端MAC地址 */
        	        			memcpy(info->dstMAC, rep.ea.arp_sha, ETH_ALEN);
        	        			ret = RET_NO_ERR;
        	        			break;
        	        		}
        	        		else
        	        		{
        	        			//printf("get mac recv invalid pack,dest ip: %d,reply ip:%d ,arp option:%d,cmp ret:%d,\n",*(UINT32*)req.ea.arp_tpa,*(UINT32*)rep.ea.arp_spa,ntohs(rep.ea.arp_op),rt);
        	        			/* 继续接收 */
        	        		}
        	        	}
        	        	/* 接收失败 */
        	        	else
        	        	{
        	        		ret = RET_IO_ERR;
        	        		break;
        	        	}
    	    		}
    	    		/* 无可读数据 */
    	    		else
    	    		{
    	    			UINT32 destIp=info->dstIP;

    	    			info->dstMAC[0]=1;
    	    			info->dstMAC[1]=0;
    	    			info->dstMAC[2]=0x5e;
    	    			memcpy((void *)&info->dstMAC[3],(const void*)((UINT8*)(&destIp)+1),3);
    	    			info->dstMAC[3]&=0x7F;

    	    			printf("tsn get mac recv no arp reply in 10 ms,destip:0x%x,set dest mac by calc: %u:%u:%u:%u:%u:%u\n"
    	    					,info->dstIP,info->dstMAC[0],info->dstMAC[1],info->dstMAC[2],info->dstMAC[3],info->dstMAC[4],info->dstMAC[5]);

	        			ret = RET_TIMEOUT_ERR;
	        			break;
    	    		}

    	        }
    		}


    		/* 获取失败 */
    		else
    		{
    			ret = RET_IO_ERR;
    		}

    		/* 关闭套接字 */
    		close(sk);
        }
        /* 创建失败 */
        else
        {
        	ret = RET_SOCK_ERR;
        }
    }
    /* 参数无效 */
    else
    {
    	ret = RET_PARAM_ERR;
    }

    return ret;
}

/**
 * @brief      初始化TSN网络
 * @details    初始化TSN网络，配置相关参数
 * @param[inout]  info   TSN网络信息指针
 * @return     STATUS_T  函数执行结果
 *     - RET_NO_ERR  成功
 *     - ohter       失败
 */
TSN_STATUS_T TSN_Init(TSNINFO *info)
{
	TSN_STATUS_T ret = RET_UNKNOWN_ERR;
	INT32 fd = 0;
	struct tsn_stream tsnopt = {0};

    /* 参数有效 */
    if (NULL != info)
    {
    	/* 打开ipic驱动 */
    	fd = open("/dev/ipic", O_RDWR);
    	/* 获取本地和远端网络地址信息 */
    	ret = arp_get_mac(info);

    	/* 打开文件、获取地址成功 */
    	if ((-1 != fd) && (RET_NO_ERR == ret || RET_TIMEOUT_ERR== ret))
    	{
        	tsnopt.src = info->srcIP;
        	tsnopt.dst = info->dstIP;
#define LOCALIP  "239.10.1.2"

        	tsnopt.sport = htons(info->srcPort);
        	tsnopt.dport = htons(info->dstPort);

        	tsnopt.proto = atoi("17") & 0xFF;  /* UDP协议? */
        	tsnopt.dscp  = 0;
        	tsnopt.vid = info->vlanID;
        	tsnopt.pcp = info->pcp;
        	memcpy(tsnopt.smac, info->srcMAC, ETH_ALEN);
        	memcpy(tsnopt.dmac, info->dstMAC, ETH_ALEN);

        	/* 设置tsn */
        	INT32 rt = ioctl(fd, SET_IPV4_TSN_INTERCEPT, &tsnopt);
        	/* 设置成功 */
        	if (0 == rt)
        	{
        		ret = RET_NO_ERR;
        	}
        	/* 设置失败 */
        	else
        	{
        		ret = RET_IO_ERR;
        		printf("tsn set tsn failed,error code:%d\n",rt);
        	}

        	close(fd);

    	}
    	/* 打开文件或获取地址失败 */
    	else
    	{
        	if(RET_NO_ERR != ret)
        		printf("tsn init get mac failed,ret:%d\n",ret);

        	if(-1 == fd)
        		printf("tsn init open file failed\n");
    		ret = RET_IO_ERR;
    	}

    }
    /* 参数无效 */
    else
    {
    	ret = RET_PARAM_ERR;
    }
    return ret;
}


/**
 * @brief      获取PTP时钟ID
 * @details    获取PTP时钟ID
 * @param[in]  name   网络名称
 * @return     INT32  时钟ID
 *     - -1    获取失败
 */
INT32 GetPTPClockID(const CHAR8 *name)
{
	INT32 ret = -1;
	INT32 phc_fd = -1;
	/* 参数有效 */
	if (NULL != name)
	{
		/* 以太网1 */
		if (0 == memcmp(name, "eth1", 4))
		{
			phc_fd = open( "/dev/ptp0", O_RDWR );
		}
		/* 以太网2 */
		else if (0 == memcmp(name, "eth2", 4))
		{
			phc_fd = open( "/dev/ptp1", O_RDWR );
		}
		/* 其他 */
		else
		{
			/* 不做处理 */
		}

		/* 驱动文件打开成功 */
		if (-1 != phc_fd)
		{
			ret = FD_TO_CLOCKID(phc_fd);
		}
		/* 驱动文件打开失败 */
		else
		{
			/* 不做处理 */
		}
	}
	/* 参数无效 */
	else
	{
		/* 不做处理 */
	}
	return ret;
}


