#!/bin/sh

echo "1">/proc/sys/net/ipv4/conf/all/arp_ignore
echo "1">/proc/sys/net/ipv4/conf/eth0/arp_ignore
echo "1">/proc/sys/net/ipv4/conf/eth1/arp_ignore
echo "1">/proc/sys/net/ipv4/conf/eth2/arp_ignore
echo "2">/proc/sys/net/ipv4/conf/all/arp_announce
echo "2">/proc/sys/net/ipv4/conf/eth0/arp_announce
echo "2">/proc/sys/net/ipv4/conf/eth1/arp_announce
echo "2">/proc/sys/net/ipv4/conf/eth2/arp_announce

ifconfig eth0 192.168.1.31
echo "eth0 192.168.1.31"
ifconfig eth1 192.168.3.31
echo "eth1 192.168.3.31"
ifconfig eth2 192.168.2.31
echo "eth2 192.168.2.31"

sleep 1

ifconfig eth0 down
ifconfig eth0 hw ether 00:0A:35:00:03:10
#ifconfig eth0 up

ifconfig eth1 down
ifconfig eth1 hw ether 00:0A:35:00:01:10
ifconfig eth1 up

ifconfig eth2 down
ifconfig eth2 hw ether 00:0A:35:00:02:10
#ifconfig eth2 up

sleep 1

#ip link set can0 up type can bitrate 500000 sample-point 0.75 dbitrate 2000000 dsample-point 0.8 fd on
#ip link set can1 up type can bitrate 500000 sample-point 0.75 dbitrate 2000000 dsample-point 0.8 fd on


cd /mnt/demo
./qbv_sched -c eth1 /mnt/etc/qbv.cfg
./ptp4l -P -2 -H -i eth1 -p /dev/ptp0 -s -f /mnt/etc/ptp4l_eth1_slave.conf &


cp /mnt/demo/libconfig.so.11.0.2 /usr/lib/
ln -s /usr/lib/libconfig.so.11.0.2 /usr/lib/libconfig.so.11

mkdir -p /lib/modules/4.19.0/extra
cp /mnt/demo/xilinx_tsn_ip_intercept.ko /lib/modules/4.19.0/extra/
insmod /lib/modules/4.19.0/extra/xilinx_tsn_ip_intercept.ko IPv4_tuple=1,1,0,0,1,1
echo "insmod xilinx_tsn_ip_intercept.ko"
sleep 1


