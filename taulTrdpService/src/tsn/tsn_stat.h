#ifndef TSN_STAT_H
#define TSN_STAT_H

#include "trdp_types.h"
//void tsn_outputTestInfo(CM_UINT32 realIP,CM_UINT8 recvIfindex,CM_UINT32 comId,CM_UINT8 missFlag,CM_UINT32 numMissed,CM_UINT32 seqCnt,CM_UINT64 commuDelay,CM_UINT64 recvTick);

void tsn_outputTestInfo(UINT32 realIP,UINT8 recvIfindex,UINT32 comId,UINT8 missFlag,UINT32 numMissed,UINT32 seqCnt,UINT64 commuDelay,UINT64 recvTick);
void LOGMSG_outputSdtInfo(UINT8* pSendBuf,UINT16 bufLen);
#endif
