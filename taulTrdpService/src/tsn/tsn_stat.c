#include "tsn_stat.h"
#include "CM_Socket.h"
#include "CM_UDP.h"

void tsn_outputTestInfo(UINT32 realIP,UINT8 recvIfindex,UINT32 comId,UINT8 missFlag,UINT32 numMissed,UINT32 seqCnt,UINT64 commuDelay,UINT64 recvTick)
{
	static CM_SOCKET testSock[2]={-1,-1};
	static CM_UINT32 testSockIp[2]={0u,0u};
	static CM_UINT32 testSockDestIp[2]={0u,0u};
	UINT32 sockIndex=0u;
	const CM_CHAR IP_G[16]="10.1.1.137";

	CM_UINT32 lowDestIp=inet_addr(IP_G);
	lowDestIp = lowDestIp & 0xffff0000u;//只取ip低位

	CM_UINT32 destIp=0u;

	UINT32	destPort=0;
	ENUM_CM_RETURN rt;

	CM_UINT8 buf[1432] = {0};
	CM_INT32 sendBufLen=0;

	struct in_addr ip;

	if((-1==testSock[0]) && (-1==testSock[1]))
	{
		if(ENUM_CM_TRUE!=CM_UDP_Init())
		{
			printf("create udp socket for tsn test info output failed!can't init CM UDP lib\n",inet_ntoa(ip),rt);
			return;
		}
	}

	for(sockIndex=0;sockIndex<2;sockIndex++)
	{
		if(realIP==testSockIp[sockIndex])
		{
			break;
		}
		else if(0u==testSockIp[sockIndex])
		{
			/*- 创建测试发送socket(非阻塞) */
			rt = CM_UDP_Socket_Create(&testSock[sockIndex], CM_TRUE, htonl(realIP),0u, 0u);
			ip.s_addr=htonl(realIP);

			if(ENUM_CM_TRUE != rt)
			{
				printf("create udp socket for tsn test info output failed!bind Ip:%s,err:%d\n",inet_ntoa(ip),rt);
			}
			else
			{
				testSockIp[sockIndex]=realIP;

				/* 产生一个小端ip */
				testSockDestIp[sockIndex]=(htonl(realIP) & 0x0000ffff) | lowDestIp;

				printf("create udp socket for tsn test info output success!bind Ip:%s\n",inet_ntoa(ip));
				ip.s_addr=testSockDestIp[sockIndex];
				printf("dest Ip:%s\n",inet_ntoa(ip));
			}

			break;
		}
		else
		{

		}
	}

	if(sockIndex>=2)
	{
		printf("both sock (%d|%d)not match for ip:%d\n",testSockIp[0],testSockIp[1],realIP);
		return;
	}

	if(-1!=testSock[sockIndex])
	{
		//destPort=60000+realIP&0x000000ff;
		destPort=60030u;

		memset(buf,0,sizeof(buf));

		ip.s_addr=htonl(realIP);

		snprintf(buf,sizeof(buf),"%s,%u,%u,%u,%u,%u,%llu,%llu\r\n"
				,inet_ntoa(ip),recvIfindex,comId,missFlag,numMissed,seqCnt,commuDelay,recvTick);

		sendBufLen=strlen(buf);

		rt = CM_UDP_Send_Msg(testSock[sockIndex], buf, sendBufLen, testSockDestIp[sockIndex], destPort);

	}
}

void LOGMSG_outputSdtInfo(UINT8* pSendBuf,UINT16 bufLen)
{
	static CM_SOCKET testSock=-1;
	const CM_CHAR IP[16]="10.1.1.144";
	//const CM_CHAR IP[16]="239.255.0.239";
	UINT16	destPort=0;
	ENUM_CM_RETURN rt;


	if(-1==testSock)
	{
		if(ENUM_CM_TRUE!=CM_UDP_Init())
		{
			printf("create udp socket for sdt test info output failed!can't init CM UDP lib\n");
			return;
		}

		/*- 创建测试发送socket(非阻塞) */
		rt = CM_UDP_Socket_Create(&testSock, CM_TRUE, 0u,0u,0u);

		if(ENUM_CM_TRUE != rt)
		{
			printf("create udp socket for sdt test info output failed!bind Ip:0,err:%d\n",rt);
		}
		else
		{
			printf("create udp socket for sdt test info output success!bind Ip:0\n");
		}
	}

	if(-1!=testSock)
	{
		destPort=60031u;

		rt = CM_UDP_Send_Msg(testSock, pSendBuf, bufLen, inet_addr(IP), destPort);

		if(ENUM_CM_TRUE != rt)
		{
			//printf("one sdt test info output with udp failed!(info:%s)\n",pSendBuf);
		}
	}

	return;

}
