/*
 * CM_ShareMem.c
 *
 *  Created on: 2022年1月24日
 *      Author: TCT_balabala
 */


#include "CM_ShareMem.h"
#include "CM_Memory.h"
#include "CM_String.h"
#include "CM_SemNamed.h"
#include "os_al.h"

#include "MSCP_Define.h"


/************************************
 * 函数名称:  CM_ShareMem_init
 * 函数功能:  共享内存初始化
 * 函数类型:  public
 * 返 回 值:  STATUS       :操作结果(ERROR:失败，OK:成功)
 * 参数说明:
 * 作    者:
 * 日    期:
 * 修改记录:
***************************************/
void CM_ShareMem_init(CM_STU_SHARE_MEM* pSharemem, const CM_CHAR *sem_name, const CM_CHAR *sharemem_name, CM_UINT32 sharemem_size)
{
	CM_INT32		fd = 0u;
	CM_UINT8*		shammry_pData = NULL;
	CM_INT32  		rtn = 0u;

	/*- 断言共享内存结构体指针是否为空 */
	CM_ASSERT(NULL != pSharemem);

	/*- 断言信号量名字、共享内存名字buff指针是否为空 */
	CM_ASSERT((NULL != sem_name) && (NULL != sharemem_name));

	/*- 拷贝信号量名字字符串到结构体中进行信号量名字管理*/
	CM_StrCpy(pSharemem->semName, sizeof(pSharemem->semName), sem_name);

	/*- 拷贝共享内存名字字符串到结构体中进行共享内存名字管理*/
	CM_StrCpy(pSharemem->shaMemName, sizeof(pSharemem->shaMemName), sharemem_name);

	/*- 打开信号量 */
	pSharemem->semSync = CM_SemNamed_Open(sem_name);

	/*- 断言检测信号量是否打开失败 */
	CM_ASSERT((sem_t*)SEM_FAILED != pSharemem->semSync);

	/* create a new SHM object 打开共享内存*/
	fd = osal_shm_open(sharemem_name);

	/*- 断言检测共享内存是否打开失败 */
	CM_ASSERT(-1 != fd);

	/* set object size  将文件修改为参数大小*/
	rtn = osal_ftruncate(fd, sharemem_size);

	if ( -1 == rtn)
	{
		printf("ftruncate error\n");
		osal_close(fd);
	}
	else
	{
		/* Map shared memory object in the address space of the process 创建内存映射*/
		shammry_pData = (CM_UINT8*)osal_mmap(0, sharemem_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		if (shammry_pData == (CM_UINT8*)MAP_FAILED)
		{
			printf("mmap error\n");
			osal_close(fd);
		}
		else
		{
			pSharemem->ShaMem_pData = shammry_pData;
			pSharemem->ShaMem_size =sharemem_size;
		}
	}

	CM_ASSERT(-1 != rtn);

	CM_ASSERT((CM_UINT8*)MAP_FAILED != shammry_pData);

	return;
}

//************************************
// 函数名称:  CM_ShareMem_write
// 函数功能:  向共享内存写入数据
// 函数类型:  public
// 返 回 值:  STATUS       :操作结果(ERROR:失败，OK:成功)
// 参数说明:
// 作    者:
// 日    期:
// 修改记录:
//************************************
CM_INT32 CM_ShareMem_write(CM_STU_SHARE_MEM* pSharemem, CM_UINT8 buffer[], CM_UINT32 datalen, CM_UINT32 offset)
{

	/*- 入参指针防空处理 */
	CM_ASSERT((NULL != pSharemem) && (NULL != buffer));

	CM_SemNamed_Wait(pSharemem->semSync);

	/*向内存写入数据*/
	CM_Memcpy(&pSharemem->ShaMem_pData[offset], (pSharemem->ShaMem_size - offset), buffer, datalen);

	CM_SemNamed_Post(pSharemem->semSync);

	return 1;
}

/************************************
 * 函数名称:  CM_ShareMem_read
 * 函数功能:  从共享内存读取数据
 * 函数类型:  public
 * 返 回 值:  STATUS       :操作结果(ERROR:失败，OK:成功)
 * 参数说明:  unsigned char *buffer  :输出读取的数据
 * 参数说明:  int length             :读取数据的长度
 * 作    者:
 * 日    期:
 * 修改记录:
 ************************************/
CM_INT32 CM_ShareMem_read(CM_STU_SHARE_MEM* pSharemem, CM_UINT8 buffer[], CM_UINT32 buffer_Size, CM_UINT32 datalen, CM_UINT32 offset)
{
	/*- 入参指针防空处理 */
	CM_ASSERT((NULL != pSharemem) && (NULL != buffer));

	/*- 断言检测共享内存空间大于拷贝数据长度*/
	CM_ASSERT(pSharemem->ShaMem_size - offset >= datalen);

	CM_SemNamed_Wait(pSharemem->semSync);

	/*- 从共享内存中读取指定长度数据到指定buffer*/
	CM_Memcpy(buffer,buffer_Size, &(pSharemem->ShaMem_pData[offset]), datalen);

	CM_SemNamed_Post(pSharemem->semSync);

	return 1;
}

void* CM_shrmem_alloc(const CM_CHAR *sharemem_name, CM_UINT32 sharemem_size)
{
	CM_INT32		fd = 0u;
	void*			shammry_pData = NULL;
	CM_INT32  		rtn = 0u;

	/* create a new SHM object 打开共享内存*/
	fd = osal_shm_open(sharemem_name);

	/*- 断言检测共享内存是否打开失败 */
	CM_ASSERT(-1 != fd);

	/* set object size  将文件修改为参数大小*/
	rtn = osal_ftruncate(fd, sharemem_size);

	if ( -1 == rtn)
	{
		printf("ftruncate error\n");
		osal_close(fd);
	}
	else
	{
		/* Map shared memory object in the address space of the process 创建内存映射*/
		shammry_pData = (CM_UINT8*)osal_mmap(0, sharemem_size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
		if (shammry_pData == (CM_UINT8*)MAP_FAILED)
		{
			printf("mmap error\n");
			osal_close(fd);
		}
		else
		{
			;
		}
	}

	CM_ASSERT(-1 != rtn);

	CM_ASSERT((CM_UINT8*)MAP_FAILED != shammry_pData);

	return shammry_pData;
}
