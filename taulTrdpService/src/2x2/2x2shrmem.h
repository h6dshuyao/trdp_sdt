/*
 * rtelib_2x2_shrmem.c
 *
 *  Created on: 2022年2月10日
 *      Author: TCT_balabala
 */

#ifndef SHRMEM_2X2_H
#define SHRMEM_2X2_H
#include "CM_Types.h"
#include "CM_Mutex.h"
#include "CM_Time.h"

#define TOLERABLE_DELAY 2000000 //容许的2x2服务更新延迟 (ns)

#ifndef MAX_REDID_NUM
#define MAX_REDID_NUM (256u)
#endif

#define	SHRMEM_2X2TRDP_NAME		"/shrmem_2x2trdp"

typedef struct{

	CM_UINT64  redunsvc_sn;  /**< 主备冗余服务序列号*/

	CM_UINT8   sys_state;	 /**< 系统状态*/

}STU_SYS_STATE;

typedef struct{

	CM_UINT32  redId; 	/**< 冗余组Id*/

	CM_BOOL		leder;	 /**< 状态 */

}STU_REDID_INFO;

typedef struct{

	CM_UINT64		  cycle;							/**< 主备冗余服务运行周期*/

	CM_UINT32		  redIdNum;							/**< 2x2冗余服务管理的应用任务数量*/

	STU_REDID_INFO		redIdStateList[MAX_REDID_NUM];	/**< 每个应用的周期开始基准时刻*/

	CM_UINT64		  timestamp;						/**< 更新时间戳*/

}STU_2X2SVC_SHRMEMINFO;	/**< 需要主备冗余服务更新的共享内存信息*/


typedef struct{

	CM_UINT64		  timestamp;						/**< 更新时间戳*/

}STU_TRDP_SHRMEMINFO;	/**< 需要trdp更新的共享内存信息*/

#ifdef __cplusplus
extern "C"
{
#endif
/**
* @brief 共享内存创建及初始化
* @param[in] app_num 应用的个数
* @param[in] app_id[] 应用id存储buffer
* @return 返回初始化结果
* @note	只允许由共享内存其中之一的使用方（主备冗余服务）调用此函数进行共享内存的初始化，要求主备冗余服务先启动。
*/
CM_UINT8 shrmem_2x2_init(IN CM_UINT32 cycle,CM_UINT16 app_num, IN CM_UINT32 app_id[]);

/**
* @brief 共享内存打开
* @note	不负责共享内存初始化的使用方（trdp任务），使用此函数申请获取共享内存空间。
*/
void shrmem_2x2_open(void);

/**
* @brief 主备冗余服务写共享内存
* @param[in] p_2x2svcinfo 待写入主备冗余服务共享内存的数据结构体指针；
* @return 写入结果
* 		-0 写入成功；
* 		-非0 写入失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_redunsvcWrite( IN STU_2X2SVC_SHRMEMINFO * p_2x2svcinfo);

/**
* @brief 应用任务读共享内存
* @param[out] p_2x2svcinfo 承接主备冗余服务共享内存的数据结构体指针；
* @return 读结果
* 		-0 读成功；
* 		-非0 读失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_trdpRead(OUT STU_2X2SVC_SHRMEMINFO * p_2x2svcinfo);

/**
* @brief 主备冗余服务读共享内存
* @param[out] p_apptaskinfo 承接应用任务共享内存的数据结构体指针；
* @return 读结果
* 		-0 读成功；
* 		-非0 读失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_redunsvcRead(OUT STU_TRDP_SHRMEMINFO * p_trdpinfo);

/**
* @brief 应用任务写共享内存
* @param[in] p_appstate 待写入应用任务共享内存的当前应用数据结构体指针；
* @return 写结果
* 		-0 写成功；
* 		-非0 写失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_trdpWrite(OUT STU_TRDP_SHRMEMINFO* p_appstate);
#ifdef __cplusplus
}
#endif
#endif /* SRC_RTE_RTELIB_2X2_SHRMEM_H_ */
