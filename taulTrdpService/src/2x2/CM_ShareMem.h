/*
 * CM_ShareMem.h
 *
 *  Created on: 2022年1月24日
 *      Author: TCT_balabala
 */

#ifndef SRC_RTE_CM_SHAREMEM_H_
#define SRC_RTE_CM_SHAREMEM_H_
#include "CM_Types.h"
#include "CM_Sem.h"
#if defined (VXWORKS)
#include <vxWorks.h>
#include <sys/mman.h>
#include <unistd.h>
#include <semaphore.h>

#elif ((defined (WIN32))||(defined(WIN64)))

#endif

#if defined (LINUX)
#include <sys/mman.h>
#include <unistd.h>
#include <semaphore.h>
#endif

typedef struct{
//    CM_UINT8        state;            /* handle from related subscribe */
    CM_UINT8*		ShaMem_pData;       /*-< 共享内存空间首地址*/
    CM_UINT32 		ShaMem_size;		/*-< 共享内存空间大小*/
    CM_SEM_T		semSync;			/*-< 同步信号量：确保共享内存独占访问*/
    CM_CHAR			semName[64];		/*-< 有名信号量名字*/
    CM_CHAR			shaMemName[64];		/*-< 共享内存文件名字*/
}CM_STU_SHARE_MEM;

#ifdef __cplusplus
extern "C"
{
#endif

void CM_ShareMem_init(CM_STU_SHARE_MEM* pSharemem, const CM_CHAR *sem_name, const CM_CHAR *sharemem_name, CM_UINT32 sharemem_size);

CM_INT32 CM_ShareMem_write(CM_STU_SHARE_MEM* pSharemem, CM_UINT8 buffer[], CM_UINT32 datalen, CM_UINT32 offset);

CM_INT32 CM_ShareMem_read(CM_STU_SHARE_MEM* pSharemem, CM_UINT8 buffer[], CM_UINT32 buffer_Size, CM_UINT32 datalen, CM_UINT32 offset);

void* CM_shrmem_alloc(const CM_CHAR *sharemem_name, CM_UINT32 sharemem_size);

#ifdef __cplusplus
}
#endif
#endif /* SRC_RTE_CM_SHAREMEM_H_ */
