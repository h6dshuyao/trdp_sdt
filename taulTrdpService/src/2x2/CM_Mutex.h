/**
* @file CM_Mutex.h
* @brief 实现了互斥信号量的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* <tr><td>2021.02.01  <td>V1.2     <td>GT-BU      <td>为一体化项目扩展vxWorks系统进程间互斥信号量的兼容
* </table>
*/
#ifndef _CM_MUTEX_H
#define _CM_MUTEX_H

#include "CM_Types.h"

#ifdef VXWORKS
#include <semLib.h>
#include <b_pthread_mutex_t.h>
#include <pthread.h>
#endif

#ifdef QNX
#include <pthread.h>
#endif

#ifdef LINUX
#include <pthread.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */

#ifdef WIN32
    typedef HANDLE  CM_MUTEX_T;  /**< 互斥信号量 */
#endif
#ifdef QNX
    typedef pthread_mutex_t* CM_MUTEX_T;/**< 互斥信号量 */

    typedef pthread_mutex_t CM_MUTEX_IPC;/**< 进程间通信使用互斥信号量*/
#endif
#ifdef VXWORKS
    typedef SEM_ID          CM_MUTEX_T;/**< 互斥信号量 */
    typedef pthread_mutex_t CM_MUTEX_IPC;/**< 进程间通信使用互斥信号量*/
    typedef pthread_mutexattr_t CM_MUTEX_ATTR_T;/**< 线程互斥锁属性*/
#endif
    
#ifdef LINUX
    typedef pthread_mutex_t* CM_MUTEX_T;/**< 互斥信号量 */

    typedef pthread_mutex_t CM_MUTEX_IPC;/**< 进程间通信使用互斥信号量*/

    typedef pthread_mutexattr_t CM_MUTEX_ATTR_T;/**< 线程互斥锁属性*/
#endif
    /**
    * @brief CM_Mutex_Init
    *
    * 初始化互斥器.
    * @param[out] mutex
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Mutex_Init(INOUT CM_MUTEX_T* pMutex);

    /**
    * @brief CM_Mutex_Destroy
    *
    * 销毁互斥器.
    * @param[in] mutex
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Mutex_Destroy(IN CM_MUTEX_T mutex);

    /**
    * @brief CM_Mutex_Lock
    *
    * 获得互斥器资源.
    * @param[in] mutex
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Mutex_Lock(IN CM_MUTEX_T mutex);

    /**
	* @brief CM_Mutex_Lock
	*
	* 计时获得互斥器资源
	* @param[in] mutex
	* @param[in] dwWaitTime 锁定时间（毫秒）
	* @return ENUM_CM_RETURN
	*/
    ENUM_CM_RETURN CM_Mutex_Lock_Time(IN CM_MUTEX_T mutex, IN CM_UINT32 dwWaitTime);

    /**
    * @brief CM_Mutex_Unlock
    *
    * 释放互斥器.
    * @param[in] mutex
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Mutex_Unlock(IN CM_MUTEX_T mutex);

    /**
    * @brief 互斥信号量初始化
    * @param pMutex 信号量指针
    * @return 执行结果
    * - ENUM_CM_TRUE 成功
    * - ENUM_CM_FALSE 失败
    */
    ENUM_CM_RETURN CM_Mutex_IPC_Init(INOUT CM_MUTEX_IPC* pMutex);

    /**
    * @brief 锁定信号量
    * @param mutex 互斥信号量
    * @return 执行结果
    * - ENUM_CM_TRUE 成功
    * - ENUM_CM_FALSE 失败
    */
    ENUM_CM_RETURN CM_Mutex_IPC_Lock(IN CM_MUTEX_IPC* pMutex);

    /**
    * @brief 解锁信号量
    * @param mutex 信号量
    * @return 执行结果
    * - ENUM_CM_TRUE 成功
    * - ENUM_CM_FALSE 失败
    */
    ENUM_CM_RETURN CM_Mutex_IPC_Unlock(IN CM_MUTEX_IPC* pMutex);

    /**
    * @brief 销毁互斥信号量
    * @param mutex 互斥信号量
    * @return 执行结果
    * - ENUM_CM_TRUE 成功
    * - ENUM_CM_FALSE 失败
    */
    ENUM_CM_RETURN CM_Mutex_IPC_Destroy(IN CM_MUTEX_IPC* pMutex);


#ifdef __cplusplus
}

#endif /**< __cplusplus */

#endif /**< _CM_MUTEX_H */
