/**
* @file CM_Memory.h
* @brief 实现了内存操作的的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/

#ifndef _CM_MEMORY_H_
#define _CM_MEMORY_H_

#include "CM_Types.h"

#ifdef __cplusplus
extern "C"
{
#endif

    /* 内存填充 */
    void CM_Memset(OUT void* destination, IN CM_UINT8 value, IN CM_SIZE_T len);

    /* 内存分配 */
    void* CM_Malloc(IN CM_SIZE_T len);

    /* 内存释放 */
    void CM_Free(INOUT void* p);

    /* 内存复制 */
    ENUM_CM_RETURN CM_Memcpy(OUT void * destination, IN CM_SIZE_T destSize, IN const void * source, IN CM_SIZE_T count);

    /* 内存比较 */
    CM_INT32 CM_Memcmp(IN const void* pBufA, IN const void* pBufB, IN CM_SIZE_T len);

#ifdef __cplusplus
}
#endif

#endif


