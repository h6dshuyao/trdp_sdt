/**
* @file CM_Atomic.h
* @brief 原子操作
* @author GT-BU
* @date 2021.05.01
* @version V1.0
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2021.05.01  <td>V1.0     <td>GT-BU      <td>创建初始版本
* </table>
*/
#ifndef _CM_ATOMIC_H
#define _CM_ATOMIC_H

#include "CM_Types.h"

#ifdef QNX
#include <stdatomic.h>
#endif

#ifdef LINUX
#include <stdatomic.h>
#endif

#if QNX
#define CM_ATOMIC _Atomic

#elif LINUX
#define CM_ATOMIC _Atomic

typedef atomic_ullong CM_ATOMIC_UINT64;

#else
#define CM_ATOMIC
#endif



#if QNX
#define CM_ATOMIC_STORE(PTR,VAL) atomic_store((PTR),(VAL))
#define CM_ATOMIC_LOAD(PTR) atomic_load(PTR)
#define CM_ATOMIC_ADD(PTR,VAL) atomic_fetch_add((PTR),(VAL))
#define CM_ATOMIC_SUB(PTR,VAL) atomic_fetch_sub((PTR),(VAL))
#define CM_ATOMIC_AND(PTR,VAL) atomic_fetch_and((PTR),(VAL))
#define CM_ATOMIC_OR(PTR,VAL) atomic_fetch_or((PTR),(VAL))
#define CM_ATOMIC_XOR(PTR,VAL) atomic_fetch_xor((PTR),(VAL))
#define CM_ATOMIC_CLEAR(PTR) atomic_flag_clear(PTR)
#elif LINUX

#define CM_ATOMIC_STORE(PTR,VAL) atomic_store((PTR),(VAL))
#define CM_ATOMIC_LOAD(PTR) atomic_load(PTR)
#define CM_ATOMIC_ADD(PTR,VAL) atomic_fetch_add((PTR),(VAL))
#define CM_ATOMIC_SUB(PTR,VAL) atomic_fetch_sub((PTR),(VAL))
#define CM_ATOMIC_AND(PTR,VAL) atomic_fetch_and((PTR),(VAL))
#define CM_ATOMIC_OR(PTR,VAL) atomic_fetch_or((PTR),(VAL))
#define CM_ATOMIC_XOR(PTR,VAL) atomic_fetch_xor((PTR),(VAL))
#define CM_ATOMIC_CLEAR(PTR) atomic_flag_clear(PTR)

#else
#define CM_ATOMIC_STORE(PTR,VAL) (*(PTR) = (VAL))
#define CM_ATOMIC_LOAD(PTR) (*(PTR))
#define CM_ATOMIC_ADD(PTR,VAL) (*(PTR) += (VAL))
#define CM_ATOMIC_SUB(PTR,VAL) (*(PTR) -= (VAL))
#define CM_ATOMIC_AND(PTR,VAL) (*(PTR) = (*(PTR))&(VAL))
#define CM_ATOMIC_OR(PTR,VAL) (*(PTR) = (*(PTR))|(VAL))
#define CM_ATOMIC_XOR(PTR,VAL) (*(PTR) = (*(PTR))^(VAL))
#define CM_ATOMIC_CLEAR(PTR) (*(PTR) = 0u)
#endif

#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */
    
#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< _CM_TYPES_H */

