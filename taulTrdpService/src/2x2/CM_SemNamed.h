/*
 * CM_SemNamed.h
 *
 *  Created on: 2022年1月24日
 *      Author: TCT_balabala
 */

#ifndef SRC_RTE_CM_SEMNAMED_H_
#define SRC_RTE_CM_SEMNAMED_H_
#include "CM_Types.h"
#include "CM_Sem.h"

#ifdef VXWORKS

#endif

#ifdef QNX
#endif

#ifdef LINUX
#include <fcntl.h>
#endif

CM_SEM_T CM_SemNamed_Open(const CM_CHAR *sem_name);

void CM_SemNamed_Wait(CM_SEM_T sem);

void CM_SemNamed_Post(CM_SEM_T sem);

 #ifdef __cplusplus
 }

 #endif /**< __cplusplus */



#endif /* SRC_RTE_CM_SEMNAMED_H_ */
