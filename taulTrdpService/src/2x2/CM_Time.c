/**
* @file CM_Time.c
* @brief 时钟相关处理
* @author GT-BU
* @date 2021.08.01
* @version V1.0
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2021.08.01  <td>V1.0     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#include "CM_Time.h"
#ifdef VXWORKS
#include <unistd.h>
#elif LINUX
#include <unistd.h>
#endif


/**
* @brief 获取精确上电时间
* @return 系统上电至调用时刻的ns数
* @details 从系统上电开始计时，不受时钟同步等操作的影响。
*/
CM_UINT64 CM_GetTick(void)
{
    /*- 变量初始化 */
    CM_UINT64 tick = 0llu;
    CM_TIMESPEC_T ts = { 0 };
    /*- 获取精确时间 */
#ifdef WIN32
    (void)timespec_get(&ts, TIME_UTC);
#elif QNX
    clock_gettime(CLOCK_MONOTONIC, &ts);

#elif LINUX
    clock_gettime(CLOCK_MONOTONIC, &ts);
#elif VXWORKS
    clock_gettime(CLOCK_MONOTONIC, &ts);
#endif

    /*- 将时间格式转换为ns数 */
    tick = ((CM_UINT64)ts.tv_sec * 1000000000u) + (CM_UINT64)ts.tv_nsec;
    /*- 返回精确时间 */
    return tick;
}


/**
* @brief 获取精确上电时间
* @return 系统上电至调用时刻的ns数
* @details 从系统上电开始计时，不受时钟同步等操作的影响。
* 未知原因，64位数据在return 之后变成了32位
*
*/
void CM_GetTick_64(CM_UINT64 *pTick)
{
    /*- 变量初始化 */
    CM_UINT64 tick = 0llu;
    CM_TIMESPEC_T ts = { 0 };
    /*- 获取精确时间 */
#ifdef WIN32
    (void)timespec_get(&ts, TIME_UTC);
#elif QNX
    clock_gettime(CLOCK_MONOTONIC, &ts);
#elif LINUX
    clock_gettime(CLOCK_MONOTONIC, &ts);
#elif VXWORKS
    clock_gettime(CLOCK_MONOTONIC, &ts);
#endif


    /*- 将时间格式转换为ns数 */
    tick = ((CM_UINT64)ts.tv_sec * 1000000000u) + (CM_UINT64)ts.tv_nsec;
    /*- 返回精确时间 */

    *pTick =  tick;
}

/**
* @brief sleep延时
* @param ms 要延时的ms数
* @details 实际延时偏差在1ms量级
*/
void CM_Sleep(CM_UINT32 ms)
{
    /*- 延时指定时间 */
#ifdef WIN32
    Sleep((unsigned long)ms);
#elif VXWORKS
    sleep(ms * 1000u);
#else
    usleep(ms * 1000u);
#endif
}

