/**
* @file CM_Mutex.c
* @brief 实现了互斥信号量的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#include "CM_Mutex.h"

#ifdef WIN32
/**
* @brief 互斥信号量初始化
* @param pMutex 信号量指针
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Init(INOUT CM_MUTEX_T* pMutex)
{
    /*- 函数执行结果初始化为失败 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;/*函数返回值*/
    /*- 输入参数是否合法 */
    if (NULL != pMutex)
    {
        /*- 创建互斥信号量 */
        *pMutex = CreateMutex(NULL, CM_FALSE, (CM_UINT16*)NULL);
        /*- 信号量是否创建成功 */
        if (0 != (*pMutex))
        {
            /*- 函数执行结果为成功 */
            rt = ENUM_CM_TRUE;
        }
    }
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 销毁互斥信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Destroy(IN CM_MUTEX_T mutex)
{
    /*- 初始化函数执行结果为失败 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE; /* 函数返回值 */
    CM_INT32 ret = 0; /* 临时变量 */

    /*- 关闭互斥信号量句柄 */
    ret = CloseHandle(mutex);

    /*- 是否关闭成功 */
    if (0 != ret)
    {
        /*- 函数执行结果为成功 */
        rt = ENUM_CM_TRUE;
    }
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量（指定等待时间）
* @param mutex 互斥信号量
* @param dwWaitTIme 等待时间（毫秒）
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock_Time(IN CM_MUTEX_T mutex, IN CM_UINT32 dwWaitTime)
{
    ENUM_CM_RETURN rt = ENUM_CM_FALSE; /* 函数返回值 */
    CM_UINT32 ret = 0u; /* 临时变量 */

    /*- 按输入时长等待信号量*/
    ret = (CM_UINT32)WaitForSingleObject(mutex, (unsigned long)dwWaitTime);

    /*- 是否等到了信号量 */
    if ((CM_UINT32)WAIT_OBJECT_0 == ret)  /* parasoft-suppress MISRA2004-10_1_a-3 "WIN API的宏WAIT_OBJECT_0不符合编码规范" */
    {
        /*- 执行结果为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
    {
        /*- 执行结果为失败 */
        rt = ENUM_CM_FALSE;
    }
    return rt;
}
/**
* @brief 锁定信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock(IN CM_MUTEX_T mutex)
{
    /*- 锁定信号量（等待无限长时间）*/
    return CM_Mutex_Lock_Time(mutex, INFINITE);
}

/**
* @brief 解锁信号量
* @param mutex 信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Unlock(IN CM_MUTEX_T mutex)
{
    /*- 初始化为执行失败 */
    ENUM_CM_RETURN rt = ENUM_CM_FALSE; /* 函数返回值 */
    CM_INT32 ret = 0; /* 临时变量 */

    /*- 释放信号量 */
    ret = ReleaseMutex(mutex);

    /*- 是否释放成功 */
    if (0 != ret)
    {
        /*- 执行结果为成功 */
        rt = ENUM_CM_TRUE;
    }
    /*- 返回执行结果 */
    return rt;
}
#endif