/**
* @file CM_Time.h
* @brief 时钟相关处理
* @author GT-BU
* @date 2021.08.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2021.08.01  <td>V1.0     <td>GT-BU      <td>创建初始版本
* </table>
*/

#ifndef _CM_TIME_H_
#define _CM_TIME_H_

#include "CM_Types.h"

#ifdef __cplusplus
extern "C"
{
#endif

    /*注意CM_UINT64需要用%lld打印*/
    CM_UINT64 CM_GetTick(void);

    /* 延时给定时间长度 ms */
    void CM_Sleep(CM_UINT32 ms);
	
    void CM_GetTick_64(CM_UINT64 *pTick);

#ifdef __cplusplus
}
#endif

#endif


