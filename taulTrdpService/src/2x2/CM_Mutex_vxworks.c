/**
* @file CM_Mutex.c
* @brief 实现了互斥信号量的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#include "CM_Mutex.h"

#ifdef VXWORKS

#define EOK   0  /* No error */
/**
* @brief 互斥信号量初始化
* @param pMutex 信号量指针
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Init(INOUT CM_MUTEX_T* pMutex)
{
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 输入参数是否合法 */
    if (NULL != pMutex)
    {
        /*- 创建互斥信号量 */
        *pMutex = semBCreate(SEM_Q_FIFO, SEM_FULL);

        /*- 创建是否成功 */
        if (NULL != (*pMutex))
        {
            /*- 函数执行结果置为成功 */
            rt = ENUM_CM_TRUE;
        }
    }
    return rt;
}

/**
* @brief 销毁互斥信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Destroy(IN CM_MUTEX_T mutex)
{
    STATUS ret = OK;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 锁定信号量 */
    ret = semTake(mutex, WAIT_FOREVER);
    /*- 锁定是否成功 */
    if (OK == ret)
    {
        /*- 销毁信号量 */
        semDelete(mutex);
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock(IN CM_MUTEX_T mutex)
{
    /*- 执行锁定信号量，等待无限长时间 */
    return CM_Mutex_Lock_Time(mutex, WAIT_FOREVER);
}

/**
* @brief 锁定信号量（指定等待时间）
* @param mutex 互斥信号量
* @param dwWaitTIme 等待时间
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock_Time(IN CM_MUTEX_T mutex, IN CM_UINT32 dwWaitTime)
{
    STATUS ret = OK;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    /*- 执行锁定信号量，等待给定时间*/
    ret = semTake(mutex, dwWaitTime);
    /*- 锁定是否成功 */
    if (OK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    return rt;
}

/**
* @brief 解锁信号量
* @param mutex 信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Unlock(IN CM_MUTEX_T mutex)
{
    STATUS ret = OK;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 释放信号量*/
    ret = semGive(mutex);
    /*- 释放是否成功 */
    if (OK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 互斥信号量初始化
* @param pMutex 信号量指针
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Init(INOUT CM_MUTEX_IPC* pMutex)
{
    /*- 变量初始化 */
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    CM_MUTEX_ATTR_T mutx_attr;
    /*- 入参是否合法 */
    if (NULL != pMutex)
    {
        /*-初始化互斥锁属性 */
        pthread_mutexattr_init(&mutx_attr);

        /*-将线程锁改变为进程间共用属性*/
        pthread_mutexattr_setpshared(&mutx_attr,PTHREAD_PROCESS_SHARED);

        /* vxworks pthread.h不提供该功能：将线程锁设置为robust属性，线程kill后，会自动释放资源 */

        /*- 初始化互斥信号量 */
        ret = pthread_mutex_init(pMutex, &mutx_attr);

        /*-销毁互斥锁属性对象 */
        pthread_mutexattr_destroy(&mutx_attr);

		/*- 创建是否成功 */
		if (EOK == ret)
		{
			/*- 函数执行结果置为成功 */
			rt = ENUM_CM_TRUE;
		}
		else
		{
			/*- 无处理 */
		}

    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Lock(IN CM_MUTEX_IPC* pMutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 执行锁定信号量操作 */
    ret = pthread_mutex_lock(pMutex);
    /*- 锁定是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
    {
    	;
    }
    
    /*- 返回函数执行结果 */
    return rt;
}
/**
* @brief 解锁信号量
* @param mutex 信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Unlock(IN CM_MUTEX_IPC* pMutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    /*- 解锁信号量 */
    ret = pthread_mutex_unlock(pMutex);
    /*- 解锁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 销毁互斥信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Destroy(IN CM_MUTEX_IPC* pMutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 销毁互斥信号量 */
    ret = pthread_mutex_destroy(pMutex);

    /*- 销毁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

#endif


