


#ifdef linux
#define __USE_GNU
#include <linux/if.h>
#include <sched.h>
#elif defined(VXWORKS)
#include <net/if.h>
#include "cpusetCommon.h"
#else
#include <net/if.h>
#endif

#include "CM_Time.h"
#include "PFM_Utils.h"


CM_UINT8 s_stopflag = 0u; /** @static 全局宕机标志 */
/**
* @brief 断言
* @param x 断言结果
* @param filename 文件路径
* @param func 函数名
* @param line 行号
*/
void MSCP_ASSERT(int x, const char *filename, const char *func, int line)
{
    /*- 全局宕机标志有效 */
    if (0x55u == s_stopflag)
    {
        /*- 控制台输出停机位置 */
        printf("shut down at %s:%s:%d\n", filename, func, line);

        /*- 宕机死循环 */
        while (1)
        {
            /*- 休眠5000ms */
            CM_Sleep(5000u);
        }
    }
    else
    {
        /*- 无处理 */
    }

    /*- 断言结果为FALSE */
    if (0 == x)
    {
    	/*- 延时等待 */
        printf("ASSERT: %s:%s:%d\n", filename, func, line);

        /*- 宕机死循环 */
        while (1)
        {
            s_stopflag = 0x55u;
            /* TODO 点error 灯*/
            //TODO 可以添加宕机打印
            //向对系发送宕机报文
            CM_Sleep(5000u);
        }
    }
	else
	{
		/*- 无操作 */
	}

    /*- 返回 */
    return;
}
