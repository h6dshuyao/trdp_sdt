/**
* @file CM_Mutex.c
* @brief 实现了互斥信号量的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#include "CM_Mutex.h"

#ifdef QNX
#include "time.h"
#include "CM_Memory.h"
/**
* @brief 互斥信号量初始化
* @param pMutex 信号量指针
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Init(INOUT CM_MUTEX_T* pMutex)
{
    /*- 变量初始化 */
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 入参是否合法 */
    if (NULL != pMutex)
    {
        /*- 创建互斥信号量 */
        *pMutex = (pthread_mutex_t*)CM_Malloc(sizeof(pthread_mutex_t));
        if(*pMutex != NULL)
        {
			ret = pthread_mutex_init(*pMutex, NULL);

			/*- 创建是否成功 */
			if (EOK == ret)
			{
				/*- 函数执行结果置为成功 */
				rt = ENUM_CM_TRUE;
			}
	        else
			{
				/*- 无处理 */
			}
        }
        else
		{
			/*- 无处理 */
		}
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 销毁互斥信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Destroy(IN CM_MUTEX_T mutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 销毁互斥信号量 */
    ret = pthread_mutex_destroy(mutex);

    /*- 销毁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量（指定等待时间）
* @param mutex 互斥信号量
* @param dwWaitTime 等待时间（毫秒）
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock_Time(IN CM_MUTEX_T mutex, IN CM_UINT32 dwWaitTime)
{
	CM_INT32 ret = 0;
	CM_TIMESPEC_T time_sruct;
	ENUM_CM_RETURN rt = ENUM_CM_FALSE;

	time_sruct.tv_sec = (CM_INT32)dwWaitTime / 1000;
	time_sruct.tv_nsec = ((CM_INT32)dwWaitTime % 1000) * CM_MILLION;

    /*- 执行锁定信号量操作 */
    ret = pthread_mutex_timedlock(mutex,&time_sruct);

    /*- 锁定是否成功 */
    if (EOK == ret)
    {
	    /*- 函数执行结果置为成功 */
	    rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}

    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock(IN CM_MUTEX_T mutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 执行锁定信号量操作 */
    ret = pthread_mutex_lock(mutex);
    /*- 锁定是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}
/**
* @brief 解锁信号量
* @param mutex 信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Unlock(IN CM_MUTEX_T mutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    /*- 解锁信号量 */
    ret = pthread_mutex_unlock(mutex);
    /*- 解锁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

#endif
