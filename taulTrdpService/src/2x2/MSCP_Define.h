#pragma once



#ifdef __cplusplus
extern "C" {
#endif

#define MSCP_CYC_TIME (10u)  /* 安全平台运行周期，单位：ms */



/* C++ test 用宏 */
#if 0
#define snprintf  _snprintf 
#define __func__ __FILE__
#endif

/* #define CM_ASSERT assert */
/* 使用平台的宕机函数 */
#define CM_ASSERT(x) MSCP_ASSERT((int)(x), __FILE__,__func__, __LINE__)
/* 平台实现的宕机函数 */
extern void MSCP_ASSERT(int x, const char* filename, const char* func, int line);

/** 双机热备状态 */
typedef enum {
    ENUM_2x2_SHUTDOWN = 0, /**< 宕机 */
    ENUM_2x2_FOLLOW,      /**< 跟随 */
    ENUM_2x2_PRE_MASTER,   /**< 预升主 */
    ENUM_2x2_MASTER,       /**< 主系 */
    ENUM_2x2_SLAVE,         /**< 备系 */
} ENUM_2x2_STATE_T;


#ifdef __cplusplus
}
#endif
