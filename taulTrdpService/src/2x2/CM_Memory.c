/**
* @file CM_Memory.c
* @brief 实现了内存操作的的封装
* @author GT-BU
* @date 2021.02.01
* @version V1.1
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2017.12.20  <td>V1.0     <td>MSCP项目组 <td>创建初始版本
* <tr><td>2021.02.01  <td>V1.1     <td>GT-BU      <td>TSCP项目集成、更新
* </table>
*/
#include "CM_Memory.h"


/**
* @brief 用指定值填充内存
* @param destination 待填充的内存区
* @param value 填充值
* @param len 内存区大小
*/
void CM_Memset(OUT void * destination, IN CM_UINT8 value, IN CM_SIZE_T len)
{
#if 1
	memset(destination, (int)value, len);
#else
    /*- 变量初始化 */
	CM_UINT32 debug_I = 0u;/* 循环变量 */
    /*- 输入是否合法 */
	if ((NULL != destination) && (len != (CM_SIZE_T)0))
	{
        /*- 初始化循环变量为0|是否达到内存区末尾|循环变量自增 */
		for (debug_I = 0u; debug_I < len; debug_I++)
		{
            /*- 用指定值填充 */
			*((CM_UINT8*)destination + debug_I) = value;
		}
	}
#endif
}

/**
* @brief 动态分配内存
* @param len 要分配的大小
* @return 新分配内存的首地址
*/
void* CM_Malloc(IN CM_SIZE_T len)
{
	void* p = NULL; /*指针*/
    /*- 内存首地址初始化为空 */

	/*- 要分配的空间是否为0 */
	if (len != (CM_SIZE_T)0)
	{
        /*- 分配内存空间 */
		p = malloc(len);
	}
    else
	{
		/*- 无处理 */
	}
    /*- 返回内存首地址 */
	return p;
}

/**
* @brief 动态释放内存
* @param p 内存指针
*/
void CM_Free(INOUT void * p)
{
    /*- 指针是否为空 */
	if (NULL != p)
	{
        /*- 释放内存 */
		free(p);
	}
    else
	{
		/*- 无处理 */
	}
}

/**
* @brief 内存复制
* @param destination 目的地址
* @param destSize 目的地址大小
* @param source 源地址
* @param count 复制数据大小
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
* @details 源地址段和目的地址段不能有重叠
*/
ENUM_CM_RETURN CM_Memcpy(OUT void * destination, IN CM_SIZE_T destSize, IN const void * source, IN CM_SIZE_T count)
{
	/*- 变量初始化 */
	ENUM_CM_RETURN ret = ENUM_CM_FALSE; /* 函数返回值 */
#if 1
	if(destSize >= count)
	{
		memcpy(destination, source, count);
		ret = ENUM_CM_TRUE;
	}
	else
	{
		ret = ENUM_CM_FALSE;
	}
#else
	CM_UINT32 debug_I = 0u; /* 循环变量 */

    /*- 输入检查是否通过 */
	if ((NULL != destination) && (NULL != source) && (count <= destSize))
	{
		/*MemCpy(destination, source, count);*/

        /*- @alias 将源地址的数据复制到目的地址 */
		for (debug_I = 0u; debug_I < count; debug_I++)
		{
			*((CM_UINT8*)destination + debug_I) = *((CM_UINT8*)source + debug_I);
		}

        /*- 执行成功 */
		ret = ENUM_CM_TRUE;
	}
#endif
	/*- 返回执行结果 */
	return ret;
}

/**
* @brief 内存比较
* @param pBufA 首地址A
* @param pBufB 首地址B
* @param len 比较长度
* @return 执行结果
* - 0 全部相同
* - 非0 不全同
*/
CM_INT32 CM_Memcmp(IN const void * pBufA,IN const void * pBufB,IN CM_SIZE_T len)
{
    /*- 变量初始化 */
	CM_INT32 ret = 1; /* 函数返回值 */
	CM_UINT32 i = 0u;/* 循环变量 */
    /*- 输入检查是否不通过  */
	if ((pBufA == NULL) || (pBufB == NULL))
	{
        /*- 比较结果为不相同 */
		ret = (CM_INT32)1;
	}
	else
	{
		/*ret = memcmp(pBufA, pBufB, len);*/
        /*- @alias 循环比较所有数据 */
		for (i = 0u; i < len; i++)
		{
			if (*((CM_UINT8*)pBufA + i) != *((CM_UINT8*)pBufB + i))
			{
				break;
			}
	        else
			{
				/* 无处理 */
			}
		}

        /*- @alias 判断比较结果 */
		if (i == len)
		{
			ret = 0;
		}
        else
		{
			/* 无处理 */
		}
	}
    /*- 输出返回值 */
	return ret;
}

