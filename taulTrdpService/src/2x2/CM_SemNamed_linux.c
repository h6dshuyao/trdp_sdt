/*
 * CM_SemNamed_linux.c
 *
 *  Created on: 2022年1月24日
 *      Author: TCT_balabala
 */
#include "CM_SemNamed.h"
#ifdef VXWORKS
#include "semaphore.h"
#endif
#ifdef LINUX


inline CM_SEM_T CM_SemNamed_Open(const CM_CHAR *sem_name)
{
	return sem_open(sem_name, O_CREAT, 1, 1);
}

inline void CM_SemNamed_Wait(CM_SEM_T sem)
{
	sem_wait(sem);
	return;
}

inline void CM_SemNamed_Post(CM_SEM_T sem)
{
	sem_post(sem);
	return;
}
#elif VXWORKS

inline CM_SEM_T CM_SemNamed_Open(const CM_CHAR *sem_name)
{
	return sem_open(sem_name, O_CREAT, 1, 1);
}

inline void CM_SemNamed_Wait(CM_SEM_T sem)
{
	sem_wait(sem);
	return;
}

inline void CM_SemNamed_Post(CM_SEM_T sem)
{
	sem_post(sem);
	return;
}
#endif


