/*
 * os_al.h
 *
 *  Created on: 2022年1月26日
 *      Author: TCT_balabala
 */

#ifndef SRC_RTE_OS_AL_H_
#define SRC_RTE_OS_AL_H_


#ifdef LINUX
#include "fcntl.h"
#include "unistd.h"
#include <sys/mman.h>
#define osal_shm_open(name)							shm_open(name, O_CREAT | O_RDWR, S_IRUSR | S_IWUSR)
#define osal_ftruncate(handle, size)				ftruncate(handle, size)
#define osal_mmap(start,lenth,prot,flag,fd,offset)	mmap(start,lenth,prot,flag,fd,offset)
#define osal_close(fd)								close(fd)
#endif

#ifdef WIN32
#endif

#ifdef VXWORKS
#include <sys/mman.h>
#include "unistd.h"
#define osal_shm_open(name)							shm_open((name), O_CREAT | O_RDWR, 0666)
#define osal_ftruncate(handle, size)				ftruncate((int)(handle),(off_t)(size))
#define osal_mmap(start,lenth,prot,flag,fd,offset)	mmap(start,lenth,prot,flag,fd,offset)
#define osal_close(fd)								close(fd)

#endif

#endif /* SRC_RTE_OS_AL_H_ */
