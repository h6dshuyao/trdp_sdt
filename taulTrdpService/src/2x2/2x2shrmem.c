/*
 * rtelib_2x2_shrmem.c
 *
 *  Created on: 2022年2月11日
 *      Author: TCT_balabala
 *
 *
 *      修改记录：1.修正CM_ASSERT(NULL == p)类错误;
 *      			2.rtelib_shrmem_2x2_appInfoInit()中增加 shrmeminfo_ipc_2x2->shrmeminfo_apptask.app_num=app_num;
 *      			3.shrmeminfo_2x2_redunsvcread()中增加p_apptaskinfo->app_num=shrmeminfo_ipc_2x2->shrmeminfo_apptask.app_num;
 *					4.读写共享平台部分数据时状态列表拷贝长度CM_Memcpy(shrmeminfo_ipc_2x2->shrmeminfo_2x2svc.sys_statelist, 3u*sizeof(STU_SYS_STATE), p_2x2svcinfo->sys_statelist, 3u*sizeof(STU_SYS_STATE));

 */
#include "2x2shrmem.h"
#include "CM_Types.h"
#include "CM_ShareMem.h"
#include "CM_Memory.h"
#include "MSCP_Define.h"

typedef struct{

	STU_2X2SVC_SHRMEMINFO	shrmeminfo_2x2svc;

	STU_TRDP_SHRMEMINFO		shrmeminfo_trdp;

	CM_MUTEX_IPC			mutex;

}STU_IPC_2X2SHRMEMINFO;

STU_IPC_2X2SHRMEMINFO* shrmeminfo_trdp_2x2 = NULL;


static CM_UINT8 shrmem_2x2_trdpInfoInit(void)
{
	CM_UINT8 		wt_rtn = 0u;

	CM_ASSERT(NULL != shrmeminfo_trdp_2x2);

	CM_Mutex_IPC_Lock(&shrmeminfo_trdp_2x2->mutex);

	shrmeminfo_trdp_2x2->shrmeminfo_trdp.timestamp=0;

	CM_Mutex_IPC_Unlock(&shrmeminfo_trdp_2x2->mutex);

	return wt_rtn;
}

static CM_UINT8 shrmem_2x2_redunsvcInit(CM_UINT32 cycle,CM_UINT16 redIdNum, IN CM_UINT32 redId[])
{
	CM_UINT8 wt_rtn = 0u;

	CM_UINT16 i = 0u;

	CM_Mutex_IPC_Lock(&shrmeminfo_trdp_2x2->mutex);

	if(MAX_REDID_NUM >= redIdNum)
	{
		CM_Memset(&shrmeminfo_trdp_2x2->shrmeminfo_2x2svc, 0x0u, sizeof(STU_2X2SVC_SHRMEMINFO));

		shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdNum = redIdNum;
		shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.cycle=cycle;

		for(i = 0; i<redIdNum; i++)
		{
			shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdStateList[i].redId = redId[i];
		}
	}
	else
	{
		wt_rtn |= 0x02u;
	}

	CM_Mutex_IPC_Unlock(&shrmeminfo_trdp_2x2->mutex);

	return wt_rtn;
}
/**
* @brief 共享内存创建及初始化
* @param[in] app_num 应用的个数
* @param[in] app_id[] 应用id存储buffer
* @return 返回初始化结果
* @note	只允许由共享内存其中之一的使用方（主备冗余服务）调用此函数进行共享内存的初始化，要求主备冗余服务先启动。
*/
CM_UINT8 shrmem_2x2_init(IN CM_UINT32 cycle,CM_UINT16 app_num, IN CM_UINT32 app_id[])
{
	CM_UINT8 rtn = 0u;

	if(NULL == shrmeminfo_trdp_2x2)
	{
		shrmeminfo_trdp_2x2 = (STU_IPC_2X2SHRMEMINFO*) CM_shrmem_alloc(SHRMEM_2X2TRDP_NAME, sizeof(STU_IPC_2X2SHRMEMINFO));
		CM_ASSERT(NULL != shrmeminfo_trdp_2x2);
	}
	else
	{
		;
	}

	CM_Mutex_IPC_Init(&shrmeminfo_trdp_2x2->mutex);

	shrmem_2x2_redunsvcInit(cycle,app_num, app_id);

	shrmem_2x2_trdpInfoInit();

	return rtn;
}
/**
* @brief 共享内存打开
* @note	不负责共享内存初始化的使用方（app任务），使用此函数申请获取共享内存空间。
*/
void shrmem_2x2_open(void)
{
	if(NULL == shrmeminfo_trdp_2x2)
	{
		shrmeminfo_trdp_2x2 = (STU_IPC_2X2SHRMEMINFO*) CM_shrmem_alloc(SHRMEM_2X2TRDP_NAME, sizeof(STU_IPC_2X2SHRMEMINFO));
	}
	else
	{
		;
	}

	return;

}
/**
* @brief 主备冗余服务写共享内存
* @param[in] p_2x2svcinfo 待写入主备冗余服务共享内存的数据结构体指针；
* @return 写入结果
* 		-0 写入成功；
* 		-非0 写入失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_redunsvcWrite( IN STU_2X2SVC_SHRMEMINFO * p_2x2svcinfo)
{
	CM_UINT8 wt_rtn = 0u;

	CM_UINT16 len = 0u;

	CM_Mutex_IPC_Lock(&shrmeminfo_trdp_2x2->mutex);

	if(MAX_REDID_NUM >= p_2x2svcinfo->redIdNum)
	{
		shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdNum = p_2x2svcinfo->redIdNum;

		len= p_2x2svcinfo->redIdNum ;

		CM_Memcpy(&shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdStateList[0],
				  len * sizeof(STU_REDID_INFO),
				  &p_2x2svcinfo->redIdStateList[0],
				  len * sizeof(STU_REDID_INFO));

		CM_Memset(&shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdStateList[len], 0x0u, (MAX_REDID_NUM-len)*sizeof(STU_REDID_INFO));

	}
	else
	{
		wt_rtn |= 0x02u;
	}

	shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.timestamp=p_2x2svcinfo->timestamp;

	CM_Mutex_IPC_Unlock(&shrmeminfo_trdp_2x2->mutex);

	return wt_rtn;
}
/**
* @brief 应用任务读共享内存
* @param[out] p_2x2svcinfo 承接主备冗余服务共享内存的数据结构体指针；
* @return 读结果
* 		-0 读成功；
* 		-非0 读失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_trdpRead(OUT STU_2X2SVC_SHRMEMINFO * p_2x2svcinfo)
{
	CM_UINT8 rtn = 0u;
	CM_UINT16 len = 0u;

	CM_ASSERT(NULL != p_2x2svcinfo);

	CM_ASSERT(NULL != shrmeminfo_trdp_2x2);

	CM_Mutex_IPC_Lock(&shrmeminfo_trdp_2x2->mutex);

	p_2x2svcinfo->cycle = shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.cycle;

	p_2x2svcinfo->redIdNum	  = shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdNum;

	if(sizeof(p_2x2svcinfo->redIdStateList)/sizeof(STU_REDID_INFO) >= shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdNum)
	{
		p_2x2svcinfo->redIdNum = shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdNum;

		len= p_2x2svcinfo->redIdNum;
		CM_Memcpy(&p_2x2svcinfo->redIdStateList[0],
				  len * sizeof(STU_REDID_INFO),
				  &shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.redIdStateList[0],
				  len * sizeof(STU_REDID_INFO));
	}
	else
	{
		rtn |= 0x02u;
	}

	p_2x2svcinfo->timestamp	  = shrmeminfo_trdp_2x2->shrmeminfo_2x2svc.timestamp;

	CM_Mutex_IPC_Unlock(&shrmeminfo_trdp_2x2->mutex);

	return rtn;
}

/**
* @brief 主备冗余服务读共享内存
* @param[out] p_apptaskinfo 承接应用任务共享内存的数据结构体指针；
* @return 读结果
* 		-0 读成功；
* 		-非0 读失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_redunsvcRead(OUT STU_TRDP_SHRMEMINFO * p_apptaskinfo)
{
	CM_UINT8 rtn = 0u;

	CM_ASSERT(NULL != p_apptaskinfo);

	CM_ASSERT(NULL != shrmeminfo_trdp_2x2);

	CM_Mutex_IPC_Lock(&shrmeminfo_trdp_2x2->mutex);

	p_apptaskinfo->timestamp=shrmeminfo_trdp_2x2->shrmeminfo_trdp.timestamp;

	CM_Mutex_IPC_Unlock(&shrmeminfo_trdp_2x2->mutex);

	return rtn;
}
/**
* @brief 应用任务写共享内存
* @param[in] p_appstate 待写入应用任务共享内存的当前应用数据结构体指针；
* @return 写结果
* 		-0 写成功；
* 		-非0 写失败；
* @note	在操作共享内存时，需保证主备冗余服务已经完成共享内存初始化。
*/
CM_UINT8 shrmem_2x2_trdpWrite( OUT STU_TRDP_SHRMEMINFO* p_appstate)
{
	CM_UINT8 wt_rtn = 0u;

	CM_ASSERT(NULL != shrmeminfo_trdp_2x2);

	CM_ASSERT(NULL != p_appstate);

	CM_Mutex_IPC_Lock(&shrmeminfo_trdp_2x2->mutex);

	shrmeminfo_trdp_2x2->shrmeminfo_trdp.timestamp=p_appstate->timestamp;

	CM_Mutex_IPC_Unlock(&shrmeminfo_trdp_2x2->mutex);

	return wt_rtn;
}
