/**
* @file CM_Sem.h
* @brief 实现了信号量的封装
* @author GT-BU
* @date 2021.05.01
* @version V1.0
* @copyright TCT
* @par 修改日志
* <table>
* <tr><th>Date        <th>Version  <th>Author     <th>Description
* <tr><td>2021.05.01  <td>V1.0     <td>GT-BU      <td>创建初始版本
* </table>
*/
#ifndef _CM_SEM_H
#define _CM_SEM_H

#include "CM_Types.h"

#ifdef VXWORKS
#include <semLib.h>
#include <semaphore.h>
#endif

#ifdef QNX
#include <semaphore.h>
#endif

#ifdef LINUX
#include <semaphore.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */

#ifdef WIN32
    typedef HANDLE CM_SEM_T;/**< 信号量 */
#endif
#ifdef QNX
    typedef sem_t* CM_SEM_T;/**< 信号量 */
#endif
#ifdef VXWORKS
    typedef sem_t* CM_SEM_T;/**< 信号量 */
#endif
    
#ifdef LINUX
    typedef sem_t* CM_SEM_T;/**< 信号量 */
#endif

    /**
    * @brief CM_Sem_Init
    *
    * 初始化信号量
    * @param[out] pSem
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Sem_Init(INOUT CM_SEM_T* pSem);

    /**
    * @brief CM_Sem_Destroy
    *
    * 销毁信号量
    * @param[in] sem
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Sem_Destroy(IN CM_SEM_T sem);

    /**
    * @brief CM_Sem_Wait
    *
    * 等待信号量
    * @param[in] sem
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Sem_Wait(IN CM_SEM_T sem);

    /**
	* @brief CM_Sem_wait_Time
	*
	* 等待信号量一定时间
	* @param[in] sem
	* @param[in] dwWaitTime 锁定时间（毫秒）
	* @return ENUM_CM_RETURN
	*/
    ENUM_CM_RETURN CM_Sem_Wait_Time(IN CM_SEM_T sem, IN CM_UINT32 dwWaitTime);

    /**
    * @brief CM_Sem_Post
    *
    * 发出信号量
    * @param[in] sem
    * @return ENUM_CM_RETURN
    */
    ENUM_CM_RETURN CM_Sem_Post(IN CM_SEM_T sem);

#ifdef __cplusplus
}

#endif /**< __cplusplus */

#endif /**< _CM_SEM_H */
