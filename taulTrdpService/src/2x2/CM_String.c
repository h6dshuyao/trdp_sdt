/*
 * CM_String.c
 *
 *  Created on: 2022年1月27日
 *      Author: TCT_balabala
 */

#include "CM_String.h"
#include "CM_Memory.h"
#include "MSCP_Define.h"

void CM_StrCpy(CM_CHAR *pdst, CM_SIZE_T dstsize, const CM_CHAR *psrc)
{
	CM_UINT32 		str_len = 0u;
	ENUM_CM_RETURN 	rtn_memcpy = ENUM_CM_FALSE;

	CM_ASSERT((NULL != pdst)&&(NULL != psrc));

	str_len = strlen(psrc);

	rtn_memcpy = CM_Memcpy(pdst, dstsize, psrc, str_len);

	CM_ASSERT(ENUM_CM_TRUE == rtn_memcpy);

	return;
}
