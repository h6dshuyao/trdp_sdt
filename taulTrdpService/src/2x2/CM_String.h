/*
 * CM_String.h
 *
 *  Created on: 2022年1月27日
 *      Author: TCT_balabala
 */

#ifndef SRC_RTE_CM_STRING_H_
#define SRC_RTE_CM_STRING_H_
#include "CM_Types.h"

#ifdef __cplusplus
extern "C"
{
#endif

void CM_StrCpy(CM_CHAR *pdst, CM_SIZE_T dstsize, const CM_CHAR *psrc);

#ifdef __cplusplus
}
#endif
#endif /* SRC_RTE_CM_STRING_H_ */
