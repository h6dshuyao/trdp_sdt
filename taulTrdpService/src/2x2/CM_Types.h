/**
@file CM_Types.h
@brief 本文件定义公共模块的基本数据类型定义
公共模块由平台或应用调用
@author 小型化安全计算机平台开发小组
@version 1.0.0.0
@date 2017-12-20
*/
#ifndef _CM_TYPES_H
#define _CM_TYPES_H
//#include "MSCP_Define.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>
#include <assert.h>

#ifdef WIN32
    /*#define OS_WINDOWS WIN32*/
    #include <winsock2.h>
#endif

#ifdef VXWORKS
#include <vxworks.h>
#ifdef VXWORKS6_6
#include <ioLib.h>
#include <sioLibCommon.h>
#endif

#ifdef VXWORKS5_5
#endif
#endif
#ifdef QNX
#include <errno.h>
#include <unistd.h>
#ifndef EOK
#define EOK 0
#endif
#endif


#ifdef __cplusplus
extern "C"
{
#endif /**< __cplusplus */
    /**
    * @BasicType
    * @brief 基本类型定义
    *
    * Detailed description.
    * @{
    */

#ifdef WIN32
#define CM_Get_Error_No() GetLastError()
#endif

#ifdef VXWORKS
#define CM_Get_Error_No() errno
#endif

#ifdef QNX
#define CM_Get_Error_No() errno
#endif

#ifdef LINUX
#define CM_Get_Error_No() errno
#endif

#ifndef NULL
#define NULL 0
#endif

#ifndef IN
#define IN 
#endif

#ifndef OUT
#define OUT
#endif

#ifndef INOUT
#define INOUT
#endif


typedef signed char    CM_INT8;   /**< 1字节有符号数 */
typedef unsigned char  CM_UINT8;  /**< 1字节无符号数 */
typedef	signed short   CM_INT16;  /**< 2字节有符号数 */
typedef	unsigned short CM_UINT16; /**< 2字节无符号数 */
typedef	signed int     CM_INT32;  /**< 4字节有符号数 */
typedef	unsigned int   CM_UINT32; /**< 4字节无符号数 */

typedef unsigned char  CM_UCHAR;  /**< 无符号字符型 */
typedef char           CM_CHAR;   /**< 字符型 */
typedef	float          CM_FLOAT32;/**< 4字节浮点型 */
typedef	double         CM_FLOAT64;/**< 8字节浮点型 */

typedef int     CM_BOOL;   /**< 布尔类型 */
typedef size_t  CM_SIZE_T; /**< size_t型 */


#ifndef CM_TRUE
#define CM_TRUE   ((CM_BOOL)(1))
#endif

#ifndef CM_FALSE
#define CM_FALSE    ((CM_BOOL)(0))
#endif

#ifndef CM_ERROR
#define CM_ERROR   ((CM_INT32)(-1))
#endif

#ifndef CM_OK
#define CM_OK   ((CM_UINT8)(0))
#endif

#define CM_NULL   ((void*)(0))

#define CM_ABS(x) (((x)>0)?(x):(-(x)))

typedef struct timespec CM_TIMESPEC_T; /**< 精确时间结构体 */

/*定义枚举型函数返回值*/
typedef enum
{
	ENUM_CM_TRUE = 0x55,   /**< 真 */
	ENUM_CM_FALSE = 0xAA,  /**< 假 */
	ENUM_CM_NULL = 0x00    /**< 未知 */
}ENUM_CM_RETURN;

#ifdef WIN32
/*#pragma pack(1)*/
#define CM_PACKED
#endif

#ifdef WIN32
    typedef unsigned long long int CM_UINT64;  /**< 8字节无符号数 */
    typedef long long int          CM_INT64;   /**< 8字节有符号数 */
#else/*其它操作系统*/
    typedef unsigned long long CM_UINT64;   /**< 8字节无符号数 */
    typedef long long          CM_INT64;    /**< 8字节有符号数 */
#endif

#ifdef WIN32
    typedef unsigned        CM_THREAD_T;   /**< 线程 */
    typedef unsigned int    CM_THREAD_FUNC_RETURN;  /**< 线程函数返回值 */
    typedef unsigned int (__stdcall* CM_THREAD_FUNC_T)(void* args); /**< 线程函数 */
#define CM_THREAD_CALL __stdcall
#endif
#ifdef QNX
    typedef pthread_t       CM_THREAD_T;  /**< 线程 */
    typedef void*           CM_THREAD_FUNC_RETURN;  /**< 线程函数返回值 */
    typedef void*(*CM_THREAD_FUNC_T)(void* args); /**< 线程函数 */
#define CM_THREAD_CALL
#endif
#ifdef VXWORKS
    typedef int             CM_THREAD_T;  /**< 线程 */
    typedef int             CM_THREAD_FUNC_RETURN;  /**< 线程函数返回值 */
    typedef FUNCPTR         CM_THREAD_FUNC_T;   /**< 线程函数 */
#define CM_THREAD_CALL
#endif

#ifdef LINUX
    typedef pthread_t       CM_THREAD_T;  /**< 线程 */
    typedef void*           CM_THREAD_FUNC_RETURN;  /**< 线程函数返回值 */
    typedef void*(*CM_THREAD_FUNC_T)(void* args); /**< 线程函数 */
#define CM_THREAD_CALL
#endif

#define CM_UINT32_MAX (4294967295U)   /**< CM_UINT32最大值 */
#define CM_UINT16_MAX (65535U)        /**< CM_UINT16最大值 */
#define CM_UINT8_MAX  (255)           /**< CM_UINT8最大值 */
#define CM_INT16_MIN  (-32768)        /**< CM_INT16最小值 */
#define CM_INT16_MAX  (32767)         /**< CM_INT16最大值 */
#define CM_INT32_MIN   (-2147483647L - 1)  /**< CM_INT32最小值 */
#define CM_INT32_MAX   (2147483647L)       /**< CM_INT32最大值 */
#define CM_FLOAT32_MAX     (3.402823466e+38F)   /**< CM_FLOAT32最大值 */
#define CM_FLOAT64_MAX     (1.7976931348623158e+308) /**< CM_FLOAT64最大值 */

#define CM_MILLION (1000000)   /**< 一百万 */



#ifdef __cplusplus
}
#endif /**< __cplusplus */

#endif /**< _CM_TYPES_H */

