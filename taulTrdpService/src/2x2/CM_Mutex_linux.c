
/** @file     CM_Mutex_linux.c
* @brief      文件简要说明
* @details    文件功能
* @author     fengr
* @date       Nov 29, 2021
* @version    vx.x
* @copyright  copyright(c) 2021 交控科技股份有限公司
*************************************************************/
#include "CM_Mutex.h"
#include "CM_Memory.h"
#ifdef LINUX
#include "time.h"


#define EOK   0  /* No error                                 */

/**
* @brief 互斥信号量初始化
* @param pMutex 信号量指针
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Init(INOUT CM_MUTEX_T* pMutex)
{
    /*- 变量初始化 */
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 入参是否合法 */
    if (NULL != pMutex)
    {
        /*- 创建互斥信号量 */
        *pMutex = (pthread_mutex_t*)CM_Malloc(sizeof(pthread_mutex_t));
        if(*pMutex != NULL)
        {
			ret = pthread_mutex_init(*pMutex, NULL);

			/*- 创建是否成功 */
			if (EOK == ret)
			{
				/*- 函数执行结果置为成功 */
				rt = ENUM_CM_TRUE;
			}
	        else
			{
				/*- 无处理 */
			}
        }
        else
		{
			/*- 无处理 */
		}
    }
    else
	{
    	printf("m NULL \r\n");
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}


/**
* @brief 互斥信号量初始化
* @param pMutex 信号量指针
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Init(INOUT CM_MUTEX_IPC* pMutex)
{
    /*- 变量初始化 */
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    CM_MUTEX_ATTR_T mutx_attr;
    /*- 入参是否合法 */
    if (NULL != pMutex)
    {
        /*-初始化互斥锁属性 */
        pthread_mutexattr_init(&mutx_attr);

        /*-将线程锁改变为进程间共用属性*/
        pthread_mutexattr_setpshared(&mutx_attr,PTHREAD_PROCESS_SHARED);

        /*- 将线程锁设置为robust属性，线程kill后，会自动释放资源*/
        pthread_mutexattr_setrobust(&mutx_attr, PTHREAD_MUTEX_ROBUST);

        /*- 初始化互斥信号量 */
        ret = pthread_mutex_init(pMutex, &mutx_attr);

        /*-销毁互斥锁属性对象 */
        pthread_mutexattr_destroy(&mutx_attr);

		/*- 创建是否成功 */
		if (EOK == ret)
		{
			/*- 函数执行结果置为成功 */
			rt = ENUM_CM_TRUE;
		}
		else
		{
			/*- 无处理 */
		}

    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Lock(IN CM_MUTEX_IPC* pMutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 执行锁定信号量操作 */
    ret = pthread_mutex_lock(pMutex);
    /*- 锁定是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    /*- 持有锁的线程kill掉 */
    else if(EOWNERDEAD == ret)
	{
    	pthread_mutex_consistent(pMutex);
    	rt = ENUM_CM_TRUE;
	}
    else
    {
    	;
    }
    /*- 返回函数执行结果 */
    return rt;
}
/**
* @brief 解锁信号量
* @param mutex 信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Unlock(IN CM_MUTEX_IPC* pMutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    /*- 解锁信号量 */
    ret = pthread_mutex_unlock(pMutex);
    /*- 解锁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 销毁互斥信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_IPC_Destroy(IN CM_MUTEX_IPC* pMutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 销毁互斥信号量 */
    ret = pthread_mutex_destroy(pMutex);

    /*- 销毁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}
/**
* @brief 销毁互斥信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Destroy(IN CM_MUTEX_T mutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 销毁互斥信号量 */
    ret = pthread_mutex_destroy(mutex);

    /*- 销毁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量（指定等待时间）
* @param mutex 互斥信号量
* @param dwWaitTime 等待时间（毫秒）
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock_Time(IN CM_MUTEX_T mutex, IN CM_UINT32 dwWaitTime)
{
	CM_INT32 ret = 0;
	CM_TIMESPEC_T time_sruct;
	ENUM_CM_RETURN rt = ENUM_CM_FALSE;

	time_sruct.tv_sec = (CM_INT32)dwWaitTime / 1000;
	time_sruct.tv_nsec = ((CM_INT32)dwWaitTime % 1000) * CM_MILLION;

    /*- 执行锁定信号量操作 */
    ret = pthread_mutex_timedlock(mutex,&time_sruct);

    /*- 锁定是否成功 */
    if (EOK == ret)
    {
	    /*- 函数执行结果置为成功 */
	    rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}

    /*- 返回函数执行结果 */
    return rt;
}

/**
* @brief 锁定信号量
* @param mutex 互斥信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Lock(IN CM_MUTEX_T mutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;
    /*- 执行锁定信号量操作 */
    ret = pthread_mutex_lock(mutex);
    /*- 锁定是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}
/**
* @brief 解锁信号量
* @param mutex 信号量
* @return 执行结果
* - ENUM_CM_TRUE 成功
* - ENUM_CM_FALSE 失败
*/
ENUM_CM_RETURN CM_Mutex_Unlock(IN CM_MUTEX_T mutex)
{
    CM_INT32 ret = 0;
    ENUM_CM_RETURN rt = ENUM_CM_FALSE;

    /*- 解锁信号量 */
    ret = pthread_mutex_unlock(mutex);
    /*- 解锁是否成功 */
    if (EOK == ret)
    {
        /*- 函数执行结果置为成功 */
        rt = ENUM_CM_TRUE;
    }
    else
	{
		/*- 无处理 */
	}
    /*- 返回函数执行结果 */
    return rt;
}

#endif
